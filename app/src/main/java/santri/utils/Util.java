package santri.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.util.TypedValue;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.NonNull;

/**
 * Created by hasan on 11/10/2017.
 */

public class Util {
    Context context;

    public native String getBaseUrl();
    public native String getInventoryUrl();
    public native String getInventoryRefUrl();

    public Util(Context context) {
        this.context = context;
    }

    public static void initializeFirebase(Context context, final String topic, final boolean uploadTokenToFirebase) {
        FirebaseApp.initializeApp(context);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();
                Log.d("Firebase Token",newToken);
                if (uploadTokenToFirebase) {
                    santri.firebasechat.managers.Utils.uploadToken(newToken);
                }
            }
        });

        FirebaseMessaging.getInstance().subscribeToTopic(topic)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.d("Subscribe to Topic", topic);
                        //Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public static void subscribeFirebaseTopic(String topic) {
        FirebaseMessaging.getInstance().subscribeToTopic(topic)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.d("Subscribe to Topic", topic);
                        //Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    /**
     * Converting dp to pixel
     */
    public int dpToPx(int dp) {
        Resources r = this.context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public int pxToDp(int px){
        Resources r = this.context.getResources();
        int resultPix = (int) (px / r.getDisplayMetrics().density);
        return resultPix;
    }

    public String getDeviceId() {
        String deviceId = "";
        try {
            deviceId = Settings.Secure.getString(this.context.getContentResolver(), Settings.Secure.ANDROID_ID);
        } catch (Exception ex) {

        }
        return deviceId;
    }

    public String getPackageName() {
        return this.context.getPackageName();
    }

    public String getVersionCode() {
        PackageInfo pInfo = null;
        try {
            pInfo = this.context.getPackageManager().getPackageInfo(this.context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName;

        return version;
    }

    public boolean checkChangeDevice(JSONObject jsonObject) {
        try {
            if (jsonObject.get("device").equals("change")) {
                return true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean checkCairkanSaldo(JSONObject jsonObject) {
        try {
            if (jsonObject.get("fiturCairkanSaldo").equals("true")) {
                return true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean checkLakuPandai(JSONObject jsonObject) {
        try {
            if (jsonObject.get("fiturLakuPandai").equals("true")) {
                return true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean checkCashToCash(JSONObject jsonObject) {
        try {
            if (jsonObject.get("fiturCashToCash").equals("true")) {
                return true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public long calculateDiffTime(String dateOne, String dateTwo, String formatDate){
        long difference = -1;
        try {
            DateFormat format = new SimpleDateFormat(formatDate, Locale.ENGLISH);
            Date one = format.parse(dateOne);
            Date two = format.parse(dateTwo);
            difference = one.getTime() - two.getTime();
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return difference;
    }

    public String formatTanggal(String strtime) {
        String strbulan = "";
        String strtanggal = "";
        String strtahun = "";
        String strjam = "";
        if (strtime != null && !strtime.isEmpty()) {
            strtahun = strtime.substring(0, 4);
            strbulan = strtime.substring(4, 6);
            if (strbulan.equals("01")) {
                strbulan = "Januari";
            } else if (strbulan.equals("02")) {
                strbulan = "Februari";
            } else if (strbulan.equals("03")) {
                strbulan = "Maret";
            } else if (strbulan.equals("04")) {
                strbulan = "April";
            } else if (strbulan.equals("05")) {
                strbulan = "Mei";
            } else if (strbulan.equals("06")) {
                strbulan = "Juni";
            } else if (strbulan.equals("07")) {
                strbulan = "Juli";
            } else if (strbulan.equals("08")) {
                strbulan = "Agustus";
            } else if (strbulan.equals("09")) {
                strbulan = "September";
            } else if (strbulan.equals("10")) {
                strbulan = "Oktober";
            } else if (strbulan.equals("11")) {
                strbulan = "November";
            } else if (strbulan.equals("12")) {
                strbulan = "Desember";
            }
            strtanggal = strtime.substring(6, 8);
            if(strtime.length() > 8) {
                strjam = " " + strtime.substring(9, 13);
            }
            return strtanggal + " " + strbulan + " " + strtahun + strjam;
        } else {
            return strtime;
        }
    }

    public String convertFormatDate(String formatOld, String formatNew, String dateString) {
        String bulanTahun = "";
        try {
            DateFormat df = new SimpleDateFormat(formatOld, Locale.ENGLISH);
            Date bulanTahunDate = df.parse(dateString);
            df = new SimpleDateFormat(formatNew, Locale.ENGLISH);
            bulanTahun = df.format(bulanTahunDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return bulanTahun;
    }

    public String date2String(Date date, String pattern) {
        if (date == null)
            return "";
        else
            return new SimpleDateFormat(pattern, Locale.US).format(date);
    }

    public Date string2Date(String sourceDate, String sourceFormat) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(sourceFormat, Locale.ENGLISH);
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(sourceDate);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return convertedDate;
    }

    public String formatMoney(String ori){
        double dbl = Double.parseDouble(ori);
        String dblStr = String.format("%,.0f", dbl);
        dblStr = dblStr.replaceAll(",", ".");
        return dblStr;
    }

    public String makeIDTrx() {
        String returnIDTrx = "";
        String timeStamp = new SimpleDateFormat("yyMMddHHmmss", Locale.ENGLISH).format(new Date());
        returnIDTrx = "3" + this.getDeviceId() + timeStamp;
        return returnIDTrx.toUpperCase();
    }

    public synchronized String makeIDTrxBni() {
        SecureRandom random = new SecureRandom();
        int numFirst = random.nextInt(999999999);
        String formattedFirst = String.format("%09d", numFirst);
        int numSecond = random.nextInt(99999);
        String formattedSecond = String.format("%05d", numSecond);
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.ENGLISH).format(new Date());
        return "3" + formattedFirst + formattedSecond + timeStamp;
    }

    /*public String resizeImage(String absFilePath, String fileName) {
        String extFile = absFilePath.substring(absFilePath.lastIndexOf(".") + 1);
        Bitmap.CompressFormat compressor = null;
        if (extFile.equalsIgnoreCase("png")) {
            compressor = Bitmap.CompressFormat.PNG;
        } else if (extFile.equalsIgnoreCase("jpg")) {
            compressor = Bitmap.CompressFormat.JPEG;
        }
        Bitmap bMap = BitmapFactory.decodeFile(absFilePath);
        BitmapFactoryOptimizer bitmapFactoryOptimizer = new BitmapFactoryOptimizer(bMap, extFile);
        Bitmap mBitmap = bitmapFactoryOptimizer.decodeSampledBitmapFromResourceMemOpt(320, 320);

        String selectedFile = null;
        //create a file to write bitmap data
        File resizedFile = null;
        if(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) != null) {
            resizedFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), fileName + "." + extFile);
        } else if(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) != null){
            resizedFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), fileName + "." + extFile);
        } else if(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) != null){
            resizedFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), fileName + "." + extFile);
        } else {
            resizedFile = new File(context.getCacheDir(), fileName + "." + extFile);
        }
        //cara 1, bisa oom
//        try {
//            resizedFile.createNewFile();
//
//            //Convert bitmap to byte array
//            ByteArrayOutputStream bos = new ByteArrayOutputStream();
//            mBitmap.compress(compressor, 100 /*ignored for PNG*//*, bos);
//            byte[] bitmapdata = bos.toByteArray();
//
//            //write the bytes in file
//            FileOutputStream fos = new FileOutputStream(resizedFile);
//            fos.write(bitmapdata);
//            fos.flush();
//            fos.close();
//            Uri selectedImage = Uri.fromFile(resizedFile);
//            selectedFile = GetPathFromUri.getPath(context, selectedImage);
//        } catch (Exception e) { // TODO
//            e.printStackTrace();
//        }

        //cara 2
        /*BufferedOutputStream bos = null;
        InputStream bis = null;
        try {
            bos = new BufferedOutputStream(new FileOutputStream(resizedFile), 8192);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            mBitmap.compress(compressor, 100, stream);
            bis = new ByteArrayInputStream(stream.toByteArray());

            byte[] buffer = new byte[1024];

            int len = 0;

            while((len = bis.read(buffer)) != -1) {
                bos.write(buffer, 0, len);
            }
            Uri selectedImage = Uri.fromFile(resizedFile);
            selectedFile = GetPathFromUri.getPath(context, selectedImage);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try { bis.close(); } catch (Exception ignored) { }
            try { bos.close(); } catch (Exception ignored) { }
        }

        return selectedFile;
    }*/

    public int getOrientationPic(String path){
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        return orientation;
    }

    public static Bitmap rotateBitmap(Bitmap bitmap, int orientation) {

        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                return bitmap;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                return bitmap;
        }
        try {
            Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            bitmap.recycle();
            return bmRotated;
        }
        catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        }
    }

    public int getPointerIndexArray(String[] source, String objectToFind){
        int pointer = 0;
        for (int i = 0; i < source.length; i++) {
            if (source[i].equalsIgnoreCase(objectToFind)) {
                pointer = i;
                break;
            }
        }
        return pointer;
    }

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
                int count=is.read(bytes, 0, buffer_size);
                if(count==-1)
                    break;
                os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public static boolean checkImageResource(Context ctx, ImageView imageView,
                                             int imageResource) {
        boolean result = false;

        if (ctx != null && imageView != null && imageView.getDrawable() != null) {
            Drawable.ConstantState constantState;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                constantState = ctx.getResources()
                        .getDrawable(imageResource, ctx.getTheme())
                        .getConstantState();
            } else {
                constantState = ctx.getResources().getDrawable(imageResource)
                        .getConstantState();
            }

            Log.e("imageview",imageView.getDrawable().getConstantState()+"");
            Log.e("img resource", constantState+"");

            if (imageView.getDrawable().getConstantState() == constantState) {
                result = true;
            }
        }

        return result;
    }

    public static boolean areDrawablesIdentical(Drawable drawableA, Drawable drawableB) {
        Drawable.ConstantState stateA = drawableA.getConstantState();
        Drawable.ConstantState stateB = drawableB.getConstantState();
        // If the constant state is identical, they are using the same drawable resource.
        // However, the opposite is not necessarily true.
        return (stateA != null && stateB != null && stateA.equals(stateB))
                || getBitmap(drawableA).sameAs(getBitmap(drawableB));
    }

    public static Bitmap getBitmap(Drawable drawable) {
        Bitmap result;
        if (drawable instanceof BitmapDrawable) {
            result = ((BitmapDrawable) drawable).getBitmap();
        } else {
            int width = drawable.getIntrinsicWidth();
            int height = drawable.getIntrinsicHeight();
            // Some drawables have no intrinsic width - e.g. solid colours.
            if (width <= 0) {
                width = 1;
            }
            if (height <= 0) {
                height = 1;
            }

            result = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(result);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
        }
        return result;
    }
}
