package santri.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import androidx.core.content.res.ResourcesCompat;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Created by acang on 3/1/2016.
 */
public class BitmapFactoryOptimizer {
    File file;
    InputStream inputStream;
    Context context;

    public BitmapFactoryOptimizer(File file){
        this.file = file;
        try {
            this.inputStream = new FileInputStream(this.file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public BitmapFactoryOptimizer(Bitmap bitmap){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream); //use the compression format of your need
        this.inputStream = new ByteArrayInputStream(stream.toByteArray());
    }


    public BitmapFactoryOptimizer(Drawable drawable){
        BitmapDrawable bitmapDrawable = ((BitmapDrawable) drawable);
        Bitmap bitmap = bitmapDrawable.getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream); //use the compression format of your need
        this.inputStream = new ByteArrayInputStream(stream.toByteArray());
    }

    public BitmapFactoryOptimizer(InputStream inputStream){
        this.inputStream = inputStream;
    }

    public BitmapFactoryOptimizer(Context context, int resource) {
        this.context = context;
        Drawable drawable;
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            drawable = ResourcesCompat.getDrawable(this.context.getResources(), resource, null);
        } else {
            drawable = this.context.getResources().getDrawable(resource);
        }

        BitmapDrawable bitmapDrawable = ((BitmapDrawable) drawable);
        Bitmap bitmap = bitmapDrawable.getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream); //use the compression format of your need
        this.inputStream = new ByteArrayInputStream(stream.toByteArray());
    }

    public BitmapFactoryOptimizer(Bitmap bitmap, String extFile){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Bitmap.CompressFormat compressor = null;
        if (extFile.equalsIgnoreCase("png")) {
            compressor = Bitmap.CompressFormat.PNG;
        } else if (extFile.equalsIgnoreCase("jpg")) {
            compressor = Bitmap.CompressFormat.JPEG;
        } else{
            compressor = Bitmap.CompressFormat.PNG;
        }
        bitmap.compress(compressor, 100, stream); //use the compression format of your need
        this.inputStream = new ByteArrayInputStream(stream.toByteArray());
    }

    public Bitmap decodeSampledBitmapFromResourceMemOpt(int reqWidth, int reqHeight) {

        byte[] byteArr = new byte[0];
        byte[] buffer = new byte[4096];
        int len;
        int count = 0;
        try {
            while ((len = this.inputStream.read(buffer)) > -1) {
                if (len != 0) {
                    if (count + len > byteArr.length) {
                        byte[] newbuf = new byte[(count + len) * 2];
                        System.arraycopy(byteArr, 0, newbuf, 0, count);
                        byteArr = newbuf;
                    }
                    System.arraycopy(buffer, 0, byteArr, count, len);
                    count += len;
                }
            }
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(byteArr, 0, count, options);
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inJustDecodeBounds = false;
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitMapOptimize = BitmapFactory.decodeByteArray(byteArr, 0, count, options);
            //fix orientation on bitmap pic, base on base pic file
            try {
                ExifInterface exif = new ExifInterface(this.file.getAbsolutePath());
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                Log.d("EXIF", "Exif: " + orientation);
                Matrix matrix = new Matrix();
                if (orientation == 6) {
                    matrix.postRotate(90);
                }
                else if (orientation == 3) {
                    matrix.postRotate(180);
                }
                else if (orientation == 8) {
                    matrix.postRotate(270);
                }
                bitMapOptimize = Bitmap.createBitmap(bitMapOptimize, 0, 0, bitMapOptimize.getWidth(), bitMapOptimize.getHeight(), matrix, true); // rotating bitmap
            }
            catch (Exception e) {

            }
            //return BitmapFactory.decodeByteArray(byteArr, 0, count, options);
            return bitMapOptimize;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            // Choose the smallest ratio as inSampleSize value, this will guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return inSampleSize;
    }

    public String encodeTobase64(Bitmap image) {
        Bitmap imagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imagex.compress(Bitmap.CompressFormat.PNG, 100, baos);

        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        Log.e("LOOK", imageEncoded);
        return imageEncoded;
    }

    public Bitmap decodeBase64(String input) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        byte[] decodedByte = Base64.decode(input, 0);
        Bitmap decodedPicture = BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length, options);
        return decodedPicture;
    }
}
