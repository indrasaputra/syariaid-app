package santri.syaria;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.DrawableRes;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import santri.Adapter.AdapterOrder;
import santri.syaria.retrofit.OrderData;
import santri.syaria.retrofit.Orders;
import santri.syaria.retrofit.ResponseHistory;
import santri.syaria.retrofit.RestAPI;
import santri.syaria.retrofit.RetrofitAdapter;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link OrderPranikahFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OrderPranikahFragment extends Fragment {

    private SwipeRefreshLayout swipe_refresh;
    private ShimmerFrameLayout shimmer_product;
    private Call<ResponseHistory> callbackCall = null;

    private RecyclerView recycler_view;
    private AdapterOrder mAdapter;
    private View btn_undang_ustad;

    private View rootView;

    private Orders application;

    private Context myApp;

    private int count_total = 0;
    private int failed_page = 0;

    private String userid;
    private static final String MY_PREFS_NAME = "Fooddelivery";
    private static final int MODE_PRIVATE = 0;

    private String regUrl;

    //private FloatingActionButton floatingActionButton;

    public OrderPranikahFragment() {
        // Required empty public constructor
        myApp = getActivity();
    }

    public static OrderPranikahFragment newInstance() {
        OrderPranikahFragment fragment = new OrderPranikahFragment();
        Bundle args = new Bundle();
        //args.putSerializable("UserProfile", userProfile);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myApp = getActivity();
        SharedPreferences sp = myApp.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userid = sp.getString("userid", "");

        application = new Orders();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_order, container, false);
        SharedPreferences sp = myApp.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userid = sp.getString("userid", "");

        initComponent();
        initShimmerLoading();

        List<OrderData> items = application.getItems();
        int _count_total = application.getCountTotalItem();
        if (items.size() == 0) {
            requestAction(1);
        } else {
            count_total = _count_total;
            displayApiResult(items);
        }

        return rootView;
    }

    private void initComponent() {
        shimmer_product = rootView.findViewById(R.id.shimmer_product);
        swipe_refresh = rootView.findViewById(R.id.swipe_refresh);
        recycler_view = rootView.findViewById(R.id.recycler_view);
        //floatingActionButton = rootView.findViewById(R.id.fab_prod);
        recycler_view.setLayoutManager(new LinearLayoutManager(myApp));
        recycler_view.setHasFixedSize(true);

        //set data and list adapter
        mAdapter = new AdapterOrder(myApp, recycler_view, new ArrayList<OrderData>());
        recycler_view.setAdapter(mAdapter);

        // on item list clicked
        mAdapter.setOnItemClickListener(new AdapterOrder.OnItemClickListener() {
            @Override
            public void onItemClick(View v, OrderData obj, int position) {
                //ProductDetailActivity.navigate((Activity)myApp, obj.id);
                Intent intent = new Intent(myApp, OrderDetailActivity.class);
                intent.putExtra("OrderData", obj);
                intent.putExtra("regUrl", regUrl);
                startActivity(intent);
            }
        });

        // detect when scroll reach bottom
        mAdapter.setOnLoadMoreListener(new AdapterOrder.OnLoadMoreListener() {
            @Override
            public void onLoadMore(int current_page) {
                if (count_total > mAdapter.getItemCount() && current_page != 0) {
                    int next_page = current_page + 1;
                    requestAction(next_page);
                } else {
                    mAdapter.setLoaded();
                }
            }
        });

        // on swipe list
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (callbackCall != null && callbackCall.isExecuted()) callbackCall.cancel();
                mAdapter.resetListData();
                requestAction(1);
            }
        });

        View btn_pranikah = rootView.findViewById(R.id.btn_pranikah);
        btn_pranikah.setBackgroundColor(getResources().getColor(R.color.abu));

        View btn_tanya_ustad = rootView.findViewById(R.id.btn_tanya_ustadz);
        btn_tanya_ustad.setBackgroundColor(getResources().getColor(R.color.white));
        btn_tanya_ustad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OrderFragment fragment = new OrderFragment();
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, fragment)
                        .commit();
            }
        });

        btn_undang_ustad = rootView.findViewById(R.id.btn_undang_ustadz);
        btn_undang_ustad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyOrderFragment fragment = new MyOrderFragment();
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, fragment)
                        .commit();
            }
        });
    }

    private void displayApiResult(final List<OrderData> items) {
        Log.e("items size",items.size()+"");
        mAdapter.insertData(items);
        swipeProgress(false);
        if (items.size() == 0) showNoItemView(true, "Anda belum memiliki layanan konsultasi ustadz, silakan lakukan sedekah");
    }

    private void requestHistoryList(final int page_no) {
        JSONObject jsonReq = new JSONObject();
        try {
            jsonReq.put("personid", userid);
            jsonReq.put("prodakid", "2");
            jsonReq.put("item_count",10);
            jsonReq.put("page_no",page_no);
            jsonReq.put("token", "385ef7144b8ebc2d48aa915056e8cca5");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonReq.toString());
        RestAPI api = RetrofitAdapter.createAPI();
        Call<ResponseHistory> call = api.getOrderHistory(bodyRequest);

        call.enqueue(new Callback<ResponseHistory>() {
            @Override
            public void onResponse(Call<ResponseHistory> call, Response<ResponseHistory> response) {
                ResponseHistory resp = response.body();
                if (resp!=null) {
                    count_total = resp.total_count;
                    regUrl = resp.link;
                    //count_total = 10;
                    displayApiResult(resp.data);
                    application.setCountTotalItem(count_total);
                    application.addItems(resp.data);
                } else {
                    onFailRequest(page_no);
                }
            }

            @Override
            public void onFailure(Call<ResponseHistory> call, Throwable t) {
                if (!call.isCanceled()) onFailRequest(page_no);
            }
        });
    }

    private void onFailRequest(int page_no) {
        failed_page = page_no;
        mAdapter.setLoaded();
        swipeProgress(false);
        //if (Tools.isConnect(myApp)) {
        showFailedView(true, getString(R.string.failed_text), R.drawable.img_failed);
        //} else {
        //showFailedView(true, getString(R.string.no_internet_text), R.drawable.img_no_internet);
        //}
    }

    private void requestAction(final int page_no) {
        showFailedView(false, "", R.drawable.img_failed);
        //showNoItemView(false);
        if (page_no == 1) {
            swipeProgress(true);
            application.resetItems();
        } else {
            mAdapter.setLoading();
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestHistoryList(page_no);
            }
        }, 1000);
    }

    private void showFailedView(boolean show, String message, @DrawableRes int icon) {
        View lyt_failed = rootView.findViewById(R.id.lyt_failed);
        ((ImageView)rootView.findViewById(R.id.failed_icon)).setImageResource(icon);
        ((TextView) rootView.findViewById(R.id.failed_message)).setText(message);
        if (show) {
            recycler_view.setVisibility(View.INVISIBLE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            recycler_view.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) rootView.findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestAction(failed_page);
            }
        });
    }

    private void showNoItemView(boolean show, String message) {
        View lyt_failed = rootView.findViewById(R.id.lyt_failed);
        ((ImageView)rootView.findViewById(R.id.failed_icon)).setVisibility(View.GONE);
        ((TextView) rootView.findViewById(R.id.failed_message)).setText(message);
        Button klik = rootView.findViewById(R.id.failed_retry);
        klik.setText("Klik di sini untuk Sedekah");
        if (show) {
            recycler_view.setVisibility(View.INVISIBLE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            recycler_view.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        //((Button) rootView.findViewById(R.id.failed_retry)).setVisibility(View.GONE);
        klik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(myApp, WebViewActivity.class);
                //intent.putExtra("url","http://syaria.id/order-produk/R1hMUklEcGprT0Y1akl3RFdZck9Kdz09");
                intent.putExtra("url",regUrl);
                intent.putExtra("purpose","order");
                startActivity(intent);
            }
        });
    }

    private void swipeProgress(final boolean show) {
        if (!show) {
            swipe_refresh.setRefreshing(show);
            shimmer_product.setVisibility(View.GONE);
            shimmer_product.stopShimmer();
            recycler_view.setVisibility(View.VISIBLE);
            return;
        }

        shimmer_product.setVisibility(View.VISIBLE);
        shimmer_product.startShimmer();
        recycler_view.setVisibility(View.INVISIBLE);
        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(show);
            }
        });
    }

    private void initShimmerLoading() {
        LinearLayout lyt_shimmer_content = rootView.findViewById(R.id.lyt_shimmer_content);
        for (int h = 0; h < 10; h++) {
            LinearLayout row_item = new LinearLayout(myApp);
            row_item.setOrientation(LinearLayout.HORIZONTAL);
            View item = getLayoutInflater().inflate(R.layout.loading_fragment_product, null);
            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
            p.weight = 1;
            item.setLayoutParams(p);
            row_item.addView(item);
            lyt_shimmer_content.addView(row_item);
        }
    }
}