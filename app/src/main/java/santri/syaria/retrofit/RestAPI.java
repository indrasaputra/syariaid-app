package santri.syaria.retrofit;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

public interface RestAPI {

    @POST("urlbitly")
    Call<ResponseChatStatus> getUserChatStatus(@Body RequestBody body);

    @POST("HistoryPo")
    Call<ResponseHistory> getOrderHistory(@Body RequestBody body);

    @POST("contentmanagement")
    Call<ResponseContent> getContent(@Body RequestBody body);

    @POST("likeContentmanagement")
    Call<ResponseLike> likeContent(@Body RequestBody body);

    @POST("UnlikeContentmanagement")
    Call<ResponseLike> unlikeContent(@Body RequestBody body);

    @Multipart
    @POST("registrasitaaruf")
    Call<ResponseTaarufReg> registerTaaruf(@PartMap Map<String, RequestBody> texts,
                                       @Part List<MultipartBody.Part> files);

    @POST("update_profile_non_mandatory")
    Call<ResponseTaarufUpdate> updateTaarufNonMandatory(@Body RequestBody body);

    @POST("get_user_list")
    Call<ResponseUserList> getTaarufUserList(@Body RequestBody body);

    @POST("get_user_detail")
    Call<ResponseUserDetail> getTaarufUserDetail(@Body RequestBody body);
}
