package santri.syaria.retrofit;

import java.util.ArrayList;
import java.util.List;

public class Users {
    private int countTotalItem = 0;
    private List<UserData> items = new ArrayList<>();

    public void setCountTotalItem(int countTotalItem) {
        this.countTotalItem = countTotalItem;
    }

    public void addItems(List<UserData> items) {
        this.items.addAll(items);
    }

    public void resetItems() {
        this.items.clear();
    }

    public List<UserData> getItems() {
        return items;
    }

    public int getCountTotalItem() {
        return countTotalItem;
    }
}
