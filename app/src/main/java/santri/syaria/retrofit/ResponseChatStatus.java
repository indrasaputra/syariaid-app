package santri.syaria.retrofit;

import java.io.Serializable;

public class ResponseChatStatus implements Serializable {
    public String status = "";
    public String link = "";
    public String error = "";
}
