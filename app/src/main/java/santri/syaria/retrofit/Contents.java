package santri.syaria.retrofit;

import java.util.ArrayList;
import java.util.List;

public class Contents {
    private int countTotalItem = 0;
    private List<ContentData> items = new ArrayList<>();

    public void setCountTotalItem(int countTotalItem) {
        this.countTotalItem = countTotalItem;
    }

    public void addItems(List<ContentData> items) {
        this.items.addAll(items);
    }

    public void resetItems() {
        this.items.clear();
    }

    public List<ContentData> getItems() {
        return items;
    }

    public int getCountTotalItem() {
        return countTotalItem;
    }
}
