package santri.syaria.retrofit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResponseContent implements Serializable {
    public String msg="";
    public int count=0;
    public List<ContentData> data = new ArrayList<>();
}
