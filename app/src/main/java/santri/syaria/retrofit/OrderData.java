package santri.syaria.retrofit;

import java.io.Serializable;

public class OrderData implements Serializable {
    public String id="",
            user_id = "",
            nama = "",
            email = "",
            tlp = "",
            tgl_register = "",
            orderno = "",
            categori = "",
            produk_id = "",
            produk = "",
            infaq = "",
            unik = "0",
            infaq_sukarela = "",
            payment_type = "",
            bank = "",
            va_number = "",
            pdf_url = "",
            created = "",
            cratedtime = "",
            status = "",
            date_acc = "",
            expired_bayar = "",
            start_awal = "",
            batas_akhir = "";
}
