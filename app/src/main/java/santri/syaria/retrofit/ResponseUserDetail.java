package santri.syaria.retrofit;

import java.io.Serializable;

public class ResponseUserDetail implements Serializable {
    public boolean status = false;
    public String status_message = "";
    public UserData detail = null;
}
