package santri.syaria.retrofit;

import java.util.ArrayList;
import java.util.List;

public class Orders {
    private int countTotalItem = 0;
    private List<OrderData> items = new ArrayList<>();

    public void setCountTotalItem(int countTotalItem) {
        this.countTotalItem = countTotalItem;
    }

    public void addItems(List<OrderData> items) {
        this.items.addAll(items);
    }

    public void resetItems() {
        this.items.clear();
    }

    public List<OrderData> getItems() {
        return items;
    }

    public int getCountTotalItem() {
        return countTotalItem;
    }
}
