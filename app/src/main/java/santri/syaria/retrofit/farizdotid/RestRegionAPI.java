package santri.syaria.retrofit.farizdotid;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestRegionAPI {

    @GET("provinsi")
    Call<ResponseProvinceList> getProvinceList();

    @GET("kota")
    Call<ResponseCityList> getCityList(@Query(value="id_provinsi", encoded=true) String id_provinsi);

    @GET("kecamatan")
    Call<ResponseDistrictList> getDistrictList(@Query(value="id_kota", encoded=true) String id_kota);

    @GET("kelurahan")
    Call<ResponseVillageList> getVillagetList(@Query(value="id_kecamatan", encoded=true) String id_kecamatan);

}
