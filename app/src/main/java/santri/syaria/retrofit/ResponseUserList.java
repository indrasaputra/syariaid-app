package santri.syaria.retrofit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import santri.firebasechat.models.User;

public class ResponseUserList implements Serializable {
    public boolean status = false;
    public String status_message = "";
    public int count = 0;
    public List<UserData> detail = new ArrayList<>();
}
