package santri.syaria.retrofit;

import java.io.Serializable;

public class ContentData implements Serializable {
    public String id="",
            title="",
            image="",
            page="",
            content="",
            like="",
            unlike="";
    public boolean liked = false;

    public void setLiked(boolean liked) {
        liked = liked;
    }
}
