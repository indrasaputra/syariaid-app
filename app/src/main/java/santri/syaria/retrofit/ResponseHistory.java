package santri.syaria.retrofit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResponseHistory implements Serializable {
    public int total_count = 0;
    public String link = "";
    public List<OrderData> data = new ArrayList<>();
}
