package santri.syaria;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MIME;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import santri.Adapter.OrderDetailAdapter;
import santri.Getset.menugetset;
import santri.Getset.orderDetailGetSet;
import santri.utils.GPSTracker;

import static santri.syaria.MainActivity.tf_opensense_medium;
import static santri.syaria.MainActivity.tf_opensense_regular;


public class DeliveryOrderDetail extends AppCompatActivity {
    private ListView list_order;
    private static final String MY_PREFS_NAME = "Fooddelivery";
    private String orderNo, orderAmount, orderItem, orderDate, orderStatus, orderPayment, orderName, orderAddress, orderContact, orderLat, orderLon, responseStr, isPick;
    private String bookDate, bookTime, bookNotes, duration, customer_photo;
    private ProgressDialog progressDialog;
    Button btn_picked, btn_reject;
    private ArrayList<orderDetailGetSet> getsetDeliveryorderdetail;
    private String reject_reason = "";
    private TextView txt_order_status;
    private DeliveryOrderDetail myApp;
    private String postSuccess, Error;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_order_detail);
        myApp = this;
        Objects.requireNonNull(getSupportActionBar()).hide();

        gettingIntents();
        getData();
    }

    private void gettingIntents() {
        Intent i = getIntent();
        orderNo = i.getStringExtra("OrderNo");
        orderStatus = i.getStringExtra("isComplete");

    }


    private void initViews() {


        TextView txt_header = findViewById(R.id.txt_header);
        txt_header.setTypeface(tf_opensense_regular);
        txt_header.setText(getString(R.string.txt_order_no) + orderNo);

        ImageView img_user = findViewById(R.id.img_user);
        if (orderStatus.equals("Order is processing")) {
            img_user.setImageDrawable(getDrawable(R.drawable.img_orderprocess));
        } else if (orderStatus.equals("Order is out for delivery")) {
            img_user.setImageDrawable(getDrawable(R.drawable.img_orderprocess));

        } else if (orderStatus.equals("Order is Delivered")) {
            img_user.setImageDrawable(getDrawable(R.drawable.img_ordercomplete));

        }

        //TextView txt_orderAmount = findViewById(R.id.txt_orderAmount);
        //txt_orderAmount.setTypeface(tf_opensense_medium);
        //txt_orderAmount.setText(getString(R.string.order_amount) + " " + getString(R.string.currency) + " " + orderAmount);

        TextView txt_orderQuantity = findViewById(R.id.txt_orderQuantity);
        txt_orderQuantity.setTypeface(tf_opensense_regular);
        txt_orderQuantity.setText(orderItem + getString(R.string.items));

        TextView txt_orderPaymentStyle = findViewById(R.id.txt_orderPaymentStyle);
        txt_orderPaymentStyle.setTypeface(tf_opensense_regular);
        String pay_text = getString(R.string.paymet) + " : ";
        // this is the text we'll be operating on
        SpannableString text = new SpannableString(pay_text + orderPayment);

        // make "Lorem" (characters 0 to 5) red
        int temp = getResources().getColor(R.color.res_green);
        ForegroundColorSpan fcs = new ForegroundColorSpan(temp);
        text.setSpan(fcs, pay_text.length(), text.length(), 0);
        txt_orderPaymentStyle.setText(text);

        TextView txt_orderDateTime = findViewById(R.id.txt_orderDateTime);
        txt_orderDateTime.setTypeface(tf_opensense_regular);
        txt_orderDateTime.setText(orderDate);

        TextView txt_name = findViewById(R.id.txt_name);
        txt_name.setTypeface(tf_opensense_regular);
        txt_name.setText(orderName);

        TextView txt_address = findViewById(R.id.txt_address);
        txt_address.setTypeface(tf_opensense_regular);
        txt_address.setText(orderAddress);

        TextView txt_contact = findViewById(R.id.txt_contact);
        txt_contact.setTypeface(tf_opensense_regular);
        txt_contact.setText(orderContact);

        TextView txt_book_schedule = findViewById(R.id.txt_book_schedule);
        txt_book_schedule.setTypeface(tf_opensense_regular);
        txt_book_schedule.setText(getSchedule(bookDate) + ", " + bookTime);

        TextView txt_date = findViewById(R.id.txt_date);
        txt_date.setTypeface(tf_opensense_regular);
        txt_date.setText(getSchedule(bookDate));


        try {
            String myTime = bookTime;
            SimpleDateFormat df = new SimpleDateFormat("HH:mm");
            Date d = df.parse(myTime);
            Calendar cal = Calendar.getInstance();
            cal.setTime(d);
            cal.add(Calendar.HOUR, Integer.parseInt(duration));
            String newTime = df.format(cal.getTime());

            TextView txt_time = findViewById(R.id.txt_time);
            txt_time.setTypeface(tf_opensense_regular);
            txt_time.setText(bookTime + " - " + newTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        TextView txt_book_notes = findViewById(R.id.txt_book_notes);
        txt_book_notes.setTypeface(tf_opensense_regular);
        txt_book_notes.setText(bookNotes);

        AlphaAnimation anim = new AlphaAnimation(0, 1);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.ABSOLUTE);
        anim.setDuration(1000);
        Picasso.with(this).load(this.getResources().getString(R.string.link) + this.getString(R.string.imagepath) + customer_photo).into(img_user);
        img_user.startAnimation(anim);

        txt_order_status = findViewById(R.id.txt_order_status);
        txt_order_status.setTypeface(tf_opensense_regular);
        if (orderStatus.equals("Order is processing")) {
            txt_order_status.setText("Request");
        } else if (orderStatus.equals("Order is out for delivery")) {
            txt_order_status.setText("On Progress");
        } else if (orderStatus.equals("Order is Delivered")) {
            txt_order_status.setText("Completed");
        }


        Button btn_call = findViewById(R.id.btn_call);
        btn_call.setTypeface(tf_opensense_regular);
        btn_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = "tel:" + orderContact;
                Intent i = new Intent(Intent.ACTION_DIAL);
                i.setData(Uri.parse(uri));
                startActivity(i);
            }
        });

        Button btn_map = findViewById(R.id.btn_map);
        btn_map.setTypeface(tf_opensense_regular);
        btn_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gettingGPSLocation();

            }
        });

        btn_picked = findViewById(R.id.btn_picked);
        btn_picked.setTypeface(tf_opensense_medium);
        btn_reject = findViewById(R.id.btn_reject);
        btn_reject.setTypeface(tf_opensense_medium);
        Log.d("checkIddsd", "" + orderStatus);
        if (orderStatus.equals("Order is processing")) {
            btn_picked.setText("Accept"); //(R.string.picked);
            btn_picked.setBackgroundColor(getResources().getColor(R.color.hijau)); //R.color.res_orange));
        } else if (orderStatus.equals("Order is out for delivery")) {
            btn_picked.setText(R.string.complete);
            btn_picked.setBackgroundColor(Color.GREEN);
            btn_reject.setVisibility(View.GONE);
        } else if (orderStatus.equals("Order is Delivered") || orderStatus.equals("Order is Rejected") || orderStatus.equals("Order is Completed")) {
            btn_picked.setVisibility(View.GONE);
            btn_reject.setVisibility(View.GONE);
        }
        btn_picked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (orderStatus.equals("Order is processing")) {
                    isPick = "pick";
                    btn_picked.setText(getString(R.string.picked));
                    btn_picked.setBackgroundColor(getResources().getColor(R.color.green));
                    new postingData().execute();
                } else if (orderStatus.equals("Order is out for delivery")) {
                    isPick = "complete";
                    btn_picked.setText(getString(R.string.complete));
                    btn_picked.setBackgroundColor(Color.GREEN);
                    new postingData().execute();
                } else if (orderStatus.equals("Order is Delivered") || orderStatus.equals("Order is Rejected")) {
                    btn_picked.setVisibility(View.GONE);
                }

            }
        });

        btn_reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(DeliveryOrderDetail.this, R.style.MyDialogTheme);
                final EditText edittext = new EditText(getApplicationContext());

                builder1.setMessage("Anda yakin menolak order ini? Mohon isi alasannya");
                builder1.setView(edittext);
                builder1.setCancelable(true);
                builder1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        reject_reason = edittext.getText().toString();
                        if (reject_reason.trim().length()>4) {
                            isPick = "reject";
                            btn_picked.setVisibility(View.GONE);
                            new postingData().execute();
                        } else {
                            Toast.makeText(getApplicationContext(), "Alasan harus diisi", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                builder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //onBackPressed();
                    }
                });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        list_order = findViewById(R.id.list_order);

        ImageButton ib_back = findViewById(R.id.ib_back);
        ib_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private String getSchedule(String date) {
        String schedule = "";
        try {
            //String dateString = "20/12/2018";
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            Date readDate = df.parse(date);
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(readDate.getTime());

            int mm = cal.get(Calendar.MONTH);
            int dd = cal.get(Calendar.DAY_OF_WEEK);
            int tgl = cal.get(Calendar.DAY_OF_MONTH);

            String[] hari = {"Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"};
            String[] bulan = {"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"};

            String year = cal.get(Calendar.YEAR) + "";
            String month = bulan[mm]; //cal.get(Calendar.MONTH) + "";
            String day = hari[dd-1]; //cal.get(Calendar.DAY_OF_MONTH) + "";

            schedule = day + ", " + tgl + " " + month + " " + year;
        } catch (Exception e) {
            Log.e("Date", e.getMessage().toString());
        }
        return schedule;
    }

    private void gettingGPSLocation() {
        GPSTracker gps = new GPSTracker();
        gps.init(DeliveryOrderDetail.this);
        // check if GPS enabled
        if (gps.canGetLocation()) {
            try {
                Double sLat = gps.getLatitude();
                Double sLong = gps.getLongitude();
                Log.w("Current Location", "Lat: " + sLat + "Long: " + sLong + "URL:::" + orderLat + "::::" + orderLon);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr=" + sLat + "," + sLong + "&daddr=" + orderLat + "," + orderLon));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);
            } catch (NullPointerException | NumberFormatException e) {
                // TODO: handle exception
                e.printStackTrace();
            }

        } else {
            gps.showSettingsAlert();
        }


    }


    private void getData() {


        //creating a string request to send request to the url
        String hp = getString(R.string.link) + getString(R.string.servicepath) + "deliveryboy_order_details.php?order_id=" + orderNo;
        Log.w(getClass().getName(), hp);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, hp,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        Log.e("Response", response);

                        try {

                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);
                            if (obj.getString("success").equals("1")) {
                                JSONObject ja_order = obj.getJSONObject("Order");
                                orderDetailGetSet getSet;
                                getsetDeliveryorderdetail = new ArrayList<>();

                                orderAmount = ja_order.getString("order_amount");
                                orderItem = ja_order.getString("items");
                                orderDate = ja_order.getString("date");
                                orderPayment = ja_order.getString("payment");
                                orderName = ja_order.getString("customer_name");
                                orderAddress = ja_order.getString("address");
                                orderContact = ja_order.getString("phone");
                                orderLat = ja_order.getString("lat");
                                orderLon = ja_order.getString("long");
                                bookDate = ja_order.getString("book_date");
                                bookTime = ja_order.getString("book_time");
                                bookNotes = ja_order.getString("book_notes");
                                duration = ja_order.getString("duration");
                                customer_photo = ja_order.getString("customer_photo");

                                initViews();
                                JSONArray jsonArray = ja_order.getJSONArray("item_name");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jo_orderDetail = jsonArray.getJSONObject(i);

                                    getSet = new orderDetailGetSet();
                                    getSet.setItemName(jo_orderDetail.getString("name"));
                                    getSet.setItemQuantity(jo_orderDetail.getString("qty"));
                                    getSet.setItemPrice(jo_orderDetail.getString("amount"));

                                    getsetDeliveryorderdetail.add(getSet);
                                }

                                OrderDetailAdapter adapter = new OrderDetailAdapter(getsetDeliveryorderdetail, DeliveryOrderDetail.this);
                                list_order.setAdapter(adapter);

                            } else {
                                Toast.makeText(DeliveryOrderDetail.this, obj.getString("order"), Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurs

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.e("Status code", String.valueOf(networkResponse.statusCode));
                            Toast.makeText(getApplicationContext(), String.valueOf(networkResponse.statusCode), Toast.LENGTH_SHORT).show();

                        }
                    }
                });

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    class postingData extends AsyncTask<Void, Void, Void> {
        @Override
        protected synchronized void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(DeliveryOrderDetail.this);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        protected synchronized HttpClient getHttpClient() {
            HttpClient httpClient = null;
            if (httpClient != null)
                return httpClient;

            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, 60);
            HttpConnectionParams.setSoTimeout(httpParameters, 60);
            HttpProtocolParams.setVersion(httpParameters, HttpVersion.HTTP_1_1);

            //Thread safe in case various AsyncTasks try to access it concurrently
            SchemeRegistry schemeRegistry = new SchemeRegistry();
            schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
            ClientConnectionManager cm = new ThreadSafeClientConnManager(httpParameters, schemeRegistry);

            httpClient = new DefaultHttpClient(cm, httpParameters);

            //CookieStore cookieStore = httpClient.getCookieStore();
            HttpContext localContext = new BasicHttpContext();
            //localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);

            return httpClient;
        }

        @Override
        protected synchronized Void doInBackground(Void... params) {
            String url = null;
            if (isPick.equals("pick")) {
                url = getString(R.string.link) + getString(R.string.servicepath) + "order_pick.php";
            } else if (isPick.equals("complete")) {
                url = getString(R.string.link) + getString(R.string.servicepath) + "order_complete.php";
            } else if (isPick.equals("reject")) {
                url = getString(R.string.link) + getString(R.string.servicepath) + "order_reject.php";
            }

            url = url+"?order_id="+orderNo+"&reject_reason="+reject_reason;

            URL hp;
            try {

                hp = new URL(url);
                Log.e("URLmenu", "" + hp);
                URLConnection hpCon = hp.openConnection();
                hpCon.connect();
                InputStream input = hpCon.getInputStream();
                Log.d("input", "" + input);
                BufferedReader r = new BufferedReader(new InputStreamReader(input));
                String x;
                x = r.readLine();
                StringBuilder total = new StringBuilder();
                while (x != null) {
                    total.append(x);
                    x = r.readLine();
                }
                Log.e("URL", "" + total);

                responseStr = total.toString();

            } catch (NullPointerException e) {
                // TODO: handle exception
                Log.e("Error",e.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }



            // TODO Auto-generated method stub
//            HttpClient httpClient = getHttpClient(); //new DefaultHttpClient();
//            HttpEntity httpEntity;
//
//            Log.e("sourceFile", "" + orderNo);
//            httpEntity = MultipartEntityBuilder.create().addTextBody("order_id", "" + orderNo, ContentType.create("text/plain", MIME.UTF8_CHARSET))
//                    .addTextBody("reject_reason", "" + reject_reason, ContentType.create("text/plain", MIME.UTF8_CHARSET))
//                    .build();
//            HttpPost httpPost = null;
//            if (isPick.equals("pick")) {
//                httpPost = new HttpPost(getString(R.string.link) + getString(R.string.servicepath) + "order_pick.php");
//            } else if (isPick.equals("complete")) {
//                httpPost = new HttpPost(getString(R.string.link) + getString(R.string.servicepath) + "order_complete.php");
//            } else if (isPick.equals("reject")) {
//                httpPost = new HttpPost(getString(R.string.link) + getString(R.string.servicepath) + "order_reject.php");
//            }
//
//            Log.e("httpPost", "" + httpPost);
//            httpPost.setEntity(httpEntity);
//            HttpResponse response = null;
//            try {
//                response = httpClient.execute(httpPost);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            HttpEntity result = null;
//            if (response != null) {
//                result = response.getEntity();
//            }
//            Log.e("result", "" + result);
//            if (result != null) {
//                try {
//                    responseStr = EntityUtils.toString(result).trim();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                Log.e("Response12322", "" + responseStr);
//            }


            return null;
        }

        @Override
        protected synchronized void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (progressDialog.isShowing())
                progressDialog.dismiss();
            new showResponse().execute();

            Log.d("onPostExecute", "onpost called... running");
        }
    }

    class showResponse extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                final JSONObject jsonObject = new JSONObject(responseStr);

                Log.e("Obj", jsonObject.toString());
                postSuccess = jsonObject.getString("success");
                if (jsonObject.getString("success").equals("1")) {


                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Toast.makeText(DeliveryOrderDetail.this, jsonObject.getString("error"), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (NullPointerException e) {
                // TODO: handle exception
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (postSuccess.equals("1")) {
                if (orderStatus.equals("Order is processing")) {
                    if (isPick.equals("reject")) {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(DeliveryOrderDetail.this, R.style.MyDialogTheme);

                        builder1.setMessage(R.string.order_rejected);
                        builder1.setCancelable(true);
                        builder1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                btn_picked.setVisibility(View.GONE);
                                onBackPressed();
                            }
                        });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        txt_order_status.setText("Order is Rejected");
                    } else {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(DeliveryOrderDetail.this, R.style.MyDialogTheme);

                        builder1.setMessage(R.string.order_piked_for_delivery);
                        builder1.setCancelable(true);
                        builder1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                btn_picked.setText(R.string.complete);
                                btn_picked.setBackgroundColor(Color.GREEN);
                                onBackPressed();
                            }
                        });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        txt_order_status.setText("Order is On Progress");
                    }
                } else if (orderStatus.equals("Order is out for delivery")) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(DeliveryOrderDetail.this, R.style.MyDialogTheme);

                    builder1.setMessage(R.string.order_complete);
                    builder1.setCancelable(true);
                    builder1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            btn_picked.setVisibility(View.GONE);
                            onBackPressed();
                        }
                    });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                    txt_order_status.setText("Order is Completed");
                } else if (orderStatus.equals("Order is Delivered") || orderStatus.equals("Order is Completed")) {
                    btn_picked.setVisibility(View.GONE);
                }
            }
        }
    }

}
