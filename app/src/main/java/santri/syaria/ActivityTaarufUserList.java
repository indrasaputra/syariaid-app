package santri.syaria;

import androidx.annotation.DrawableRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import santri.Adapter.AdapterRisalah;
import santri.Adapter.AdapterTaarufUserList;
import santri.syaria.retrofit.ContentData;
import santri.syaria.retrofit.Contents;
import santri.syaria.retrofit.ResponseContent;
import santri.syaria.retrofit.ResponseUserDetail;
import santri.syaria.retrofit.ResponseUserList;
import santri.syaria.retrofit.RestAPI;
import santri.syaria.retrofit.RetrofitAdapter;
import santri.syaria.retrofit.UserData;
import santri.syaria.retrofit.Users;
import santri.utils.GPSTracker;
import santri.video.utils.NetworkCheck;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ActivityTaarufUserList extends AppCompatActivity {

    private SwipeRefreshLayout swipe_refresh;
    private ShimmerFrameLayout shimmer_video;
    private Call<ResponseUserList> callbackCall = null;

    private RecyclerView recycler_view;
    private AdapterTaarufUserList mAdapter;
    private AdView mAdView = null;

    private int count_total = 0;
    private int failed_page = 0;

    //private ThisApplication application;
    private Users application;

    private ActivityTaarufUserList myApp;

    private final String[] permissions =
            {Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION};
    public static final int MULTIPLE_PERMISSIONS = 10;
    public static final int PERMISSION_REQUEST_CODE = 1001;

    private SharedPreferences prefs;
    private static final String MY_PREFS_NAME = "Fooddelivery";

    private double latitudecur = 0;
    private double longitudecur = 0;

    private String userid, taaruf_userid, taaruf_sex, taaruf_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taaruf_user_list);
        //application = ThisApplication.getInstance();
        application = new Users();
        myApp = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SpannableString s = new SpannableString("Taaruf Syari");
        s.setSpan(new TypefaceSpan("MyTypeface.otf"), 0, s.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(
                (Html.fromHtml("<font color=\"#ffffff\">" + s + "</font>")));

        prefs = myApp.getSharedPreferences(MY_PREFS_NAME, myApp.MODE_PRIVATE);

        if (!checkPermission()) {
            requestPermission();
        }

        gettingGPSLocation();

        initComponent();
        initShimmerLoading();
    }

    private void initComponent() {
        shimmer_video = findViewById(R.id.shimmer_video);
        swipe_refresh = findViewById(R.id.swipe_refresh);
        recycler_view = findViewById(R.id.recycler_view);

        userid = prefs.getString("userid","");
        taaruf_userid = prefs.getString("taaruf_userid", "");
        taaruf_sex = prefs.getString("taaruf_sex", "");

        if (userid.equals(taaruf_userid))
            showUserList();
        else
            getUserDetail();

        GridLayoutManager pLayoutManager = new GridLayoutManager(myApp, 2);
        recycler_view.setLayoutManager(pLayoutManager);
        //recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setHasFixedSize(true);

        //set data and list adapter
        mAdapter = new AdapterTaarufUserList(this, recycler_view, new ArrayList<UserData>());
        recycler_view.setAdapter(mAdapter);

        // on item list clicked
        mAdapter.setOnItemClickListener(new AdapterTaarufUserList.OnItemClickListener() {
            @Override
            public void onItemClick(View v, UserData obj, int position) {
                Log.e("call RisalahDetail",obj.fullname);
                //ActivityVideoDetails.navigate(myApp, obj., false);
//                Intent intent = new Intent(myApp, ActivityRisalahDetail.class);
//                intent.putExtra("menu_title","Doa dan Amalan");
//                intent.putExtra("txt_title",obj.title);
//                intent.putExtra("txt_content",obj.content);
//                intent.putExtra("image_url","");
//                startActivity(intent);
            }
        });

        // detect when scroll reach bottom
        mAdapter.setOnLoadMoreListener(new AdapterTaarufUserList.OnLoadMoreListener() {
            @Override
            public void onLoadMore(int current_page) {
                if (count_total > mAdapter.getItemCount() && current_page != 0) {
                    Log.d("current page ======= ", ""+current_page);
                    int next_page = current_page + 1;
                    requestAction(next_page);
                } else {
                    mAdapter.setLoaded();
                }
            }
        });

        // on swipe list
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (callbackCall != null && callbackCall.isExecuted()) callbackCall.cancel();
                mAdapter.resetListData();
                requestAction(1);
            }
        });

        MobileAds.initialize(myApp, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(LoadAdError errorCode) {
                Log.e("failed load banner", errorCode+"");
                // Code to be executed when an ad request fails.
                //myApp.findViewById(R.id.startAppBanner).setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }

    private void getUserDetail() {
        JSONObject jsonReq = new JSONObject();
        try {
            jsonReq.put("user_id", userid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonReq.toString());
        RestAPI api = RetrofitAdapter.createAPI();
        Call<ResponseUserDetail> callbackCall = api.getTaarufUserDetail(bodyRequest);
        callbackCall.enqueue(new Callback<ResponseUserDetail>() {
            @Override
            public void onResponse(Call<ResponseUserDetail> call, Response<ResponseUserDetail> response) {
                Log.e("response video",response.body().toString());
                ResponseUserDetail resp = response.body();
                if (resp != null && resp.status) {
                    taaruf_userid = userid;
                    taaruf_sex = resp.detail.sex;
                    taaruf_image = resp.detail.image;
                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME,
                            MODE_PRIVATE).edit();
                    editor.putString("taaruf_userid", taaruf_userid);
                    editor.putString("taaruf_sex", taaruf_sex);
                    editor.putString("taaruf_image", taaruf_image);
                    editor.apply();
                    showUserList();
                } else {
                    Intent intent = new Intent(myApp, ActivityTaarufRegister.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<ResponseUserDetail> call, Throwable t) {
                //if (!call.isCanceled()) onFailRequest(page_no);
            }

        });
    }

    private void showUserList() {
        List<UserData> videos = application.getItems();
        int _count_total = application.getCountTotalItem();
        if (videos.size() == 0) {
            requestAction(1);
        } else {
            count_total = _count_total;
            displayApiResult(videos);
        }
    }

    private void displayApiResult(final List<UserData> items) {
        mAdapter.insertData(items);
        swipeProgress(false);
        // if (items.size() == 0) showNoItemView(true);
    }

    private void requestListPlaylist(final int page_no) {
        JSONObject jsonReq = new JSONObject();
        try {
            //jsonReq.put("search", "tanya");
            jsonReq.put("limit", "20");
            jsonReq.put("pageno",""+page_no);
            jsonReq.put("sex", taaruf_sex.equals("Laki-laki") ? "Perempuan" : "Laki-laki");
            jsonReq.put("lat", latitudecur);
            jsonReq.put("lon", longitudecur);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonReq.toString());
        RestAPI api = RetrofitAdapter.createAPI();
        Call<ResponseUserList> callbackCall = api.getTaarufUserList(bodyRequest);
        callbackCall.enqueue(new Callback<ResponseUserList>() {
            @Override
            public void onResponse(Call<ResponseUserList> call, Response<ResponseUserList> response) {
                Log.e("response video",response.body().toString());
                ResponseUserList resp = response.body();
                if (resp != null && resp.detail.size() > 0) {
                    count_total = resp.count;
                    displayApiResult(resp.detail);

                    application.setCountTotalItem(count_total);
                    application.addItems(resp.detail);
                } else {
                    onFailRequest(page_no);
                }
            }

            @Override
            public void onFailure(Call<ResponseUserList> call, Throwable t) {
                if (!call.isCanceled()) onFailRequest(page_no);
            }

        });
    }

    private void onFailRequest(int page_no) {
        failed_page = page_no;
        mAdapter.setLoaded();
        swipeProgress(false);
        if (NetworkCheck.isConnect(myApp)) {
            showFailedView(true, getString(R.string.failed_text), R.drawable.img_failed);
        } else {
            showFailedView(true, getString(R.string.no_internet_text), R.drawable.img_no_internet);
        }
    }

    private void requestAction(final int page_no) {
        showFailedView(false, "", R.drawable.img_failed);
        //showNoItemView(false);
        if (page_no == 1) {
            swipeProgress(true);
            application.resetItems();
        } else {
            mAdapter.setLoading();
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestListPlaylist(page_no);
            }
        }, 1000);
    }

    private void showFailedView(boolean show, String message, @DrawableRes int icon) {
        View lyt_failed = (View) findViewById(R.id.lyt_failed);
        ((ImageView) findViewById(R.id.failed_icon)).setImageResource(icon);
        ((TextView) findViewById(R.id.failed_message)).setText(message);
        if (show) {
            recycler_view.setVisibility(View.INVISIBLE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            recycler_view.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestAction(failed_page);
            }
        });
    }

    private void swipeProgress(final boolean show) {
        if (!show) {
            swipe_refresh.setRefreshing(show);
            shimmer_video.setVisibility(View.GONE);
            shimmer_video.stopShimmer();
            recycler_view.setVisibility(View.VISIBLE);
            return;
        }

        shimmer_video.setVisibility(View.VISIBLE);
        shimmer_video.startShimmer();
        recycler_view.setVisibility(View.INVISIBLE);
        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(show);
            }
        });
    }

    private void initShimmerLoading() {
        LinearLayout lyt_shimmer_content = findViewById(R.id.lyt_shimmer_content);
        for (int h = 0; h < 10; h++) {
            LinearLayout row_item = new LinearLayout(myApp);
            row_item.setOrientation(LinearLayout.HORIZONTAL);
            View item = getLayoutInflater().inflate(R.layout.loading_fragment_video, null);
            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
            p.weight = 1;
            item.setLayoutParams(p);
            row_item.addView(item);
            lyt_shimmer_content.addView(row_item);
        }
    }

    public boolean checkPermission() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p:permissions) {
            result = ContextCompat.checkSelfPermission(this,p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),MULTIPLE_PERMISSIONS );
            return false;
        }
        return true;
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_CODE);
    }

    private void gettingGPSLocation() {
        GPSTracker gps = new GPSTracker();
        gps.init(ActivityTaarufUserList.this);
        // check if GPS enabled
        if (gps.canGetLocation()) {
            try {
                latitudecur = gps.getLatitude();
                longitudecur = gps.getLongitude();
                Log.w("Current Location", "Lat: " + latitudecur + "Long: " + longitudecur);
            } catch (NullPointerException | NumberFormatException e) {
                // TODO: handle exception
            }

        } else {
            gps.showSettingsAlert();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}