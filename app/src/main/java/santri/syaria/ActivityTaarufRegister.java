package santri.syaria;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Multipart;
import santri.syaria.retrofit.ResponseTaarufReg;
import santri.syaria.retrofit.RestAPI;
import santri.syaria.retrofit.RetrofitAdapter;
import santri.syaria.retrofit.farizdotid.City;
import santri.syaria.retrofit.farizdotid.District;
import santri.syaria.retrofit.farizdotid.Province;
import santri.syaria.retrofit.farizdotid.ResponseCityList;
import santri.syaria.retrofit.farizdotid.ResponseDistrictList;
import santri.syaria.retrofit.farizdotid.ResponseProvinceList;
import santri.syaria.retrofit.farizdotid.ResponseVillageList;
import santri.syaria.retrofit.farizdotid.RestRegionAPI;
import santri.syaria.retrofit.farizdotid.RetrofitRegionAdapter;
import santri.syaria.retrofit.farizdotid.Village;
import santri.utils.GPSTracker;
import santri.utils.ImagePicker;
import santri.video.connection.RestAdapter;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.R.layout.simple_spinner_item;

public class ActivityTaarufRegister extends AppCompatActivity {

    private ActivityTaarufRegister myApp;

    private EditText
    edit_fullname,
    edit_nickname,
    edit_birthdate,
    edit_address,
    edit_hobby,
    edit_biography,

    edit_job,
    edit_job_desc,
    edit_job_level,

    edit_vision,
    edit_mission,
    edit_marriage_purpose,
    edit_ten_year_plan,

    edit_couple_height,
    edit_couple_weight,
    edit_couple_criteria;

    private Spinner
    sp_sex,
    sp_province,
    sp_city,
    sp_district,
    sp_village,
    sp_ethnicity,
    sp_religion,
    sp_status,

    sp_education,
    sp_relgious_education,

    sp_couple_physical,
    sp_couple_skin,
    sp_couple_ethnicity,
    sp_couple_education,
    sp_couple_province,
    sp_couple_city,

    sp_five_time_pray,
    sp_sunnah_fasting,
    sp_quran_literacy,
    sp_quran_routinity,
    sp_charity_routinity,
    sp_islamic_study_routinity;

    private Switch switch_blurred;

    private String province;
    private String userid;

    private View ll_certificate;

    private ImageView image_profile;
    private String fileFoto = "";
    private static final int PICK_IMAGE_ID = 2345;
    private String mCurrentPhotoPath;

    private List<Province> provinces = new ArrayList<>();
    private ArrayList<String> provinceList = new ArrayList<>();

    private List<City> cities = new ArrayList<>();
    private ArrayList<String> cityList = new ArrayList<>();

    private List<District> districts = new ArrayList<>();
    private ArrayList<String> districtList = new ArrayList<>();

    private List<Village> villages = new ArrayList<>();
    private ArrayList<String> villageList = new ArrayList<>();

    private List<City> couple_cities = new ArrayList<>();
    private ArrayList<String> couple_cityList = new ArrayList<>();

    private long id_provinsi, id_kota, id_kecamatan, couple_id_provinsi, couple_id_kota;

    private DatePickerDialog picker;

    private final String[] permissions =
            {Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION};
    public static final int MULTIPLE_PERMISSIONS = 10;
    public static final int PERMISSION_REQUEST_CODE = 1001;

    private SharedPreferences prefs;
    private static final String MY_PREFS_NAME = "Fooddelivery";

    private double latitudecur = 0;
    private double longitudecur = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taaruf_register);

        myApp = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SpannableString s = new SpannableString("Taaruf Syari");
        s.setSpan(new TypefaceSpan("MyTypeface.otf"), 0, s.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(
                (Html.fromHtml("<font color=\"#ffffff\">" + s + "</font>")));

        prefs = myApp.getSharedPreferences(MY_PREFS_NAME, myApp.MODE_PRIVATE);

        if (!checkPermission()) {
            requestPermission();
        }

        gettingGPSLocation();

        initComponent();
        requestProvince();
    }

    private void initComponent() {
        edit_fullname = findViewById(R.id.edit_fullname);
        edit_nickname = findViewById(R.id.edit_nickname);
        edit_birthdate = findViewById(R.id.edit_birthdate);
        edit_address = findViewById(R.id.edit_address);
        edit_hobby = findViewById(R.id.edit_hobby);
        edit_biography = findViewById(R.id.edit_biography);

        edit_job = findViewById(R.id.edit_job);
        edit_job_desc = findViewById(R.id.edit_job_desc);
        edit_job_level = findViewById(R.id.edit_job_level);

        edit_vision = findViewById(R.id.edit_vision);
        edit_mission = findViewById(R.id.edit_mission);
        edit_marriage_purpose = findViewById(R.id.edit_marriage_purpose);
        edit_ten_year_plan = findViewById(R.id.edit_ten_year_plan);

        edit_couple_height = findViewById(R.id.edit_couple_height);
        edit_couple_weight = findViewById(R.id.edit_couple_weight);
        edit_couple_criteria = findViewById(R.id.edit_couple_criteria);

        sp_sex = findViewById(R.id.sp_sex);
        sp_province = findViewById(R.id.sp_province);
        sp_city = findViewById(R.id.sp_city);
        sp_district = findViewById(R.id.sp_district);
        sp_village = findViewById(R.id.sp_village);
        sp_ethnicity = findViewById(R.id.sp_ethnicity);
        sp_religion = findViewById(R.id.sp_religion);
        sp_status = findViewById(R.id.sp_status);

        sp_education = findViewById(R.id.sp_education);
        sp_relgious_education = findViewById(R.id.sp_religious_education);

        sp_couple_physical = findViewById(R.id.sp_couple_physical);
        sp_couple_skin = findViewById(R.id.sp_couple_skin);
        sp_couple_ethnicity = findViewById(R.id.sp_couple_ethnicity);
        sp_couple_education = findViewById(R.id.sp_couple_education);
        sp_couple_province = findViewById(R.id.sp_couple_province);
        sp_couple_city = findViewById(R.id.sp_couple_city);

        sp_five_time_pray = findViewById(R.id.sp_five_time_pray);
        sp_sunnah_fasting = findViewById(R.id.sp_sunnah_fasting);
        sp_quran_literacy = findViewById(R.id.sp_quran_literacy);
        sp_quran_routinity = findViewById(R.id.sp_quran_routinity);
        sp_charity_routinity = findViewById(R.id.sp_charity_routinity);
        sp_islamic_study_routinity = findViewById(R.id.sp_islamic_study_routinity);

        switch_blurred = findViewById(R.id.switch_blurred);

        image_profile = findViewById(R.id.image_profile);

        Log.d("fullname", prefs.getString("username", ""));
        edit_fullname.setText(prefs.getString("username", ""));
        userid = prefs.getString("userid","");

        image_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) myApp.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(image_profile.getWindowToken(), 0);
                Intent chooseImageIntent = ImagePicker.getPickImageIntent(myApp);
                startActivityForResult(chooseImageIntent, PICK_IMAGE_ID);
            }
        });

        edit_birthdate.setInputType(InputType.TYPE_NULL);
        edit_birthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setEdit_birthdate();
            }
        });
        edit_birthdate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    setEdit_birthdate();
                }
            }
        });

        Button btn_submit = findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitRegister();
            }
        });
    }

    private void submitRegister() {
        View focusView = null;
        boolean cancel = false;

        if (TextUtils.isEmpty(edit_fullname.getText())) {
            edit_fullname.setError(getResources().getString(R.string.notif_mandatory));
            focusView = edit_fullname;
            cancel = true;
        }

        if (TextUtils.isEmpty(edit_nickname.getText())) {
            edit_nickname.setError(getResources().getString(R.string.notif_mandatory));
            focusView = edit_nickname;
            cancel = true;
        }

        if (TextUtils.isEmpty(edit_birthdate.getText())) {
            edit_birthdate.setError(getResources().getString(R.string.notif_mandatory));
            focusView = edit_birthdate;
            cancel = true;
        }

        if (TextUtils.isEmpty(edit_address.getText())) {
            edit_address.setError(getResources().getString(R.string.notif_mandatory));
            focusView = edit_address;
            cancel = true;
        }

        if (TextUtils.isEmpty(edit_couple_criteria.getText())) {
            edit_couple_criteria.setError(getResources().getString(R.string.notif_mandatory));
            focusView = edit_couple_criteria;
            cancel = true;
        }

        province = sp_province.getSelectedItem().toString();
        if (province.equals("-")) {
            TextView errorText = (TextView)sp_province.getSelectedView();
            errorText.setError("Silakan pilih provinsi alamat");
            focusView = errorText;
            cancel = true;
        }

        if (sp_sex.getSelectedItem().toString().equals("-")) {
            TextView errorText = (TextView)sp_sex.getSelectedView();
            errorText.setError("Belum pilih");
            focusView = errorText;
            cancel = true;
        }

        if (sp_ethnicity.getSelectedItem().toString().equals("-")) {
            TextView errorText = (TextView)sp_ethnicity.getSelectedView();
            errorText.setError("Belum pilih");
            focusView = errorText;
            cancel = true;
        }

        if (sp_religion.getSelectedItem().toString().equals("-")) {
            TextView errorText = (TextView)sp_religion.getSelectedView();
            errorText.setError("Belum pilih");
            focusView = errorText;
            cancel = true;
        }

        if (sp_status.getSelectedItem().toString().equals("-")) {
            TextView errorText = (TextView)sp_status.getSelectedView();
            errorText.setError("Belum pilih");
            focusView = errorText;
            cancel = true;
        }

        if (sp_education.getSelectedItem().toString().equals("-")) {
            TextView errorText = (TextView)sp_education.getSelectedView();
            errorText.setError("Belum pilih");
            focusView = errorText;
            cancel = true;
        }

        if (sp_relgious_education.getSelectedItem().toString().equals("-")) {
            TextView errorText = (TextView)sp_relgious_education.getSelectedView();
            errorText.setError("Belum pilih");
            focusView = errorText;
            cancel = true;
        }

        if (sp_couple_physical.getSelectedItem().toString().equals("-")) {
            TextView errorText = (TextView)sp_couple_physical.getSelectedView();
            errorText.setError("Belum pilih");
            focusView = errorText;
            cancel = true;
        }

        if (sp_couple_skin.getSelectedItem().toString().equals("-")) {
            TextView errorText = (TextView)sp_couple_skin.getSelectedView();
            errorText.setError("Belum pilih");
            focusView = errorText;
            cancel = true;
        }

        if (sp_five_time_pray.getSelectedItem().toString().equals("-")) {
            TextView errorText = (TextView)sp_five_time_pray.getSelectedView();
            errorText.setError("Belum pilih");
            focusView = errorText;
            cancel = true;
        }

        if (sp_sunnah_fasting.getSelectedItem().toString().equals("-")) {
            TextView errorText = (TextView)sp_sunnah_fasting.getSelectedView();
            errorText.setError("Belum pilih");
            focusView = errorText;
            cancel = true;
        }

        if (sp_quran_literacy.getSelectedItem().toString().equals("-")) {
            TextView errorText = (TextView)sp_quran_literacy.getSelectedView();
            errorText.setError("Belum pilih");
            focusView = errorText;
            cancel = true;
        }

        if (sp_quran_routinity.getSelectedItem().toString().equals("-")) {
            TextView errorText = (TextView)sp_quran_routinity.getSelectedView();
            errorText.setError("Belum pilih");
            focusView = errorText;
            cancel = true;
        }

        if (sp_charity_routinity.getSelectedItem().toString().equals("-")) {
            TextView errorText = (TextView)sp_charity_routinity.getSelectedView();
            errorText.setError("Belum pilih");
            focusView = errorText;
            cancel = true;
        }

        if (sp_islamic_study_routinity.getSelectedItem().toString().equals("-")) {
            TextView errorText = (TextView)sp_islamic_study_routinity.getSelectedView();
            errorText.setError("Belum pilih");
            focusView = errorText;
            cancel = true;
        }

        if (fileFoto.equals("")) {
            Toast.makeText(myApp, "Mohon unggah Foto Profil", Toast.LENGTH_LONG);
            cancel = true;
        }

        if (TextUtils.isEmpty(edit_hobby.getText())) {
            edit_hobby.setError(getResources().getString(R.string.notif_mandatory));
            focusView = edit_hobby;
            cancel = true;
        }

        if (TextUtils.isEmpty(edit_biography.getText())) {
            edit_biography.setError(getResources().getString(R.string.notif_mandatory));
            focusView = edit_biography;
            cancel = true;
        }

        if (TextUtils.isEmpty(edit_job.getText())) {
            edit_job.setError(getResources().getString(R.string.notif_mandatory));
            focusView = edit_job;
            cancel = true;
        }

        if (TextUtils.isEmpty(edit_job_desc.getText())) {
            edit_job_desc.setError(getResources().getString(R.string.notif_mandatory));
            focusView = edit_job_desc;
            cancel = true;
        }

        if (TextUtils.isEmpty(edit_job_level.getText())) {
            edit_job_level.setError(getResources().getString(R.string.notif_mandatory));
            focusView = edit_job_level;
            cancel = true;
        }

        if (TextUtils.isEmpty(edit_vision.getText())) {
            edit_vision.setError(getResources().getString(R.string.notif_mandatory));
            focusView = edit_vision;
            cancel = true;
        }

        if (TextUtils.isEmpty(edit_mission.getText())) {
            edit_mission.setError(getResources().getString(R.string.notif_mandatory));
            focusView = edit_mission;
            cancel = true;
        }

        if (TextUtils.isEmpty(edit_marriage_purpose.getText())) {
            edit_marriage_purpose.setError(getResources().getString(R.string.notif_mandatory));
            focusView = edit_marriage_purpose;
            cancel = true;
        }

        if (TextUtils.isEmpty(edit_ten_year_plan.getText())) {
            edit_ten_year_plan.setError(getResources().getString(R.string.notif_mandatory));
            focusView = edit_ten_year_plan;
            cancel = true;
        }

        if (TextUtils.isEmpty(edit_couple_height.getText())) {
            edit_couple_height.setError(getResources().getString(R.string.notif_mandatory));
            focusView = edit_couple_height;
            cancel = true;
        }

        if (TextUtils.isEmpty(edit_couple_weight.getText())) {
            edit_couple_weight.setError(getResources().getString(R.string.notif_mandatory));
            focusView = edit_couple_weight;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            callRegisterApi();
        }
    }

    private void callRegisterApi() {
        final ProgressBar pb = findViewById(R.id.progress_circular);
        pb.setVisibility(View.VISIBLE);

        RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), userid);
        RequestBody fullname = RequestBody.create(MediaType.parse("text/plain"), edit_fullname.getText().toString().trim());
        RequestBody nickname = RequestBody.create(MediaType.parse("text/plain"), edit_nickname.getText().toString().trim());
        RequestBody birthdate = RequestBody.create(MediaType.parse("text/plain"), edit_birthdate.getText().toString().trim());
        RequestBody address = RequestBody.create(MediaType.parse("text/plain"), edit_address.getText().toString().trim());
        RequestBody hobby = RequestBody.create(MediaType.parse("text/plain"), edit_hobby.getText().toString().trim());
        RequestBody biography = RequestBody.create(MediaType.parse("text/plain"), edit_biography.getText().toString().trim());
        RequestBody job = RequestBody.create(MediaType.parse("text/plain"), edit_job.getText().toString().trim());
        RequestBody job_desc = RequestBody.create(MediaType.parse("text/plain"), edit_job_desc.getText().toString().trim());
        RequestBody job_level = RequestBody.create(MediaType.parse("text/plain"), edit_job_level.getText().toString().trim());
        RequestBody vision = RequestBody.create(MediaType.parse("text/plain"), edit_vision.getText().toString().trim());
        RequestBody mission = RequestBody.create(MediaType.parse("text/plain"), edit_mission.getText().toString().trim());
        RequestBody marriage_purpose = RequestBody.create(MediaType.parse("text/plain"), edit_marriage_purpose.getText().toString().trim());
        RequestBody ten_year_plan = RequestBody.create(MediaType.parse("text/plain"), edit_ten_year_plan.getText().toString().trim());
        RequestBody couple_height = RequestBody.create(MediaType.parse("text/plain"), edit_couple_height.getText().toString().trim());
        RequestBody couple_weight = RequestBody.create(MediaType.parse("text/plain"), edit_couple_weight.getText().toString().trim());
        RequestBody couple_criteria = RequestBody.create(MediaType.parse("text/plain"), edit_couple_criteria.getText().toString().trim());
        RequestBody country = RequestBody.create(MediaType.parse("text/plain"), "Indonesia");

        RequestBody province = RequestBody.create(MediaType.parse("text/plain"), sp_province.getSelectedItem().toString());
        RequestBody city = RequestBody.create(MediaType.parse("text/plain"), sp_city.getSelectedItem().toString());
        RequestBody district = RequestBody.create(MediaType.parse("text/plain"), sp_district.getSelectedItem().toString());
        RequestBody village = RequestBody.create(MediaType.parse("text/plain"), sp_village.getSelectedItem().toString());
        RequestBody ethnicity = RequestBody.create(MediaType.parse("text/plain"), sp_ethnicity.getSelectedItem().toString());
        RequestBody religion = RequestBody.create(MediaType.parse("text/plain"), sp_religion.getSelectedItem().toString());
        RequestBody status = RequestBody.create(MediaType.parse("text/plain"), sp_status.getSelectedItem().toString());
        RequestBody education = RequestBody.create(MediaType.parse("text/plain"), sp_education.getSelectedItem().toString());
        RequestBody religious_education = RequestBody.create(MediaType.parse("text/plain"), sp_relgious_education.getSelectedItem().toString());
        RequestBody couple_physical = RequestBody.create(MediaType.parse("text/plain"), sp_couple_physical.getSelectedItem().toString());
        RequestBody couple_skin = RequestBody.create(MediaType.parse("text/plain"), sp_couple_skin.getSelectedItem().toString());
        RequestBody couple_ethnicity = RequestBody.create(MediaType.parse("text/plain"), sp_couple_ethnicity.getSelectedItem().toString());
        RequestBody couple_education = RequestBody.create(MediaType.parse("text/plain"), sp_couple_education.getSelectedItem().toString());
        RequestBody couple_province = RequestBody.create(MediaType.parse("text/plain"), sp_couple_province.getSelectedItem().toString());
        RequestBody couple_city = RequestBody.create(MediaType.parse("text/plain"), sp_couple_city.getSelectedItem().toString());
        RequestBody five_time_pray = RequestBody.create(MediaType.parse("text/plain"), sp_five_time_pray.getSelectedItem().toString());
        RequestBody sunnah_fasting = RequestBody.create(MediaType.parse("text/plain"), sp_sunnah_fasting.getSelectedItem().toString());
        RequestBody quran_literacy = RequestBody.create(MediaType.parse("text/plain"), sp_quran_literacy.getSelectedItem().toString());
        RequestBody quran_routinity = RequestBody.create(MediaType.parse("text/plain"), sp_quran_routinity.getSelectedItem().toString());
        RequestBody charity_routinity = RequestBody.create(MediaType.parse("text/plain"), sp_charity_routinity.getSelectedItem().toString());
        RequestBody islamic_study_routinity = RequestBody.create(MediaType.parse("text/plain"), sp_islamic_study_routinity.getSelectedItem().toString());
        RequestBody sex = RequestBody.create(MediaType.parse("text/plain"), sp_sex.getSelectedItem().toString());
        RequestBody marital_status = RequestBody.create(MediaType.parse("text/plain"), sp_status.getSelectedItem().toString());

        RequestBody lat = RequestBody.create(MediaType.parse("text/plain"), latitudecur+"");
        RequestBody lon = RequestBody.create(MediaType.parse("text/plain"), longitudecur+"");

        String blurred = switch_blurred.isChecked() ? "1" : "0";
        RequestBody blurred_image = RequestBody.create(MediaType.parse("text/plain"), blurred);

        Map<String, RequestBody> textMap = new HashMap<>();
        textMap.put("marital_status", marital_status);
        textMap.put("lat", lat);
        textMap.put("lon", lon);
        textMap.put("blurred_image", blurred_image);
        textMap.put("country", country);
        textMap.put("sex", sex);
        textMap.put("user_id", user_id);
        textMap.put("province", province);
        textMap.put("city", city);
        textMap.put("district", district);
        textMap.put("village", village);
        textMap.put("ethnicity", ethnicity);
        textMap.put("religion", religion);
        textMap.put("status", status);
        textMap.put("education", education);
        textMap.put("religious_education", religious_education);
        textMap.put("couple_physical", couple_physical);
        textMap.put("couple_skin", couple_skin);
        textMap.put("couple_ethnicity", couple_ethnicity);
        textMap.put("fullname", fullname);
        textMap.put("couple_education", couple_education);
        textMap.put("couple_province", couple_province);
        textMap.put("couple_city", couple_city);
        textMap.put("couple_criteria", couple_criteria);
        // textMap.put("couple_domicile", couple_city);
        textMap.put("five_time_pray", five_time_pray);
        textMap.put("sunnah_fasting", sunnah_fasting);
        textMap.put("quran_reading", quran_literacy);
        textMap.put("quran_routinity", quran_routinity);
        textMap.put("charity_routinity", charity_routinity);
        textMap.put("study_routinity", islamic_study_routinity);

        textMap.put("nickname", nickname);
        textMap.put("birthdate", birthdate);
        textMap.put("address", address);
        textMap.put("hobby", hobby);
        textMap.put("biography", biography);
        textMap.put("job", job);
        textMap.put("job_desc", job_desc);
        textMap.put("job_level", job_level);
        textMap.put("vision", vision);
        textMap.put("mission", mission);
        textMap.put("marriage_purpose", marriage_purpose);
        textMap.put("ten_year_plan", ten_year_plan);
        textMap.put("couple_height", couple_height);
        textMap.put("couple_weight", couple_weight);

        List<MultipartBody.Part> fileList = new ArrayList<>();
        File file = new File(fileFoto);
        RequestBody picture = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part image = MultipartBody.Part.createFormData("image", file.getName(), picture);
        fileList.add(image);

        RestAPI api = RetrofitAdapter.createAPI();

        Call<ResponseTaarufReg> callbackCall = api.registerTaaruf(textMap, fileList);
        callbackCall.enqueue(new Callback<ResponseTaarufReg>() {
            @Override
            public void onResponse(Call<ResponseTaarufReg> call, Response<ResponseTaarufReg> response) {
                ResponseTaarufReg resp = response.body();
                if (resp!=null) {
                    if (resp.status) {
                        Toast.makeText(myApp, "Pendaftaran Taaruf sukses", Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);
                        Intent intent = new Intent(ActivityTaarufRegister.this, ActivityTaarufNonMandatory.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(myApp, resp.status_message, Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);
                    }
                } else {
                    Toast.makeText(myApp, "Server error", Toast.LENGTH_LONG).show();
                    pb.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ResponseTaarufReg> call, Throwable t) {
                Toast.makeText(myApp, "Gagal melakukan registrasi (kesalahan sistem)", Toast.LENGTH_LONG).show();
                pb.setVisibility(View.GONE);
            }
        });
    }

    private void setEdit_birthdate() {
        InputMethodManager imm = (InputMethodManager) myApp.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(image_profile.getWindowToken(), 0);
        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);
        // date picker dialog
        picker = new DatePickerDialog(ActivityTaarufRegister.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        edit_birthdate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                    }
                }, year, month, day);
        picker.show();
    }

    public boolean checkPermission() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p:permissions) {
            result = ContextCompat.checkSelfPermission(this,p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),MULTIPLE_PERMISSIONS );
            return false;
        }
        return true;
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_CODE);
    }

    private void gettingGPSLocation() {
        GPSTracker gps = new GPSTracker();
        gps.init(ActivityTaarufRegister.this);
        // check if GPS enabled
        if (gps.canGetLocation()) {
            try {
                latitudecur = gps.getLatitude();
                longitudecur = gps.getLongitude();
                Log.w("Current Location", "Lat: " + latitudecur + "Long: " + longitudecur);
            } catch (NullPointerException | NumberFormatException e) {
                // TODO: handle exception
            }

        } else {
            gps.showSettingsAlert();
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode==PICK_IMAGE_ID) {
            if (resultCode == Activity.RESULT_OK) {
                final Bitmap mBitmap = ImagePicker.getImageFromResult(myApp, resultCode, data);
                image_profile.setImageBitmap(mBitmap);
                //File file = null;
                File resizedFile = createImageFile(); //new File(myFragment.getContext().getCacheDir(), "foto.png");
                BufferedOutputStream bos = null;
                InputStream bis = null;
                try {
                    bos = new BufferedOutputStream(new FileOutputStream(resizedFile), 1024);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    mBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    bis = new ByteArrayInputStream(stream.toByteArray());

                    byte[] buffer = new byte[1024];

                    int len = 0;

                    while ((len = bis.read(buffer)) != -1) {
                        bos.write(buffer, 0, len);
                    }
                    Uri selectedImage = FileProvider.getUriForFile(ActivityTaarufRegister.this, BuildConfig.APPLICATION_ID + ".provider",resizedFile);
                    fileFoto = resizedFile.getAbsolutePath();
                    System.out.println("resize path : " + fileFoto);
                    System.out.println("resize path : " + resizedFile.getAbsolutePath());
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        bis.close();
                    } catch (Exception ignored) {
                    }
                    try {
                        bos.close();
                    } catch (Exception ignored) {
                    }
                }
            }
        }
    }

    private File createImageFile() {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("HHmmss").format(new Date());
        //String imageFileName = "tb_" + timeStamp + "_";
        String imageFileName = "tb_";
        File storageDir = null;
        if(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) != null) {
            storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        } else if(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) != null){
            storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        } else if(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) != null){
            storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        } else {
            storageDir = this.getCacheDir();
        }
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  // prefix
                    ".jpg",   // suffix
                    storageDir      // directory
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void requestProvince() {
        RestRegionAPI api = RetrofitRegionAdapter.createAPI();
        Call<ResponseProvinceList> callbackCall = api.getProvinceList();
        callbackCall.enqueue(new Callback<ResponseProvinceList>() {
            @Override
            public void onResponse(Call<ResponseProvinceList> call, Response<ResponseProvinceList> response) {
                ResponseProvinceList resp = response.body();
                provinces = resp.provinsi;
                provinceList = new ArrayList<>();
                provinceList.add("-");
                for (int i=0; i<provinces.size(); i++) {
                    provinceList.add(provinces.get(i).nama);
                }
                setSp_province();
                setSp_couple_province();
            }

            @Override
            public void onFailure(Call<ResponseProvinceList> call, Throwable t) {

            }
        });
    }

    private void setSp_province() {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, simple_spinner_item, provinceList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        sp_province.setAdapter(spinnerArrayAdapter);

        sp_province.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i==0) {
                    cityList.clear();
                    sp_city.setAdapter(null);
                } else {
                    id_provinsi = provinces.get(i-1).id;
                    requestCity(id_provinsi);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void requestCity(long province_id) {
        RestRegionAPI api = RetrofitRegionAdapter.createAPI();
        Call<ResponseCityList> callbackCall = api.getCityList(province_id+"");
        callbackCall.enqueue(new Callback<ResponseCityList>() {
            @Override
            public void onResponse(Call<ResponseCityList> call, Response<ResponseCityList> response) {
                ResponseCityList resp = response.body();
                cities = resp.kota_kabupaten;
                cityList = new ArrayList<>();
                for (int i=0; i<cities.size(); i++) {
                    cityList.add(cities.get(i).nama);
                }
                setSp_city();
            }

            @Override
            public void onFailure(Call<ResponseCityList> call, Throwable t) {

            }
        });
    }

    private void setSp_city() {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, simple_spinner_item, cityList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        sp_city.setAdapter(spinnerArrayAdapter);

        sp_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                requestDistrict(cities.get(i).id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void requestDistrict(long city_id) {
        RestRegionAPI api = RetrofitRegionAdapter.createAPI();
        Call<ResponseDistrictList> callbackCall = api.getDistrictList(city_id+"");
        callbackCall.enqueue(new Callback<ResponseDistrictList>() {
            @Override
            public void onResponse(Call<ResponseDistrictList> call, Response<ResponseDistrictList> response) {
                ResponseDistrictList resp = response.body();
                districts = resp.kecamatan;
                districtList = new ArrayList<>();
                for (int i=0; i<districts.size(); i++) {
                    districtList.add(districts.get(i).nama);
                }
                setSp_district();
            }

            @Override
            public void onFailure(Call<ResponseDistrictList> call, Throwable t) {

            }
        });
    }

    private void setSp_district() {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, simple_spinner_item, districtList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        sp_district.setAdapter(spinnerArrayAdapter);

        sp_district.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                requestVillage(districts.get(i).id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void requestVillage(long district_id) {
        RestRegionAPI api = RetrofitRegionAdapter.createAPI();
        Call<ResponseVillageList> callbackCall = api.getVillagetList(district_id+"");
        callbackCall.enqueue(new Callback<ResponseVillageList>() {
            @Override
            public void onResponse(Call<ResponseVillageList> call, Response<ResponseVillageList> response) {
                Log.e("response body kel",response.body().toString());
                ResponseVillageList resp = response.body();
                villages = resp.kelurahan;
                villageList = new ArrayList<>();
                for (int i=0; i<villages.size(); i++) {
                    villageList.add(villages.get(i).nama);
                    Log.e("village name:",villages.get(i).nama);
                }
                setSp_village();
            }

            @Override
            public void onFailure(Call<ResponseVillageList> call, Throwable t) {
                Log.e("Failure", t.getMessage());
            }
        });
    }

    private void setSp_village() {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, simple_spinner_item, villageList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        sp_village.setAdapter(spinnerArrayAdapter);

        sp_village.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("selected village", villages.get(i).nama);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setSp_couple_province() {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, simple_spinner_item, provinceList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        sp_couple_province.setAdapter(spinnerArrayAdapter);

        sp_couple_province.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i==0) {
                    couple_cityList.clear();
                    sp_couple_city.setAdapter(null);
                } else {
                    couple_id_provinsi = provinces.get(i-1).id;
                    requestCoupleCity(couple_id_provinsi);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void requestCoupleCity(long province_id) {
        RestRegionAPI api = RetrofitRegionAdapter.createAPI();
        Call<ResponseCityList> callbackCall = api.getCityList(province_id+"");
        callbackCall.enqueue(new Callback<ResponseCityList>() {
            @Override
            public void onResponse(Call<ResponseCityList> call, Response<ResponseCityList> response) {
                ResponseCityList resp = response.body();
                couple_cities = resp.kota_kabupaten;
                couple_cityList = new ArrayList<>();
                for (int i=0; i<couple_cities.size(); i++) {
                    couple_cityList.add(couple_cities.get(i).nama);
                }
                setSp_couple_city();
            }

            @Override
            public void onFailure(Call<ResponseCityList> call, Throwable t) {

            }
        });
    }

    private void setSp_couple_city() {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, simple_spinner_item, couple_cityList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        sp_couple_city.setAdapter(spinnerArrayAdapter);

        sp_couple_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}