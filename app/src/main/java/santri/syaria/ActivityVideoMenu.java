package santri.syaria;

import androidx.annotation.DrawableRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import santri.Adapter.AdapterVideoMenu;
import santri.azan.activity.ActivitySettings;
import santri.azan.utils.Utils;
import santri.syaria.R;
import santri.video.ActivityVideo;
import santri.video.ActivityVideoDetails;
import santri.video.adapter.AdapterVideo;
import santri.video.connection.API;
import santri.video.connection.RestAdapter;
import santri.video.data.Constant;
import santri.video.data.ThisApplication;
import santri.video.model.Video;
import santri.video.model.Videos;
import santri.video.response.ResponseVideo;
import santri.video.utils.NetworkCheck;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.List;

public class ActivityVideoMenu extends AppCompatActivity {

    private SwipeRefreshLayout swipe_refresh;
    private ShimmerFrameLayout shimmer_video;
    private Call<ResponseVideo> callbackCall = null;

    private RecyclerView recycler_view;
    private AdapterVideoMenu mAdapter;
    private AdView mAdView = null;

    private int count_total = 0;
    private int failed_page = 0;

    //private ThisApplication application;
    private Videos application;

    private ActivityVideoMenu myApp;

    private Long playlist_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_menu);
        //application = ThisApplication.getInstance();
        application = new Videos();
        myApp = this;

        playlist_id = getIntent().getLongExtra("playlist_id", new Long(11));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SpannableString s = new SpannableString(getIntent().getStringExtra("title"));
        s.setSpan(new TypefaceSpan("MyTypeface.otf"), 0, s.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(
                (Html.fromHtml("<font color=\"#ffffff\">" + s + "</font>")));

        initComponent();
        initShimmerLoading();

        List<Video> videos = application.getVideos();
        int _count_total = application.getCountTotalVideo();
        if (videos.size() == 0) {
            requestAction(1);
        } else {
            count_total = _count_total;
            displayApiResult(videos);
        }
    }

    private void initComponent() {
        shimmer_video = findViewById(R.id.shimmer_video);
        swipe_refresh = findViewById(R.id.swipe_refresh);
        recycler_view = findViewById(R.id.recycler_view);
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setHasFixedSize(true);

        //set data and list adapter
        mAdapter = new AdapterVideoMenu(this, recycler_view, new ArrayList<Video>());
        recycler_view.setAdapter(mAdapter);

        // on item list clicked
        mAdapter.setOnItemClickListener(new AdapterVideoMenu.OnItemClickListener() {
            @Override
            public void onItemClick(View v, Video obj, int position) {
                ActivityVideoPlayer.navigate(myApp, obj.id, obj.url, false);
            }
        });

        // detect when scroll reach bottom
        mAdapter.setOnLoadMoreListener(new AdapterVideoMenu.OnLoadMoreListener() {
            @Override
            public void onLoadMore(int current_page) {
                if (count_total > mAdapter.getItemCount() && current_page != 0) {
                    int next_page = current_page + 1;
                    requestAction(next_page);
                } else {
                    mAdapter.setLoaded();
                }
            }
        });

        // on swipe list
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (callbackCall != null && callbackCall.isExecuted()) callbackCall.cancel();
                mAdapter.resetListData();
                requestAction(1);
            }
        });
    }

    private void displayApiResult(final List<Video> items) {
        mAdapter.insertData(items);
        swipeProgress(false);
        //if (items.size() == 0) showNoItemView(true);
    }

    private void requestListPlaylist(final int page_no) {
        API api = RestAdapter.createAPI();
        callbackCall = api.getListCustomVideo(page_no, Constant.VIDEO_PER_REQUEST, null, playlist_id);
        callbackCall.enqueue(new Callback<ResponseVideo>() {
            @Override
            public void onResponse(Call<ResponseVideo> call, Response<ResponseVideo> response) {
                Log.e("response video",response.body().toString());
                ResponseVideo resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    count_total = resp.count_total;
                    displayApiResult(resp.videos);

                    application.setCountTotalVideo(count_total);
                    application.addVideos(resp.videos);
                } else {
                    onFailRequest(page_no);
                }
            }

            @Override
            public void onFailure(Call<ResponseVideo> call, Throwable t) {
                if (!call.isCanceled()) onFailRequest(page_no);
            }

        });
    }

    private void onFailRequest(int page_no) {
        failed_page = page_no;
        mAdapter.setLoaded();
        swipeProgress(false);
        if (NetworkCheck.isConnect(myApp)) {
            showFailedView(true, getString(R.string.failed_text), R.drawable.img_failed);
        } else {
            showFailedView(true, getString(R.string.no_internet_text), R.drawable.img_no_internet);
        }
    }

    private void requestAction(final int page_no) {
        showFailedView(false, "", R.drawable.img_failed);
        //showNoItemView(false);
        if (page_no == 1) {
            swipeProgress(true);
            application.resetVideos();
        } else {
            mAdapter.setLoading();
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestListPlaylist(page_no);
            }
        }, 1000);
    }

    private void showFailedView(boolean show, String message, @DrawableRes int icon) {
        View lyt_failed = (View) findViewById(R.id.lyt_failed);
        ((ImageView) findViewById(R.id.failed_icon)).setImageResource(icon);
        ((TextView) findViewById(R.id.failed_message)).setText(message);
        if (show) {
            recycler_view.setVisibility(View.INVISIBLE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            recycler_view.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestAction(failed_page);
            }
        });
    }

    private void swipeProgress(final boolean show) {
        if (!show) {
            swipe_refresh.setRefreshing(show);
            shimmer_video.setVisibility(View.GONE);
            shimmer_video.stopShimmer();
            recycler_view.setVisibility(View.VISIBLE);
            return;
        }

        shimmer_video.setVisibility(View.VISIBLE);
        shimmer_video.startShimmer();
        recycler_view.setVisibility(View.INVISIBLE);
        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(show);
            }
        });
    }

    private void initShimmerLoading() {
        LinearLayout lyt_shimmer_content = findViewById(R.id.lyt_shimmer_content);
        for (int h = 0; h < 10; h++) {
            LinearLayout row_item = new LinearLayout(myApp);
            row_item.setOrientation(LinearLayout.HORIZONTAL);
            View item = getLayoutInflater().inflate(R.layout.loading_fragment_video, null);
            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
            p.weight = 1;
            item.setLayoutParams(p);
            row_item.addView(item);
            lyt_shimmer_content.addView(row_item);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}