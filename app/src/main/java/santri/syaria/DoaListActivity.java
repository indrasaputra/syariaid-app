package santri.syaria;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import santri.Getset.CategoryHeaderGetSet;
import santri.Getset.DoaListGetSet;

import static santri.syaria.MainActivity.changeStatsBarColor;
import static santri.syaria.MainActivity.checkInternet;
import static santri.syaria.MainActivity.showErrorDialog;
import static santri.syaria.MainActivity.tf_opensense_regular;

public class DoaListActivity extends AppCompatActivity {
    private ArrayList<DoaListGetSet> doaListGetSetArrayList;
    private ProgressDialog progressDialog;
    private ListView listView;
    private DoaListActivity myApp;
    private AdView mAdView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doa_list);

        myApp = this;

        changeStatsBarColor(DoaListActivity.this);

        initialization();
    }

    private void initialization() {
        listView = findViewById(R.id.listview);
        doaListGetSetArrayList = new ArrayList<>();
        ((TextView) findViewById(R.id.txt_title)).setTypeface(tf_opensense_regular);
        ImageButton ib_back = findViewById(R.id.ib_back);
        ib_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if (checkInternet(DoaListActivity.this))
            new GetDataAsyncTask().execute();
        else showErrorDialog(DoaListActivity.this);

        MobileAds.initialize(myApp, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(LoadAdError errorCode) {
                Log.e("failed load banner", errorCode+"");
                // Code to be executed when an ad request fails.
                //myApp.findViewById(R.id.startAppBanner).setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }

    class GetDataAsyncTask extends AsyncTask<Void, Void, Integer> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(DoaListActivity.this);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            URL hp;
            try {
                doaListGetSetArrayList.clear();

                hp = new URL(getString(R.string.link) + getString(R.string.servicepath) + "doa_list.php");
                Log.e("URL", "" + hp);
                URLConnection hpCon = hp.openConnection();
                hpCon.connect();
                InputStream input = hpCon.getInputStream();
                Log.d("input", "" + input);
                BufferedReader r = new BufferedReader(new InputStreamReader(input));
                String x;
                x = r.readLine();
                StringBuilder total = new StringBuilder();
                while (x != null) {
                    total.append(x);
                    x = r.readLine();
                }
                Log.d("URL", "" + total);
                final JSONObject jObject = new JSONObject(total.toString());

                if (Objects.equals(jObject.getString("status"), "Success")) {
                    JSONArray content_array = jObject.getJSONArray("Content");
                    for (int i = 0; i < content_array.length(); i++) {
                        DoaListGetSet temp = new DoaListGetSet();
                        temp.setId(content_array.getJSONObject(i).getString("id"));
                        temp.setTitle(content_array.getJSONObject(i).getString("title"));
                        temp.setDate(content_array.getJSONObject(i).getString("date"));
                        doaListGetSetArrayList.add(temp);
                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Toast.makeText(DoaListActivity.this, jObject.getString("status"), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }

            } catch (JSONException | IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (NullPointerException e) {
                // TODO: handle exception
            }
            return null;
        }

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        @Override
        protected void onPostExecute(Integer aVoid) {

        }

    }

    class DoaListAdapter extends BaseAdapter {
        private final ArrayList data1;
        private final Activity activity;
        private LayoutInflater inflater = null;
        private static final int TYPE_ITEM = 1;

        DoaListAdapter(Activity a, ArrayList str) {
            activity = a;
            data1 = str;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return data1.size();
        }

        @Override
        public Object getItem(int position) {
            return data1.get(position);
        }

        @Override
        public int getItemViewType(int position) {
            return TYPE_ITEM;

        }

        @Override
        public boolean isEnabled(int position) {
            return (getItemViewType(position) == TYPE_ITEM);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;

            Typeface opensansregular = Typeface.createFromAsset(activity.getAssets(), "fonts/OpenSans-Regular.ttf");

            int type = getItemViewType(position);

            vi = inflater.inflate(R.layout.cell_doalist, parent, false);

            return vi;
        }

    }

}
