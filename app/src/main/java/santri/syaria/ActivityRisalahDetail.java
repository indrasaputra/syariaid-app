package santri.syaria;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import santri.video.ActivityVideoDetails;
import santri.video.utils.Tools;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

public class ActivityRisalahDetail extends AppCompatActivity {

    private ActivityRisalahDetail myApp;

    private ImageView image;
    private TextView title, content;

    private AdView mAdView = null;

    private String menu_title, image_url, txt_title, txt_content;

    public static void navigate(Activity activity, String menu_title, String title, String content, String image_url) {
        Intent i = navigateBase(activity, menu_title, title, content, image_url);
        activity.startActivity(i);
    }

    public static Intent navigateBase(Context context, String menu_title, String title, String content, String image_url) {
        Intent i = new Intent(context, ActivityRisalahDetail.class);
        i.putExtra("menu_title", menu_title);
        i.putExtra("txt_title", title);
        i.putExtra("txt_content", content);
        i.putExtra("image_url", image_url);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_risalah_detail);

        myApp = this;

        menu_title = getIntent().getStringExtra("menu_title");
        image_url = getIntent().getStringExtra("image_url");
        txt_title = getIntent().getStringExtra("txt_title");
        txt_content = getIntent().getStringExtra("txt_content");

        Log.e("ActivityRisalahDetail",menu_title+image_url+txt_title);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SpannableString s = new SpannableString(menu_title);
        s.setSpan(new TypefaceSpan("MyTypeface.otf"), 0, s.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(
                (Html.fromHtml("<font color=\"#ffffff\">" + s + "</font>")));

        initComponent();
    }

    private void initComponent() {
        image = findViewById(R.id.image);
        title = findViewById(R.id.title);
        content = findViewById(R.id.content);

        if (!image_url.equals(""))
            Tools.displayImage(myApp, image, image_url.replace("http:","https:"));
        title.setText(txt_title);
        content.setText(Html.fromHtml(txt_content));

        MobileAds.initialize(myApp, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(LoadAdError errorCode) {
                Log.e("failed load banner", errorCode+"");
                // Code to be executed when an ad request fails.
                //myApp.findViewById(R.id.startAppBanner).setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}