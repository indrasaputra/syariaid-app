package santri.syaria;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.ViewPager;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import santri.ActivityVideoContent;
import santri.firebasechat.MessageActivity;
import santri.syaria.retrofit.ResponseChatStatus;
import santri.syaria.retrofit.RestAPI;
import santri.syaria.retrofit.RetrofitAdapter;
import santri.video.ActivityVideoDetails;
import santri.video.adapter.AdapterVideoRecent;
import santri.video.connection.API;
import santri.video.connection.RestAdapter;
import santri.video.model.Video;
import santri.video.response.ResponseHome;
import santri.video.utils.Tools;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class ActivityPraNikah extends AppCompatActivity {
    private ActivityPraNikah myApp;

    private static final String MY_PREFS_NAME = "Fooddelivery";
    private SharedPreferences prefs;

    private Call<ResponseHome> callbackCall;

    private ProgressBar progressBar;
    private String regUrl;
    private static final int VIDEO_MENU = 1;
    private static final int VIDEO_CONTENT = 2;
    private static final int EBOOK_MENU = 3;
    private static final int EBOOK_CONTENT = 4;
    private static final int CHAT_USTADZ = 5;
    private static final int CERTIFICATE_MENU = 6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pra_nikah);

        myApp = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SpannableString s = new SpannableString("Madrasah Online Pra-Nikah");
        s.setSpan(new TypefaceSpan("MyTypeface.otf"), 0, s.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(
                (Html.fromHtml("<font color=\"#ffffff\">" + s + "</font>")));

        prefs = myApp.getSharedPreferences(MY_PREFS_NAME, myApp.MODE_PRIVATE);

        initComponent();
        //requestVideoData();
    }

    private void initComponent() {
        progressBar = findViewById(R.id.progress_bar);

        findViewById(R.id.card_video).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkSubscription(VIDEO_MENU, null);
            }
        });

        findViewById(R.id.card_ebook).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkSubscription(EBOOK_MENU, null);
            }
        });

        findViewById(R.id.card_certificate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkSubscription(CERTIFICATE_MENU, null);
            }
        });
    }

    private void requestVideoData() {
        API api = RestAdapter.createAPI();
        callbackCall = api.getHome();
        callbackCall.enqueue(new Callback<ResponseHome>() {
            @Override
            public void onResponse(Call<ResponseHome> call, Response<ResponseHome> response) {
                ResponseHome resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    displayVideo(resp.recent);
                    //ThisApplication.getInstance().setResponseHome(resp);
                    //swipeProgress(false);
                    //lyt_main_content.setVisibility(View.VISIBLE);
                } else {
                    //onFailRequest();
                }
            }

            @Override
            public void onFailure(Call<ResponseHome> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                //if (!call.isCanceled()) onFailRequest();
            }
        });
    }

    private void displayVideo(List<Video> items) {
        ViewPager view_pager = findViewById(R.id.view_pager_video);
        final AdapterVideoRecent mAdapter = new AdapterVideoRecent(myApp, items);
        view_pager.setAdapter(mAdapter);
        view_pager.setOffscreenPageLimit(4);
        final int padding = Tools.getPaddingCard(getResources().getDimensionPixelOffset(R.dimen.item_card_height));
        view_pager.setPadding(padding, 0, padding, 0);
        view_pager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
                page.setTranslationX(-(padding - Tools.dpToPx(myApp, 12)));
            }
        });

        mAdapter.setOnItemClickListener(new AdapterVideoRecent.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Video obj) {

                //ActivityVideoContent.navigate(myApp, obj.id, false);

                checkSubscription(VIDEO_CONTENT, obj.id);
            }
        });
    }

    private void checkSubscription(int menu, Long id) {
        SharedPreferences sp = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String userid = sp.getString("userid", "");
        String subStatus = sp.getString("pranikahStatus",null);
        String timeStamp = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
        String pranikahStatus = subStatus!=null ? subStatus.split("\\|")[0] : "0";
        String lastCheck = subStatus!=null ? subStatus.split("\\|")[1] : timeStamp;
        String useridCheck = subStatus!=null ? subStatus.split("\\|")[2] : "0";

        Log.e("pranikah status",pranikahStatus+"|"+lastCheck+"|"+useridCheck);

        if (pranikahStatus.equals("1") && lastCheck.equals(timeStamp) && useridCheck.equals(userid)) {
            openMenu(menu, id);
        } else {
            getSubscriptionStatus(userid, menu, id);
        }
    }

    private void getSubscriptionStatus(String userid, int menu, Long id) {
        progressBar.setVisibility(View.VISIBLE);
        JSONObject jsonReq = new JSONObject();
        try {
            jsonReq.put("personid", userid);
            jsonReq.put("prodakid", "2");
            jsonReq.put("token", "385ef7144b8ebc2d48aa915056e8cca5");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonReq.toString());
        RestAPI api = RetrofitAdapter.createAPI();
        Call<ResponseChatStatus> callbackCall = api.getUserChatStatus(bodyRequest);
        callbackCall.enqueue(new Callback<ResponseChatStatus>() {
            @Override
            public void onResponse(Call<ResponseChatStatus> call, Response<ResponseChatStatus> response) {
                ResponseChatStatus resp = response.body();
                if (resp.status.equals("1")) {
                    String timeStamp = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
                    String chatStatus = "1";
                    String subStatus = chatStatus + "|" + timeStamp + "|" + userid;
                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putString("pranikahStatus", subStatus);
                    editor.apply();
                    editor.commit();
                    openMenu(menu, id);
                } else {
                    regUrl = resp.link;
                    openLandingPage();
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ResponseChatStatus> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }
//        try {
//            Response<ResponseChatStatus> response = callbackCall.execute();
//            ResponseChatStatus resp = response.body();
//            if (resp.status.equals("1")) {
//                String timeStamp = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
//                String chatStatus = "1";
//                String subStatus = chatStatus+"|"+timeStamp+"|"+userid;
//                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
//                editor.putString("chatStatus", subStatus);
//                editor.apply();
//                editor.commit();
//                progressBar.setVisibility(View.GONE);
//                return true;
//            } else {
//                regUrl = resp.link;
//                progressBar.setVisibility(View.GONE);
//                return false;
//            }
//        } catch (Exception e) {
//            progressBar.setVisibility(View.GONE);
//            return false;
//        }

//    }

    private void openMenu(int menu, Long id) {
        Intent intent = null;
        switch (menu) {
            case VIDEO_MENU:
                intent = new Intent(ActivityPraNikah.this, ActivityVideoMenu.class);
                intent.putExtra("title","Video Pra-Nikah");
                intent.putExtra("playlist_id", new Long(12));
                startActivity(intent);
                break;
            case VIDEO_CONTENT:
                //ActivityVideoContent.navigate(myApp, id, false);
                // Intent intent = new Intent(ActivityPraNikah.this, ActivityVideoPlayer.class);
                intent = new Intent(ActivityPraNikah.this, ActivityVideoMenu.class);
                intent.putExtra("title","Video Pra-Nikah");
                intent.putExtra("playlist_id", new Long(12));
                startActivity(intent);
                break;
            case EBOOK_MENU:
                intent = new Intent(ActivityPraNikah.this, ActivityPranikahEbook.class);
                intent.putExtra("title","Ebook Pra-Nikah");
                startActivity(intent);
                break;
            case EBOOK_CONTENT:
                break;
            case CHAT_USTADZ:
                break;
            case CERTIFICATE_MENU:
                intent = new Intent(ActivityPraNikah.this, WebViewActivity.class);
                intent.putExtra("url","https://docs.google.com/forms/d/e/1FAIpQLSc-ow_dJky0_eGPu7wBqyno1w1yJesVBXje4I-R6djcvtafSg/viewform");
                intent.putExtra("purpose","");
                intent.putExtra("title","Tes Sertifikasi Pra Nikah");
                startActivity(intent);
                break;
        }
    }

    private void openLandingPage() {
        Intent intent = new Intent(ActivityPraNikah.this, WebViewActivity.class);
        //intent.putExtra("url","http://syaria.id/order-produk/R1hMUklEcGprT0Y1akl3RFdZck9Kdz09");
        intent.putExtra("url",regUrl);
        intent.putExtra("purpose","order");
        intent.putExtra("title","E-Learning Pra Nikah");
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}