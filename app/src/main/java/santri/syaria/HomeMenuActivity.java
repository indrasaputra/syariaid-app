package santri.syaria;

import android.Manifest;
import android.app.AlertDialog;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
//import com.startapp.sdk.adsbase.StartAppAd;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import androidx.fragment.app.Fragment;
import santri.azan.service.AzanJobService;
import santri.azan.support.AppLocationService;
import santri.azan.utils.LogUtils;
import santri.azan.utils.Utils;
import santri.utils.Util;

public class HomeMenuActivity extends ChatActivity {

    //private TextView mTextMessage;
    private String key;
    private final int PERMISSION_REQUEST_CODE = 1001;
    private final int PERMISSION_REQUEST_CODE1 = 10011;
    private final String[] permission_location = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    private final String[] permission_location1 = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};

    public static final int MULTIPLE_PERMISSIONS = 10;

    private HomeMenuActivity myApp;

    AppLocationService appLocationService;

    public String USER_COUNTRY = "user_country";
    public String USER_STATE = "user_state";
    public String USER_CITY = "user_city";
    public String USER_LAT = "user_lat";
    public String USER_LNG = "user_lng";
    public String USER_MLAT = "user_mlat";
    public String USER_MLNG = "user_mlng";
    public String USER_STREET = "user_street";
    public String USER_LANGUAGES = "user_langu";

    SharedPreferences spref;
    SharedPreferences.Editor editor;

    private AdView mAdView;
    private AdRequest adRequest;
    private InterstitialAd mInterstitialAd;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = new HomeMenuFragment();
                    break;
                case R.id.navigation_dashboard:
                    //fragment = new MyOrderFragment();
                    fragment = new OrderFragment();
                    break;
                case R.id.navigation_notifications:
                    fragment = new MenusFragment();
                    break;
            }
            return loadFragment(fragment);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_menu);

        myApp = this;

//        ActionBar actionBar = getSupportActionBar();
//        if (actionBar != null) {
//            //actionBar.setHomeButtonEnabled(true);
//            //actionBar.setDisplayHomeAsUpEnabled(true);
//
//            actionBar.setTitle(R.string.app_name);
//        }

        session.setOnOffNotification(true);

        getSupportActionBar().hide();

        MobileAds.initialize(myApp, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(LoadAdError errorCode) {
                Log.e("failed load banner", errorCode+"");
                // Code to be executed when an ad request fails.
                //myApp.findViewById(R.id.startAppBanner).setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });

//        FirebaseApp.initializeApp(this);
//        //FirebaseInstanceId.getInstance().getInstanceId();
//        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
//            @Override
//            public void onSuccess(InstanceIdResult instanceIdResult) {
//                String newToken = instanceIdResult.getToken();
//                Log.d("Firebase Token",newToken);
//                santri.firebasechat.managers.Utils.uploadToken(newToken);
//            }
//        });
//
//        Log.e("userid in pref",LoadPref("userid"));
//
//        if (LoadPref("userid") != null && !LoadPref("userid").equals("delete")) {
//            Util.subscribeFirebaseTopic("reg_users");
//        }


        //scheduleJob();

        Bundle extras = getIntent().getExtras();
        //if (!extras.getString("key").equals(null)) {
        if(extras!=null && extras.containsKey("key")) {
            key = extras.getString("key");
            if (key.equals("orderplace")) {
                loadFragment(new OrderFragment());
            } else {
                loadFragment(new HomeMenuFragment());
            }
        } else {
            loadFragment(new HomeMenuFragment());
        }

        appLocationService = new AppLocationService(HomeMenuActivity.this);

        if (!checkPermission()) {
            requestPermission();
        }

        LogUtils.i(LoadPref(USER_MLAT) + " lat " + LoadPref(USER_MLNG));
        if (LoadPref(USER_LAT).equals("")) {
            SavePref(USER_LAT, "0.0");
            SavePref(USER_LNG, "0.0");

        }
        if (!LoadPref(USER_MLAT).equals("") || !LoadPref(USER_MLNG).equals("") && LoadPref(USER_LAT).equals("") || LoadPref(USER_LAT).equals("0.0") ) {

            if (isOnline()) {


                {
                    getLoc();
                }

            } else {
                showalert();
            }
        }


        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private void scheduleJob() {
        String schedule = LoadPref("schedule");

        if (schedule.equals(null) || !schedule.equals("1")) {
            azanScheduleUpdate();
            SavePref("schedule","1");
        }
    }

    private void azanScheduleUpdate() {
        JobScheduler jobScheduler = (JobScheduler)getApplicationContext()
                .getSystemService(JOB_SCHEDULER_SERVICE);

        ComponentName componentName = new ComponentName(this,
                AzanJobService.class);

        JobInfo jobInfo = new JobInfo.Builder(1, componentName)
                .setPeriodic(21600000).setRequiredNetworkType(
                        JobInfo.NETWORK_TYPE_NOT_ROAMING)
                .setPersisted(true).build();
        jobScheduler.schedule(jobInfo);
    }

    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(HomeMenuActivity.this, R.style.MyDialogTheme);
        builder1.setTitle(getString(R.string.Quit));
        builder1.setMessage(getString(R.string.statementquit));
        builder1.setCancelable(true);
        builder1.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                //StartAppAd.showAd(myApp);
                myApp.starInterstitialAd();
                finishAffinity();
            }
        });
        builder1.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void starInterstitialAd() {
        InterstitialAd.load(this, myApp.getResources().getString(R.string.admob_insertitial_id), adRequest,
        new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                // The mInterstitialAd reference will be null until
                // an ad is loaded.
                mInterstitialAd = interstitialAd;
                Log.i("Interstitial", "onAdLoaded");
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                // Handle the error
                // StartAppAd.showAd(myApp);
                Log.i("Interstitial", loadAdError.getMessage());
                mInterstitialAd = null;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        //loadFragment(new HomeMenuFragment());
    }

    private boolean checkPermission() {
        //int result = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
        //return result == PackageManager.PERMISSION_GRANTED;

        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p:permission_location) {
            result = ContextCompat.checkSelfPermission(this,p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),MULTIPLE_PERMISSIONS );
            return false;
        }
        return true;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, permission_location, PERMISSION_REQUEST_CODE);
        ActivityCompat.requestPermissions(this, permission_location1, PERMISSION_REQUEST_CODE1);
    }

    public void getLoc() {
        try {

            Location location = appLocationService.getLocation();

            LogUtils.i("location " + location);
            if (location != null) {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                saveString(USER_LAT,String.valueOf(latitude));
                saveString(USER_LNG,String.valueOf(longitude));
                Geocoder geocoder = new Geocoder(HomeMenuActivity.this,
                        Locale.getDefault());

                List<Address> addresses = geocoder.getFromLocation(latitude,
                        longitude, 1);

                for (Address adrs : addresses) {

                    LogUtils.i(adrs.getLocality() + " city "
                            + adrs.getAdminArea() + " state "
                            + adrs.getCountryName());
                    if (adrs != null) {

                        String city = adrs.getLocality();
                        String Country = adrs.getCountryName();
                        String State = adrs.getAdminArea();
                        String Street = adrs.getSubLocality();
                        SavePref(USER_CITY, city);
                        SavePref(USER_STATE, State);
                        SavePref(USER_COUNTRY, Country);
                        SavePref(USER_STREET, Street);

                    }

                }

            } else {
                showSettingsAlert();
                SavePref(USER_CITY, "");
                SavePref(USER_STATE, "");

                SavePref(USER_STREET, "");
                SavePref(USER_COUNTRY, "");
            }

        } catch (IOException e) {
            e.printStackTrace();
            SavePref(USER_CITY, "");
            SavePref(USER_STATE, "");

            SavePref(USER_STREET, "");
            SavePref(USER_COUNTRY, "");
        }

    }

    // Method to save map type setting to SharedPreferences
    public void saveString(String param, String value) {
        SharedPreferences sharedPreferences = getSharedPreferences(
                "Prayer_pref", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(param, value);
        editor.commit();
    }

    public void SavePref(String key, String value) {
        spref = getSharedPreferences(Utils.SharedPref_filename, MODE_PRIVATE);
        editor = spref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String LoadPref(String key) {
        spref = getSharedPreferences(Utils.SharedPref_filename, MODE_PRIVATE);
        return spref.getString(key, "");
    }

    public void showSettingsAlert() {

        new MaterialDialog.Builder(this).title("GPS is Not Enabled")
                .content(getResources().getString(R.string.gps_openmsg)).cancelable(false)
                .positiveText("Ok").positiveColor(Color.parseColor("#379e24"))
                .negativeText(android.R.string.cancel)
                .negativeColor(Color.parseColor("#379e24"))
                .callback(new MaterialDialog.ButtonCallback() {

                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                        // finish();
                    }

                }).build().show();
    }

    public void showalert() {

        new MaterialDialog.Builder(this).title("No Internet Connection")
                .content("Enable your Network Connectivity").cancelable(false)
                .positiveText("Ok").positiveColor(Color.parseColor("#379e24"))
                .negativeText(android.R.string.cancel)
                .negativeColor(Color.parseColor("#379e24"))
                .callback(new MaterialDialog.ButtonCallback() {

                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                        // finish();
                    }

                }).build().show();
    }

    public boolean isOnline() {
        ConnectivityManager manager = (ConnectivityManager) this
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager.getActiveNetworkInfo() != null
                && manager.getActiveNetworkInfo().isAvailable()
                && manager.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            return false;
        }
    }

}
