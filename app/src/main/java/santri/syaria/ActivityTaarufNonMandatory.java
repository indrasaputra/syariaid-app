package santri.syaria;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import santri.syaria.retrofit.ResponseContent;
import santri.syaria.retrofit.ResponseTaarufUpdate;
import santri.syaria.retrofit.RestAPI;
import santri.syaria.retrofit.RetrofitAdapter;
import santri.syaria.retrofit.farizdotid.City;
import santri.syaria.retrofit.farizdotid.Province;
import santri.syaria.retrofit.farizdotid.ResponseCityList;
import santri.syaria.retrofit.farizdotid.ResponseProvinceList;
import santri.syaria.retrofit.farizdotid.RestRegionAPI;
import santri.syaria.retrofit.farizdotid.RetrofitRegionAdapter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.R.layout.simple_spinner_item;

public class ActivityTaarufNonMandatory extends AppCompatActivity {

    private ActivityTaarufNonMandatory myApp;

    private EditText
    edit_positive_chars,
    edit_negative_chars,
    edit_most_liked,
    edit_most_disliked,
    edit_anger_habit,
    edit_spare_time,
    edit_other_personal_info,
    edit_height,
    edit_weight,
    edit_other_physical_info,
    edit_father_age,
    edit_mother_age,
    edit_order_in_family,
    edit_total_siblings,
    edit_other_family_info,
    edit_other_financial_info,
    edit_married_target,
    edit_children_own_target,
    edit_wedding_plan;

    private Spinner
    sp_physical,
    sp_skin,
    sp_hair,
    sp_hair_color,
    sp_father_ethnicity,
    sp_father_religion,
    sp_father_nationality,
    sp_mother_ethnicity,
    sp_mother_religion,
    sp_mother_nationality,
    sp_family_province,
    sp_family_city,
    sp_home_status,
    sp_vehicle,
    sp_own_saving,
    sp_assets,
    sp_business,
    sp_marriage_knowledge;

    private List<Province> provinces = new ArrayList<>();
    private ArrayList<String> provinceList = new ArrayList<>();

    private List<City> cities = new ArrayList<>();
    private ArrayList<String> cityList = new ArrayList<>();

    private SharedPreferences prefs;
    private static final String MY_PREFS_NAME = "Fooddelivery";

    private String userid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taaruf_non_mandatory);

        myApp = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SpannableString s = new SpannableString("Taaruf Syari");
        s.setSpan(new TypefaceSpan("MyTypeface.otf"), 0, s.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(
                (Html.fromHtml("<font color=\"#ffffff\">" + s + "</font>")));

        prefs = myApp.getSharedPreferences(MY_PREFS_NAME, myApp.MODE_PRIVATE);

        initComponent();
        requestProvince();
    }

    private void initComponent() {
        edit_positive_chars = findViewById(R.id.edit_positive_chars);
        edit_negative_chars = findViewById(R.id.edit_negative_chars);
        edit_most_liked = findViewById(R.id.edit_most_liked);
        edit_most_disliked = findViewById(R.id.edit_most_disliked);
        edit_anger_habit = findViewById(R.id.edit_anger_habit);
        edit_spare_time = findViewById(R.id.edit_spare_time);
        edit_other_personal_info = findViewById(R.id.edit_other_personal_info);
        edit_height = findViewById(R.id.edit_height);
        edit_weight = findViewById(R.id.edit_weight);
        edit_other_physical_info = findViewById(R.id.edit_other_physical_info);
        edit_father_age = findViewById(R.id.edit_father_age);
        edit_mother_age = findViewById(R.id.edit_mother_age);
        edit_order_in_family = findViewById(R.id.edit_order_in_family);
        edit_total_siblings = findViewById(R.id.edit_total_siblings);
        edit_other_family_info = findViewById(R.id.edit_other_family_info);
        edit_other_financial_info = findViewById(R.id.edit_other_financial_info);
        edit_married_target = findViewById(R.id.edit_married_target);
        edit_children_own_target = findViewById(R.id.edit_children_own_target);
        edit_wedding_plan = findViewById(R.id.edit_wedding_plan);

        sp_physical = findViewById(R.id.sp_physical);
        sp_skin = findViewById(R.id.sp_skin);
        sp_hair = findViewById(R.id.sp_hair);
        sp_hair_color = findViewById(R.id.sp_hair_color);
        sp_father_ethnicity = findViewById(R.id.sp_father_ethnicity);
        sp_father_religion = findViewById(R.id.sp_father_religion);
        sp_father_nationality = findViewById(R.id.sp_father_nationality);
        sp_mother_ethnicity = findViewById(R.id.sp_mother_ethnicity);
        sp_mother_religion = findViewById(R.id.sp_mother_religion);
        sp_mother_nationality = findViewById(R.id.sp_mother_nationality);
        sp_family_province = findViewById(R.id.sp_family_province);
        sp_family_city = findViewById(R.id.sp_family_city);
        sp_home_status = findViewById(R.id.sp_home_status);
        sp_vehicle = findViewById(R.id.sp_vehicle);
        sp_own_saving = findViewById(R.id.sp_own_saving);
        sp_assets = findViewById(R.id.sp_assets);
        sp_business = findViewById(R.id.sp_business);
        sp_marriage_knowledge = findViewById(R.id.sp_marriage_knowledge);

        userid = prefs.getString("userid","");

        Button btn_submit = findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitUpdate();
            }
        });
    }

    private void submitUpdate() {
        final ProgressBar pb = findViewById(R.id.progress_circular);
        pb.setVisibility(View.VISIBLE);

        JSONObject jsonReq = new JSONObject();
        try {
            jsonReq.put("user_id", userid);
            jsonReq.put("positive_chars",edit_positive_chars.getText());
            jsonReq.put("negative_chars", edit_negative_chars.getText());
            jsonReq.put("most_liked", edit_most_liked.getText());
            jsonReq.put("most_disliked", edit_most_disliked.getText());
            jsonReq.put("anger_habit", edit_anger_habit.getText());
            jsonReq.put("spare_time", edit_spare_time.getText());
            jsonReq.put("other_personal_info", edit_other_personal_info.getText());
            jsonReq.put("physical", sp_physical.getSelectedItem().toString());
            jsonReq.put("skin", sp_skin.getSelectedItem().toString());
            jsonReq.put("height", edit_height.getText());
            jsonReq.put("weight", edit_weight.getText());
            jsonReq.put("hair", sp_hair.getSelectedItem().toString());
            jsonReq.put("hair_color", sp_hair_color.getSelectedItem().toString());
            jsonReq.put("other_physical_info", edit_other_physical_info.getText());
            jsonReq.put("father_age", edit_father_age.getText());
            jsonReq.put("father_ethnicity", sp_father_ethnicity.getSelectedItem().toString());
            jsonReq.put("father_religion", sp_father_religion.getSelectedItem().toString());
            jsonReq.put("father_nationality", sp_father_nationality.getSelectedItem().toString());
            jsonReq.put("mother_age", edit_mother_age.getText());
            jsonReq.put("mother_ethnicity", sp_mother_ethnicity.getSelectedItem().toString());
            jsonReq.put("mother_religion", sp_mother_religion.getSelectedItem().toString());
            jsonReq.put("mother_nationality", sp_mother_nationality.getSelectedItem().toString());
            jsonReq.put("order_in_family", edit_order_in_family.getText());
            jsonReq.put("total_siblings", edit_total_siblings.getText());
            jsonReq.put("family_domicile", sp_family_city.getSelectedItem().toString() +", "+ sp_family_province.getSelectedItem().toString());
            jsonReq.put("other_family_info", edit_other_family_info.getText());
            jsonReq.put("home_status", sp_home_status.getSelectedItem().toString());
            jsonReq.put("vehicle", sp_vehicle.getSelectedItem().toString());
            jsonReq.put("own_saving", sp_own_saving.getSelectedItem().toString());
            jsonReq.put("assets", sp_assets.getSelectedItem().toString());
            jsonReq.put("business", sp_business.getSelectedItem().toString());
            jsonReq.put("other_financial_info", edit_other_financial_info.getText());
            jsonReq.put("married_target", edit_married_target.getText());
            jsonReq.put("children_own_target", edit_children_own_target.getText());
            jsonReq.put("marriage_knowledge", sp_marriage_knowledge.getSelectedItem().toString());
            jsonReq.put("wedding_plan", edit_wedding_plan.getText());
            jsonReq.put("marriage_certificate", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonReq.toString());
        RestAPI api = RetrofitAdapter.createAPI();
        Call<ResponseTaarufUpdate> callbackCall = api.updateTaarufNonMandatory(bodyRequest);
        callbackCall.enqueue(new Callback<ResponseTaarufUpdate>() {
            @Override
            public void onResponse(Call<ResponseTaarufUpdate> call, Response<ResponseTaarufUpdate> response) {
                Log.e("response ",response.body().toString());
                ResponseTaarufUpdate resp = response.body();
                if (resp != null) {
                    if (resp.status) {
                        Toast.makeText(myApp, "Update Profil Taaruf Sukses", Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);
                    } else {
                        Toast.makeText(myApp, resp.status_message, Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);
                    }
                } else {
                    Toast.makeText(myApp, "Server error", Toast.LENGTH_LONG).show();
                    pb.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ResponseTaarufUpdate> call, Throwable t) {
                Toast.makeText(myApp, "Gagal melakukan registrasi (kesalahan sistem)", Toast.LENGTH_LONG).show();
                pb.setVisibility(View.GONE);
            }
        });
    }

    private void requestProvince() {
        RestRegionAPI api = RetrofitRegionAdapter.createAPI();
        Call<ResponseProvinceList> callbackCall = api.getProvinceList();
        callbackCall.enqueue(new Callback<ResponseProvinceList>() {
            @Override
            public void onResponse(Call<ResponseProvinceList> call, Response<ResponseProvinceList> response) {
                ResponseProvinceList resp = response.body();
                provinces = resp.provinsi;
                provinceList = new ArrayList<>();
                provinceList.add("-");
                for (int i=0; i<provinces.size(); i++) {
                    provinceList.add(provinces.get(i).nama);
                }
                setSp_province();
            }

            @Override
            public void onFailure(Call<ResponseProvinceList> call, Throwable t) {

            }
        });
    }

    private void setSp_province() {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, simple_spinner_item, provinceList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        sp_family_province.setAdapter(spinnerArrayAdapter);

        sp_family_province.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i==0) {
                    cityList.clear();
                    sp_family_city.setAdapter(null);
                } else {
                    long id_provinsi = provinces.get(i-1).id;
                    requestCity(id_provinsi);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void requestCity(long province_id) {
        RestRegionAPI api = RetrofitRegionAdapter.createAPI();
        Call<ResponseCityList> callbackCall = api.getCityList(province_id+"");
        callbackCall.enqueue(new Callback<ResponseCityList>() {
            @Override
            public void onResponse(Call<ResponseCityList> call, Response<ResponseCityList> response) {
                ResponseCityList resp = response.body();
                cities = resp.kota_kabupaten;
                cityList = new ArrayList<>();
                for (int i=0; i<cities.size(); i++) {
                    cityList.add(cities.get(i).nama);
                }
                setSp_city();
            }

            @Override
            public void onFailure(Call<ResponseCityList> call, Throwable t) {

            }
        });
    }

    private void setSp_city() {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, simple_spinner_item, cityList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        sp_family_city.setAdapter(spinnerArrayAdapter);

        sp_family_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}