package santri.syaria;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import santri.firebasechat.managers.SessionManager;

public class ChatActivity extends AppCompatActivity {

    public FirebaseAuth auth; //Auth init
    public DatabaseReference reference; //Database releated
    public FirebaseUser firebaseUser; //Current User
    public ChatActivity myApp;

    String IMG_DEFAULTS = "default";

    String REF_USERS = "Users";

    String EXTRA_USER_ID = "userId";

    String EXTRA_SENDER = "sender";
    String EXTRA_RECEIVER = "receiver";
    String EXTRA_MESSAGE = "message";
    String EXTRA_TYPE = "type";
    String EXTRA_IMGPATH = "imgPath";
    String EXTRA_DATETIME = "datetime";
    String EXTRA_SEEN = "msgseen";
    String EXTRA_STATUS = "status";
    String EXTRA_SEARCH = "search";
    String EXTRA_VERSION = "version";

    String EXTRA_ID = "id";
    String EXTRA_EMAIL = "email";
    String EXTRA_USERNAME = "username";
    String EXTRA_PASSWORD = "password";
    String EXTRA_IMAGEURL = "imageURL";
    String EXTRA_ACTIVE = "active";
    String EXTRA_TYPING = "typing";
    String EXTRA_TYPINGWITH = "typingwith";
    String EXTRA_LINK = "linkPath";
    String EXTRA_ABOUT = "about";
    String EXTRA_GENDER = "gender";
    String EXTRA_LASTSEEN = "lastSeen";
    String EXTRA_REKENING = "rekening";

    long CLICK_DELAY_TIME = 250;

    private static final String MY_PREFS_NAME = "Fooddelivery";
    public SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myApp = this;

        session = new SessionManager(myApp);

        auth = FirebaseAuth.getInstance();
    }

    public void loginChat(String email, String password, String username, String rekening, String imgUrl) {

        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    FirebaseUser firebaseUser = auth.getCurrentUser();
                    assert firebaseUser != null;
                    String userId = firebaseUser.getUid();
                    Log.d("login firebase userId",userId);

                    reference = FirebaseDatabase.getInstance().getReference(REF_USERS).child(userId);

                    HashMap<String, Object> hashMap = new HashMap<>();
                    hashMap.put(EXTRA_ID, userId);
                    hashMap.put(EXTRA_EMAIL, email);
                    hashMap.put(EXTRA_USERNAME, username);
                    hashMap.put(EXTRA_PASSWORD, password);
                    hashMap.put(EXTRA_IMAGEURL, imgUrl);
                    hashMap.put(EXTRA_ACTIVE, true);
                    hashMap.put(EXTRA_STATUS, getString(R.string.strOnline));
                    hashMap.put(EXTRA_SEARCH, username.toLowerCase().trim());
                    hashMap.put(EXTRA_VERSION, BuildConfig.VERSION_NAME);
                    hashMap.put(EXTRA_REKENING, rekening);
                    reference.updateChildren(hashMap);

                    session.setOnOffNotification(true);

                    //Toast.makeText(myApp, "Success Chat Login", Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(myApp, "Fail Chat Login", Toast.LENGTH_SHORT).show();
                    register(email, username, password, rekening, imgUrl);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        }).addOnCanceledListener(new OnCanceledListener() {
            @Override
            public void onCanceled() {

            }
        });
    }

    public void register(final String email, final String username, final String password, final String rekening, String imgUrl) {

        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()) {
                    FirebaseUser firebaseUser = auth.getCurrentUser();
                    assert firebaseUser != null;
                    String userId = firebaseUser.getUid();
                    Log.d("reg firebase userId",userId);

                    reference = FirebaseDatabase.getInstance().getReference(REF_USERS).child(userId);

                    HashMap<String, Object> hashMap = new HashMap<>();
                    hashMap.put(EXTRA_ID, userId);
                    hashMap.put(EXTRA_EMAIL, email);
                    hashMap.put(EXTRA_USERNAME, username);
                    hashMap.put(EXTRA_PASSWORD, password);
                    hashMap.put(EXTRA_IMAGEURL, imgUrl);
                    hashMap.put(EXTRA_ACTIVE, true);
                    hashMap.put(EXTRA_STATUS, getString(R.string.strOnline));
                    hashMap.put(EXTRA_SEARCH, username.toLowerCase().trim());
                    hashMap.put(EXTRA_VERSION, BuildConfig.VERSION_NAME);
                    hashMap.put(EXTRA_REKENING, rekening);

                    session.setOnOffNotification(true);

                    reference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Log.e("set firebase value",hashMap.size()+"");
                        }
                    });
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        }).addOnCanceledListener(new OnCanceledListener() {
            @Override
            public void onCanceled() {

            }
        });
    }

    public void logout(final Activity mActivity) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                readStatus(mActivity, mActivity.getString(R.string.strOffline));
                FirebaseAuth.getInstance().signOut();
            }
        }, CLICK_DELAY_TIME);

    }

    public void updateOnlineStatus(final Context mContext, final String userId, final String status) {
        try {
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference(REF_USERS).child(userId);
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put(EXTRA_STATUS, status);
            if (status.equalsIgnoreCase(mContext.getString(R.string.strOffline)))
                hashMap.put(EXTRA_LASTSEEN, getDateTime());
            reference.updateChildren(hashMap);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }
    }

    public void readStatus(Context mActivity, String status) {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            updateOnlineStatus(mActivity, firebaseUser.getUid(), status);
        }
    }

    public String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = new Date();

        return dateFormat.format(date);
    }

}
