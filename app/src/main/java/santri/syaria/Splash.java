package santri.syaria;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import santri.utils.Util;

import static santri.syaria.MainActivity.changeStatsBarColor;

public class Splash extends AppCompatActivity {
    private static final String MY_PREFS_NAME = "Fooddelivery";
    private boolean isDeliveryAccountActive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        changeStatsBarColor(Splash.this);

        Util.initializeFirebase(this,"all_users",false);

        int SPLASH_TIME_OUT = getResources().getInteger(R.integer.splash_time_out);
        new Handler().postDelayed(new Runnable() {

                                      /*
                                       * Showing splash screen with a timer. This will be useful when you
                                       * want to show case your app logo / company
                                       */

                                      @Override
                                      public void run() {
                                          launchHomeScreen();
//                                          SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
//                                          isDeliveryAccountActive = prefs.getBoolean("isDeliverAccountActive", false);
//                                          if (isDeliveryAccountActive) {
//                                              //Intent iv = new Intent(Splash.this, DeliveryStatus.class);
//                                              //Intent iv = new Intent(Splash.this, MainMenuActivity.class);
//                                              Intent iv = new Intent(Splash.this, WelcomeActivity.class);
//                                              startActivity(iv);
//                                              finish();
//                                          } else {
//                                              //Intent iv = new Intent(Splash.this, MainActivity.class);
//                                              //Intent iv = new Intent(Splash.this, MainMenuActivity.class);
//                                              Intent iv = new Intent(Splash.this, WelcomeActivity.class);
//                                              startActivity(iv);
//                                              finish();
//
//                                          }
                                      }
                                  },
                SPLASH_TIME_OUT);
    }

    private void launchHomeScreen() {
        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putBoolean("firsttime", false);
        //startActivity(new Intent(WelcomeActivity.this, MainMenuActivity.class));
        //startActivity(new Intent(WelcomeActivity.this, HomeMenuActivity.class));
        if (checkingSignIn()) {
            startActivity(new Intent(Splash.this, HomeMenuActivity.class));
        } else if (checkingPartnerSignIn()) {
            startActivity(new Intent(Splash.this, DeliveryStatus.class));
        } else {
            startActivity(new Intent(Splash.this, WelcomeActivity.class));
        }
        finish();
    }

    private boolean checkingSignIn() {
        //getting shared preference
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        Log.e("user", "" + prefs.getString("userid", null));
        // check user is created or not
        // if user is already logged in
        if (prefs.getString("userid", null) != null) {
            String userid = prefs.getString("userid", null);
            return !userid.equals("delete");
        } else {
            return false;
        }
    }

    private boolean checkingPartnerSignIn() {
        //getting shared preference
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        Log.e("user", "" + prefs.getBoolean("isDeliverAccountActive", false));
        // check user is created or not
        // if user is already logged in
        if (prefs.getBoolean("isDeliverAccountActive", false)) {
            //String userid = prefs.getString("userid", null);
            //return !userid.equals("delete");
            return true;
        } else {
            return false;
        }
    }

}

