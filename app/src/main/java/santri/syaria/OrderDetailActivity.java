package santri.syaria;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import santri.Getset.orderTimelineGetSet;
import santri.syaria.retrofit.OrderData;
import santri.timeline.OrderStatus;
import santri.timeline.TimeLineAdapter;
import santri.timeline.TimeLineModel;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static santri.syaria.MainActivity.changeStatsBarColor;
import static santri.syaria.MainActivity.tf_opensense_medium;
import static santri.syaria.MainActivity.tf_opensense_regular;

public class OrderDetailActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private final List<TimeLineModel> mDataList = new ArrayList<>();
    private String orderId;
    private String[] ordertxt;
    private TextView txt_name;
    private TextView txt_contact;
    private TextView txt_address;
    private TextView txt_order_amount;
    private TextView txt_order_EstimatedTime;
    private TextView txt_order_time;
    private TextView txt_book_date;
    private TextView txt_book_time;
    private TextView txt_book_note;
    private TextView txt_book_duration;
    private TextView txt_cara_pembayaran;
    private ImageView rest_image;
    private ArrayList<orderTimelineGetSet> orderGetSet;
    private orderTimelineGetSet oData;
    private String key = "";
    private String bookdate, booktime, booknote, bookduration;
    String rejectedOrder;
    private AdView mAdView;
    private OrderDetailActivity myApp;

    private OrderData orderData;
    private String regUrl;
    private String pdfUrl;
    private Button btn_start;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        myApp = this;

        changeStatsBarColor(OrderDetailActivity.this);

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        gettingIntents();
        initView();
        updateTimeLine();
    }

    private void gettingIntents() {
        Intent i = getIntent();
        orderData = (OrderData) i.getSerializableExtra("OrderData");
        regUrl = i.getStringExtra("regUrl");
    }

    private void initView() {

        //settine time line adapter for order status
        mRecyclerView = findViewById(R.id.time_line);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);

        getSupportActionBar().hide();

        //initialization
        TextView txt_title = findViewById(R.id.txt_title);
        //txt_title.setTypeface(tf_opensense_regular);

        TextView txt_orderno = findViewById(R.id.txt_orderno);
        //txt_orderno.setTypeface(tf_opensense_regular);
        txt_orderno.setText("Order No : " + orderData.orderno);

        rest_image = findViewById(R.id.image);

        txt_name = findViewById(R.id.txt_name);
        //txt_name.setTypeface(tf_opensense_regular);

        txt_address = findViewById(R.id.txt_address);
        txt_address.setTypeface(tf_opensense_regular);

        txt_contact = findViewById(R.id.txt_contact);
        //txt_contact.setTypeface(tf_opensense_regular);

        TextView txt_order_title = findViewById(R.id.txt_order_title);
        //txt_order_title.setTypeface(tf_opensense_regular);

        TextView txt_order = findViewById(R.id.txt_order);
        //txt_order.setTypeface(tf_opensense_regular);
        txt_order.setText(orderData.orderno);

        TextView txt_order_amount_title = findViewById(R.id.txt_order_amount_title);
        //txt_order_amount_title.setTypeface(tf_opensense_regular);

        txt_order_amount = findViewById(R.id.txt_order_amount);
        //txt_order_amount.setTypeface(tf_opensense_regular);

        TextView txt_order_estimatedtime_title = findViewById(R.id.txt_order_estimatedtime_title);
        //txt_order_estimatedtime_title.setTypeface(tf_opensense_regular);

        txt_order_EstimatedTime = findViewById(R.id.txt_order_estimatedtime);
        //txt_order_EstimatedTime.setTypeface(tf_opensense_regular);

        TextView txt_order_time_title = findViewById(R.id.txt_order_time_title);
        //txt_order_time_title.setTypeface(tf_opensense_regular);

        txt_order_time = findViewById(R.id.txt_order_time);
        //txt_order_time.setTypeface(tf_opensense_regular);

        TextView txt_book_date_title = findViewById(R.id.txt_book_date_title);
        //txt_book_date_title.setTypeface(tf_opensense_regular);

        txt_book_date = findViewById(R.id.txt_book_date);
        //txt_book_date.setTypeface(tf_opensense_regular);

        TextView txt_book_time_title = findViewById(R.id.txt_book_time_title);
        //txt_book_time_title.setTypeface(tf_opensense_regular);

        txt_book_time = findViewById(R.id.txt_book_time);
        //txt_book_time.setTypeface(tf_opensense_regular);

        TextView txt_book_note_title = findViewById(R.id.txt_book_note_title);
        //txt_book_time_title.setTypeface(tf_opensense_regular);

        txt_book_note = findViewById(R.id.txt_book_note);
        //txt_book_note.setTypeface(tf_opensense_regular);

        txt_book_duration = findViewById(R.id.txt_book_duration);
        //txt_book_duration.setTypeface(tf_opensense_regular);
        TextView txt_total_amount = findViewById(R.id.txt_total_amount);

        //on click listeners

        txt_name.setText(orderData.produk);
        txt_address.setText(orderData.tgl_register);
        //txt_order_title.setText(orderData.orderno);
        txt_order_amount.setText("Rp " + String.format(Locale.ITALY,"%1$,.0f", Float.parseFloat(orderData.infaq)));//orderData.infaq);
        txt_order_time.setText("Rp " + String.format(Locale.ITALY,"%1$,.0f", Float.parseFloat(orderData.infaq_sukarela)));
        String paymentType = orderData.payment_type!=null ? orderData.payment_type.toUpperCase() : "";
        String bank = orderData.bank!=null ? orderData.bank.toUpperCase() : "";
        txt_book_date.setText(paymentType + " " + bank);
        txt_book_time.setText(orderData.va_number);

        String totalAmount = (Integer.parseInt(orderData.infaq)+Integer.parseInt(orderData.infaq_sukarela))+"";
        txt_total_amount.setText("Rp " + String.format(Locale.ITALY,"%1$,.0f", Float.parseFloat(totalAmount)));

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy HH:mm:ss");
        String paymentExpired = null;
        try {
            paymentExpired = LocalDateTime.parse(orderData.expired_bayar.replace(" ", "T")).format(formatter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        txt_book_duration.setText(paymentExpired);

        Button btn_placeorder = findViewById(R.id.btn_placeorder);
        //btn_placeorder.setTypeface(tf_opensense_regular);

        if (orderData.status.equals("200")) {
            btn_placeorder.setVisibility(View.VISIBLE);
        } else {
            btn_placeorder.setVisibility(View.GONE);
        }

        btn_placeorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if (orderData.produk_id.equals("1")) {
                    Intent intent = new Intent(myApp, WebViewActivity.class);
                    //intent.putExtra("url","http://syaria.id/order-produk/R1hMUklEcGprT0Y1akl3RFdZck9Kdz09");
                    intent.putExtra("url",regUrl);
                    intent.putExtra("purpose","order");
                    startActivity(intent);
                //}
            }
        });

        ImageButton ib_back = findViewById(R.id.ib_back);
        ib_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (key.equals("orderplace")) {
                    Intent i = new Intent(OrderDetailActivity.this, HomeMenuActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);
                    startActivity(i);
                } else onBackPressed();
            }
        });

        RelativeLayout rel_order = findViewById(R.id.rel_order);
//        rel_order.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(OrderDetailActivity.this, OrderSpecification.class);
//                i.putExtra("OrderId", orderId);
//                startActivity(i);
//            }
//        });

        txt_cara_pembayaran = findViewById(R.id.txt_cara_pembayaran);
        //txt_cara_pembayaran.setTypeface(tf_opensense_medium);

        if (orderData.pdf_url!=null && !orderData.pdf_url.equals("")) {
            txt_cara_pembayaran.setVisibility(View.VISIBLE);

            try {
                pdfUrl = "https://docs.google.com/viewer?embedded=true&url=" + URLEncoder.encode(orderData.pdf_url, "utf-8");
                //url = item.source.split("=")[1];
            } catch (Exception e) {
                Log.e("Error",e.getMessage().toString());
            }
            txt_cara_pembayaran.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(myApp, WebViewActivity.class);
                    intent.putExtra("url", pdfUrl);
                    intent.putExtra("purpose", "");
                    startActivity(intent);
                }
            });
        } else {
            txt_cara_pembayaran.setVisibility(View.GONE);
        }

        TextView txt_copy = findViewById(R.id.txt_copy);
        txt_copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setClipboard(myApp, txt_book_time.getText().toString());
            }
        });

        btn_start = findViewById(R.id.btn_start);
        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (orderData.produk_id.equals("1")) {
                    Intent intent = new Intent(myApp, TanyaUstadzActivity.class);
                    intent.putExtra("catName","TanyaUstad");
                    intent.putExtra("sub_category_name","Pengajian Umum");
                    intent.putExtra("CityName","all");
                    startActivity(intent);
                } else if (orderData.produk_id.equals("2")) {
                    Intent intent = new Intent(myApp, ActivityPraNikah.class);
                    startActivity(intent);
                }
            }
        });
    }

    private void setClipboard(Context context, String text) {
        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
            android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText(text);
            Toast.makeText(myApp, "Kode sudah tersalin", Toast.LENGTH_SHORT).show();
        } else {
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", text);
            clipboard.setPrimaryClip(clip);
            Toast.makeText(myApp, "Kode sudah tersalin", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateTimeLine() {
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy HH:mm:ss");

        Date regDate = null;
        Date expiredDate = null;
        Date startDate = null;
        Date endDate = null;
        Date now = null;

        String strRegDate = null;
        String strExpiredDate = null;
        String strStartDate = null;
        String strEndDate = null;
        String strNow = null;
        try {
            regDate = sdf.parse(orderData.cratedtime);
            expiredDate = sdf.parse(orderData.expired_bayar);
            startDate = orderData.start_awal!=null ? sdf.parse(orderData.start_awal) : null;
            endDate = orderData.batas_akhir!=null ? sdf.parse(orderData.batas_akhir) : null;
            now = sdf.parse(sdf.format(new Date()));

            strRegDate = LocalDateTime.parse(orderData.cratedtime.replace(" ", "T")).format(formatter);
            strExpiredDate = LocalDateTime.parse(orderData.expired_bayar.replace(" ", "T")).format(formatter);
            strStartDate = orderData.start_awal!=null ? LocalDateTime.parse(orderData.start_awal.replace(" ", "T")).format(formatter) : "";
            strEndDate = orderData.batas_akhir!=null ? LocalDateTime.parse(orderData.batas_akhir.replace(" ", "T")).format(formatter) : "";
            Log.e("strRegData",strRegDate);
            Log.e("strExpiredDate",strExpiredDate);
            Log.e("strStartDate",strStartDate);
            Log.e("strEndDate",strEndDate);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        mDataList.add(new TimeLineModel("Order", strRegDate+"", OrderStatus.ACTIVE));
        if (orderData.status.equals("201")) {
            if (now.before(expiredDate)) {
                mDataList.add(new TimeLineModel("Menunggu Pembayaran", "", OrderStatus.ACTIVE));
                mDataList.add(new TimeLineModel("Active", "", OrderStatus.INACTIVE));
                mDataList.add(new TimeLineModel("Completed", "", OrderStatus.INACTIVE));
                //txt_cara_pembayaran.setVisibility(View.VISIBLE);
            } else {
                mDataList.add(new TimeLineModel("Menunggu Pembayaran", "", OrderStatus.ACTIVE));
                mDataList.add(new TimeLineModel("Expired", strExpiredDate, OrderStatus.ACTIVE));
                //txt_cara_pembayaran.setVisibility(View.GONE);
            }
        } else if (orderData.status.equals("200")) {
            mDataList.add(new TimeLineModel("Menunggu Pembayaran", "", OrderStatus.ACTIVE));
            mDataList.add(new TimeLineModel("Active", strStartDate, OrderStatus.ACTIVE));
            //txt_cara_pembayaran.setVisibility(View.GONE);
            if (endDate!=null) {
                if (now.before(endDate)) {
                    mDataList.add(new TimeLineModel("Completed", strEndDate, OrderStatus.INACTIVE));
                    btn_start.setVisibility(View.VISIBLE);
                } else {
                    mDataList.add(new TimeLineModel("Completed", strEndDate, OrderStatus.ACTIVE));
                }
            } else {
                mDataList.add(new TimeLineModel("Completed", "", OrderStatus.INACTIVE));
            }
        } else {
            mDataList.add(new TimeLineModel("Menunggu Pembayaran", strRegDate, OrderStatus.ACTIVE));
            //mDataList.add(new TimeLineModel("Expired", "", OrderStatus.ACTIVE));
            mDataList.add(new TimeLineModel("Failed", "", OrderStatus.INACTIVE));
            //txt_cara_pembayaran.setVisibility(View.GONE);
        }
        boolean mWithLinePadding = false;
        TimeLineAdapter mTimeLineAdapter = new TimeLineAdapter(mDataList, mWithLinePadding);
        mRecyclerView.setAdapter(mTimeLineAdapter);
    }
}