package santri.syaria;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import static santri.syaria.DeliveryStatus.tf_opensense_regular;

public class DeliveryUserProfile extends AppCompatActivity {
    private static final String MY_PREFS_NAME = "Fooddelivery";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_user_profile);
        getSupportActionBar().hide();

        initViews();

    }



    private void initViews() {
        TextView txt_header = findViewById(R.id.txt_header);
        txt_header.setTypeface(tf_opensense_regular);
        TextView txt_name_tittle = findViewById(R.id.txt_name_tittle);
        txt_name_tittle.setTypeface(tf_opensense_regular);
        TextView txt_name = findViewById(R.id.txt_name);
        txt_name.setTypeface(tf_opensense_regular);
        TextView txt_contact_tittle = findViewById(R.id.txt_contact_tittle);
        txt_contact_tittle.setTypeface(tf_opensense_regular);
        TextView txt_contact = findViewById(R.id.txt_contact);
        txt_contact.setTypeface(tf_opensense_regular);
        TextView txt_email_tittle = findViewById(R.id.txt_email_tittle);
        txt_email_tittle.setTypeface(tf_opensense_regular);
        TextView txt_email = findViewById(R.id.txt_email);
        txt_email.setTypeface(tf_opensense_regular);
        TextView txt_vehicle_no_tittle = findViewById(R.id.txt_vehicle_no_tittle);
        txt_vehicle_no_tittle.setTypeface(tf_opensense_regular);
        TextView txt_vehicle_no = findViewById(R.id.txt_vehicle_no);
        txt_vehicle_no.setTypeface(tf_opensense_regular);
        TextView txt_vehicle_type_tittle = findViewById(R.id.txt_vehicle_type_tittle);
        txt_vehicle_type_tittle.setTypeface(tf_opensense_regular);
        TextView txt_vehicle_type = findViewById(R.id.txt_vehicle_type);
        txt_vehicle_type.setTypeface(tf_opensense_regular);

        TextView txt_rekening_tittle = findViewById(R.id.txt_rekening_tittle);
        txt_rekening_tittle.setTypeface(tf_opensense_regular);
        TextView txt_rekening = findViewById(R.id.txt_rekening);
        txt_rekening.setTypeface(tf_opensense_regular);
        TextView txt_pendidikan_tittle = findViewById(R.id.txt_pendidikan_tittle);
        txt_pendidikan_tittle.setTypeface(tf_opensense_regular);
        TextView txt_pendidikan = findViewById(R.id.txt_pendidikan);
        txt_pendidikan.setTypeface(tf_opensense_regular);
        TextView txt_pesantren_tittle = findViewById(R.id.txt_pesantren_tittle);
        txt_pesantren_tittle.setTypeface(tf_opensense_regular);
        TextView txt_pendidikan_pesantren = findViewById(R.id.txt_pendidikan_pesantren);
        txt_pendidikan_pesantren.setTypeface(tf_opensense_regular);
        TextView txt_pekerjaan_tittle = findViewById(R.id.txt_pekerjaan_tittle);
        txt_pekerjaan_tittle.setTypeface(tf_opensense_regular);
        TextView txt_pekerjaan = findViewById(R.id.txt_pekerjaan);
        txt_pekerjaan.setTypeface(tf_opensense_regular);
        TextView txt_kitab_tittle = findViewById(R.id.txt_kitab_tittle);
        txt_kitab_tittle.setTypeface(tf_opensense_regular);
        TextView txt_kitab = findViewById(R.id.txt_kitab);
        txt_kitab.setTypeface(tf_opensense_regular);
        TextView txt_desc_tittle = findViewById(R.id.txt_desc_tittle);
        txt_desc_tittle.setTypeface(tf_opensense_regular);
        TextView txt_desc = findViewById(R.id.txt_desc);
        txt_desc.setTypeface(tf_opensense_regular);
        TextView txt_address_tittle = findViewById(R.id.txt_address_tittle);
        txt_address_tittle.setTypeface(tf_opensense_regular);
        TextView txt_address = findViewById(R.id.txt_address);
        txt_address.setTypeface(tf_opensense_regular);

        ImageButton ib_back = findViewById(R.id.ib_back);
        ib_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        //getting shared preference

        SharedPreferences sp = getSharedPreferences(MY_PREFS_NAME,MODE_PRIVATE);
        txt_name.setText(sp.getString("DeliveryUserName", ""));
        txt_contact.setText(sp.getString("DeliveryUserPhone", ""));
        txt_email.setText(sp.getString("DeliveryUserEmail", ""));
        txt_vehicle_no.setText(sp.getString("DeliveryUserVNo", ""));
        txt_vehicle_type.setText(sp.getString("DeliveryUserVType", ""));

        txt_rekening.setText(sp.getString("DeliveryUserRek", ""));
        txt_pendidikan.setText(sp.getString("DeliveryUserPendidikan", ""));
        txt_pendidikan_pesantren.setText(sp.getString("DeliveryUserPesantren", ""));
        txt_pekerjaan.setText(sp.getString("DeliveryUserPekerjaan", ""));
        txt_kitab.setText(sp.getString("DeliveryUserKitab", ""));
        txt_desc.setText(sp.getString("DeliveryUserDesc", ""));
        txt_address.setText(sp.getString("DeliveryUserAddress", ""));
    }
}
