package santri.syaria;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import santri.Adapter.DeliveryAdapter;
import santri.Getset.DeliveryGetSet;
import santri.firebasechat.MainChatActivity;
import santri.utils.Config;
import santri.utils.ConnectionDetector;
import santri.utils.GPSTracker;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static santri.firebasechat.constants.IConstants.EXTRA_SEEN;
import static santri.firebasechat.constants.IConstants.REF_CHATS;
import static santri.firebasechat.constants.IConstants.SLASH;

public class DeliveryStatus extends ChatActivity {

    private ArrayList<DeliveryGetSet> data;
    private ListView listView_delivery;
    private static final String MY_PREFS_NAME = "Fooddelivery";
    private String status,DeliveryBoyId,regId;
    public static Typeface tf_opensense_regular;
    public static Typeface tf_opensense_medium;
    private double latitudecur = 0;
    private double longitudecur = 0;
    private final String[] permission_location = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    private final String[] permission_location1 = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    private final int PERMISSION_REQUEST_CODE = 1002;
    private final int PERMISSION_REQUEST_CODE1 = 10012;
    private FrameLayout fab_layout;
    private TextView text_count;
    private int unreadCount;
    private FloatingActionButton fabChat;
    private ToggleButton toggle_chat;

    private DeliveryStatus myApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_status);
        getSupportActionBar().hide();

        myApp = this;

        if (checkPermission()) {
            gettingGPSLocation();
        } else {
            requestPermission();
        }

        SharedPreferences prefsDeliveryBoyId = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        DeliveryBoyId = prefsDeliveryBoyId.getString("DeliveryUserId",null);
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        regId = pref.getString("regId", null);
        Log.d("checksfds",""+regId+":::"+DeliveryBoyId);
        new RegisterMobile().execute();

        FirebaseApp.initializeApp(this);
        //FirebaseInstanceId.getInstance().getInstanceId();
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();
                Log.d("Firebase Token",newToken);
                santri.firebasechat.managers.Utils.uploadToken(newToken);
            }
        });

        //setChatButton();

//        if (prefsDeliveryBoyId.getString("DeliveryUserChat",null).equals("1")) {
//            fab_layout.setVisibility(View.VISIBLE);
//            fabChat.show();
//            setTotalUnreadMessages();
//            fabChat.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(myApp, MainChatActivity.class);
//                    startActivity(intent);
//                }
//            });
//        } else {
//            fabChat.hide();
//            fab_layout.setVisibility(View.GONE);
//        }
    }

    private void setChatButton() {
        SharedPreferences prefsDeliveryBoyId = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        //if (prefsDeliveryBoyId.getString("DeliveryUserChat",null).equals("1")) {
            fab_layout.setVisibility(View.VISIBLE);
            fabChat.show();
            String email = prefsDeliveryBoyId.getString("DeliveryUserEmail",null);
            String username = prefsDeliveryBoyId.getString("DeliveryUserName",null);
            String rekening = prefsDeliveryBoyId.getString("DeliveryUserRek",null);
            String imgUrl = prefsDeliveryBoyId.getString("DeliveryUserImage",null);
            loginChat(email, "123456", username, rekening, imgUrl);
            setTotalUnreadMessages();
            fabChat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(myApp, MainChatActivity.class);
                    startActivity(intent);
                }
            });
//        } else {
//            fabChat.hide();
//            fab_layout.setVisibility(View.GONE);
//        }
    }

    private void setTotalUnreadMessages() {
        unreadCount = 0;
        text_count.setVisibility(View.GONE);
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        if (firebaseUser!=null) {
            Query reference = FirebaseDatabase.getInstance().getReference(REF_CHATS).child(firebaseUser.getUid());
            reference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.hasChildren()) {
                        Log.d("datasnapshot children",dataSnapshot.getChildrenCount()+"");
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Query query = FirebaseDatabase.getInstance().getReference(REF_CHATS).child(firebaseUser.getUid() + SLASH + snapshot.getKey()).orderByChild(EXTRA_SEEN).equalTo(false);
                            query.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot2) {
                                    if (dataSnapshot2.hasChildren()) {
                                        Log.d("datasnapshot2 children",dataSnapshot2.getChildrenCount()+"");
                                        unreadCount = unreadCount+(int)dataSnapshot2.getChildrenCount();
                                        Log.d("unreadCount in",unreadCount+"");
                                        if (unreadCount > 0) {
                                            text_count.setText(unreadCount+"");
                                            text_count.setVisibility(View.VISIBLE);
                                        } else {
                                            text_count.setVisibility(View.GONE);
                                        }

                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                        }
                        Log.d("unreadcount after for", unreadCount+"");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    private void gettingGPSLocation() {
        GPSTracker gps = new GPSTracker();
        gps.init(DeliveryStatus.this);
        // check if GPS enabled
        if (gps.canGetLocation()) {
            try {
                latitudecur = gps.getLatitude();
                longitudecur = gps.getLongitude();
                Log.w("Current Location", "Lat: " + latitudecur + "Long: " + longitudecur);
            } catch (NullPointerException | NumberFormatException e) {
                // TODO: handle exception
            }

        } else {
            gps.showSettingsAlert();
        }


    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, permission_location, PERMISSION_REQUEST_CODE);
        ActivityCompat.requestPermissions(this, permission_location1, PERMISSION_REQUEST_CODE1);
    }

    private void initViews() {

        TextView txt_header = findViewById(R.id.txt_header);
        TextView txt_name = findViewById(R.id.txt_name);
        TextView btn_deilverb = findViewById(R.id.btn_deilverb);
        TextView btn_signout = findViewById(R.id.btn_signout);
        TextView txt_presenceOn = findViewById(R.id.txt_presenceOn);
        TextView list_order_none = findViewById(R.id.list_order_none);
        TextView btn_order_history = findViewById(R.id.btn_order_history);
        TextView btn_my_profile = findViewById(R.id.btn_my_profile);
        SwitchCompat sw_radius_onoff = findViewById(R.id.Sw_radius_onoff);
        final ImageView img_user = findViewById(R.id.img_user);

        listView_delivery = findViewById(R.id.list_order_info);
        txt_header.setTypeface(tf_opensense_regular);
        txt_name.setTypeface(tf_opensense_regular);
        btn_deilverb.setTypeface(tf_opensense_regular);
        btn_signout.setTypeface(tf_opensense_regular);
        txt_presenceOn.setTypeface(tf_opensense_regular);
        list_order_none.setTypeface(tf_opensense_regular);
        btn_order_history.setTypeface(tf_opensense_regular);
        btn_my_profile.setTypeface(tf_opensense_regular);

        fabChat = findViewById(R.id.fabChat);
        fab_layout = findViewById(R.id.fab_layout);
        text_count = findViewById(R.id.text_count);

        //getting shared pref and setting data

        txt_name.setText(getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).getString("DeliveryUserName", ""));

        sw_radius_onoff.setChecked(getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).getBoolean("isPresent", false));

        AlphaAnimation anim = new AlphaAnimation(0, 1);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.ABSOLUTE);
        anim.setDuration(1000);
        //Picasso.with(this).load(this.getResources().getString(R.string.link) + this.getString(R.string.imagepath) + getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).getString("DeliveryUserImage", "")).into(img_user);
        Picasso.with(this).load(getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).getString("DeliveryUserImage", "")).into(img_user);
        img_user.startAnimation(anim);

        sw_radius_onoff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    status = "yes";
                } else {
                    status = "no";
                }

                sendPresence(status);

            }
        });

        btn_signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor edit = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                edit.putBoolean("isDeliverAccountActive", false);
                edit.putString("DeliveryUserId", "");
                edit.putString("DeliveryUserName", "");
                edit.putString("DeliveryUserPhone", "");
                edit.putString("DeliveryUserEmail", "");
                edit.putString("DeliveryUserVNo", "");
                edit.putString("DeliveryUserVType", "");
                edit.putBoolean("isPresent", false);
                edit.apply();

                //singout Firebase chat
                logout(myApp);

                Intent i = new Intent(DeliveryStatus.this, Login.class);
                startActivity(i);
                finish();

            }
        });

        btn_my_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DeliveryStatus.this, DeliveryUserProfile.class);
                startActivity(i);
            }
        });

        btn_order_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DeliveryStatus.this, DeliveryOrderHistory.class);
                startActivity(i);
            }
        });

        ImageButton ib_back = findViewById(R.id.ib_back);
        ib_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        listView_delivery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(DeliveryStatus.this, DeliveryOrderDetail.class);
                i.putExtra("OrderNo", data.get(position).getOrderNo());
                i.putExtra("OrderAmount", data.get(position).getOrderAmount());
                i.putExtra("isComplete", data.get(position).getComplete());
                i.putExtra("OrderQuantity", data.get(position).getOrderQuantity());
                i.putExtra("OrderTimeDate", data.get(position).getOrderTimeDate());
                startActivity(i);
            }
        });

        ImageView btn_share = findViewById(R.id.btn_share);
        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                String name = prefs.getString("DeliveryUserName",null)+"\n\n";
                String desc = "";
                if (prefs.getString("DeliveryUserDesc",null)!=null)
                    desc = prefs.getString("DeliveryUserDesc",null)+"\n\n";
                String kitab = "";
                if (prefs.getString("DeliveryUserKitab",null)!=null)
                    kitab = "Kitab yang dikuasai: "+prefs.getString("DeliveryUserKitab",null)+"\n\n";
                String syariaid = "Download Syariaid: https://play.google.com/store/apps/details?id=santri.syaria";
                String sharee = name+desc+kitab+syariaid;
                Uri bmpUri = getLocalBitmapUri(img_user);
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.setType("image/*");
                share.setType("image/jpeg");
                share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                share.putExtra(Intent.EXTRA_SUBJECT, "Syariaid");
                share.putExtra("android.intent.extra.TEXT", sharee);
                share.putExtra(Intent.EXTRA_STREAM, bmpUri);
                startActivity(Intent.createChooser(share, "Share Syariaid App"));
            }
        });

        toggle_chat = findViewById(R.id.toggle_chat);
        toggle_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (toggle_chat.isChecked()) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(DeliveryStatus.this, R.style.MyDialogTheme);
                    builder1.setTitle("Chat Status");
                    builder1.setMessage(getString(R.string.msg_activate_chat));
                    builder1.setCancelable(false);
                    builder1.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            chatStatus("update","1");
                        }
                    });
                    builder1.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            toggle_chat.setChecked(false);
                        }
                    });
                    AlertDialog alert11 = builder1.create();
                    alert11.setCanceledOnTouchOutside(false);
                    alert11.show();
                } else {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(DeliveryStatus.this, R.style.MyDialogTheme);
                    builder1.setTitle("Chat Status");
                    builder1.setMessage(getString(R.string.msg_deactivate_chat));
                    builder1.setCancelable(false);
                    builder1.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            chatStatus("update","0");
                        }
                    });
                    builder1.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            toggle_chat.setChecked(true);
                        }
                    });
                    AlertDialog alert11 = builder1.create();
                    alert11.setCanceledOnTouchOutside(false);
                    alert11.show();
                }
            }
        });

        settingData();
        setChatButton();
        chatStatus("check","0");
    }

    private Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap numberOfRecords ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp;
        if (drawable instanceof BitmapDrawable) {
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                    "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = FileProvider.getUriForFile(DeliveryStatus.this, BuildConfig.APPLICATION_ID + ".provider", file);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    public static void showErrorDialog(Context c) {
        final NiftyDialogBuilder material;
        material = NiftyDialogBuilder.getInstance(c);
        material.withTitle(c.getString(R.string.text_warning))
                .withMessage(c.getString(R.string.internet_check_error))
                .withDialogColor(c.getString(R.string.colorErrorDialog))
                .withButton1Text("OK").setButton1Click(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                material.cancel();
            }
        })
                .withDuration(1000)
                .withEffect(Effectstype.Fadein)
                .show();
    }

    public static boolean checkInternet(Context context) {
        // TODO Auto-generated method stub
        ConnectionDetector cd = new ConnectionDetector(context);
        return cd.isConnectingToInternet();
    }

    public static void changeStatsBarColor(Activity activity) {
        Window window = activity.getWindow();

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        // finally change the color
        window.setStatusBarColor(ContextCompat.getColor(activity, R.color.hijau));
    }

    private void sendPresence(final String status) {
        //creating a string request to send request to the url
        String hp = getString(R.string.link) + getString(R.string.servicepath) + "deliveryboy_presence.php";
        Log.w(getClass().getName(), hp);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, hp,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        Log.e("Response123", response);
                        //    {"data":{"success":"1","presence":"false"}}

                        try {
                            JSONObject jo_main = new JSONObject(response);
                            JSONObject jo_data = jo_main.getJSONObject("data");
                            if (jo_data.getString("success").equals("1")) {
                                String isPresent = jo_data.getString("presence");
                                getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit().putBoolean("isPresent", returnBool(isPresent)).apply();
                            } else {
                                Toast.makeText(DeliveryStatus.this, jo_data.getString("presence"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurs

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.e("Status code", String.valueOf(networkResponse.statusCode));
                            Toast.makeText(getApplicationContext(), String.valueOf(networkResponse.statusCode), Toast.LENGTH_SHORT).show();
                        }
                    }
                }) {
            protected Map<String, String> getParams() {
                Map<String, String> MyData = new HashMap<>();
                MyData.put("status", status); //Add the data you'd like to send to the server.
                MyData.put("deliverboy_id",getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).getString("DeliveryUserId", ""));
                return MyData;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    private void chatStatus(String action, String status) {
        //creating a string request to send request to the url
        String hp = getString(R.string.link) + getString(R.string.servicepath) + "chat_status.php?res_id=" + getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).getString("DeliveryUserId", "")+"&action="+action+"&status="+status;
        Log.w(getClass().getName(), hp);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, hp,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        Log.e("Response123", response);

                        try {

                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);
                            if (obj.getString("success").equals("1")) {
                                JSONArray jsonArray = obj.getJSONArray("result");
                                JSONObject jsonResult = jsonArray.getJSONObject(0);
                                String status = jsonResult.getString("chat");

                                if (status.equals("1")) {
                                    toggle_chat.setChecked(true);
                                    //toggle_chat.setBackgroundColor(Color.parseColor("#e07b39"));

                                } else if (status.equalsIgnoreCase("0")){
                                    toggle_chat.setChecked(false);
                                }

                                SharedPreferences.Editor edit = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                edit.putString("DeliveryUserChat", status);
                                if (action.equalsIgnoreCase("check")) {
                                    edit.putString("DeliveryUserDesc", jsonResult.getString("desc"));
                                    edit.putString("DeliveryUserKitab", jsonResult.getString("kitab"));
                                }
                                edit.apply();
                            } else {
                                SharedPreferences prefsDeliveryBoyId = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                                if (prefsDeliveryBoyId.getString("DeliveryUserChat",null).equals("1")) {
                                    toggle_chat.setChecked(true);
                                } else {
                                    toggle_chat.setChecked(false);
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurs

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.e("Status code", String.valueOf(networkResponse.statusCode));
                            Toast.makeText(getApplicationContext(), String.valueOf(networkResponse.statusCode), Toast.LENGTH_SHORT).show();

                        }
                    }
                });

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    private void settingData() {

        //creating a string request to send request to the url
        String hp = getString(R.string.link) + getString(R.string.servicepath) + "deliveryboy_order.php?deliverboy_id=" + getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).getString("DeliveryUserId", "");
        Log.w(getClass().getName(), hp);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, hp,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        Log.e("Response123", response);

                        try {

                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);
                            if (obj.getString("success").equals("1")) {
                                JSONArray ja_order = obj.getJSONArray("order");
                                DeliveryGetSet getSet;
                                data = new ArrayList<>();

                                for (int i = 0; i < ja_order.length(); i++) {
                                    JSONObject jo_orderDetail = ja_order.getJSONObject(i);
                                    getSet = new DeliveryGetSet();
                                    getSet.setOrderNo(jo_orderDetail.getString("order_no"));
                                    getSet.setOrderTimeDate(jo_orderDetail.getString("date"));
                                    getSet.setOrderQuantity(jo_orderDetail.getString("items"));
                                    getSet.setOrderAmount(jo_orderDetail.getString("total_amount"));
                                    getSet.setComplete(jo_orderDetail.getString("status"));
                                    getSet.setUser_name(jo_orderDetail.getString("user_name"));
                                    Log.d("Chehdfsf",""+jo_orderDetail.getString("status"));

                                    data.add(getSet);
                                }

                                DeliveryAdapter adapter = new DeliveryAdapter(data, DeliveryStatus.this);
                                listView_delivery.setAdapter(adapter);

                            } else {
                                //Toast.makeText(DeliveryStatus.this, obj.getString("order"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurs

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.e("Status code", String.valueOf(networkResponse.statusCode));
                            Toast.makeText(getApplicationContext(), String.valueOf(networkResponse.statusCode), Toast.LENGTH_SHORT).show();

                        }
                    }
                });

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        showExitDialog();

    }

    @Override
    protected void onResume() {
        super.onResume();
        initViews();
    }

    private void showExitDialog() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(DeliveryStatus.this, R.style.MyDialogTheme);
        builder1.setTitle(getString(R.string.Quit));
        builder1.setMessage(getString(R.string.statementquit));
        builder1.setCancelable(true);
        builder1.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                finishAffinity();
            }
        });
        builder1.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private boolean returnBool(String status) {
        return Objects.equals(status, "yes");
    }

    public class RegisterMobile extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            URL hp;
            try {

                http://192.168.1.116/freak/FoodDeliverySystem/api/token.php?token=1253aaa&type=ios&user_id=3
                hp = new URL(getString(R.string.link) + getString(R.string.servicepath) + "token.php?token="+regId+"&type=android&user_id=null&delivery_boyid="+DeliveryBoyId);
                Log.d("URL4564", "" + hp);

                // URL Connection

                URLConnection hpCon = hp.openConnection();
                hpCon.connect();
                InputStream input = hpCon.getInputStream();
                Log.d("input", "" + input);
                BufferedReader r = new BufferedReader(new InputStreamReader(input));
                String x;
                x = r.readLine();
                StringBuilder total = new StringBuilder();
                while (x != null) {
                    total.append(x);
                    x = r.readLine();
                }
                Log.d("URL", "" + total);

                // Json Parsing

                JSONObject jObject = new JSONObject(total.toString());
                Log.d("URL12", "" + jObject);



            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (NullPointerException e) {
                // TODO: handle exception
                e.printStackTrace();

            } catch (MalformedURLException e) {
                e.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);



        }
    }

}

