package santri.syaria;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MIME;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import static santri.syaria.MainActivity.changeStatsBarColor;
import static santri.syaria.MainActivity.tf_opensense_regular;

public class Profile extends AppCompatActivity {
    private static final String MY_PREFS_NAME = "Fooddelivery";
    private ProgressBar progress_bar;
    private String uservalue, uname, telepon;
    private String response;
    private String picturepath = "", profileimage;
    private ImageView img_user, img;
    private EditText input_name;

    private static final int REQUEST_EXTERNAL_STORAGE = 2;
    private static final String[] PERMISSIONS_STORAGE = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA

    };
    private static final int RESULT_LOAD_IMAGE = 1;
    private static final int RESULT_cam_IMAGE = 2;

    private boolean updateImage = false;
    private boolean success = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_profile);

        changeStatsBarColor(Profile.this);


        initView();


    }

    private void initView() {
        input_name = findViewById(R.id.input_name);
        EditText input_email = findViewById(R.id.input_email);
        img_user = findViewById(R.id.img_user);
        img = findViewById(R.id.img12);
        ((TextView) findViewById(R.id.txt_title)).setTypeface(tf_opensense_regular);
        progress_bar = findViewById(R.id.progress_bar);

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        uservalue = prefs.getString("userid", null);
        String image = prefs.getString("imagepath", null);
        profileimage = prefs.getString("imageprofile", null);
        Log.d("profileimg", "" + getString(R.string.link) + getString(R.string.imagepath) + profileimage);

        String mail_id = prefs.getString("usermailid", null);
        uname = prefs.getString("username", null);
        telepon = prefs.getString("usermobileno", null);
        Log.d("profileimg", "" + uname);
        input_name.setText(uname);
        input_email.setText(mail_id);
        try {
            Picasso.with(getApplicationContext())
                    .load(profileimage)
                    .into(img_user);
            Picasso.with(getApplicationContext())
                    .load(profileimage)
                    .into(img);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();

        }
        ImageButton ib_back = findViewById(R.id.ib_back);
        ib_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        img_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int permission = ActivityCompat.checkSelfPermission(Profile.this, Manifest.permission.CAMERA);

                if (permission != PackageManager.PERMISSION_GRANTED) {
                    // We don't have permission so prompt the user
                    verifyStoragePermissions(Profile.this);

                } else {
                    Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                    startActivityForResult(intent, RESULT_cam_IMAGE);
                }
            }
        });

        Button btn_simpan = findViewById(R.id.btn_simpan);
        btn_simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uname = input_name.getText().toString();
                new postingData().execute();
            }
        });
    }

    private void setImage(String imageUrl) {
        try {
            Picasso.with(getApplicationContext())
                    .load(imageUrl)
                    .into(img_user);
            Picasso.with(getApplicationContext())
                    .load(imageUrl)
                    .into(img);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {


            Uri selectedImage = data.getData();
            Bitmap photo;
            if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK) {
                String[] filePathColumn = {MediaStore.MediaColumns.DATA};

                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturepath = cursor.getString(columnIndex);

                Log.d("picturepath", "" + picturepath);
                cursor.close();
                try {
                    photo = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                    img_user.setImageBitmap(decodeFile(picturepath));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                updateImage = true;
            } else if (requestCode == RESULT_cam_IMAGE && resultCode == RESULT_OK && null != data) {
                Log.d("photo", "" + data.getExtras().get("data"));
                photo = (Bitmap) data.getExtras().get("data");
                img_user.setImageBitmap(photo);
                img_user.setImageBitmap(photo);
                Uri tempUri = getImageUri(getApplicationContext(), photo);
                // CALL THIS METHOD TO GET THE ACTUAL PATH
                File finalFile = new File(getRealPathFromURI(tempUri));
                picturepath = String.valueOf(finalFile);

                updateImage = true;
            }
            new postingData().execute();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "IMG_" + System.currentTimeMillis(), null);
        return Uri.parse(path);
    }


    private String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        String temp = cursor.getString(idx);
        cursor.close();
        return temp;
    }

    private Bitmap decodeFile(String path) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, o);
            // The new size we want to scale to
            final int REQUIRED_SIZE = 70;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeFile(path, o2);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;

    }

    class postingData extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            success = false;
            progress_bar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub
            postdata();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d("post execute, response",response);

            progress_bar.setVisibility(View.GONE);
            if (success) {
                Toast.makeText(Profile.this, "Data berhasil disimpan", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(Profile.this, "Update profil gagal", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void postdata() {
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("https",
                SSLSocketFactory.getSocketFactory(), 443));

        HttpParams params = new BasicHttpParams();

        SingleClientConnManager mgr = new SingleClientConnManager(params, schemeRegistry);
        // TODO Auto-generated method stub
        //HttpClient httpClient = new DefaultHttpClient();
        HttpClient httpClient = new DefaultHttpClient(mgr,params);
        String boundary = "-------------" + System.currentTimeMillis();

        HttpEntity entity = null;

        if (updateImage) {
            entity = MultipartEntityBuilder.create().setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
                    .addTextBody("id", uservalue, ContentType.create("text/plain", MIME.UTF8_CHARSET))
                    .addTextBody("nama", uname, ContentType.create("text/plain", MIME.UTF8_CHARSET))
                    .addTextBody("telepon", telepon)
                    .addBinaryBody("file", new File(picturepath), ContentType.create("application/octet-stream"), "filename").build();
        } else {
            entity = MultipartEntityBuilder.create().setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
                    .addTextBody("id", uservalue, ContentType.create("text/plain", MIME.UTF8_CHARSET))
                    .addTextBody("nama", uname, ContentType.create("text/plain", MIME.UTF8_CHARSET))
                    .addTextBody("telepon", telepon).build();
        }


        HttpPost httpPost = new HttpPost("https://syaria.id/api/updateprofileme");
        //httpPost.setHeader("Content-type", "multipart/form-data; boundary=" + boundary);
        httpPost.setEntity(entity);
        HttpResponse response1 = null;
        try {
            response1 = httpClient.execute(httpPost);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        HttpEntity result = response1.getEntity();
        Log.e("http result",result.toString());
        if (result != null) {
            Log.e("result","not null");
            try {
                Log.e("try response","try");
                //String responseStr = EntityUtils.toString(result).trim();
                response = EntityUtils.toString(result).trim();
                Log.e("Response", response);
                //response = responseStr;

                JSONObject json = new JSONObject(response);
                if (json.getString("errcode")!=null && json.getString("errcode").equals("000")) {

                    profileimage = json.getString("avatar");
                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME,
                            MODE_PRIVATE).edit();
                    editor.putString("username",uname);
                    if (updateImage) {
                        editor.putString("imageprofile", profileimage);
                    }
                    editor.apply();
                    success = true;
                } else {
                    setImage(profileimage);
                    success = false;
                }


            } catch (org.apache.http.ParseException | IOException | JSONException e) {
                // TODO Auto-generated catch block
                Log.e("error apache",e.getMessage());
                e.printStackTrace();
                success = false;
            }
        }
    }

    private static void verifyStoragePermissions(final Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            new AlertDialog.Builder(activity)
                    .setTitle(R.string.msg)
                    .setMessage(R.string.mssg)
                    .setPositiveButton(activity.getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ActivityCompat.requestPermissions(activity,
                                    PERMISSIONS_STORAGE,
                                    REQUEST_EXTERNAL_STORAGE);
                        }
                    })
                    .create()
                    .show();


        } else {
            ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
        }
    }

}
