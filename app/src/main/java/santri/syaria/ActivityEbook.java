package santri.syaria;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.net.URLEncoder;

public class ActivityEbook extends AppCompatActivity {

    private WebView web;
    private String ebook_url;

    @Override
    @SuppressLint("SetJavaScriptEnabled")
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ebook);

        web = findViewById(R.id.web);

        ebook_url = getIntent().getStringExtra("txt_content");

        String url = "";

        try {
            url = "https://docs.google.com/viewer?embedded=true&url=" + URLEncoder.encode(ebook_url, "utf-8");
            //url = item.source.split("=")[1];
        } catch (Exception e) {
            Log.e("Error",e.getMessage().toString());
        }

        setupWeb(url);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setupWeb(String url) {
        ViewCompat.setOnApplyWindowInsetsListener(web, (v, insets) -> {
            web.setPadding(0, insets.getSystemWindowInsetTop(), 0, 0);
            return insets;
        });
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading Data...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        web.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(web, url);

                if (view.getTitle().equals("")) {
                    view.reload();
                } else {
                    progressDialog.dismiss();
                    //view.loadUrl("javascript:(function() { " +
                    //        "document.getElementsByClassName('ndfHFb-c4YZDc-GSQQnc-LgbsSe ndfHFb-c4YZDc-to915-LgbsSe VIpgJd-TzA9Ye-eEGnhe ndfHFb-c4YZDc-LgbsSe')[0].style.display='none'; })()");
                    view .loadUrl("javascript:(function() {document.querySelector('[class=\"ndfHFb-c4YZDc-Wrql6b\"]').remove();})()");
                }
            }
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        web.getSettings().setJavaScriptEnabled(true);
        web.getSettings().setSupportZoom(true);
        web.getSettings().setBuiltInZoomControls(true);
        web.getSettings().setDisplayZoomControls(false);
        web.getSettings().setAllowFileAccessFromFileURLs(true);
        web.getSettings().setAllowUniversalAccessFromFileURLs(true);
        web.loadUrl(url);
    }
}