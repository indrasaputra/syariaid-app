package santri.syaria;

import androidx.annotation.DrawableRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import santri.video.connection.API;
import santri.video.connection.RestAdapter;
import santri.video.model.Video;
import santri.video.response.ResponseVideoDetails;
import santri.video.utils.Tools;
import santri.video.utils.ViewAnimation;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

public class ActivityVideoPlayer extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener, View.OnClickListener {
    private static final String EXTRA_OBJECT_ID = "key.EXTRA_OBJECT_ID";
    private static final String EXTRA_OBJECT_URL = "key.EXTRA_OBJECT_URL";
    private static final String EXTRA_FROM_NOTIF = "key.EXTRA_FROM_NOTIF";

    // activity transition
    public static void navigate(Activity activity, Long id, String url, Boolean from_notif) {
        Intent i = navigateBase(activity, id, url, from_notif);
        activity.startActivity(i);
    }

    public static Intent navigateBase(Context context, Long id, String url, Boolean from_notif) {
        Intent i = new Intent(context, ActivityVideoPlayer.class);
        i.putExtra(EXTRA_OBJECT_ID, id);
        i.putExtra(EXTRA_OBJECT_URL, url );
        i.putExtra(EXTRA_FROM_NOTIF, from_notif);
        return i;
    }

    private static final String TAG = ActivityVideoPlayer.class.getSimpleName();

    public static final String API_KEY = "AIzaSyBx7v0YOb140fDO7EbfMx4l87raxezDWFw";

    //https://www.youtube.com/watch?v=<VIDEO_ID>
    //public static final String VIDEO_ID = "-m3V8w_7vhk";
    private String VIDEO_ID = "";

    private YouTubePlayer mPlayer;

    private View mPlayButtonLayout;
    private TextView mPlayTimeTextView;

    private Handler mHandler = null;
    private SeekBar mSeekBar;

    private View lyt_webview;
    private ShimmerFrameLayout shimmer_layout;
    private View lyt_main_content;
    private WebView webview;

    private Video video;
    private Call<ResponseVideoDetails> callbackCall = null;
    private Long video_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // attaching layout xml
        setContentView(R.layout.activity_video_player);

        video_id = getIntent().getLongExtra(EXTRA_OBJECT_ID, -1L);
        VIDEO_ID = getIntent().getStringExtra(EXTRA_OBJECT_URL);

        // Initializing YouTube player view
        YouTubePlayerView youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player_view);
        youTubePlayerView.initialize(API_KEY, this);

        //Add play button to explicitly play video in YouTubePlayerView
        mPlayButtonLayout = findViewById(R.id.video_control);
        findViewById(R.id.play_video).setOnClickListener(this);
        findViewById(R.id.pause_video).setOnClickListener(this);

        mPlayTimeTextView = (TextView) findViewById(R.id.play_time);
        mSeekBar = (SeekBar) findViewById(R.id.video_seekbar);
        mSeekBar.setOnSeekBarChangeListener(mVideoSeekBarChangeListener);

        lyt_webview = findViewById(R.id.lyt_webview);
        shimmer_layout = findViewById(R.id.shimmer_layout);
        lyt_main_content = (View) findViewById(R.id.lyt_main_content);

        mHandler = new Handler();

        requestVideoDetailsApi();
    }

    private void requestVideoDetailsApi() {
        shimmer_layout.setVisibility(View.VISIBLE);
        shimmer_layout.startShimmer();
        lyt_main_content.setVisibility(View.INVISIBLE);
        API api = RestAdapter.createAPI();
        callbackCall = api.getVideoDetails(video_id);
        callbackCall.enqueue(new Callback<ResponseVideoDetails>() {
            @Override
            public void onResponse(Call<ResponseVideoDetails> call, Response<ResponseVideoDetails> response) {
                ResponseVideoDetails resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    video = resp.video;
                    displayVideoData();
                } else {
                    //onFailRequest();
                    showFailedView(true, getString(R.string.failed_text), R.drawable.img_failed);
                }
            }

            @Override
            public void onFailure(Call<ResponseVideoDetails> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled())
                    showFailedView(true, getString(R.string.failed_text), R.drawable.img_failed);
                    //onFailRequest();
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void displayVideoData() {
        shimmer_layout.setVisibility(View.INVISIBLE);
        shimmer_layout.stopShimmer();
        lyt_main_content.setVisibility(View.VISIBLE);

        //VIDEO_ID = video.url;

        ((TextView) findViewById(R.id.title)).setText(video.name);
        ((TextView) findViewById(R.id.duration)).setText(video.duration);

        webview = (WebView) findViewById(R.id.detail_desc);
        String html_data = "<style>img{max-width:100%;height:auto;} iframe{width:100%;}</style> ";
        html_data += video.description;
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings();
        webview.getSettings().setBuiltInZoomControls(true);
        webview.setBackgroundColor(Color.TRANSPARENT);
        webview.setWebChromeClient(new WebChromeClient());
        webview.loadData(html_data, "text/html; charset=UTF-8", null);
        // disable scroll on touch
        webview.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });
        // override url direct
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Tools.directLinkToBrowser(ActivityVideoPlayer.this, url);
                return true;
            }
        });

        ((TextView) findViewById(R.id.date)).setText(Tools.getFormattedDateFull(video.last_update));
        if (video.featured == 0) {
            ((TextView) findViewById(R.id.featured)).setVisibility(View.GONE);
        }

        lyt_main_content.setVisibility(View.VISIBLE);
        lyt_webview.setVisibility(View.VISIBLE);

        ImageView btn_toggle_desc = findViewById(R.id.btn_toggle_desc);
        btn_toggle_desc.setRotation(180);
        btn_toggle_desc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleSectionText(v);
            }
        });
    }

    private void toggleSectionText(View view) {
        boolean show = toggleArrow(view);
        if (show) {
            ViewAnimation.collapse(lyt_webview);
        } else {
            ViewAnimation.expand(lyt_webview);
        }
    }

    public boolean toggleArrow(View view) {
        if (view.getRotation() == 0) {
            view.animate().setDuration(200).rotation(180);
            return true;
        } else {
            view.animate().setDuration(200).rotation(0);
            return false;
        }
    }

    private void showFailedView(boolean show, String message, @DrawableRes int icon) {
        View lyt_failed = (View) findViewById(R.id.lyt_failed);

        ((ImageView) findViewById(R.id.failed_icon)).setImageResource(icon);
        ((TextView) findViewById(R.id.failed_message)).setText(message);
        if (show) {
            lyt_main_content.setVisibility(View.INVISIBLE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            lyt_main_content.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayVideoData();
            }
        });
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult result) {
        Toast.makeText(this, "Failed to initialize.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
        if (null == player) return;
        mPlayer = player;

        displayCurrentTime();

        // Start buffering
        if (!wasRestored) {
            player.cueVideo(VIDEO_ID);
        }

        player.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
        mPlayButtonLayout.setVisibility(View.VISIBLE);

        // Add listeners to YouTubePlayer instance
        player.setPlayerStateChangeListener(mPlayerStateChangeListener);
        player.setPlaybackEventListener(mPlaybackEventListener);

        player.setOnFullscreenListener(new YouTubePlayer.OnFullscreenListener() {
            @Override
            public void onFullscreen(boolean b) {
                mPlayer.loadVideo(VIDEO_ID);
            }
        });
    }

    YouTubePlayer.OnFullscreenListener onFullscreenListener = new YouTubePlayer.OnFullscreenListener() {
        @Override
        public void onFullscreen(boolean b) {

        }
    };

    YouTubePlayer.PlaybackEventListener mPlaybackEventListener = new YouTubePlayer.PlaybackEventListener() {
        @Override
        public void onBuffering(boolean arg0) {
        }

        @Override
        public void onPaused() {
            mHandler.removeCallbacks(runnable);
        }

        @Override
        public void onPlaying() {
            mHandler.postDelayed(runnable, 100);
            displayCurrentTime();
        }

        @Override
        public void onSeekTo(int arg0) {
            mHandler.postDelayed(runnable, 100);
        }

        @Override
        public void onStopped() {
            mHandler.removeCallbacks(runnable);
        }
    };

    YouTubePlayer.PlayerStateChangeListener mPlayerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {
        @Override
        public void onAdStarted() {
        }

        @Override
        public void onError(YouTubePlayer.ErrorReason arg0) {
        }

        @Override
        public void onLoaded(String arg0) {
        }

        @Override
        public void onLoading() {
        }

        @Override
        public void onVideoEnded() {
        }

        @Override
        public void onVideoStarted() {
            displayCurrentTime();
        }
    };

    SeekBar.OnSeekBarChangeListener mVideoSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            long lengthPlayed = (mPlayer.getDurationMillis() * progress) / 100;
            mPlayer.seekToMillis((int) lengthPlayed);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.play_video:
                if (null != mPlayer && !mPlayer.isPlaying())
                    mPlayer.play();
                break;
            case R.id.pause_video:
                if (null != mPlayer && mPlayer.isPlaying())
                    mPlayer.pause();
                break;
        }
    }

    private void displayCurrentTime() {
        if (null == mPlayer) return;
        if (mPlayer!=null) {
            String formattedTime = formatTime(mPlayer.getDurationMillis() - mPlayer.getCurrentTimeMillis());
            mPlayTimeTextView.setText(formattedTime);
        }

    }

    private String formatTime(int millis) {
        int seconds = millis / 1000;
        int minutes = seconds / 60;
        int hours = minutes / 60;

        return (hours == 0 ? "--:" : hours + ":") + String.format("%02d:%02d", minutes % 60, seconds % 60);
    }


    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            displayCurrentTime();
            mHandler.postDelayed(this, 100);
        }
    };

}