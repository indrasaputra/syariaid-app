package santri.syaria;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Objects;

import androidx.fragment.app.Fragment;
import santri.Getset.myOrderGetSet;
import santri.utils.sqliteHelper;

import static santri.syaria.MainActivity.checkInternet;
import static santri.syaria.MainActivity.showErrorDialog;
import static santri.syaria.MainActivity.tf_opensense_medium;
import static santri.syaria.MainActivity.tf_opensense_regular;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyOrderFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyOrderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyOrderFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private static final String MY_PREFS_NAME = "Fooddelivery";

    private ListView list_notification;
    private ArrayList<myOrderGetSet> data;
    private notificationAdapter adapter;
    santri.utils.sqliteHelper sqliteHelper;
    private InterstitialAd mInterstitialAd;
    private String key = "";
    private View rootView;
    private String Error;

    public MyOrderFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyOrderFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyOrderFragment newInstance(String param1, String param2) {
        MyOrderFragment fragment = new MyOrderFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_my_order, container, false);

        initialization();

        SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, getActivity().MODE_PRIVATE);
        if (prefs.getString("userid", null) != null) {
            String userId = prefs.getString("userid", null);
            if (Objects.equals(userId, "delete")) {
                rootView.findViewById(R.id.txt_no_orders).setVisibility(View.VISIBLE);
            } else {
                if (checkInternet(getActivity())) {
                    new GetDataAsyncTask().execute();
                } else showErrorDialog(getActivity());
            }
        } else {
            rootView.findViewById(R.id.txt_no_orders).setVisibility(View.VISIBLE);
        }

        return rootView;
    }

    class GetDataAsyncTask extends AsyncTask<Void, Void, Void> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            //gettingDataFromDatabase();
            gettingData();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d("post","postExecute");

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (data != null) {
                adapter = new notificationAdapter(data);
                list_notification.setAdapter(adapter);
            } else {
                Toast.makeText(getActivity(), R.string.noorder, Toast.LENGTH_SHORT).show();
                rootView.findViewById(R.id.txt_no_orders).setVisibility(View.VISIBLE);
            }
        }
    }

    private void gettingData() {

        //creating a string request to send request to the url
        String uri = getString(R.string.link) + getString(R.string.servicepath) + "order_history_user.php?user_id=" + getActivity().getSharedPreferences(MY_PREFS_NAME, getActivity().MODE_PRIVATE).getString("userid", "");

        try {
            URL hp = new URL(uri.replace(" ", "%20"));
            Log.e("URLs", "" + hp);
            URLConnection hpCon = hp.openConnection();
            hpCon.connect();
            InputStream input = hpCon.getInputStream();
            Log.d("input", "" + input);
            BufferedReader r = new BufferedReader(new InputStreamReader(input));
            String x;
            x = r.readLine();
            StringBuilder total = new StringBuilder();
            while (x != null) {
                total.append(x);
                x = r.readLine();
            }
            Log.d("response",total.toString());
            //JSONArray jObject = new JSONArray(total.toString());
            JSONObject obj = new JSONObject(total.toString()); //jObject.getJSONObject(0);
            if (obj.getString("success").equals("1")) {
                JSONArray ja_order = obj.getJSONArray("order");
                myOrderGetSet getSet;
                data = new ArrayList<>();

                for (int i = 0; i < ja_order.length(); i++) {
                    JSONObject jo_orderDetail = ja_order.getJSONObject(i);

                    getSet = new myOrderGetSet();
                    getSet.setOrder_id(jo_orderDetail.getString("order_id"));
                    getSet.setOrder_dateTime(jo_orderDetail.getString("date"));
                    getSet.setOrder_total(jo_orderDetail.getString("total_amount"));
                    getSet.setResName(jo_orderDetail.getString("name"));
                    getSet.setResAddress(jo_orderDetail.getString("city"));
                    getSet.setImage(jo_orderDetail.getString("image"));
                    getSet.setStatus(jo_orderDetail.getString("status"));
                    data.add(getSet);
                    Log.d("data","data added");
                }

            } else {
                //Toast.makeText(getActivity(), obj.getString("order"), Toast.LENGTH_SHORT).show();
                //rootView.findViewById(R.id.txt_no_orders).setVisibility(View.VISIBLE);
                data = null;
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Error = e.getMessage();
        } catch (NullPointerException e) {
            // TODO: handle exception
            Error = e.getMessage();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void gettingDataFromDatabase() {
        sqliteHelper = new sqliteHelper(getActivity());
        SQLiteDatabase db1 = sqliteHelper.getWritableDatabase();
//        DBAdapter myDbHelper = new DBAdapter(MyOrderPage.this);
//        myDbHelper = new DBAdapter(MyOrderPage.this);
//        try {
//            myDbHelper.createDataBase();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        try {
//            myDbHelper.openDataBase();
//        } catch (SQLException sqle) {
//            sqle.printStackTrace();
//        }
//        SQLiteDatabase db = myDbHelper.getReadableDatabase();
        Cursor cur;
        try {
            cur = db1.rawQuery("SELECT * FROM order_detail;", null);
            data = new ArrayList<>();
            Log.e("SIZWA", "" + cur.getCount());
            if (cur.getCount() != 0) {
                if (cur.moveToFirst()) {
                    do {
                        myOrderGetSet obj = new myOrderGetSet();
                        String txt_restaurantName = cur.getString(cur.getColumnIndex("restaurantName"));
                        String txt_restaurantAddress = cur.getString(cur.getColumnIndex("restaurantAddress"));
                        String txt_orderId = cur.getString(cur.getColumnIndex("orderId"));
                        String txt_orderAmount = cur.getString(cur.getColumnIndex("orderAmount"));
                        String txt_orderTime = cur.getString(cur.getColumnIndex("orderTime"));

                        obj.setResName(txt_restaurantName);
                        obj.setResAddress(txt_restaurantAddress);
                        obj.setOrder_id(txt_orderId);
                        obj.setOrder_dateTime(txt_orderTime);
                        obj.setOrder_total(txt_orderAmount);


                        data.add(obj);
                    } while (cur.moveToNext());
                }
            }
            cur.close();
            db1.close();
//            myDbHelper.close();
        } catch (Exception e) {
            Log.e("Error", e.getMessage());

        }

    }

    private void initialization() {
        rootView.findViewById(R.id.txt_no_orders).setVisibility(View.GONE);
        list_notification = rootView.findViewById(R.id.list_notification);

        View btn_undang_ustad = rootView.findViewById(R.id.btn_undang_ustadz);
        btn_undang_ustad.setBackgroundColor(getResources().getColor(R.color.abu));

        View btn_tanya_ustad = rootView.findViewById(R.id.btn_tanya_ustadz);
        btn_tanya_ustad.setBackgroundColor(getResources().getColor(R.color.white));
        btn_tanya_ustad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OrderFragment fragment = new OrderFragment();
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, fragment)
                        .commit();
            }
        });

        View btn_pranikah = rootView.findViewById(R.id.btn_pranikah);
        btn_pranikah.setBackgroundColor(getResources().getColor(R.color.white));
        btn_pranikah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OrderPranikahFragment fragment = new OrderPranikahFragment();
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, fragment)
                        .commit();
            }
        });

        list_notification.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent i = new Intent(getActivity(), CompleteOrder.class);
                i.putExtra("orderid", adapter.dat.get(position).getOrder_id());
                i.putExtra("key", "notification");
                startActivity(i);
                if (getResources().getString(R.string.show_admob_ads).equals("yes")) {
//                    if (mInterstitialAd.isLoaded()) {
//                        mInterstitialAd.show();
//
//                    }
                }
            }
        });

    }


    private class notificationAdapter extends BaseAdapter {
        final ArrayList<myOrderGetSet> dat;
        private LayoutInflater inflater = null;

        notificationAdapter(ArrayList<myOrderGetSet> dat) {
            this.dat = dat;
            inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return dat.size();
        }

        @Override
        public Object getItem(int position) {
            return dat.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            if (vi == null)
                vi = inflater.inflate(R.layout.cell_order, parent, false);

            ImageView imageview = vi.findViewById(R.id.image);
            TextView txt_name = vi.findViewById(R.id.txt_name);
            txt_name.setText(dat.get(position).getResName());
            txt_name.setTypeface(tf_opensense_medium);
            TextView txt_address = vi.findViewById(R.id.txt_address);
            txt_address.setText(dat.get(position).getResAddress());
            txt_address.setTypeface(tf_opensense_regular);

            TextView txt_res_name = vi.findViewById(R.id.txt_res_name);
            txt_res_name.setText(dat.get(position).getResName().substring(0, 1));

            TextView txt_order_date = vi.findViewById(R.id.txt_order_date);
            txt_order_date.setText("Order Date: " + dat.get(position).getOrder_dateTime());

            TextView txt_total_tittle = vi.findViewById(R.id.txt_total_tittle);
            TextView txt_total = vi.findViewById(R.id.txt_total);
            txt_total.setText(getString(R.string.currency) + " " + dat.get(position).getOrder_total());

            TextView txt_status = vi.findViewById(R.id.txt_status);
            txt_status.setText(dat.get(position).getStatus());

            txt_res_name.setTypeface(tf_opensense_regular);
            txt_order_date.setTypeface(tf_opensense_regular);
            txt_total_tittle.setTypeface(tf_opensense_regular);
            txt_total.setTypeface(tf_opensense_regular);
            txt_status.setTypeface(tf_opensense_regular);

            AlphaAnimation anim = new AlphaAnimation(0, 1);
            anim.setInterpolator(new LinearInterpolator());
            anim.setRepeatCount(Animation.ABSOLUTE);
            anim.setDuration(1000);
            Picasso.with(getActivity()).load(getActivity().getResources().getString(R.string.link) + getActivity().getString(R.string.imagepath) + dat.get(position).getImage()).into(imageview);
            imageview.startAnimation(anim);

            return vi;
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
