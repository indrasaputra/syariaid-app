package santri.syaria;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Environment;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Objects;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import santri.Adapter.MainMenuAdapter;
import santri.Getset.CitylistGetSet;
import santri.Getset.MainMenuDataSource;
import santri.Getset.restaurentGetSet;
import santri.firebasechat.managers.Utils;
import santri.utils.ConnectionDetector;

public class MainMenuActivity extends AppCompatActivity {
    private String DB_PATH = Environment.getDataDirectory() + "/Bhagirath/databases/";
    private static final String DB_NAME = "restaurant.sqlite";
    private static final String MY_PREFS_NAME = "Fooddelivery";
    private static ArrayList<restaurentGetSet> restaurentlist;
    private String Error;
    private RecyclerView recyclerView;
    private String timezoneID;
    private String search = "";
    private String res_name;
    private ProgressDialog progressDialog;
    private ProgressDialog pd;
    private double latitudecur = 0;
    private double longitudecur = 0;
    private TextView txt_nameuser;
    private TextView txt_profile;
    private RelativeLayout rel_main, rl_profile;
    private String Location;
    private ImageView img_profile;
    private String CategoryTotal = "", regId;
    //private SwipyRefreshLayout mSwipeRefreshLayout;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    //private restaurentadapter adapter;
    private static int pageCount;
    public static int numberOfRecord;
    private String subCategoryName;
    public static Typeface tf_opensense_regular;
    public static Typeface tf_opensense_medium;
    private SharedPreferences prefs;
    private int radius;
    private final int PERMISSION_REQUEST_CODE = 1001;
    private final int PERMISSION_REQUEST_CODE1 = 10011;
    private final String[] permission_location = {android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    private final String[] permission_location1 = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    private ArrayList<CitylistGetSet> getSet;
    AdRequest adRequest;
    String userId1, DeliveryBoyId;
    AdView mAdView;
    private InterstitialAd mInterstitialAd;

    private ArrayList<MainMenuDataSource> mainMenuList;
    private MainMenuAdapter adapter;
    private boolean hasLoadedOnce = false;

    public static boolean checkInternet(Context context) {
        // TODO Auto-generated method stub
        ConnectionDetector cd = new ConnectionDetector(context);
        return cd.isConnectingToInternet();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        setContentView(R.layout.activity_main_menu);

        FirebaseApp.initializeApp(this);
        //FirebaseInstanceId.getInstance().getInstanceId();
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();
                Log.d("Firebase Token",newToken);
                Utils.uploadToken(newToken);
            }
        });

        //generate key hash for facebook
        /*try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("MainActivity:", "hhy== " + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }*/

        font();

        rel_main = findViewById(R.id.rel_main);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            setupDrawer();
            drawer();
            /*SpannableString s;
            if (prefs.getString("CityName", null) == null) {
                s = new SpannableString(getString(R.string.txt_home_header));
                s.setSpan(new TypefaceSpan("OpenSans-Regular.ttf"), 0, s.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            } else {
                s = new SpannableString(prefs.getString("CityName", null));
                s.setSpan(new TypefaceSpan("OpenSans-Regular.ttf"), 0, s.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }*/
            actionBar.setTitle("Carii");

        }

        mainMenuList = new ArrayList<MainMenuDataSource>();
        adapter = new MainMenuAdapter(this, mainMenuList);

        GridLayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        //this access method getItemViewType on adapter, setting number of grid per row
        mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (adapter.getItemViewType(position) == 0) {  //|| adapter.getItemViewType(position) == 2) {
                    return 2; // 4 bagian diambil penuh
                } else {
                    return 1; // pakai 1 bagian dari 4
                }
            }
        });

        //Util util = new Util(this);

        recyclerView = findViewById(R.id.recyler_view);
        recyclerView.setLayoutManager(mLayoutManager);
        //recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, util.dpToPx(0), true));
        //recyclerView.addItemDecoration(new MyDividerItemDecoration(myApp.getContext(), LinearLayoutManager.VERTICAL, 16));
        //recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        if (!hasLoadedOnce) {
            hasLoadedOnce = true;
            prepareHomeMenu();
        }

    }

    private void prepareHomeMenu() {
        int[] covers = new int[]{
                R.drawable.ustad,               //0
                R.drawable.guru,               //1
        };

        MainMenuDataSource a = null;

        //slide
        a = new MainMenuDataSource("notif_activity");
        mainMenuList.add(a);

        //body
        a = new MainMenuDataSource(1, "USTAD", covers[0], R.color.putih, R.color.hijau, "body");
        mainMenuList.add(a);
        a = new MainMenuDataSource(2, "GURU", covers[1], R.color.putih, R.color.hijau, "body");
        mainMenuList.add(a);

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void font() {

        Typeface tf_worksans = Typeface.createFromAsset(MainMenuActivity.this.getAssets(), "fonts/WorkSans-Regular.otf");
        tf_opensense_regular = Typeface.createFromAsset(MainMenuActivity.this.getAssets(), "fonts/OpenSans-Regular.ttf");
        tf_opensense_medium = Typeface.createFromAsset(MainMenuActivity.this.getAssets(), "fonts/OpenSans-Semibold.ttf");
        TextView txt_search = findViewById(R.id.txt_search);
        txt_search.setTypeface(tf_worksans);
        TextView txt_rated = findViewById(R.id.txt_rated);
        txt_rated.setTypeface(tf_worksans);
        TextView txt_suggested = findViewById(R.id.txt_suggested);
        txt_suggested.setTypeface(tf_worksans);
        TextView txt_cusine = findViewById(R.id.txt_cusine);
        txt_cusine.setTypeface(tf_worksans);
        TextView txt_fav = findViewById(R.id.txt_fav);
        txt_fav.setTypeface(tf_worksans);
        TextView txt_share = findViewById(R.id.txt_share);
        txt_share.setTypeface(tf_worksans);
        TextView txt_terms = findViewById(R.id.txt_terms);
        txt_terms.setTypeface(tf_worksans);
        TextView txt_aboutus = findViewById(R.id.txt_aboutus);
        txt_aboutus.setTypeface(tf_worksans);
        TextView txt_logout = findViewById(R.id.txt_logout);
        txt_logout.setTypeface(tf_worksans);
        txt_nameuser = findViewById(R.id.txt_nameuser);
        txt_nameuser.setTypeface(tf_opensense_regular);
        txt_profile = findViewById(R.id.txt_profile);
        txt_profile.setTypeface(tf_worksans);
        img_profile = findViewById(R.id.img_profile);

        rl_profile = findViewById(R.id.rl_profile);
    }

    private void drawer() {
        LinearLayout ll_fav = findViewById(R.id.ll_fav);
        LinearLayout ll_share = findViewById(R.id.ll_share);
        final LinearLayout ll_aboutus = findViewById(R.id.ll_aboutus);
        final LinearLayout ll_terms = findViewById(R.id.ll_terms);
        LinearLayout ll_cusine = findViewById(R.id.ll_cusine);
        LinearLayout ll_search = findViewById(R.id.ll_search);
        LinearLayout ll_rated = findViewById(R.id.ll_rated);
        LinearLayout ll_suggested = findViewById(R.id.ll_suggested);
        LinearLayout ll_signout = findViewById(R.id.ll_signout);
        LinearLayout ll_notification = findViewById(R.id.ll_notification);

        ll_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iv = new Intent(MainMenuActivity.this, Favourite.class);
                startActivity(iv);
            }
        });
        ll_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iv = new Intent(MainMenuActivity.this, MyOrderPage.class);
                startActivity(iv);
            }
        });

        ll_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url1 = "Download Food delivery app and order nearby food" + "\n" + "https://play.google.com/store/apps/details?id=" + MainMenuActivity.this.getPackageName();
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Food Delivery");
                intent.putExtra(android.content.Intent.EXTRA_TEXT, url1);

                startActivity(Intent.createChooser(intent, "Share Food Delivery with"));
            }
        });


        ll_aboutus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iv = new Intent(MainMenuActivity.this, Aboutus.class);
                startActivity(iv);

            }
        });

        ll_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent iv = new Intent(MainMenuActivity.this, Termcondition.class);
                startActivity(iv);
            }
        });


        ll_cusine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iv = new Intent(MainMenuActivity.this, Category.class);
                startActivity(iv);
            }
        });

        ll_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iv = new Intent(MainMenuActivity.this, MainActivity.class);
                startActivity(iv);
            }
        });


        ll_rated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iv = new Intent(MainMenuActivity.this, Mapactivity.class);
                startActivity(iv);
            }
        });


        ll_suggested.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iv = new Intent(MainMenuActivity.this, MostRatedRestaurant.class);
                startActivity(iv);
            }
        });


        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        if (prefs.getString("userid", null) != null)


        {
            String userId = prefs.getString("userid", null);
            String image = prefs.getString("imagepath", null);
            String profileimage = prefs.getString("imageprofile", null);

            Log.e("image121", "" + profileimage);
            if (Objects.equals(userId, "delete")) {
                ll_signout.setVisibility(View.GONE);
                txt_nameuser.setText(R.string.txt_signin);
                txt_profile.setText(R.string.txt_profile);
                rl_profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent iv = new Intent(MainMenuActivity.this, Login.class);
                        startActivity(iv);
                    }
                });
            } else


            {
                String uname = prefs.getString("username", null);
                try {
                    Picasso.with(getApplicationContext())
                            .load(profileimage)
                            .into(img_profile);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }

                ll_signout.setVisibility(View.VISIBLE);
                ll_signout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog alertDialog = new AlertDialog.Builder(MainMenuActivity.this).create(); // Read
                        // Update
                        alertDialog.setTitle(getString(R.string.log_out));
                        alertDialog.setMessage(getString(R.string.error_logout));
                        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.conti), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // here you can add functions
                                if (LoginManager.getInstance() != null) {
                                    LoginManager.getInstance().logOut();
                                }
                                String prodel = "delete";
                                String userid = "delete";
                                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                editor.putString("delete", "" + prodel);
                                editor.putString("userid", "" + userid);
                                editor.apply();
                                Intent iv = new Intent(MainMenuActivity.this, MainMenuActivity.class);
                                startActivity(iv);
                            }
                        });

                        alertDialog.show();
                        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.black));

                    }


                });
                txt_nameuser.setText(uname);
                txt_profile.setText(R.string.txt_profile);
                txt_profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent iv = new Intent(MainMenuActivity.this, Profile.class);
                        startActivity(iv);
                    }
                });
            }
        } else {
            ll_signout.setVisibility(View.GONE);
            txt_profile.setText(R.string.txt_profile);
            txt_nameuser.setText(R.string.txt_signin);
            txt_nameuser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent iv = new Intent(MainMenuActivity.this, Login.class);
                    startActivity(iv);
                }
            });
        }

    }

    private void setupDrawer() {
        mDrawerLayout = findViewById(R.id.drawer_layout1);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                drawerView.setEnabled(true);
                recyclerView.setClickable(false);
                recyclerView.setFocusable(false);

            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                view.setEnabled(false);
                rel_main.setEnabled(true);
                rel_main.setVisibility(View.VISIBLE);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        if (mDrawerToggle != null)
            mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mDrawerToggle != null)
            mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_setting) {
            Intent iv = new Intent(MainMenuActivity.this, SettingPage.class);
            iv.putExtra("key", "main");
            startActivity(iv);
            return true;
        }

        // Activate the navigation drawer toggle
        return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {

        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT))
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        else {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(MainMenuActivity.this, R.style.MyDialogTheme);
            builder1.setTitle(getString(R.string.Quit));
            builder1.setMessage(getString(R.string.statementquit));
            builder1.setCancelable(true);
            builder1.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    finishAffinity();
                }
            });
            builder1.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
    }
}
