package santri.syaria;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import androidx.appcompat.app.AppCompatActivity;
import santri.Adapter.ListViewHolderCart;
import santri.Getset.cartgetset;
import santri.utils.sqliteHelper;

import static santri.syaria.MainActivity.changeStatsBarColor;

public class Cart extends AppCompatActivity {
    private static ArrayList<cartgetset> cartlist;
    private ProgressDialog progressDialog;
    private String menuid;
    private String s1;
    private float total = 0;
    private TextView txt_finalans;
    private EditText txt_date, txt_time, txt_note;
    private int quantity;
    ImageButton ib_back;
    private static final String MyPREFERENCES = "Fooddelivery";
    sqliteHelper sqliteHelper;
    private TimePickerDialog timePickerDialog;
    private String catName;
    private Spinner sp_duration;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        changeStatsBarColor(Cart.this);

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        gettingIntent();
        ib_back = findViewById(R.id.ib_back);
        ib_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getData();

        txt_date = (EditText) findViewById(R.id.txt_date);
        txt_time = (EditText) findViewById(R.id.txt_time);
        txt_note = (EditText) findViewById(R.id.txt_note);
        txt_note.setHint("Catatan untuk " + catName);

        sp_duration = (Spinner) findViewById(R.id.sp_duration);
        String[] duration = new String[]{"1","2","3","4","5","6","7","8","9","10","11","12",
                "13","14","15","16","17","18","19","20","21","22","23","24"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, duration);
        sp_duration.setAdapter(adapter);
        sp_duration.setSelection(0);

        TextView txt_info = (TextView) findViewById(R.id.txt_info);
        if (catName.equalsIgnoreCase("ustad")) {
            txt_info.setVisibility(View.VISIBLE);
        } else {
            txt_info.setVisibility(View.INVISIBLE);
        }

        txt_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDate(view);
            }
        });

        txt_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTimeDialog();
            }
        });
    }

    private void gettingIntent() {
        Intent iv = getIntent();
        String detail_id = iv.getStringExtra("detail_id");
        String restaurent_name = iv.getStringExtra("restaurent_name");
        catName = iv.getStringExtra("catName");
        Log.e("detail_id", "" + detail_id);
    }

    private void getData() {

        txt_finalans = findViewById(R.id.txt_finalans);
        ((TextView) findViewById(R.id.txt_title)).setTypeface(MainActivity.tf_opensense_medium);


        cartlist = new ArrayList<>();
        new getList().execute();
    }

    // Memangil fungsi kalender  di editText date

    public void setDate(View view) {
        showDialog(999);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            int currentYear = calendar.get(Calendar.YEAR);
            int currentMonth = calendar.get(Calendar.MONTH);
            int currentDay = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(this,
                    myDateListener, currentYear, currentMonth, currentDay);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    // arg1 = year
                    // arg2 = month
                    // arg3 = day
                    showDate(arg1, arg2+1, arg3);
                }
            };

    private void showDate(int year, int month, int day) {
        txt_date.setText(new StringBuilder().append(day).append("-")
                .append(month).append("-").append(year));
    }

    private void showTimeDialog() {

        /**
         * Calendar untuk mendapatkan waktu saat ini
         */
        Calendar calendar = Calendar.getInstance();

        /**
         * Initialize TimePicker Dialog
         */
        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                /**
                 * Method ini dipanggil saat kita selesai memilih waktu di DatePicker
                 */
                String jam = hourOfDay+"";
                String menit = minute+"";
                if (hourOfDay<10)
                    jam = "0"+hourOfDay;
                if (minute<10)
                    menit = "0"+minute;
                txt_time.setText(jam+":"+menit);
            }
        },
                /**
                 * Tampilkan jam saat ini ketika TimePicker pertama kali dibuka
                 */
                calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),

                /**
                 * Cek apakah format waktu menggunakan 24-hour format
                 */
                DateFormat.is24HourFormat(this));

        timePickerDialog.show();
    }

    private class getList extends AsyncTask<String, Integer, Integer> {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(Cart.this);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            // TODO Auto-generated method stub
            cartlist.clear();
            sqliteHelper = new sqliteHelper(Cart.this);
            SQLiteDatabase db1 = sqliteHelper.getWritableDatabase();
//            DBAdapter myDbHelper = new DBAdapter(Cart.this);
            /*myDbHelper = new DBAdapter(Cart.this);
            try {
                myDbHelper.createDataBase();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                myDbHelper.openDataBase();
            } catch (SQLException sqle) {
                sqle.printStackTrace();
            }*/
            int i = 1;
//            SQLiteDatabase db = myDbHelper.getReadableDatabase();
            try {
                Cursor cur = db1.rawQuery("select * from cart where foodprice >=1;", null);
                Log.e("cartlisting", "" + ("select * numberOfRecords cart where foodprice <=0;"));
                Log.d("SIZWA", "" + cur.getCount());
                if (cur.getCount() != 0) {
                    if (cur.moveToFirst()) {
                        do {
                            cartgetset obj = new cartgetset();
                            String resid = cur.getString(cur.getColumnIndex("resid"));
                            String foodid = cur.getString(cur.getColumnIndex("foodname"));
                            menuid = cur.getString(cur.getColumnIndex("menuid"));
                            String foodname = cur.getString(cur.getColumnIndex("foodname"));
                            String foodprice = cur.getString(cur.getColumnIndex("foodprice"));
                            String fooddesc = cur.getString(cur.getColumnIndex("fooddesc"));
                            String restcurrency = cur.getString(cur.getColumnIndex("restcurrency"));
                            restcurrency = restcurrency.replace("$", "");
                            obj.setResid(resid);
                            obj.setFoodid(foodid);
                            obj.setMenuid(menuid);
                            obj.setFoodname(foodname);
                            obj.setFoodprice(foodprice);
                            obj.setFooddesc(fooddesc);
                            obj.setRestcurrency(restcurrency);
                            cartlist.add(obj);
                            try {
                                float quant = Float.parseFloat(foodprice);
                                float single = Float.parseFloat(restcurrency);
                                Log.e("12345", "" + quant + single);
                                float totalsum = quant * single;
                                total = totalsum + total;
                            } catch (NumberFormatException e) {
                                Log.e("Error", e.getMessage());
                            }
                        } while (cur.moveToNext());
                    }
                }
                cur.close();
                db1.close();
//                myDbHelper.close();
            } catch (Exception e) {
                // TODO: handle exception
            }
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            ListView list_cart = findViewById(R.id.list_cart);
            Log.d("file", "" + cartlist.size());
            if (cartlist.size() == 0) {
                Toast.makeText(getApplicationContext(), getString(R.string.norecord), Toast.LENGTH_LONG).show();
                list_cart.setVisibility(View.INVISIBLE);
            } else {
                list_cart = findViewById(R.id.list_cart);
                Button btncheckout = findViewById(R.id.btn_checkout);
                btncheckout.setTypeface(MainActivity.tf_opensense_regular);
                btncheckout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (checkingSignIn()) {
                            if (txt_date.length()>0 && txt_time.length()>0) {
                                Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
                                int currentYear = calendar.get(Calendar.YEAR);
                                int currentMonth = calendar.get(Calendar.MONTH)+1;
                                int currentDay = calendar.get(Calendar.DAY_OF_MONTH);

                                int inputYear = Integer.parseInt(txt_date.getText().toString().split("-")[2]);
                                int inputMonth = Integer.parseInt(txt_date.getText().toString().split("-")[1]);
                                int inputDay = Integer.parseInt(txt_date.getText().toString().split("-")[0]);

                                Date currentDate = new Date();

                                String myFormatString = "dd-MM-yyyy";
                                SimpleDateFormat df = new SimpleDateFormat(myFormatString);

                                Date givenDate = null;
                                try {
                                    givenDate = df.parse(txt_date.getText().toString());
                                } catch (ParseException e) {
                                    Log.e("Parse", e.getMessage());
                                }
                                String strCurrent = df.format(currentDate);
                                String strGiven = df.format(givenDate);


                                if (givenDate.compareTo(currentDate) >= 0 || strCurrent.equals(strGiven)) {
                                    Intent iv = new Intent(Cart.this, PlaceOrder.class);
                                    iv.putExtra("order_price", "" + s1);
                                    iv.putExtra("res_id", menuid);
                                    iv.putExtra("bookdate", txt_date.getText().toString());
                                    iv.putExtra("booktime", txt_time.getText().toString());
                                    iv.putExtra("booknote", txt_note.getText().toString());
                                    iv.putExtra("catName", catName);
                                    iv.putExtra("duration", sp_duration.getSelectedItem().toString());

                                    SharedPreferences.Editor editor = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE).edit();
                                    editor.putString("bookdate", txt_date.getText().toString());
                                    editor.putString("booktime", txt_time.getText().toString());
                                    editor.putString("booknote", txt_note.getText().toString());
                                    editor.putString("catName", catName);
                                    editor.putString("duration", sp_duration.getSelectedItem().toString());
                                    editor.apply();

                                    startActivity(iv);
                                } else {
                                    Toast.makeText(getApplicationContext(), "Date cannot be older than today", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "Please set the booking date & time", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent iv = new Intent(Cart.this, Login.class);
                            iv.putExtra("key", "PlaceOrder");
                            iv.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                            SharedPreferences.Editor editor = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE).edit();
                            editor.putString("bookdate", txt_date.getText().toString());
                            editor.putString("booktime", txt_time.getText().toString());
                            editor.putString("booknote", txt_note.getText().toString());
                            editor.putString("catName", catName);
                            editor.putString("duration", sp_duration.getSelectedItem().toString());
                            editor.apply();

                            startActivity(iv);
                            Toast.makeText(Cart.this, R.string.loginmsg, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                cartadapter1 adapter = new cartadapter1(Cart.this, cartlist, menuid, quantity);
                list_cart.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                list_cart.getAdapter().getCount();
                String totalcartitem = String.valueOf(list_cart.getAdapter().getCount());
                if (totalcartitem.equals("0")) {
                    btncheckout.setEnabled(false);
                    Toast.makeText(getApplicationContext(), R.string.cart_empty, Toast.LENGTH_SHORT).show();
                } else {
                    btncheckout.setEnabled(true);
                }
                list_cart.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    }
                });
                double roundOff = (double) Math.round(total * 100) / 100;
                txt_finalans.setText(getString(R.string.currency) + " " + roundOff);
                Button btn_cart = findViewById(R.id.btn_cart);
                btn_cart.setText(totalcartitem);
            }
        }
    }

    private boolean checkingSignIn() {
        //getting shared preference
        SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        Log.e("user", "" + prefs.getString("userid", null));
        // check user is created or not
        // if user is already logged in
        if (prefs.getString("userid", null) != null) {
            String userid = prefs.getString("userid", null);
            return !userid.equals("delete");
        } else {
            return false;
        }
    }


    class cartadapter1 extends BaseAdapter {
        final ArrayList<Integer> quantity = new ArrayList<>();
        SQLiteDatabase db;
        Cursor cur = null;
        final String menuid1;
        String menuid321, foodprice321, restcurrency321;
        final int quen;
        String foodid, foodname, fooddesc;
        int val1;
        float add, sub;
        private final ArrayList<cartgetset> data1;
        private final Activity activity;
        private LayoutInflater inflater = null;

        cartadapter1(Activity a, ArrayList<cartgetset> str, String menuid, int quantity) {
            activity = a;
            data1 = str;
            menuid1 = menuid;
            quen = quantity;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            for (int i = 0; i < data1.size(); i++) {
                this.quantity.add(i);
            }
        }

        @Override
        public int getCount() {
            return data1.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View row;
            final ListViewHolderCart listViewHolder;
            Typeface opensansregular = Typeface.createFromAsset(activity.getAssets(), "fonts/OpenSans-Regular.ttf");
            if (convertView == null) {
                row = inflater.inflate(R.layout.cell_cart_new, parent, false);
                listViewHolder = new ListViewHolderCart();
                listViewHolder.txt_name1 = row.findViewById(R.id.txt_name1);
                listViewHolder.txt_name1.setTypeface(opensansregular);
                listViewHolder.txt_desc = row.findViewById(R.id.txt_desc);
                listViewHolder.txt_desc.setTypeface(opensansregular);
                listViewHolder.txt_totalprice = row.findViewById(R.id.txt_totalprice);
                listViewHolder.txt_basic_price = row.findViewById(R.id.txt_basic_price);
                listViewHolder.txt_totalprice.setTypeface(MainActivity.tf_opensense_medium);
                listViewHolder.txt_basic_price.setTypeface(opensansregular);
                //listViewHolder.txt_quantity = row.findViewById(R.id.txt_quantity);
                //listViewHolder.txt_quantity.setTypeface(opensansregular);

                //listViewHolder.btn_plus = row.findViewById(R.id.btn_plus);
                //listViewHolder.btn_minus1 = row.findViewById(R.id.btn_minus1);
                //listViewHolder.edTextQuantity = row.findViewById(R.id.edTextQuantity);
                //listViewHolder.edTextQuantity.setTypeface(opensansregular);
                //listViewHolder.btn_plus.setTag(listViewHolder);
                //listViewHolder.btn_minus1.setTag(listViewHolder);
                row.setTag(listViewHolder);
            } else {
                row = convertView;
                listViewHolder = (ListViewHolderCart) row.getTag();
            }
            listViewHolder.txt_name1.setText((data1.get(position).getFoodname()));
            listViewHolder.txt_desc.setText(data1.get(position).getFooddesc());
            listViewHolder.txt_totalprice.setText(getString(R.string.currency) + " " + (data1.get(position).getRestcurrency()));
            //listViewHolder.edTextQuantity.setText(data1.get(position).getFoodprice());
            val1 = Integer.parseInt(data1.get(position).getFoodprice());
//            listViewHolder.btn_plus.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    View p = (View) v.getParent();
//                    ListViewHolderCart holder1 = (ListViewHolderCart) (v.getTag());
//                    val1 = Integer.valueOf(holder1.edTextQuantity.getText().toString());
//
//                    String add_price = holder1.txt_totalprice.getText().toString().trim().replace("$", "");
//                    add = Float.valueOf(add_price);
//                    total = total + add;
//                    Log.e("totalansadd ", "" + total);
//                    double roundOff = (double) Math.round(total * 100) / 100;
//                    txt_finalans.setText(getString(R.string.currency) + " " + roundOff);
//                    val1++;
//                    holder1.edTextQuantity.setText(String.valueOf(val1));
////                    DBAdapter myDbHelper;
////                    myDbHelper = new DBAdapter(activity);
//                    sqliteHelper = new sqliteHelper(Cart.this);
//                    SQLiteDatabase db1 = sqliteHelper.getWritableDatabase();
//                    /*try {
//                        myDbHelper.createDataBase();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    try {
//                        myDbHelper.openDataBase();
//                    } catch (SQLException sqle) {
//                        sqle.printStackTrace();
//                    }
//                    db = myDbHelper.getReadableDatabase();*/
//                    try {
//                        cur = db1.rawQuery("UPDATE cart SET foodprice ='" + val1 + "' Where menuid ='" + data1.get(position).getMenuid() + "';", null);
//                        Log.e("updatequeryplus", "" + "UPDATE cart SET foodprice ='" + val1 + "' Where menuid ='" + data1.get(position).getMenuid() + "';");
//                        Log.e("SIZWA", "" + cur.getCount());
//                        if (cur.getCount() != 0) {
//                            if (cur.moveToFirst()) {
//                                do {
//                                    cartgetset obj = new cartgetset();
//                                    menuid321 = cur.getString(cur.getColumnIndex("menuid"));
//                                    foodid = cur.getString(cur.getColumnIndex("foodid"));
//                                    foodname = cur.getString(cur.getColumnIndex("foodname"));
//                                    foodprice321 = cur.getString(cur.getColumnIndex("foodprice"));
//                                    fooddesc = cur.getString(cur.getColumnIndex("fooddesc"));
//                                    restcurrency321 = cur.getString(cur.getColumnIndex("restcurrency"));
//                                    obj.setFoodid(foodid);
//                                    obj.setMenuid(menuid321);
//                                    obj.setFoodname(foodname);
//                                    obj.setFoodprice(foodprice321);
//                                    obj.setFooddesc(fooddesc);
//                                    obj.setRestcurrency(restcurrency321);
//                                    Log.e("menuid321updatedcart", "" + menuid321);
//                                    Log.e("foodp321updatedcart", "" + foodprice321);
//                                    data1.add(obj);
//                                    new getList().execute();
//                                } while (cur.moveToNext());
//                            }
//                        }
//                        cur.close();
//                        db1.close();
////                        myDbHelper.close();
//                    } catch (Exception e) {
//                        Log.e("Error", e.getMessage());
//
//                    }
//                }
//            });
//            listViewHolder.btn_minus1.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    ListViewHolderCart holder1 = (ListViewHolderCart) (v.getTag());
//                    String sub_price = holder1.txt_totalprice.getText().toString().trim().replace("$", "");
//
//                    sub = Float.valueOf(sub_price);
//                    if (val1 <= 0) {
//                    } else {
//                        val1 = Integer.valueOf(holder1.edTextQuantity.getText().toString());
//                        val1--;
//                        total = total - sub;
//                        Log.e("totalanssub ", "" + total);
//                        double roundOff = (double) Math.round(total * 100) / 100;
//                        txt_finalans.setText(getString(R.string.currency) + " " + roundOff);
//                        holder1.edTextQuantity.setText(String.valueOf(val1));
//
//
//                        sqliteHelper = new sqliteHelper(Cart.this);
//                        SQLiteDatabase db1 = sqliteHelper.getWritableDatabase();
//
////                        DBAdapter myDbHelper = new DBAdapter(activity);
////                        myDbHelper = new DBAdapter(activity);
//       /*                 try {
//                            myDbHelper.createDataBase();
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                        try {
//                            myDbHelper.openDataBase();
//                        } catch (SQLException sqle) {
//                            sqle.printStackTrace();
//                        }
//                        db = myDbHelper.getReadableDatabase();*/
//                        try {
//                            cur = db1.rawQuery("UPDATE cart SET foodprice ='" + val1 + "' Where menuid ='" + data1.get(position).getMenuid() + "';", null);
//                            Log.e("updatequeryminus", "" + "UPDATE cart SET foodprice ='" + val1 + "' Where menuid ='" + data1.get(position).getMenuid() + "';");
//                            Log.e("SIZWA", "" + cur.getCount());
//                            if (cur.getCount() != 0) {
//                                if (cur.moveToFirst()) {
//                                    do {
//                                        cartgetset obj = new cartgetset();
//                                        menuid321 = cur.getString(cur.getColumnIndex("menuid"));
//                                        foodid = cur.getString(cur.getColumnIndex("foodid"));
//                                        foodname = cur.getString(cur.getColumnIndex("foodname"));
//                                        foodprice321 = cur.getString(cur.getColumnIndex("foodprice"));
//                                        fooddesc = cur.getString(cur.getColumnIndex("fooddesc"));
//                                        restcurrency321 = cur.getString(cur.getColumnIndex("restcurrency"));
//                                        obj.setFoodid(foodid);
//                                        obj.setMenuid(menuid321);
//                                        obj.setFoodname(foodname);
//                                        obj.setFoodprice(foodprice321);
//                                        obj.setFooddesc(fooddesc);
//                                        obj.setRestcurrency(restcurrency321);
//                                        Log.e("menuid321updatedcart", "" + menuid321);
//                                        Log.e("foodp321updatedcart", "" + foodprice321);
//                                        data1.add(obj);
//                                        new getList().execute();
//                                    } while (cur.moveToNext());
//                                }
//                            }
//                            cur.close();
//                            db1.close();
////                            myDbHelper.close();
//                        } catch (Exception e) {
//                            Log.e("Error", e.getMessage());
//                        }
//                    }
//                }
//            });
            return row;
        }
    }
}





