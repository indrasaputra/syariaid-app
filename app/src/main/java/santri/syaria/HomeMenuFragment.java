package santri.syaria;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import santri.Adapter.MainMenuAdapter;
import santri.Getset.MainMenuDataSource;
import santri.firebasechat.MainChatActivity;
import santri.syaria.retrofit.ContentData;
import santri.syaria.retrofit.ResponseContent;
import santri.syaria.retrofit.ResponseHistory;
import santri.syaria.retrofit.ResponseLike;
import santri.syaria.retrofit.RestAPI;
import santri.syaria.retrofit.RetrofitAdapter;
import santri.utils.Util;
import santri.video.ActivityVideo;
import santri.video.ActivityVideoDetails;
import santri.video.adapter.AdapterVideoRecent;
import santri.video.connection.API;
import santri.video.connection.RestAdapter;
import santri.video.data.Constant;
import santri.video.data.ThisApplication;
import santri.video.model.Video;
import santri.video.response.ResponseHome;
import santri.video.utils.Tools;

import static santri.firebasechat.constants.IConstants.EXTRA_SEEN;
import static santri.firebasechat.constants.IConstants.REF_CHATS;
import static santri.firebasechat.constants.IConstants.SLASH;
import static santri.firebasechat.constants.IConstants.TYPE_TEXT;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeMenuFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeMenuFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeMenuFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private static final String MY_PREFS_NAME = "Fooddelivery";
    private RecyclerView recyclerView;
    public static Typeface tf_opensense_regular;
    public static Typeface tf_opensense_medium;

    private ArrayList<MainMenuDataSource> mainMenuList;
    private MainMenuAdapter adapter;
    private boolean hasLoadedOnce = false;
    private Activity activity;
    private SharedPreferences prefs;
    private FloatingActionButton fabChat;
    private FrameLayout fab_layout;
    private int unreadCount = 0;
    private TextView text_count;
    private View rootView;
    private LinearLayout ll_hikmah;
    private ImageView img_hikmah;
    private ImageView img_love;
    private TextView txt_like_number;
    private ImageView img_share;
    private TextView title, content;
    private boolean hikmahLiked;

    private LinearLayout ll_risalah;
    private ImageView img_risalah;
    private TextView title_risalah, content_risalah;
    private MaterialRippleLayout lyt_parent_risalah;

    private Call<ResponseHome> callbackCall;

//    @BindView(R.id.section1)
//    ShimmerRecyclerView section1;

//    private SiramanRohaniAdapter adapter1;

    public HomeMenuFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeMenuFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeMenuFragment newInstance(String param1, String param2) {
        HomeMenuFragment fragment = new HomeMenuFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        activity = getActivity();
        prefs = activity.getSharedPreferences(MY_PREFS_NAME, getActivity().MODE_PRIVATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_home_menu, container, false);

        fab_layout = rootView.findViewById(R.id.fab_layout);
        fabChat = rootView.findViewById(R.id.fabChat);
        text_count = rootView.findViewById(R.id.text_count);
        ll_hikmah = rootView.findViewById(R.id.ll_hikmah);
        img_hikmah = rootView.findViewById(R.id.img_hikmah);
        img_love = rootView.findViewById(R.id.img_love);
        txt_like_number = rootView.findViewById(R.id.txt_like_number);
        img_share = rootView.findViewById(R.id.img_share);
        title = rootView.findViewById(R.id.title);
        content = rootView.findViewById(R.id.content);

        ll_risalah = rootView.findViewById(R.id.ll_risalah);
        img_risalah = rootView.findViewById(R.id.img_risalah);
        title_risalah = rootView.findViewById(R.id.title_risalah);
        content_risalah = rootView.findViewById(R.id.content_risalah);
        lyt_parent_risalah = rootView.findViewById(R.id.lyt_parent_risalah);

        mainMenuList = new ArrayList<MainMenuDataSource>();
        adapter = new MainMenuAdapter(container.getContext(), mainMenuList);

        GridLayoutManager mLayoutManager = new GridLayoutManager(container.getContext(), 4);
        //this access method getItemViewType on adapter, setting number of grid per row
        mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (adapter.getItemViewType(position) == 0 || adapter.getItemViewType(position) == 2 || adapter.getItemViewType(position) == 4 || adapter.getItemViewType(position) == 5) {
                    return 4; // 4 bagian diambil penuh
                } else {
                    return 1; // pakai 1 bagian dari 4
                }
            }
        });

        //Util util = new Util(this);

        recyclerView = rootView.findViewById(R.id.recyler_view);
        recyclerView.setLayoutManager(mLayoutManager);
        //recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, util.dpToPx(0), true));
        //recyclerView.addItemDecoration(new MyDividerItemDecoration(myApp.getContext(), LinearLayoutManager.VERTICAL, 16));
        //recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        if (!hasLoadedOnce) {
            hasLoadedOnce = true;
            //prepareHomeMenu();
            new GetDataAsyncTask().execute();
        }

        fabChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, MainChatActivity.class);
                intent.putExtra("from_home",true);
                activity.startActivity(intent);
            }
        });

        rootView.findViewById(R.id.more_video).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, ActivityVideo.class);
                activity.startActivity(intent);
            }
        });

        rootView.findViewById(R.id.more_hikmah).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, ActivityHikmah.class);
                activity.startActivity(intent);
            }
        });

        rootView.findViewById(R.id.more_risalah).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, ActivityRisalah.class);
                activity.startActivity(intent);
            }
        });

        if (prefs.getString("userid", null) != null && !prefs.getString("userid", null).equals("delete")) {
            Util.initializeFirebase(activity,"login_users",true);
        }

        return rootView;
    }

//    private void setupSectionHorizon() {
//        adapter1 = new SiramanRohaniAdapter(getActivity());
//        section1.setAdapter(adapter1);
//        section1.showShimmerAdapter();
//        FeedTask task = new FeedTask();
//        if (task != null) {
//            task.getSiramanRohani(1);
//        }
//    }

    private void requestVideoData() {
        API api = RestAdapter.createAPI();
        callbackCall = api.getHome();
        callbackCall.enqueue(new Callback<ResponseHome>() {
            @Override
            public void onResponse(Call<ResponseHome> call, Response<ResponseHome> response) {
                ResponseHome resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    displayRecent(resp.recent);
                    //ThisApplication.getInstance().setResponseHome(resp);
                    //swipeProgress(false);
                    //lyt_main_content.setVisibility(View.VISIBLE);
                } else {
                    //onFailRequest();
                }
            }

            @Override
            public void onFailure(Call<ResponseHome> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                //if (!call.isCanceled()) onFailRequest();
            }
        });
    }

    private void requestRisalah() {
        JSONObject jsonReq = new JSONObject();
        try {
            //jsonReq.put("search", "tanya");
            jsonReq.put("limit", "1");
            jsonReq.put("pageno","1");
            jsonReq.put("tipe", "dakwah");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonReq.toString());
        RestAPI api = RetrofitAdapter.createAPI();
        Call<ResponseContent> call = api.getContent(bodyRequest);

        call.enqueue(new Callback<ResponseContent>() {
            @Override
            public void onResponse(Call<ResponseContent> call, Response<ResponseContent> response) {
                ResponseContent resp = response.body();
                if (resp!=null) {
                    if (resp.data.size() > 0) {
                        displayRisalah(resp.data);
                    } else {
                        ll_risalah.setVisibility(View.GONE);
                    }
                } else {
                    ll_risalah.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ResponseContent> call, Throwable t) {
                if (!call.isCanceled()) ll_hikmah.setVisibility(View.GONE);
            }
        });
    }

    private void displayRisalah(List<ContentData> data) {
        Log.e("response",data.get(0).image+data.get(0).title+data.get(0).like);
        //Picasso.with(activity).load(data.get(0).image).into(img_hikmah);
        Tools.displayImage(activity, img_risalah, data.get(0).image.replace("http:","https:"));
        title_risalah.setText(data.get(0).title);
        content_risalah.setText(Html.fromHtml(data.get(0).content));
        ll_risalah.setVisibility(View.VISIBLE);

        rootView.findViewById(R.id.lyt_parent_risalah).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, ActivityRisalahDetail.class);
                intent.putExtra("menu_title","Risalah");
                intent.putExtra("txt_title",data.get(0).title);
                intent.putExtra("txt_content",data.get(0).content);
                intent.putExtra("image_url",data.get(0).image);
                activity.startActivity(intent);
            }
        });
    }

    private void requestHikmah() {
        JSONObject jsonReq = new JSONObject();
        try {
            //jsonReq.put("search", "tanya");
            jsonReq.put("limit", "1");
            jsonReq.put("pageno","1");
            jsonReq.put("tipe", "flayer");
            if (prefs.getString("userid", null) != null && !prefs.getString("userid", null).equals("delete")) {
                jsonReq.put("iduser", prefs.getString("userid", null));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonReq.toString());
        RestAPI api = RetrofitAdapter.createAPI();
        Call<ResponseContent> call = api.getContent(bodyRequest);

        call.enqueue(new Callback<ResponseContent>() {
            @Override
            public void onResponse(Call<ResponseContent> call, Response<ResponseContent> response) {
                ResponseContent resp = response.body();
                if (resp!=null) {
                    if (resp.data.size() > 0) {
                        displayHikmah(resp.data);
                    } else {
                        ll_hikmah.setVisibility(View.GONE);
                    }
                } else {
                    ll_hikmah.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ResponseContent> call, Throwable t) {
                if (!call.isCanceled()) ll_hikmah.setVisibility(View.GONE);
            }
        });
    }

    private void displayHikmah(List<ContentData> data) {
        Log.e("response",data.get(0).image+data.get(0).title+data.get(0).like);
        //Picasso.with(activity).load(data.get(0).image).into(img_hikmah);
        Tools.displayImage(activity, img_hikmah, data.get(0).image.replace("http:","https:"));
        title.setText(data.get(0).title);
        content.setText(Html.fromHtml(data.get(0).content));
        txt_like_number.setText(data.get(0).like);
        ll_hikmah.setVisibility(View.VISIBLE);
        if (data.get(0).liked) {
            img_love.setImageDrawable(activity.getDrawable(R.drawable.ic_love_on));
            hikmahLiked = true;
        } else {
            img_love.setImageDrawable(activity.getDrawable(R.drawable.ic_love));
            hikmahLiked = false;
        }

        img_love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (prefs.getString("userid", null) != null && !prefs.getString("userid", null).equals("delete")) {
                    if (!hikmahLiked) {
                        hikmahLiked = true;
                        img_love.setImageResource(R.drawable.ic_love_on);
                        sendLike(data.get(0).id, txt_like_number, img_love);

                    } //else {
                        //hikmahLiked = false;
                        //img_love.setImageResource(R.drawable.ic_love);
                        //sendUnlike(data.get(0).id, txt_like_number, img_love);
                    //}
                }
            }
        });

        img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String shareMsg = data.get(0).title+"\n\n"+data.get(0).content;
                String footerMsg = "\n\nDownload Aplikasi konsultasi dengan Ustadz/Ustadzah di sini:\nhttps://play.google.com/store/apps/details?id=santri.syaria";
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                share.putExtra(Intent.EXTRA_SUBJECT, "Syariaid Hikmah");
                Uri bmpUri = Tools.getLocalBitmapUri(activity, img_hikmah);
                share.setType("image/*");
                share.setType("image/jpeg");
                share.putExtra(Intent.EXTRA_STREAM, bmpUri);
                share.putExtra(Intent.EXTRA_TEXT, shareMsg+footerMsg);
                activity.startActivity(Intent.createChooser(share, "Share Hikmah Syariaid"));
            }
        });
    }

    private void sendLike(String id, TextView txt, ImageView img) {
        JSONObject jsonReq = new JSONObject();
        try {
            jsonReq.put("idcontent", id);
            jsonReq.put("iduser",prefs.getString("userid", null));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonReq.toString());
        RestAPI api = RetrofitAdapter.createAPI();
        Call<ResponseLike> call = api.likeContent(bodyRequest);

        call.enqueue(new Callback<ResponseLike>() {
            @Override
            public void onResponse(Call<ResponseLike> call, Response<ResponseLike> response) {
                ResponseLike resp = response.body();
                if (resp!=null) {
                    if (resp.like > 0) {
                        // txt_like_number.setText(resp.like);
                        Log.e("like",resp.like+"");
                        setLikeNumber(resp.like);
                        hikmahLiked = true;
                    } else {
                        img.setImageDrawable(activity.getDrawable(R.drawable.ic_love));
                        hikmahLiked = false;
                    }
                } else {
                    img.setImageDrawable(activity.getDrawable(R.drawable.ic_love));
                    hikmahLiked = false;
                }
            }

            @Override
            public void onFailure(Call<ResponseLike> call, Throwable t) {
                img.setImageDrawable(activity.getDrawable(R.drawable.ic_love));
                hikmahLiked = false;
            }
        });
    }

    private void setLikeNumber(int likeNumber) {
        txt_like_number.setText(likeNumber+"");
    }

    private void sendUnlike(String id, final TextView txt, ImageView img) {
        JSONObject jsonReq = new JSONObject();
        try {
            jsonReq.put("idcontent", id);
            jsonReq.put("iduser",prefs.getString("userid", null));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonReq.toString());
        RestAPI api = RetrofitAdapter.createAPI();
        Call<ResponseLike> call = api.unlikeContent(bodyRequest);

        call.enqueue(new Callback<ResponseLike>() {
            @Override
            public void onResponse(Call<ResponseLike> call, Response<ResponseLike> response) {
                ResponseLike resp = response.body();
                if (resp!=null) {
                    if (resp.like > 0) {
                        //txt_like_number.setText(resp.like);
                        Log.e("like",resp.like+"");
                        hikmahLiked = false;
                    } else {
                        img.setImageDrawable(activity.getDrawable(R.drawable.ic_love));
                        hikmahLiked = true;
                    }
                } else {
                    img.setImageDrawable(activity.getDrawable(R.drawable.ic_love_on));
                    hikmahLiked = true;
                }
            }

            @Override
            public void onFailure(Call<ResponseLike> call, Throwable t) {
                img.setImageDrawable(activity.getDrawable(R.drawable.ic_love_on));
                hikmahLiked = true;
            }
        });
    }

    private void displayRecent(List<Video> items) {
        ViewPager view_pager = rootView.findViewById(R.id.view_pager_recent);
        final AdapterVideoRecent mAdapter = new AdapterVideoRecent(getContext(), items);
        view_pager.setAdapter(mAdapter);
        view_pager.setOffscreenPageLimit(4);
        final int padding = Tools.getPaddingCard(rootView.getResources().getDimensionPixelOffset(R.dimen.item_card_height));
        view_pager.setPadding(padding, 0, padding, 0);
        view_pager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
                page.setTranslationX(-(padding - Tools.dpToPx(getContext(), 12)));
            }
        });

        mAdapter.setOnItemClickListener(new AdapterVideoRecent.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Video obj) {
                ActivityVideoDetails.navigate(getActivity(), obj.id, false);
            }
        });
    }

    private void prepareHomeMenu() {
        int[] covers = new int[]{
                R.drawable.ustad,               //0
                R.drawable.chat_ustad,               //1
                R.drawable.konsulnikah,
                R.drawable.guru
        };

        int[] menu = new int[]{
                R.drawable.icon_quran,
                R.drawable.icon_murottal,
                R.drawable.icon_tasbih,
                R.drawable.icon_kalendar,
                R.drawable.icon_sholat,
                R.drawable.icon_qiblat,
                R.drawable.icon_penanggalan,
                R.drawable.icon_asmaul,
                R.drawable.icon_doa
        };

        int[] elearning = new int[]{
                R.drawable.konsulnikah
        };

        MainMenuDataSource a = null;

        //slide
        a = new MainMenuDataSource("notif_activity");
        mainMenuList.add(a);

        //title body
        a = new MainMenuDataSource("main_title");
        mainMenuList.add(a);

        //body
        a = new MainMenuDataSource(5, "TAARUF SYARI", covers[0], R.color.green, R.color.hitam, "body");
        mainMenuList.add(a);

        if (prefs.getString("userid", null) != null && !prefs.getString("userid", null).equals("delete")) {
            a = new MainMenuDataSource(2, "CHAT USTAD", covers[1], R.color.green, R.color.hitam, "body");
            mainMenuList.add(a);
            //a = new MainMenuDataSource(3, "KONSULTASI PERNIKAHAN", covers[2], R.color.green, R.color.hitam, "body");
            //mainMenuList.add(a);
        } else {
            a = new MainMenuDataSource(0, "CHAT USTAD", covers[1], R.color.green, R.color.hitam, "body");
            mainMenuList.add(a);
            //a = new MainMenuDataSource(0, "KONSULTASI NIKAH", covers[2], R.color.green, R.color.hitam, "body");
            //mainMenuList.add(a);
        }
        a = new MainMenuDataSource(1, "UNDANG USTAD", covers[0], R.color.green, R.color.hitam, "body");
        mainMenuList.add(a);
        //a = new MainMenuDataSource(4, "CARI GURU", covers[3], R.color.green, R.color.hitam, "body");
        //mainMenuList.add(a);
        //a = new MainMenuDataSource(4, "PENGINAPAN", covers[3], R.color.putih, R.color.hitam, "body");
        //mainMenuList.add(a);

        //e-learning
        a = new MainMenuDataSource("elearn_title");
        mainMenuList.add(a);

        boolean isLogin = prefs.getString("userid", null) != null && !prefs.getString("userid", null).equals("delete") ? true : false;

        a = new MainMenuDataSource(isLogin ? 1 : 0, "Pra Nikah", elearning[0], R.color.com_facebook_messenger_blue, R.color.hitam, "elearn_content");
        mainMenuList.add(a);

        //body1
        a = new MainMenuDataSource("body1");
        mainMenuList.add(a);

        //body2
        a = new MainMenuDataSource(1, "Al Quran", menu[0], R.color.putih, R.color.hitam, "body2");
        mainMenuList.add(a);
        a = new MainMenuDataSource(2, "Murottal", menu[1], R.color.putih, R.color.hitam, "body2");
        mainMenuList.add(a);
        a = new MainMenuDataSource(3, "Tasbih", menu[2], R.color.putih, R.color.hitam, "body2");
        mainMenuList.add(a);
        a = new MainMenuDataSource(5, "Waktu Sholat", menu[4], R.color.putih, R.color.hitam, "body2");
        mainMenuList.add(a);
        a = new MainMenuDataSource(6, "Qiblat", menu[5], R.color.putih, R.color.hitam, "body2");
        mainMenuList.add(a);
        a = new MainMenuDataSource(8, "Asmaul Husna ", menu[7], R.color.putih, R.color.hitam, "body2");
        mainMenuList.add(a);
        a = new MainMenuDataSource(4, "Kalender", menu[3], R.color.putih, R.color.hitam, "body2");
        mainMenuList.add(a);
        a = new MainMenuDataSource(7, "Penanggalan", menu[6], R.color.putih, R.color.hitam, "body2");
        mainMenuList.add(a);
        a = new MainMenuDataSource(9, "Doa &\nAmalan", menu[8], R.color.putih, R.color.hitam, "body2");
        mainMenuList.add(a);

        adapter.notifyDataSetChanged();

//        getActivity().runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                adapter.notifyDataSetChanged();
//            }
//        });
    }

    private void checkingSignIn() {
        //getting shared preference
        prefs = activity.getSharedPreferences(MY_PREFS_NAME, activity.MODE_PRIVATE);
        Log.e("user", "" + prefs.getString("userid", null));
        fab_layout = rootView.findViewById(R.id.fab_layout);
        fabChat = rootView.findViewById(R.id.fabChat);
        // check user is created or not
        // if user is already logged in
        if (prefs.getString("userid", null)!=null &&
                !prefs.getString("userid", null).equals("delete")) {
            boolean isChatActive = prefs.getBoolean("activeuserchat", false);
            Log.d("isChatActive",isChatActive+"");
            //if (!isChatActive) {
                String username = prefs.getString("username",null);
                String email = prefs.getString("usermailid",null);
                String imgUrl = prefs.getString("imageprofile",null);
                ((HomeMenuActivity)activity).loginChat(email, "123456", username, "",imgUrl);
                SharedPreferences.Editor editor = activity.getSharedPreferences(MY_PREFS_NAME,
                        activity.MODE_PRIVATE).edit();
                editor.putBoolean("activeuserchat",true);
                editor.apply();
                fab_layout.setVisibility(View.VISIBLE);
                fabChat.show();
                Log.d("get chat boolean",prefs.getBoolean("activeuserchat", false)+"");
                Log.d("testing","after get chat");
            //} else {
                fab_layout.setVisibility(View.VISIBLE);
                fabChat.show();
            //}
        } else {
            fab_layout.setVisibility(View.INVISIBLE);
            fabChat.hide();
        }
    }

    class GetDataAsyncTask extends AsyncTask<Void, Void, Void> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            //gettingDataFromDatabase();
            //check sign in and login to chat
            checkingSignIn();
//            prepareHomeMenu();
//            setupSectionHorizon();
//            setTotalUnreadMessages();
//            requestVideoData();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d("post","postExecute");
            prepareHomeMenu();
            setTotalUnreadMessages();
            requestVideoData();
            requestRisalah();
            requestHikmah();
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        setTotalUnreadMessages();
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void setTotalUnreadMessages() {
        unreadCount = 0;
        text_count.setVisibility(View.GONE);
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        if (firebaseUser!=null) {
            Query reference = FirebaseDatabase.getInstance().getReference(REF_CHATS).child(firebaseUser.getUid());
            reference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.hasChildren()) {
                        Log.d("datasnapshot children",dataSnapshot.getChildrenCount()+"");
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Query query = FirebaseDatabase.getInstance().getReference(REF_CHATS).child(firebaseUser.getUid() + SLASH + snapshot.getKey()).orderByChild(EXTRA_SEEN).equalTo(false);
                            query.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot2) {
                                    if (dataSnapshot2.hasChildren()) {
                                        Log.d("datasnapshot2 children",dataSnapshot2.getChildrenCount()+"");
                                        unreadCount = unreadCount+(int)dataSnapshot2.getChildrenCount();
                                        Log.d("unreadCount in",unreadCount+"");
                                        if (unreadCount > 0) {
                                            text_count.setText(unreadCount+"");
                                            text_count.setVisibility(View.VISIBLE);
                                        } else {
                                            text_count.setVisibility(View.GONE);
                                        }

                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                        }
                        unreadCount = 0;
                        Log.d("unreadcount after for", unreadCount+"");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

}
