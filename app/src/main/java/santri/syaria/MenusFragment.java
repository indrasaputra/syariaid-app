package santri.syaria;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

//import com.facebook.login.LoginManager;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import androidx.fragment.app.Fragment;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MenusFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MenusFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MenusFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private static final String MY_PREFS_NAME = "Fooddelivery";
    private TextView txt_nameuser;
    private TextView txt_profile;
    private RelativeLayout rel_main, rl_profile, ll_profile;
    private ImageView img_profile;
    public static Typeface tf_opensense_regular;
    public static Typeface tf_opensense_medium;
    private View rootView;

    public MenusFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MenusFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MenusFragment newInstance(String param1, String param2) {
        MenusFragment fragment = new MenusFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_menus, container, false);

        initialize();
        setupMenu();

        return rootView;
    }

    private void initialize() {
        Typeface tf_worksans = Typeface.createFromAsset(getActivity().getAssets(), "fonts/WorkSans-Regular.otf");
        tf_opensense_regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
        tf_opensense_medium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Semibold.ttf");
        TextView txt_terms = rootView.findViewById(R.id.txt_terms);
        txt_terms.setTypeface(tf_worksans);
        TextView txt_aboutus = rootView.findViewById(R.id.txt_aboutus);
        txt_aboutus.setTypeface(tf_worksans);
        TextView txt_logout = rootView.findViewById(R.id.txt_logout);
        txt_logout.setTypeface(tf_worksans);
        TextView txt_share = rootView.findViewById(R.id.txt_share);
        txt_share.setTypeface(tf_worksans);
        txt_nameuser = rootView.findViewById(R.id.txt_nameuser);
        txt_nameuser.setTypeface(tf_opensense_regular);
        txt_profile = rootView.findViewById(R.id.txt_profile);
        txt_profile.setTypeface(tf_worksans);
        img_profile = rootView.findViewById(R.id.img_profile);

        rl_profile = rootView.findViewById(R.id.rl_profile);
        ll_profile = rootView.findViewById(R.id.ll_profile);
    }

    private void setupMenu() {
        final LinearLayout ll_aboutus = rootView.findViewById(R.id.ll_aboutus);
        final LinearLayout ll_terms = rootView.findViewById(R.id.ll_terms);
        final LinearLayout ll_email = rootView.findViewById(R.id.ll_email);
        final LinearLayout ll_whatsapp = rootView.findViewById(R.id.ll_whatsapp);
        final LinearLayout ll_share = rootView.findViewById(R.id.ll_share);
        LinearLayout ll_signout = rootView.findViewById(R.id.ll_signout);

        ll_aboutus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iv = new Intent(getActivity(), Aboutus.class);
                startActivity(iv);

            }
        });

        ll_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent iv = new Intent(getActivity(), Termcondition.class);
                startActivity(iv);
            }
        });

        ll_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("mailto:" + "syariaidofficial@gmail.com")
                        .buildUpon()
                        .appendQueryParameter("subject", "[Syariaid] Feedback")
                        .appendQueryParameter("body", "")
                        .build();

                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, uri);
                startActivity(Intent.createChooser(emailIntent, "Kirim Email"));
            }
        });

        ll_whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "https://api.whatsapp.com/send?phone="+"+6285716204599"; //"+6282298081341";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        ll_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sharee = "Mau konsultasi dan bertanya langsung ke ustadz/ustadzah via chat tentang Islam, rumah tangga, jodoh, karier, dll? Yuk download aplikasi Syariaid\nhttps://play.google.com/store/apps/details?id=santri.syaria";
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                share.putExtra(Intent.EXTRA_SUBJECT, "Syariaid");
                share.putExtra("android.intent.extra.TEXT", sharee);
                startActivity(Intent.createChooser(share, "Share Syariaid App!"));
            }
        });

        SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, getActivity().MODE_PRIVATE);
        if (prefs.getString("userid", null) != null)


        {
            Log.d("userid",prefs.getString("userid", null));
            String userId = prefs.getString("userid", null);
            String image = prefs.getString("imagepath", null);
            String profileimage = prefs.getString("imageprofile", null);

            Log.e("image121", "" + profileimage);
            if (Objects.equals(userId, "delete")) {
                ll_signout.setVisibility(View.GONE);
                txt_nameuser.setText(R.string.txt_signin);
                txt_profile.setText(R.string.txt_profile);
                ll_profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent iv = new Intent(getActivity(), Login.class);
                        startActivity(iv);
                    }
                });
                rl_profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent iv = new Intent(getActivity(), Login.class);
                        startActivity(iv);
                    }
                });
            } else


            {
                String uname = prefs.getString("username", null);
                try {
                    Picasso.with(getActivity().getApplicationContext())
                            .load(profileimage)
                            .into(img_profile);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }

                ll_signout.setVisibility(View.VISIBLE);
                ll_signout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create(); // Read
                        // Update
                        alertDialog.setTitle(getString(R.string.log_out));
                        alertDialog.setMessage(getString(R.string.error_logout));
                        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.conti), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // here you can add functions
                                //if (LoginManager.getInstance() != null) {
                                //    LoginManager.getInstance().logOut();
                                //}
                                String prodel = "delete";
                                String userid = "delete";
                                SharedPreferences.Editor editor = getActivity().getSharedPreferences(MY_PREFS_NAME, getActivity().MODE_PRIVATE).edit();
                                editor.putString("delete", "" + prodel);
                                editor.putString("userid", "" + userid);
                                editor.putBoolean("activeuserchat",false);
                                editor.apply();
                                //signout Firebase chat
                                ((HomeMenuActivity)getActivity()).logout(getActivity());
                                Intent iv = new Intent(getActivity(), HomeMenuActivity.class); //MainMenuActivity.class);
                                startActivity(iv);
                            }
                        });

                        alertDialog.show();
                        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.black));

                    }


                });
                txt_nameuser.setText(uname);
                txt_profile.setText(R.string.txt_profile);
                txt_profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent iv = new Intent(getActivity(), Profile.class);
                        startActivity(iv);
                    }
                });
            }
        } else {
            ll_signout.setVisibility(View.GONE);
            txt_profile.setText(R.string.txt_profile);
            txt_nameuser.setText(R.string.txt_signin);
            txt_nameuser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent iv = new Intent(getActivity(), Login.class);
                    startActivity(iv);
                }
            });
        }

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
