package santri.firebasechat;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import santri.firebasechat.adapters.GroupsMessageAdapters;
import santri.firebasechat.constants.IDialogListener;
import santri.firebasechat.fcm.APIService;
import santri.firebasechat.fcm.RetroClient;
import santri.firebasechat.fcmmodels.Data;
import santri.firebasechat.fcmmodels.GroupSender;
import santri.firebasechat.fcmmodels.MyResponse;
import santri.firebasechat.fcmmodels.Notification;
import santri.firebasechat.fcmmodels.Sender;
import santri.firebasechat.fcmmodels.Token;
import santri.firebasechat.managers.Utils;
import santri.firebasechat.models.Chat;
import santri.firebasechat.models.Groups;
import santri.firebasechat.models.User;
import santri.syaria.R;
import santri.syaria.WebViewActivity;
import santri.syaria.retrofit.ResponseChatStatus;
import santri.syaria.retrofit.RestAPI;
import santri.syaria.retrofit.RetrofitAdapter;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static santri.firebasechat.constants.IConstants.EXTRA_DATETIME;
import static santri.firebasechat.constants.IConstants.EXTRA_GROUPS_IN_BOTH;
import static santri.firebasechat.constants.IConstants.EXTRA_GROUP_ID;
import static santri.firebasechat.constants.IConstants.EXTRA_IMGPATH;
import static santri.firebasechat.constants.IConstants.EXTRA_LAST_MSG;
import static santri.firebasechat.constants.IConstants.EXTRA_LAST_TIME;
import static santri.firebasechat.constants.IConstants.EXTRA_MESSAGE;
import static santri.firebasechat.constants.IConstants.EXTRA_OBJ_GROUP;
import static santri.firebasechat.constants.IConstants.EXTRA_RECEIVER;
import static santri.firebasechat.constants.IConstants.EXTRA_SEEN;
import static santri.firebasechat.constants.IConstants.EXTRA_SENDER;
import static santri.firebasechat.constants.IConstants.EXTRA_TYPE;
import static santri.firebasechat.constants.IConstants.FALSE;
import static santri.firebasechat.constants.IConstants.FCM_URL;
import static santri.firebasechat.constants.IConstants.REF_GROUPS;
import static santri.firebasechat.constants.IConstants.REF_GROUPS_MESSAGES;
import static santri.firebasechat.constants.IConstants.REF_GROUPS_S;
import static santri.firebasechat.constants.IConstants.REF_GROUP_MEMBERS_S;
import static santri.firebasechat.constants.IConstants.REF_GROUP_PHOTO_UPLOAD;
import static santri.firebasechat.constants.IConstants.REF_TOKENS;
import static santri.firebasechat.constants.IConstants.REF_USERS;
import static santri.firebasechat.constants.IConstants.REQUEST_EDIT_GROUP;
import static santri.firebasechat.constants.IConstants.REQUEST_PARTICIPATE;
import static santri.firebasechat.constants.IConstants.SLASH;
import static santri.firebasechat.constants.IConstants.TRUE;
import static santri.firebasechat.constants.IConstants.TWO;
import static santri.firebasechat.constants.IConstants.TYPE_IMAGE;
import static santri.firebasechat.constants.IConstants.TYPE_TEXT;

public class GroupsMessageActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mImageView, imgAvatar;
    private TextView txtGroupName, txtTyping;
    private RecyclerView mRecyclerView;
    private ProgressBar progress_bar;
    private Groups groups;
    private String groupId, groupName = "", userName = "Sender";
    private AppCompatEditText mEditTextChat;
    private FloatingActionButton mButtonChatSend;
    private Toolbar mToolbar;
    private Intent intent;
    private ArrayList<Chat> chats;
    private GroupsMessageAdapters messageAdapters;

    private Map<String, User> userList;
    private List<String> memberList;

    private APIService apiService;

    boolean notify = false;

    private String strUsername, strGroups;
    private Uri imageUri = null;
    private StorageTask uploadTask;
    private FirebaseStorage storage;
    private StorageReference storageReference;

    private static final String MY_PREFS_NAME = "Fooddelivery";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groups_message);

        mActivity = this;

        apiService = RetroClient.getClient(FCM_URL).create(APIService.class);

        imgAvatar = findViewById(R.id.imgAvatar);
        mImageView = findViewById(R.id.imageView);
        txtTyping = findViewById(R.id.txtTyping);
        txtGroupName = findViewById(R.id.txtGroupName);
        mToolbar = findViewById(R.id.toolbar);
        mRecyclerView = findViewById(R.id.recyclerView);
        mEditTextChat = findViewById(R.id.editTextChat);
        mButtonChatSend = findViewById(R.id.buttonChatSend);
        progress_bar = findViewById(R.id.progress_bar);

        txtTyping.setText(getString(R.string.strGroupInfo));

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putBoolean("isChannelLast", true);
        editor.apply();
        editor.commit();
    }

    private void initPage() {
        progress_bar.setVisibility(View.VISIBLE);
        if (checkingPartnerSignIn()) {
            Log.e("Action:","initfirebase");
            initFirebase();
            findViewById(R.id.llChatBottom).setVisibility(View.VISIBLE);
            findViewById(R.id.banner_infaq).setVisibility(View.INVISIBLE);
        } else {
            Log.e("Action:","checkChatSubscription");
            checkChatSubscription();
            findViewById(R.id.llChatBottom).setVisibility(View.INVISIBLE);
            findViewById(R.id.banner_infaq).setVisibility(View.VISIBLE);
        }
    }

    private boolean checkingPartnerSignIn() {
        //getting shared preference
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        Log.e("user", "" + prefs.getBoolean("isDeliverAccountActive", false));
        // check user is created or not
        // if user is already logged in
        if (prefs.getBoolean("isDeliverAccountActive", false)) {
            //String userid = prefs.getString("userid", null);
            //return !userid.equals("delete");
            return true;
        } else {
            return false;
        }
    }

    private void checkChatSubscription() {
        SharedPreferences sp = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String userid = sp.getString("userid", "");
        String subStatus = sp.getString("chatStatus",null);
        String timeStamp = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
        String chatStatus = subStatus!=null ? subStatus.split("\\|")[0] : "0";
        String lastCheck = subStatus!=null ? subStatus.split("\\|")[1] : timeStamp;
        String useridCheck = subStatus!=null ? subStatus.split("\\|")[2] : "0";

        Log.e("chat status",chatStatus+"|"+lastCheck+"|"+useridCheck);

        if (chatStatus.equals("1") && lastCheck.equals(timeStamp) && useridCheck.equals(userid)) {
            initFirebase();
        } else {
            getChatStatus(userid);
        }
    }

    private void getChatStatus(String userid) {
        //progressBar.setVisibility(View.VISIBLE);
        JSONObject jsonReq = new JSONObject();
        try {
            jsonReq.put("personid", userid);
            jsonReq.put("prodakid", "1");
            jsonReq.put("token", "385ef7144b8ebc2d48aa915056e8cca5");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonReq.toString());
        RestAPI api = RetrofitAdapter.createAPI();
        Call<ResponseChatStatus> callbackCall = api.getUserChatStatus(bodyRequest);
        callbackCall.enqueue(new Callback<ResponseChatStatus>() {
            @Override
            public void onResponse(Call<ResponseChatStatus> call, Response<ResponseChatStatus> response) {
                ResponseChatStatus resp = response.body();
                if (resp.status.equals("1")) {
                    String timeStamp = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
                    String chatStatus = "1";
                    String subStatus = chatStatus+"|"+timeStamp+"|"+userid;
                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putString("chatStatus", subStatus);
                    editor.apply();
                    editor.commit();
                    FirebaseMessaging.getInstance().subscribeToTopic("chat_users");
                    initFirebase();
                    //if (thread.isInterrupted()) {
                    //progressBar.setVisibility(View.INVISIBLE);
                    //return;
                    //}
                } else {
                    FirebaseMessaging.getInstance().unsubscribeFromTopic("chat_users");
                    AlertDialog alertDialog = new AlertDialog.Builder(GroupsMessageActivity.this, R.style.MyDialogTheme).create();
                    alertDialog.setTitle(getString(R.string.tanya_title));
                    alertDialog.setMessage(getString(R.string.tanya_ustad_msg));
                    // share on gmail,hike etc
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "INFAQ",
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int id) {
                                    Intent intent = new Intent(mActivity, WebViewActivity.class);
                                    intent.putExtra("url", resp.link);
                                    intent.putExtra("purpose","order");
                                    startActivity(intent);
                                }
                            });

                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Batal",
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int id) {
                                    alertDialog.cancel();
                                    finish();
                                }
                            });
                    //progressBar.setVisibility(View.INVISIBLE);
                    alertDialog.show();
                }
            }

            @Override
            public void onFailure(Call<ResponseChatStatus> call, Throwable t) {

            }
        });
    }

    private void initFirebase() {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        intent = getIntent();
        groups = (Groups) intent.getSerializableExtra(EXTRA_OBJ_GROUP);
        groupId = groups.getId();
        groupName = groups.getGroupName();
        strGroups = new Gson().toJson(groups);

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference(REF_GROUP_PHOTO_UPLOAD + SLASH + groupId);

        reference = FirebaseDatabase.getInstance().getReference(REF_USERS).child(firebaseUser.getUid());
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChildren()) {
                    User user = dataSnapshot.getValue(User.class);
                    strUsername = user.getUsername();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

//        try {
//            final String letter = String.valueOf(groups.getGroupName().charAt(0));
//            final TextDrawable drawable = TextDrawable.builder().buildRound(letter, Utils.getImageColor(groups.getGroupName()));
//            mImageView.setImageDrawable(drawable);
//        } catch (Exception e) {
//            Utils.getErrors(e);
//        }

        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setStackFromEnd(true);
        mRecyclerView.setLayoutManager(layoutManager);

        readGroupTitle();

        readMessages(groupId);

        mButtonChatSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txtMessage = mEditTextChat.getText().toString().trim();
                if (TextUtils.isEmpty(txtMessage)) {
                    screens.showToast(getString(R.string.strEmptyMsg));
                } else {
                    sendMessage(TYPE_TEXT, txtMessage);
                }
                mEditTextChat.setText("");
            }
        });


        final LinearLayout viewProfile = findViewById(R.id.viewProfile);
        viewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screens.openGroupParticipantActivity(groups);
            }
        });

        imgAvatar.setOnClickListener(this);

        InputMethodManager mImm = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        //mImm.showSoftInput(mEditTextChat, InputMethodManager.SHOW_IMPLICIT);
        mEditTextChat.setFocusable(true);
        mEditTextChat.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    mImm.showSoftInput(mEditTextChat, InputMethodManager.SHOW_IMPLICIT);
                else
                    mImm.hideSoftInputFromWindow(mEditTextChat.getWindowToken(), 0);
            }
        });

        mEditTextChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mImm.showSoftInput(mEditTextChat, InputMethodManager.SHOW_IMPLICIT);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgAvatar:
                openImageCropper();
                break;
        }
    }

    private void openImageCropper() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setFixAspectRatio(true)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .start(this);
    }

    private void readGroupTitle() {
        groupName = groups.getGroupName();
        txtGroupName.setText(groupName);
        Utils.setGroupImage(groups.getGroupImg(), mImageView, this);
    }

    private void sendMessage(String type, String message) {
        notify = true;
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        HashMap<String, Object> hashMap = new HashMap<>();
        final String date = Utils.getDateTime();
        hashMap.put(EXTRA_SENDER, firebaseUser.getUid());
        hashMap.put(EXTRA_RECEIVER, groupId);
        hashMap.put(EXTRA_TYPE, type);
        hashMap.put(EXTRA_MESSAGE, message);

        if (type.equalsIgnoreCase(TYPE_TEXT)) {


        } else if (type.equalsIgnoreCase(TYPE_IMAGE)) {

            hashMap.put(EXTRA_IMGPATH, message);

        }

        hashMap.put(EXTRA_SEEN, FALSE);
        hashMap.put(EXTRA_DATETIME, date);

        reference.child(REF_GROUPS_MESSAGES + SLASH + groupId).push().setValue(hashMap);

        HashMap<String, Object> mapGroupLastMsg = new HashMap<>();
        mapGroupLastMsg.put(EXTRA_LAST_MSG, message);
        mapGroupLastMsg.put(EXTRA_TYPE, type);
        mapGroupLastMsg.put(EXTRA_LAST_TIME, date);

        reference.child(REF_GROUPS + SLASH + groupId).updateChildren(mapGroupLastMsg);

        Utils.chatSendSound(getApplicationContext());

        try {
            final String msg = message.length() > 300 ? message.substring(0,300)+"... lanjut >" : message;

            final List<String> memberList = groups.getMembers();

            memberList.remove(firebaseUser.getUid());//Remove current Logged in ID from list, cause no need to send notification to us.
            if (notify) {
                //for (int i = 0; i < memberList.size(); i++) {

                    //final String memReceiver = memberList.get(i);

                    //sendNotification(memReceiver, strUsername, msg, type);
                    sendNotification(strUsername, msg, type);
                //}
                notify = false;
            }
        } catch (Exception e) {
        }

    }

    private void sendNotification(final String username, final String message, final String type) {
        Log.e("send notification ===== ", username+" "+message+" "+type);
        Log.e("user id",firebaseUser.getUid());
        final Data data = new Data(firebaseUser.getUid(), R.drawable.ic_stat_ic_notification, username, message, getString(R.string.strNewGroupMessage), "/topics/chat_users", strGroups, type);
        final Notification notification = new Notification(".Channel", getString(R.string.strNewGroupMessage), message);
        final GroupSender sender = new GroupSender(notification, data, "/topics/chat_users");

        apiService.sendNotification(sender).enqueue(new Callback<MyResponse>() {
            @Override
            public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                Log.e("response fcm",response.toString());
                Log.e("response fcm body", response.body().success+"");
                if (response.code() == 200) {
                    if (response.body().success != 1) {
                        //Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<MyResponse> call, Throwable t) {

            }
        });
    }

    private void sendNotification(final String receiver, final String username, final String message, final String type) {
        DatabaseReference tokenRef = FirebaseDatabase.getInstance().getReference(REF_TOKENS);
        Query query = tokenRef.orderByKey().equalTo(receiver);

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChildren()) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Token token = snapshot.getValue(Token.class);

                        final Data data = new Data(firebaseUser.getUid(), R.drawable.ic_stat_ic_notification, username, message, getString(R.string.strNewGroupMessage), receiver, strGroups, type);

                        final Sender sender = new Sender(data, token.getToken());

                        apiService.sendNotification(sender).enqueue(new Callback<MyResponse>() {
                            @Override
                            public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                                if (response.code() == 200) {
                                    if (response.body().success != 1) {
                                        //Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<MyResponse> call, Throwable t) {

                            }
                        });

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    private void readMessages(final String groupId) {
        chats = new ArrayList<>();

        reference = FirebaseDatabase.getInstance().getReference(REF_GROUPS_MESSAGES + SLASH + groupId);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                chats.clear();
                if (dataSnapshot.hasChildren()) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        try {
                            Chat chat = snapshot.getValue(Chat.class);
                            if (!Utils.isEmpty(chat.getMessage())) {
                                chats.add(chat);
                            }

                        } catch (Exception e) {
                        }

                    }
                }
                readUsers();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void readUsers() {
        userList = new HashMap<>();
        memberList = groups.getMembers();

        Query query = FirebaseDatabase.getInstance().getReference(REF_USERS);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userList.clear();
                if (dataSnapshot.hasChildren()) {
                    // for (int i = 0; i < memberList.size(); i++) {
                    for (int i = 0; i < chats.size(); i++) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            final User user = snapshot.getValue(User.class);
                            //if (memberList.get(i).equalsIgnoreCase(user.getId())) {
                            if (chats.get(i).getSender().equalsIgnoreCase(user.getId())) {
                                userList.put(user.getId(), user);
                                break;
                            }
                        }
                    }
                }
                try {
                    messageAdapters = new GroupsMessageAdapters(mActivity, chats, userList);
                    mRecyclerView.setAdapter(messageAdapters);
                    progress_bar.setVisibility(View.INVISIBLE);
                } catch (Exception e) {
                    Utils.getErrors(e);
                }
                //readGroupMembers();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void readGroupMembers() {
        userList = Utils.sortByUser(userList, FALSE);
        String u = "";
        for (User user : userList.values()) {
            if (!user.getId().equalsIgnoreCase(firebaseUser.getUid())) {
                u = u + (user.getUsername() + ", ");
            }
        }
        txtTyping.setText(u + getString(R.string.strYou));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_groups, menu);
        MenuItem item = menu.findItem(R.id.itemAddGroup);
        item.setVisible(false);

        if (!checkingPartnerSignIn()) {
            MenuItem itemViewUser = menu.findItem(R.id.itemGroupInfo);
            MenuItem itemAdd = menu.findItem(R.id.itemAddGroup);
            MenuItem itemEdit = menu.findItem(R.id.itemEditGroup);
            MenuItem itemLeave = menu.findItem(R.id.itemLeaveGroup);
            MenuItem itemDelete = menu.findItem(R.id.itemDeleteGroup);
            MenuItem itemClear = menu.findItem(R.id.itemClearMyChats);

            itemAdd.setVisible(false);
            itemEdit.setVisible(false);
            itemLeave.setVisible(false);
            itemDelete.setVisible(false);
            itemClear.setVisible(false);
            itemViewUser.setTitle(R.string.strUserInfo);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itemGroupInfo:
                screens.openGroupParticipantActivity(groups);
                break;
            case R.id.itemClearMyChats:

                Utils.showYesNoDialog(mActivity, R.string.strDelete, R.string.strDeleteOwnChats, new IDialogListener() {
                    @Override
                    public void yesButton() {
                        showProgress();
                        deleteOwnChats(FALSE);//False means don't close current screen, just delete my own chats
                    }
                });

                break;

            case R.id.itemEditGroup:
                if (isAdmin()) {
                    final Intent intent = new Intent(mActivity, GroupsAddActivity.class);
                    intent.putExtra(EXTRA_GROUP_ID, groupId);
                    intent.putExtra(EXTRA_OBJ_GROUP, groups);
                    startActivityForResult(intent, REQUEST_EDIT_GROUP);
                } else {
                    screens.showToast(getString(R.string.msgOnlyAdminEdit));
                }

                break;

            case R.id.itemLeaveGroup:

                Utils.showYesNoDialog(mActivity, R.string.strLeave, R.string.strLeaveFromGroup, new IDialogListener() {
                    @Override
                    public void yesButton() {
                        showProgress();
                        if (isAdmin()) {

                            if (groups.getMembers().size() >= TWO) {//Make other Person to Admin for this group cause more than 2 person available.

                                String newAdminId = groups.getMembers().get(1);//Make Admin to next USER.

                                groups.setAdmin(newAdminId);

                                groups.getMembers().remove(firebaseUser.getUid());

                                leaveFromGroup(TRUE);//True means close current screen, cause first we leave from group and than delete own chats

                            } else {//You are alone in this Groups. So Delete group and its DATA.

                                deleteWholeGroupsData(); // In this case only groups have Single User and can delete whole groups data.

                            }

                        } else {

                            List<String> removeId = groups.getMembers();
                            removeId.remove(firebaseUser.getUid());
                            groups.setMembers(removeId);

                            leaveFromGroup(TRUE);//True means close current screen, cause first we leave from group and than delete own chats

                        }
                    }
                });


                break;

            case R.id.itemDeleteGroup:
                if (isAdmin()) {
                    Utils.showYesNoDialog(mActivity, R.string.strDelete, R.string.strDeleteWholeGroup, new IDialogListener() {
                        @Override
                        public void yesButton() {
                            showProgress();
                            deleteWholeGroupsData();// Delete whole /groups, /messagesGroups and /membersJoined for that groups
                        }
                    });
                } else {
                    screens.showToast(getString(R.string.msgOnlyAdminDelete));
                }
                break;
        }
        return true;
    }

    private void leaveFromGroup(final boolean isFinishActivity) {
        //Remove from Main Group info /groupId/members/<removeId>
        FirebaseDatabase.getInstance().getReference().child(REF_GROUPS_S + groupId).setValue(groups).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                //Remove from MembersGroup/groupsIn/<groupId>
                FirebaseDatabase.getInstance().getReference().child(REF_GROUP_MEMBERS_S + firebaseUser.getUid() + EXTRA_GROUPS_IN_BOTH + groupId).removeValue()
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                hideProgress();
                                deleteOwnChats(isFinishActivity);//True means close current screen, cause first we leave from group and than delete own chats
                            }
                        });
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                hideProgress();
            }
        });
    }

    private void deleteWholeGroupsData() {
        final int members = groups.getMembers().size();
        FirebaseDatabase.getInstance().getReference().child(REF_GROUPS_S + groupId).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                for (int i = 0; i < members; i++) {
                    FirebaseDatabase.getInstance().getReference().child(REF_GROUP_MEMBERS_S + groups.getMembers().get(i) + EXTRA_GROUPS_IN_BOTH + groupId).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                        }
                    });
                    if (i == (members - 1)) {
                        hideProgress();
                        finish();
                    }
                }
                FirebaseDatabase.getInstance().getReference().child(REF_GROUPS_MESSAGES + SLASH + groupId).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                    }
                });
            }
        });
    }

    /**
     * False means don't close current screen, just delete my own chats
     * True  means close current screen, cause first we leave from group and than delete own chats
     */
    private void deleteOwnChats(final boolean isFinishActivity) {
        FirebaseDatabase.getInstance().getReference().child(REF_GROUPS_MESSAGES + SLASH + groupId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    if (dataSnapshot.exists()) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Chat chat = snapshot.getValue(Chat.class);
                            if (chat.getSender().equalsIgnoreCase(firebaseUser.getUid())) {

                                if (!Utils.isEmpty(chat.getType())) {
                                    if (chat.getType().equalsIgnoreCase(TYPE_IMAGE)) {
                                        StorageReference photoRef = storage.getReferenceFromUrl(chat.getImgPath());
                                        photoRef.delete();
                                    }
                                }
                                snapshot.getRef().removeValue();
                            }
                        }
                    }

                } catch (Exception e) {
                }
                hideProgress();
                if (isFinishActivity) {
                    finish();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void uploadImage() {
        final ProgressDialog pd = new ProgressDialog(mActivity);
        pd.setMessage(getString(R.string.msg_image_upload));
        pd.show();

        if (imageUri != null) {
            final StorageReference fileReference = storageReference.child(firebaseUser.getUid() + "_" + System.currentTimeMillis() + "." + Utils.getExtension(mActivity, imageUri));
            uploadTask = fileReference.putFile(imageUri);

            uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    return fileReference.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();
                        String mUrl = downloadUri.toString();
                        if (!Utils.isEmpty(groupId)) {
                            sendMessage(TYPE_IMAGE, mUrl);
                        }
                    } else {
                        Toast.makeText(mActivity, getString(R.string.msgFailedToUplod), Toast.LENGTH_LONG).show();
                    }
                    pd.dismiss();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Utils.getErrors(e);
                    Toast.makeText(mActivity, e.getMessage(), Toast.LENGTH_LONG).show();
                    pd.dismiss();
                }
            });
        } else {
            Toast.makeText(mActivity, "No Image selected!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_PARTICIPATE && resultCode == RESULT_FIRST_USER && data != null) {
            finish();
        }
        if (requestCode == REQUEST_EDIT_GROUP && resultCode == RESULT_OK && data != null) {
            groups = (Groups) data.getSerializableExtra(EXTRA_OBJ_GROUP);
            refreshGroupsData();
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                imageUri = result.getUri();
                if (uploadTask != null && uploadTask.isInProgress()) {
                    Toast.makeText(mActivity, mActivity.getString(R.string.msgUploadInProgress), Toast.LENGTH_LONG).show();
                } else {
                    uploadImage();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    private void refreshGroupsData() {
        readGroupTitle();
        readUsers();
    }

    private boolean isAdmin() {
        if (groups.getAdmin().equalsIgnoreCase(firebaseUser.getUid())) {
            return TRUE;
        }
        return FALSE;
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
//        FragmentManager fm = getFragmentManager();
//        if (fm.getBackStackEntryCount() > 0) {
//            Log.i("MainActivity", "popping backstack");
//            fm.popBackStack();
//        } else {
//            Log.i("MainActivity", "nothing on backstack, calling super");
//            super.onBackPressed();
//        }
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        initPage();
    }
}