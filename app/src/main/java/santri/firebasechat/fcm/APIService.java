package santri.firebasechat.fcm;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import santri.firebasechat.fcmmodels.GroupSender;
import santri.firebasechat.fcmmodels.MyResponse;
import santri.firebasechat.fcmmodels.Sender;

public interface APIService {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAARIK9ayc:APA91bHtj_IDPEK3x-en_RHtsu6swMiHLeKa45aZ2zL5oVxbKOXlR4Rq9d6ANI5cfHY6iwbl5Rq7yGpqMVUu7sA68UERx4FHRys6vA_8fKxGkwzpRY-2KFiAzcO9O_Tv_fq2b0IT7ZHN"
            }
    )
    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body Sender body);

    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAARIK9ayc:APA91bHtj_IDPEK3x-en_RHtsu6swMiHLeKa45aZ2zL5oVxbKOXlR4Rq9d6ANI5cfHY6iwbl5Rq7yGpqMVUu7sA68UERx4FHRys6vA_8fKxGkwzpRY-2KFiAzcO9O_Tv_fq2b0IT7ZHN"
            }
    )
    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body GroupSender body);
}
