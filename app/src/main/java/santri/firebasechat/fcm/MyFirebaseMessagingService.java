package santri.firebasechat.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

import androidx.core.app.NotificationCompat;
import santri.firebasechat.GroupsMessageActivity;
import santri.firebasechat.MainChatActivity;
import santri.firebasechat.MessageActivity;
import santri.firebasechat.managers.SessionManager;
import santri.firebasechat.managers.Utils;
import santri.firebasechat.models.Groups;
import santri.syaria.R;

import static santri.firebasechat.constants.IConstants.EXTRA_OBJ_GROUP;
import static santri.firebasechat.constants.IConstants.EXTRA_USER_ID;
import static santri.firebasechat.constants.IConstants.FCM_BODY;
import static santri.firebasechat.constants.IConstants.FCM_GROUPS;
import static santri.firebasechat.constants.IConstants.FCM_ICON;
import static santri.firebasechat.constants.IConstants.FCM_SENT;
import static santri.firebasechat.constants.IConstants.FCM_TITLE;
import static santri.firebasechat.constants.IConstants.FCM_TYPE;
import static santri.firebasechat.constants.IConstants.FCM_USER;
import static santri.firebasechat.constants.IConstants.FCM_USERNAME;
import static santri.firebasechat.constants.IConstants.TYPE_IMAGE;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private SessionManager session;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);

        Utils.uploadToken(s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if (remoteMessage == null)
            return;

        session = new SessionManager(getApplicationContext());
        if (!session.isNotificationOn()) {
            return;
        }
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            String sent = remoteMessage.getData().get(FCM_SENT)!=null ? remoteMessage.getData().get(FCM_SENT) : "";
            if (!ApplicationLifecycleManager.isAppVisible()) {
                FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                if (firebaseUser != null && sent.equalsIgnoreCase(firebaseUser.getUid())) {
                    sendNotification(remoteMessage);
                }
            }
        }

    }

    private Groups groups = null;
    private String strGroups = "";
    private Intent intent;
    private Bitmap bitmap = null;
    private String body = "";
    private String type = "";
    private String username = "";

    private void sendNotification(RemoteMessage remoteMessage) {
        final String user = remoteMessage.getData().get(FCM_USER);
        final String icon = remoteMessage.getData().get(FCM_ICON);
        final String title = remoteMessage.getData().get(FCM_TITLE);
        final String message = remoteMessage.getData().get(FCM_BODY);
        final String dailyNotif = remoteMessage.getData().get("daily_notif");
        try {
            type = remoteMessage.getData().get(FCM_TYPE);
            username = remoteMessage.getData().get(FCM_USERNAME);
        } catch (Exception e) {
        }
        try {
            strGroups = remoteMessage.getData().get(FCM_GROUPS);
        } catch (Exception e) {
        }

        bitmap = null;

        if (!Utils.isEmpty(type)) {
            if (type.equalsIgnoreCase(TYPE_IMAGE)) {
                bitmap = getBitmapFromURL(message);
                body = String.format(getString(R.string.strPhotoSent), username);
            } else {
                body = username + ": " + message;
            }
        } else {
            body = message;
        }

        Bundle bundle = new Bundle();

        if (Utils.isEmpty(strGroups)) {
            intent = new Intent(this, MessageActivity.class);
            //intent = new Intent(this, MainChatActivity.class);
            bundle.putString(EXTRA_USER_ID, user);
        } else {
            intent = new Intent(this, GroupsMessageActivity.class);
            groups = new Gson().fromJson(strGroups, Groups.class);
            bundle.putSerializable(EXTRA_OBJ_GROUP, groups);
        }

        intent.putExtras(bundle);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);
        notificationBuilder
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(body)
                .setTicker(body)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        if (bitmap != null) {
            notificationBuilder.setLargeIcon(bitmap);
            notificationBuilder.setStyle(new NotificationCompat.BigPictureStyle()
                    .bigPicture(bitmap)
                    .bigLargeIcon(bitmap));
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, "Chat Notification", NotificationManager.IMPORTANCE_DEFAULT);
            channel.setShowBadge(true);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify((int) new Date().getTime(), notificationBuilder.build());
    }

    /**
     * Downloading push notification image before displaying it in
     * the notification tray
     */
    private Bitmap getBitmapFromURL(String strURL) {
        try {
            final URL url = new URL(strURL);
            final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            final InputStream input = connection.getInputStream();
            final Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (Exception e) {
            Utils.getErrors(e);
            return null;
        }
    }

}
