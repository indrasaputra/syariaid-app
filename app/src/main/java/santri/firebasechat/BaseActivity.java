package santri.firebasechat;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;

import androidx.appcompat.app.AppCompatActivity;
import santri.firebasechat.managers.Screens;
import santri.firebasechat.managers.Utils;
import santri.syaria.R;

public class BaseActivity extends AppCompatActivity {

    public Activity mActivity;
    public FirebaseAuth auth; //Auth init
    public DatabaseReference reference; //Database releated
    public FirebaseUser firebaseUser; //Current User
    public Screens screens;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = this;
        screens = new Screens(mActivity);
    }

    private ProgressDialog pd = null;

    public void showProgress() {
        try {
            if (pd == null) {
                pd = new ProgressDialog(mActivity);
            }
            pd.setMessage(getString(R.string.please_wait));
            pd.show();
        } catch (Exception e) {
            Utils.getErrors(e);
        }
    }

    public void hideProgress() {
        try {
            if (pd != null) {
                pd.dismiss();
                pd = null;
            }
        } catch (Exception e) {
            Utils.getErrors(e);
        }
    }
}
