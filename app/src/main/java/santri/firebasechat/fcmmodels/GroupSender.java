package santri.firebasechat.fcmmodels;

public class GroupSender {
    private Notification notification;
    private Data data;
    private String to;

    public GroupSender(Notification notification, Data data, String to) {
        this.notification = notification;
        this.data = data;
        this.to = to;
    }
}
