package santri.firebasechat.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import santri.firebasechat.adapters.UserAdapters;
import santri.firebasechat.constants.IFilterListener;
import santri.firebasechat.managers.Utils;
import santri.firebasechat.models.Chat;
import santri.firebasechat.models.User;
import santri.syaria.R;
import santri.syaria.TanyaUstadzActivity;

import static santri.firebasechat.constants.IConstants.REF_CHATS;
import static santri.firebasechat.constants.IConstants.REF_USERS;
import static santri.firebasechat.constants.IConstants.SLASH;
import static santri.firebasechat.constants.IConstants.TRUE;
import static santri.firebasechat.managers.Utils.female;
import static santri.firebasechat.managers.Utils.male;
import static santri.firebasechat.managers.Utils.notset;
import static santri.firebasechat.managers.Utils.offline;
import static santri.firebasechat.managers.Utils.online;

public class ChatsFragment extends BaseFragment {

    private FirebaseUser firebaseUser;
    private ArrayList<String> userList;
    private RelativeLayout imgNoMessage;
    private String currentId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chats, container, false);

        final FloatingActionButton fabChat = view.findViewById(R.id.fabChat);
        imgNoMessage = view.findViewById(R.id.imgNoMessage);
        imgNoMessage.setVisibility(View.GONE);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView = view.findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(), layoutManager.getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        currentId = firebaseUser.getUid();
        Log.d("currentId chfragment",currentId);

        userList = new ArrayList<>();

        Query query = FirebaseDatabase.getInstance().getReference(REF_CHATS).child(currentId);
//        query.addListenerForSingleValueEvent(new ValueEventListener() {
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userList.clear();
                uList.clear();
                if (dataSnapshot.hasChildren()) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        userList.add(snapshot.getKey());
                        Log.d("snapshot key",snapshot.getKey());
                    }
                }

                if (userList.size() > 0) {
                    Log.d("userlist ada",userList.size()+"");
                    sortChats();
                } else {
                    Log.d("userlist","kosongggggg");
                    imgNoMessage.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

//        query.addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//                Log.d("on child added snapshot", dataSnapshot.getKey());
//                Log.d("on child added", "finish");
//            }
//
//            @Override
//            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//                Log.d("on child changed snapshot", dataSnapshot.getKey());
//                sortChats();
//            }
//
//            @Override
//            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
//                Log.d("on child added removed", dataSnapshot.getKey());
//            }
//
//            @Override
//            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//                Log.d("on child moved snapshot", dataSnapshot.getKey());
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });


//        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
//            @Override
//            public void onSuccess(InstanceIdResult instanceIdResult) {
//                String newToken = instanceIdResult.getToken();
//                Log.d("Firebase Token",newToken);
//                Utils.uploadToken(newToken);
//            }
//        });

//        fabChat.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                screens.showCustomScreen(UsersActivity.class);
//            }
//        });

        Button btn_start = view.findViewById(R.id.btn_start);
        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, TanyaUstadzActivity.class);
                intent.putExtra("catName","TanyaUstad");
                intent.putExtra("sub_category_name","Pengajian Umum");
                intent.putExtra("CityName","all");
                mContext.startActivity(intent);
            }
        });

        return view;
    }

    Map<String, Chat> uList = new HashMap<>();

    private void sortChats() {
        for (int i = 0; i < userList.size(); i++) {
            final String key = userList.get(i);
            Log.d("sortchat key",key);

            Query query = FirebaseDatabase.getInstance().getReference(REF_CHATS).child(currentId + SLASH + key).limitToLast(1);
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.hasChildren()) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Chat chat = snapshot.getValue(Chat.class);
                            uList.put(key, chat);
                            Log.d("ulist put", chat.getMessage());
                        }
                    }

                    if (uList.size() == userList.size()) {

                        if (uList.size() > 0) {
                            Log.d("uList size ===", uList.size()+"");
                            uList = Utils.sortByChatDateTime(uList, false);
                        }

                        userList = new ArrayList(uList.keySet());

                        readChats();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }

    }

    private void readChats() {
        mUsers = new ArrayList<>();

        for (int i = 0; i < userList.size(); i++) {
            final String key = userList.get(i);
            Log.d("i + users key",i + " - " +key);
            final int in = i;

            Query query = FirebaseDatabase.getInstance().getReference(REF_USERS).child(key);
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Log.d("dœatasnapshot count",in + " " +dataSnapshot.getChildrenCount());
                    if (dataSnapshot.hasChildren()) {
                        User user = dataSnapshot.getValue(User.class);
                        if (user.getId()!=null) {
                            Log.d("userId",user.getId());
                            Log.d("userList size, mUsers size",userList.size() + ", "+mUsers.size());
                            //if (mUsers.size()<=userList.size()) {
                                mUsers.add(user);
                            //}
                        }
                        Log.d("mUsers size",in + " " +mUsers.size());
                    }
                    if (in == userList.size()-1) {
                        if (mUsers.size() > 0) {
                            imgNoMessage.setVisibility(View.GONE);
                            mRecyclerView.setVisibility(View.VISIBLE);
                            userAdapters = new UserAdapters(getContext(), mUsers, TRUE);
                            mRecyclerView.setAdapter(userAdapters);
                        } else {
                            Log.d("mUsers","kosonggggg");
                            imgNoMessage.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            if (in == userList.size()-1){
                Log.d("try breaking loop in ================", in+"");
                break;
            }

        }



//        Query reference = Utils.getQuerySortBySearch();
//        reference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                mUsers.clear();
//                Log.d("onDataChange",userList.size()+"");
//                Log.d("user datasnapshot size", dataSnapshot.getChildrenCount()+"" );
//                if (dataSnapshot.hasChildren()) {
//                    try {
//                        for (String id : userList) {
//                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
//                                User user = snapshot.getValue(User.class);
//                                Log.d("string id",id);
//                                Log.d("userId",user.getId());
//                                if (user.getId().equalsIgnoreCase(id) && user.isActive()) {
//                                    //onlineOptionFilter(user);
//                                    Log.d("add user",user.getId());
//                                    mUsers.add(user);
//                                    break;
//                                } else {
//                                }
//                            }
//                        }
//                    } catch (Exception e) {
//                        //Utils.getErrors(e);
//                    }
//                }
//
//                if (mUsers.size() > 0) {
//                    imgNoMessage.setVisibility(View.GONE);
//                    mRecyclerView.setVisibility(View.VISIBLE);
//                    userAdapters = new UserAdapters(getContext(), mUsers, TRUE);
//                    mRecyclerView.setAdapter(userAdapters);
//                } else {
//                    Log.d("mUsers","kosonggggg");
//                    imgNoMessage.setVisibility(View.VISIBLE);
//                    mRecyclerView.setVisibility(View.GONE);
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });

    }

    private void onlineOptionFilter(final User user) {
        levelOptionFilter(user);
//        if (user.getStatus().equalsIgnoreCase(getString(R.string.strOnline))) {
//            if (online)
//                levelOptionFilter(user);
//        } else if (user.getStatus().equalsIgnoreCase(getString(R.string.strOffline))) {
//            if (offline)
//                levelOptionFilter(user);
//        }
    }

    private void levelOptionFilter(final User user) {
        addNewUserDataToList(user);
//        try {
//            if (Utils.isEmpty(user.getGender())) {
//                if (notset)
//                    addNewUserDataToList(user);
//            } else {
//                if (user.getGender().equalsIgnoreCase(getString(R.string.strMale))) {
//                    if (male)
//                        addNewUserDataToList(user);
//                } else if (user.getGender().equalsIgnoreCase(getString(R.string.strFemale))) {
//                    if (female)
//                        addNewUserDataToList(user);
//                }
//            }
//        } catch (Exception e) {
//            addNewUserDataToList(user);
//        }
    }

    private void addNewUserDataToList(User user) {
        mUsers.add(user);
    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.menu_filter, menu);
//
//        MenuItem searchItem = menu.findItem(R.id.itemFilter);
//
//        searchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                Utils.filterPopup(getActivity(), new IFilterListener() {
//                    @Override
//                    public void showFilterUsers() {
//                        readChats();
//                    }
//                });
//                return true;
//            }
//        });
//
//        super.onCreateOptionsMenu(menu, inflater);
//    }



}
