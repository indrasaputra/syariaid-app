package santri.firebasechat.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import santri.firebasechat.GroupsAddActivity;
import santri.firebasechat.adapters.GroupsAdapters;
import santri.firebasechat.managers.Utils;
import santri.firebasechat.models.Groups;
import santri.syaria.R;

import static santri.firebasechat.constants.IConstants.EXTRA_GROUPS_IN_BOTH;
import static santri.firebasechat.constants.IConstants.REF_GROUPS;
import static santri.firebasechat.constants.IConstants.REF_GROUP_MEMBERS_S;

public class GroupsFragment extends BaseFragment {

    private FirebaseUser firebaseUser;
    private ArrayList<String> groupList;
    private RelativeLayout imgNoMessage;
    private GroupsAdapters groupsAdapters;
    private ArrayList<Groups> mGroups;
    private static final String MY_PREFS_NAME = "Fooddelivery";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_groups, container, false);

        final FloatingActionButton fabGroupAdd = view.findViewById(R.id.fabGroupAdd);
        imgNoMessage = view.findViewById(R.id.imgNoMessage);
        imgNoMessage.setVisibility(View.GONE);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView = view.findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(), layoutManager.getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        groupList = new ArrayList<>();

        fabGroupAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screens.showCustomScreen(GroupsAddActivity.class);
            }
        });

        if (!checkingPartnerSignIn()) {
            fabGroupAdd.setVisibility(View.GONE);
        }

        readGroups();

        return view;
    }

    private boolean checkingPartnerSignIn() {
        //getting shared preference
        SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, 0);
        Log.e("user", "" + prefs.getBoolean("isDeliverAccountActive", false));
        // check user is created or not
        // if user is already logged in
        if (prefs.getBoolean("isDeliverAccountActive", false)) {
            //String userid = prefs.getString("userid", null);
            //return !userid.equals("delete");
            return true;
        } else {
            return false;
        }
    }

    private void readGroups() {
        mGroups = new ArrayList<>();

        Query reference = FirebaseDatabase.getInstance().getReference(REF_GROUPS);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mGroups.clear();
                Map<String, Groups> uList = new HashMap<>();
                if (dataSnapshot.hasChildren()) {
                    //Map<String, Groups> uList = new HashMap<>();
                    try {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Groups groups = snapshot.getValue(Groups.class);

                            Log.e("group id",groups.getId());
                            uList.put(groups.getId(), groups);
                        }
                    } catch (Exception e) {
                        Utils.getErrors(e);
                    }
                }

                if (uList.size() > 0) {
                    uList = Utils.sortByGroupDateTime(uList, false);

                    for (Groups groups : uList.values()) {
                        mGroups.add(groups);
                    }
                    imgNoMessage.setVisibility(View.GONE);
                    mRecyclerView.setVisibility(View.VISIBLE);

                    groupsAdapters = new GroupsAdapters(getContext(), mGroups);
                    mRecyclerView.setAdapter(groupsAdapters);
                } else {
                    imgNoMessage.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

}