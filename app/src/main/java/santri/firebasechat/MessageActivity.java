package santri.firebasechat;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import santri.firebasechat.adapters.MessageAdapters;
import santri.firebasechat.constants.IDialogListener;
import santri.firebasechat.fcm.APIService;
import santri.firebasechat.fcm.RetroClient;
import santri.firebasechat.fcmmodels.Data;
import santri.firebasechat.fcmmodels.MyResponse;
import santri.firebasechat.fcmmodels.Sender;
import santri.firebasechat.fcmmodels.Token;
import santri.firebasechat.managers.Utils;
import santri.firebasechat.models.Chat;
import santri.firebasechat.models.Others;
import santri.firebasechat.models.User;
import santri.syaria.R;
import santri.syaria.WebViewActivity;
import santri.syaria.retrofit.ResponseChatStatus;
import santri.syaria.retrofit.RestAPI;
import santri.syaria.retrofit.RetrofitAdapter;

import static santri.firebasechat.constants.IConstants.EXTRA_DATETIME;
import static santri.firebasechat.constants.IConstants.EXTRA_IMGPATH;
import static santri.firebasechat.constants.IConstants.EXTRA_MESSAGE;
import static santri.firebasechat.constants.IConstants.EXTRA_RECEIVER;
import static santri.firebasechat.constants.IConstants.EXTRA_SEEN;
import static santri.firebasechat.constants.IConstants.EXTRA_SENDER;
import static santri.firebasechat.constants.IConstants.EXTRA_TYPE;
import static santri.firebasechat.constants.IConstants.EXTRA_TYPING;
import static santri.firebasechat.constants.IConstants.EXTRA_TYPINGWITH;
import static santri.firebasechat.constants.IConstants.EXTRA_TYPING_DELAY;
import static santri.firebasechat.constants.IConstants.EXTRA_USER_ID;
import static santri.firebasechat.constants.IConstants.FALSE;
import static santri.firebasechat.constants.IConstants.FCM_URL;
import static santri.firebasechat.constants.IConstants.REF_CHATS;
import static santri.firebasechat.constants.IConstants.REF_CHAT_PHOTO_UPLOAD;
import static santri.firebasechat.constants.IConstants.REF_OTHERS;
import static santri.firebasechat.constants.IConstants.REF_TOKENS;
import static santri.firebasechat.constants.IConstants.REF_USERS;
import static santri.firebasechat.constants.IConstants.SLASH;
import static santri.firebasechat.constants.IConstants.TRUE;
import static santri.firebasechat.constants.IConstants.TYPE_IMAGE;
import static santri.firebasechat.constants.IConstants.TYPE_TEXT;

public class MessageActivity extends BaseActivity implements View.OnClickListener {
    private CircleImageView mImageView;
    private TextView mTxtUsername, txtTyping, txtRekening;
    private RelativeLayout llChatBottom, llInfaqButton;
    private RecyclerView mRecyclerView;
    private String currentId, userId, userName = "Sender";
    private String strSender, strReceiver;
    private AppCompatEditText mEditTextChat;
    private FloatingActionButton mButtonChatSend;
    private Toolbar mToolbar;
    private Intent intent;
    private ArrayList<Chat> chats;
    private MessageAdapters messageAdapters;

    private ValueEventListener seenListenerSender;
    private Query seenReferenceSender;

    private APIService apiService;

    boolean notify = false;

    private String onlineStatus, strUsername, strRekening;

    private ImageView imgAvatar;
    private Uri imageUri = null;
    private StorageTask uploadTask;
    private FirebaseStorage storage;
    private StorageReference storageReference;

    private static final String MY_PREFS_NAME = "Fooddelivery";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        mActivity = this;

        apiService = RetroClient.getClient(FCM_URL).create(APIService.class);

        imgAvatar = findViewById(R.id.imgAvatar);
        mImageView = findViewById(R.id.imageView);
        txtTyping = findViewById(R.id.txtTyping);
        mTxtUsername = findViewById(R.id.txtUsername);
        mToolbar = findViewById(R.id.toolbar);
        mRecyclerView = findViewById(R.id.recyclerView);
        mEditTextChat = findViewById(R.id.editTextChat);
        mButtonChatSend = findViewById(R.id.buttonChatSend);
        txtRekening = findViewById(R.id.txtRekening);
        llChatBottom = findViewById(R.id.llChatBottom);
        llInfaqButton = findViewById(R.id.llInfaqButton);

        txtTyping.setText("");

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putBoolean("isChannelLast", false);
        editor.apply();
        editor.commit();
    }

    private void initPage() {
        if (checkingPartnerSignIn()) {
            Log.e("Action:","initfirebase");
            llChatBottom.setVisibility(View.VISIBLE);
            llInfaqButton.setVisibility(View.INVISIBLE);
            initFirebase();
        } else {
            Log.e("Action:","checkChatSubscription");
            checkChatSubscription();
        }
    }

    private boolean checkingPartnerSignIn() {
        //getting shared preference
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        Log.e("user", "" + prefs.getBoolean("isDeliverAccountActive", false));
        // check user is created or not
        // if user is already logged in
        if (prefs.getBoolean("isDeliverAccountActive", false)) {
            //String userid = prefs.getString("userid", null);
            //return !userid.equals("delete");
            return true;
        } else {
            return false;
        }
    }

    private void checkChatSubscription() {
        SharedPreferences sp = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String userid = sp.getString("userid", "");
        String subStatus = sp.getString("chatStatus",null);
        String timeStamp = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
        String chatStatus = subStatus!=null ? subStatus.split("\\|")[0] : "0";
        String lastCheck = subStatus!=null ? subStatus.split("\\|")[1] : timeStamp;
        String useridCheck = subStatus!=null ? subStatus.split("\\|")[2] : "0";

        Log.e("chat status",chatStatus+"|"+lastCheck+"|"+useridCheck);

        if (chatStatus.equals("1") && lastCheck.equals(timeStamp) && useridCheck.equals(userid)) {
            llChatBottom.setVisibility(View.VISIBLE);
            llInfaqButton.setVisibility(View.INVISIBLE);
            initFirebase();
        } else {
            llChatBottom.setVisibility(View.INVISIBLE);
            llInfaqButton.setVisibility(View.VISIBLE);
            getChatStatus(userid);
        }
    }

    private void getChatStatus(String userid) {
        //progressBar.setVisibility(View.VISIBLE);
        JSONObject jsonReq = new JSONObject();
        try {
            jsonReq.put("personid", userid);
            jsonReq.put("prodakid", "1");
            jsonReq.put("token", "385ef7144b8ebc2d48aa915056e8cca5");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonReq.toString());
        RestAPI api = RetrofitAdapter.createAPI();
        Call<ResponseChatStatus> callbackCall = api.getUserChatStatus(bodyRequest);
        callbackCall.enqueue(new Callback<ResponseChatStatus>() {
            @Override
            public void onResponse(Call<ResponseChatStatus> call, Response<ResponseChatStatus> response) {
                ResponseChatStatus resp = response.body();
                if (resp.status.equals("1")) {
                    String timeStamp = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
                    String chatStatus = "1";
                    String subStatus = chatStatus+"|"+timeStamp+"|"+userid;
                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putString("chatStatus", subStatus);
                    editor.apply();
                    editor.commit();
                    FirebaseMessaging.getInstance().subscribeToTopic("chat_users");
                    llChatBottom.setVisibility(View.VISIBLE);
                    llInfaqButton.setVisibility(View.INVISIBLE);
                    initFirebase();
                    //if (thread.isInterrupted()) {
                    //progressBar.setVisibility(View.INVISIBLE);
                    //return;
                    //}
                } else {
                    llChatBottom.setVisibility(View.INVISIBLE);
                    llInfaqButton.setVisibility(View.VISIBLE);
                    initFirebase();
                    Button place_order = findViewById(R.id.btn_placeorder);
                    place_order.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(MessageActivity.this, WebViewActivity.class);
                            //intent.putExtra("url","http://syaria.id/order-produk/R1hMUklEcGprT0Y1akl3RFdZck9Kdz09");
                            intent.putExtra("url",resp.link);
                            intent.putExtra("purpose","order");
                            startActivity(intent);
                        }
                    });

//                    FirebaseMessaging.getInstance().unsubscribeFromTopic("chat_users");
//                    AlertDialog alertDialog = new AlertDialog.Builder(MessageActivity.this, R.style.MyDialogTheme).create();
//                    alertDialog.setTitle(getString(R.string.tanya_title));
//                    alertDialog.setMessage(getString(R.string.tanya_ustad_msg));
//                    // share on gmail,hike etc
//                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "INFAQ",
//                            new DialogInterface.OnClickListener() {
//
//                                public void onClick(DialogInterface dialog, int id) {
//                                    Intent intent = new Intent(mActivity, WebViewActivity.class);
//                                    intent.putExtra("url", resp.link);
//                                    intent.putExtra("purpose","order");
//                                    startActivity(intent);
//                                }
//                            });
//
//                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Batal",
//                            new DialogInterface.OnClickListener() {
//
//                                public void onClick(DialogInterface dialog, int id) {
//                                    alertDialog.cancel();
//                                }
//                            });
//                    //progressBar.setVisibility(View.INVISIBLE);
//                    alertDialog.show();
                }
            }

            @Override
            public void onFailure(Call<ResponseChatStatus> call, Throwable t) {

            }
        });
    }

    private void initFirebase() {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        currentId = firebaseUser.getUid();
        Log.d("currentId",currentId);

        reference = FirebaseDatabase.getInstance().getReference(REF_USERS).child(currentId);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChildren()) {
                    User user = dataSnapshot.getValue(User.class);
                    strUsername = user.getUsername();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        intent = getIntent();
        userId = intent.getStringExtra(EXTRA_USER_ID);
        Log.d("ustad userId",userId+"");

        strSender = currentId + SLASH + userId;
        strReceiver = userId + SLASH + currentId;

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference(REF_CHAT_PHOTO_UPLOAD + SLASH + strSender);

        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setStackFromEnd(true);
        mRecyclerView.setLayoutManager(layoutManager);

        reference = FirebaseDatabase.getInstance().getReference(REF_USERS).child(userId);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChildren()) {
                    final User user = dataSnapshot.getValue(User.class);
                    mTxtUsername.setText(user.getUsername());
                    userName = user.getUsername();
                    onlineStatus = user.getStatus();

                    txtTyping.setText(onlineStatus);

                    strRekening = user.getRekening();
                    if (strRekening!=null) {
                        txtRekening.setText("Infaq: "+strRekening);
                    } else {
                        ImageView image_banner = findViewById(R.id.banner_infaq);
                        image_banner.setVisibility(View.GONE);
                        txtRekening.setVisibility(View.GONE);
                    }

                    Utils.setProfileImage(getApplicationContext(), user.getImageURL(), mImageView);

                    readMessages(user.getImageURL());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mButtonChatSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txtMessage = mEditTextChat.getText().toString().trim();
                if (TextUtils.isEmpty(txtMessage)) {
                    screens.showToast(getString(R.string.strCantSendMsg));
                } else {
                    sendMessage(TYPE_TEXT, txtMessage);
                }
                mEditTextChat.setText("");
            }
        });

        InputMethodManager mImm = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        //mImm.showSoftInput(mEditTextChat, InputMethodManager.SHOW_IMPLICIT);
        mEditTextChat.setFocusable(true);
        mEditTextChat.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    mImm.showSoftInput(mEditTextChat, InputMethodManager.SHOW_IMPLICIT);
                else
                    mImm.hideSoftInputFromWindow(mEditTextChat.getWindowToken(), 0);
            }
        });

        mEditTextChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mImm.showSoftInput(mEditTextChat, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        final LinearLayout viewProfile = findViewById(R.id.viewProfile);
        viewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screens.openViewProfileActivity(userId);
            }
        });

        imgAvatar.setOnClickListener(this);

        Utils.uploadTypingStatus();
        typingListening();
        readTyping();
        seenMessage();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgAvatar:
                openImageCropper();
                break;
        }
    }

    private void openImageCropper() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setFixAspectRatio(true)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .start(this);
    }

    private void sendMessage(final String type, final String message) {
        notify = true;
        Log.d("notify sendMessage",notify+"");
        final String sender = currentId;
        final String receiver = userId;

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        HashMap<String, Object> hashMap = new HashMap<>();

        hashMap.put(EXTRA_SENDER, sender);
        hashMap.put(EXTRA_RECEIVER, receiver);
        hashMap.put(EXTRA_MESSAGE, message);
        hashMap.put(EXTRA_TYPE, type);

        if (type.equalsIgnoreCase(TYPE_TEXT)) {

        } else if (type.equalsIgnoreCase(TYPE_IMAGE)) {

            hashMap.put(EXTRA_IMGPATH, message);

        }

        hashMap.put(EXTRA_SEEN, FALSE);
        hashMap.put(EXTRA_DATETIME, Utils.getDateTime());

        reference.child(REF_CHATS).child(strSender).push().setValue(hashMap);
        reference.child(REF_CHATS).child(strReceiver).push().setValue(hashMap);

        Utils.chatSendSound(getApplicationContext());

        try {
            final String msg = message;

            if (notify) {
                sendNotification(receiver, strUsername, msg, type);
            }
            notify = false;
        } catch (Exception e) {
        }
    }

    private void sendNotification(String receiver, final String username, final String message, final String type) {
        Log.d("msg receiver",receiver);
        Log.d("msg username",username);
        Log.d("msg type",type);
        DatabaseReference tokenRef = FirebaseDatabase.getInstance().getReference(REF_TOKENS);
        Query query = tokenRef.orderByKey().equalTo(receiver);

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChildren()) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Token token = snapshot.getValue(Token.class);

                        final Data data = new Data(currentId, R.mipmap.ic_launcher_foreground, username, message, getString(R.string.strNewMessage), userId, type);

                        final Sender sender = new Sender(data, token.getToken());
                        Log.d("getToken",token.getToken());

                        apiService.sendNotification(sender).enqueue(new Callback<MyResponse>() {
                            @Override
                            public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                                Log.e("Response fcm", response.toString());
                                Log.e("response fcm body", response.body().success+"");
                                if (response.code() == 200) {
                                    if (response.body().success != 1) {
                                        //Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<MyResponse> call, Throwable t) {

                            }
                        });

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    private void readMessages(final String imageUrl) {
        chats = new ArrayList<>();

        reference = FirebaseDatabase.getInstance().getReference(REF_CHATS).child(strReceiver);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                chats.clear();
                if (dataSnapshot.hasChildren()) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        try {
                            Chat chat = snapshot.getValue(Chat.class);
                            if (!Utils.isEmpty(chat.getMessage())) {
                                chats.add(chat);
                            }

                        } catch (Exception e) {
                        }
                    }
                }
                try {
                    messageAdapters = new MessageAdapters(mActivity, chats, userName, imageUrl);
                    mRecyclerView.setAdapter(messageAdapters);
                } catch (Exception e) {
                    Utils.getErrors(e);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void seenMessage() {
        seenReferenceSender = FirebaseDatabase.getInstance().getReference(REF_CHATS).child(strSender).orderByChild(EXTRA_SEEN).equalTo(false);
        seenListenerSender = seenReferenceSender.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChildren()) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        try {
                            Chat chat = snapshot.getValue(Chat.class);
                            if (!Utils.isEmpty(chat.getMessage())) {
                                HashMap<String, Object> hashMap = new HashMap<>();
                                hashMap.put(EXTRA_SEEN, TRUE);
                                snapshot.getRef().updateChildren(hashMap);
                            }
                        } catch (Exception e) {
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_groups, menu);
        MenuItem itemViewUser = menu.findItem(R.id.itemGroupInfo);
        MenuItem itemAdd = menu.findItem(R.id.itemAddGroup);
        MenuItem itemEdit = menu.findItem(R.id.itemEditGroup);
        MenuItem itemLeave = menu.findItem(R.id.itemLeaveGroup);
        MenuItem itemDelete = menu.findItem(R.id.itemDeleteGroup);
        itemAdd.setVisible(false);
        itemEdit.setVisible(false);
        itemLeave.setVisible(false);
        itemDelete.setVisible(false);
        itemViewUser.setTitle(R.string.strUserInfo);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itemGroupInfo:
                screens.openViewProfileActivity(userId);
                break;
            case R.id.itemClearMyChats:
                Utils.showYesNoDialog(mActivity, R.string.strDelete, R.string.strDeleteOwnChats, new IDialogListener() {
                    @Override
                    public void yesButton() {
                        deleteOwnChats();
                    }
                });

                break;
        }
        return true;
    }

    /**
     * False means don't close current screen, just delete my own chats
     * True  means close current screen, cause first we leave from group and than delete own chats
     */
    private void deleteOwnChats() {
        showProgress();
        final Query chatsSender = FirebaseDatabase.getInstance().getReference(REF_CHATS).child(strSender).orderByChild(EXTRA_SENDER).equalTo(currentId);
        chatsSender.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    if (dataSnapshot.exists()) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Chat chat = snapshot.getValue(Chat.class);
                            if (!Utils.isEmpty(chat.getType())) {
                                if (chat.getType().equalsIgnoreCase(TYPE_IMAGE)) {
                                    StorageReference photoRef = storage.getReferenceFromUrl(chat.getImgPath());
                                    photoRef.delete();
                                }
                            }
                            snapshot.getRef().removeValue();
                        }
                    }
                    hideProgress();
                } catch (Exception e) {
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

        final Query chatsReceiver = FirebaseDatabase.getInstance().getReference(REF_CHATS).child(strReceiver).orderByChild(EXTRA_SENDER).equalTo(currentId);
        chatsReceiver.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    if (dataSnapshot.exists()) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            snapshot.getRef().removeValue();
                        }
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void readTyping() {
        reference = FirebaseDatabase.getInstance().getReference(REF_OTHERS).child(currentId);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    if (dataSnapshot.hasChildren()) {
                        Others user = dataSnapshot.getValue(Others.class);
                        if (user.isTyping() && user.getTypingwith().equalsIgnoreCase(userId)) {
                            txtTyping.setText(getString(R.string.strTyping));
                        } else {
                            txtTyping.setText(onlineStatus);
                        }
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void typingListening() {
        mEditTextChat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (s.length() == 0) {
                        stopTyping();
                    } else if (s.length() > 0) {
                        startTyping();
                        idleTyping(s.length());
                    }
                } catch (Exception e) {
                    stopTyping();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void idleTyping(final int currentLen) {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                int newLen = mEditTextChat.getText().length();
                if (currentLen == newLen) {
                    stopTyping();
                }

            }
        }, EXTRA_TYPING_DELAY);
    }

    private void startTyping() {
        typingStatus(TRUE);
    }

    private void stopTyping() {
        typingStatus(FALSE);
    }

    /**
     * typingStatus - Update typing and userId with db
     * isTyping = True means 'startTyping' method called
     * isTyping = False means 'stopTyping' method called
     */
    private void typingStatus(boolean isTyping) {
        try {
            reference = FirebaseDatabase.getInstance().getReference(REF_OTHERS).child(userId);
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put(EXTRA_TYPINGWITH, currentId);
            hashMap.put(EXTRA_TYPING, isTyping);
            reference.updateChildren(hashMap);
        } catch (Exception e) {
        }
    }

    private void uploadImage() {
        final ProgressDialog pd = new ProgressDialog(mActivity);
        pd.setMessage(getString(R.string.msg_image_upload));
        pd.show();

        if (imageUri != null) {
            final StorageReference fileReference = storageReference.child(System.currentTimeMillis() + "." + Utils.getExtension(mActivity, imageUri));
            uploadTask = fileReference.putFile(imageUri);

            uploadTask
                    .continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if (!task.isSuccessful()) {
                                throw task.getException();
                            }

                            return fileReference.getDownloadUrl();
                        }
                    })
                    .addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            if (task.isSuccessful()) {
                                final Uri downloadUri = task.getResult();
                                final String mUrl = downloadUri.toString();
                                sendMessage(TYPE_IMAGE, mUrl);
                            } else {
                                Toast.makeText(mActivity, getString(R.string.msgFailedToUplod), Toast.LENGTH_LONG).show();
                            }
                            pd.dismiss();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Utils.getErrors(e);
                    Toast.makeText(mActivity, e.getMessage(), Toast.LENGTH_LONG).show();
                    pd.dismiss();
                }
            });
        } else {
            Toast.makeText(mActivity, "No Image selected!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                imageUri = result.getUri();
                if (uploadTask != null && uploadTask.isInProgress()) {
                    Toast.makeText(mActivity, mActivity.getString(R.string.msgUploadInProgress), Toast.LENGTH_LONG).show();
                } else {
                    uploadImage();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            seenReferenceSender.removeEventListener(seenListenerSender);
            stopTyping();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            seenReferenceSender.removeEventListener(seenListenerSender);
            stopTyping();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //initializeViewPager();
        initPage();
    }
}
