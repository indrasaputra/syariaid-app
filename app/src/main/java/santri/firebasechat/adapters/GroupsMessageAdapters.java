package santri.firebasechat.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;
import santri.firebasechat.managers.Screens;
import santri.firebasechat.managers.Utils;
import santri.firebasechat.models.Chat;
import santri.firebasechat.models.User;
import santri.syaria.BuildConfig;
import santri.syaria.DetailPage;
import santri.syaria.R;

import static santri.firebasechat.constants.IConstants.TYPE_TEXT;

public class GroupsMessageAdapters extends RecyclerView.Adapter<GroupsMessageAdapters.ViewHolder> {

    private final int MSG_TYPE_RIGHT = 0;
    private final int MSG_TYPE_LEFT = 1;

    private Context mContext;
    private ArrayList<Chat> mChats;
    private FirebaseUser firebaseUser;
    private Map<String, User> userList;

    public GroupsMessageAdapters(Context mContext, ArrayList<Chat> usersList, final Map<String, User> userList) {
        this.mContext = mContext;
        this.mChats = usersList;
        this.userList = userList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == MSG_TYPE_RIGHT) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.list_group_chat_right, viewGroup, false);
            return new GroupsMessageAdapters.ViewHolder(view);
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.list_group_chat_left, viewGroup, false);
            return new GroupsMessageAdapters.ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        final Chat chat = mChats.get(position);

        if (Utils.isEmpty(chat.getType())) {
            viewHolder.txtShowMessage.setVisibility(View.VISIBLE);
            viewHolder.imgPath.setVisibility(View.GONE);
            viewHolder.txtShowMessage.setText(chat.getMessage());
        } else {
            if (chat.getType().equalsIgnoreCase(TYPE_TEXT)) {
                viewHolder.txtShowMessage.setVisibility(View.VISIBLE);
                viewHolder.imgPath.setVisibility(View.GONE);
                viewHolder.txtShowMessage.setText(chat.getMessage());
            } else {
                viewHolder.txtShowMessage.setVisibility(View.GONE);
                viewHolder.imgPath.setVisibility(View.VISIBLE);
                Utils.setChatImage(chat.getImgPath(), viewHolder.imgPath, mContext);
                viewHolder.imgPath.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Screens screens = new Screens(mContext);
                        screens.openFullImageViewActivity(v, chat.getImgPath());
                    }
                });
            }
        }

        viewHolder.txtOnlyDate.setVisibility(View.GONE);
        try {
            final long first = Utils.dateToMillis(mChats.get(position - 1).getDatetime());
            final long second = Utils.dateToMillis(chat.getDatetime());
            if (!Utils.hasSameDate(first, second)) {
                viewHolder.txtOnlyDate.setVisibility(View.VISIBLE);
                viewHolder.txtOnlyDate.setText(Utils.formatFullDate(chat.getDatetime()));
            }
        } catch (Exception e) {
            if (position == 0) {
                viewHolder.txtOnlyDate.setVisibility(View.VISIBLE);
                viewHolder.txtOnlyDate.setText(Utils.formatFullDate(chat.getDatetime()));
            }
        }

        final User user = userList.get(chat.getSender());

        switch (viewHolder.getItemViewType()) {
            case MSG_TYPE_LEFT:
                try {
                    viewHolder.txtName.setText(user.getUsername());
                    viewHolder.img_share.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String shareMsg = "";
                            String footerMsg = "\n\n"+user.getUsername()+"\n\nDownload Aplikasi konsultasi dengan Ustadz/Ustadzah di sini:\nhttps://play.google.com/store/apps/details?id=santri.syaria";
                            Intent share = new Intent(Intent.ACTION_SEND);
                            share.setType("text/plain");
                            share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                            share.putExtra(Intent.EXTRA_SUBJECT, "Syariaid");
                            if (chat.getType().equalsIgnoreCase(TYPE_TEXT)) {
                                shareMsg = viewHolder.txtShowMessage.getText().toString();
                            } else {
                                Uri bmpUri = getLocalBitmapUri(viewHolder.imgPath);
                                share.setType("image/*");
                                share.setType("image/jpeg");
                                share.putExtra(Intent.EXTRA_STREAM, bmpUri);
                            }
                            share.putExtra(Intent.EXTRA_TEXT, shareMsg+footerMsg);
                            mContext.startActivity(Intent.createChooser(share, "Share Dakwah Syariaid"));
                        }
                    });
                } catch (Exception e) {
                    Utils.getErrors(e);
                }
                break;
            case MSG_TYPE_RIGHT:
                break;
        }

        long timeMilliSeconds = 0;
        try {
            timeMilliSeconds = Utils.dateToMillis(chat.getDatetime());
        } catch (Exception e) {
        }

        if (timeMilliSeconds > 0) {
            viewHolder.txtMsgTime.setText(Utils.formatLocalTime(timeMilliSeconds));
        } else {
            viewHolder.txtMsgTime.setVisibility(View.GONE);
        }

        viewHolder.imgMsgSeen.setVisibility(View.GONE);

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView chatImageView;
        public TextView txtName;
        public TextView txtShowMessage;
        public TextView txtMsgTime;
        public ImageView imgMsgSeen;
        public TextView txtOnlyDate;
        public ImageView imgPath;
        public ImageView img_share;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txtOnlyDate = itemView.findViewById(R.id.txtOnlyDate);
            chatImageView = itemView.findViewById(R.id.chatImageView);
            txtShowMessage = itemView.findViewById(R.id.txtShowMessage);
            txtName = itemView.findViewById(R.id.txtName);
            imgMsgSeen = itemView.findViewById(R.id.imgMsgSeen);
            txtMsgTime = itemView.findViewById(R.id.txtMsgTime);
            imgPath = itemView.findViewById(R.id.imgPath);
            img_share = itemView.findViewById(R.id.img_share);
        }
    }

    @Override
    public int getItemCount() {
        return mChats.size();
    }

    @Override
    public int getItemViewType(int position) {
        final Chat chat = mChats.get(position);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        if (chat.getSender().equalsIgnoreCase(firebaseUser.getUid())) {
            return MSG_TYPE_RIGHT;
        } else {
            return MSG_TYPE_LEFT;
        }
    }

    private Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap numberOfRecords ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp;
        if (drawable instanceof BitmapDrawable) {
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            File file = new File(mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                    "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID + ".provider", file);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }
}
