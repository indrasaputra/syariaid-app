package santri.firebasechat.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import santri.firebasechat.GroupsMessageActivity;
import santri.firebasechat.managers.Screens;
import santri.firebasechat.managers.Utils;
import santri.firebasechat.models.Groups;
import santri.syaria.R;

import static santri.firebasechat.constants.IConstants.EXTRA_OBJ_GROUP;
import static santri.firebasechat.constants.IConstants.TYPE_IMAGE;

public class GroupsAdapters extends RecyclerView.Adapter<GroupsAdapters.ViewHolder> {

    private Context mContext;
    private ArrayList<Groups> mGroups;
    private Screens screens;

    public GroupsAdapters(Context mContext, ArrayList<Groups> groupsList) {
        this.mContext = mContext;
        this.mGroups = groupsList;
        screens = new Screens(mContext);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_groups, viewGroup, false);
        return new GroupsAdapters.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final Groups groups = mGroups.get(i);

        viewHolder.txtGroupName.setText(groups.getGroupName());

        try {
//            final String letter = String.valueOf(groups.getGroupName().charAt(0));
//            final TextDrawable drawable = TextDrawable.builder().buildRound(letter, Utils.getImageColor(groups.getGroupName()));
//            viewHolder.imageView.setImageDrawable(drawable);
            Utils.setGroupImage(groups.getGroupImg(), viewHolder.imageView, mContext);
        } catch (Exception e) {
        }

        viewHolder.txtLastMsg.setVisibility(View.VISIBLE);
        viewHolder.txtLastDate.setVisibility(View.VISIBLE);
        viewHolder.imgPhoto.setVisibility(View.GONE);
        try {
            if (Utils.isEmpty(groups.getType())) {
                if (Utils.isEmpty(groups.getLastMsg())) {
                    viewHolder.txtLastMsg.setText(mContext.getString(R.string.msgTapToStartChat));
                } else {
                    viewHolder.txtLastMsg.setText(groups.getLastMsg());
                }
            } else {
                if (groups.getType().equalsIgnoreCase(TYPE_IMAGE)) {
                    viewHolder.imgPhoto.setVisibility(View.VISIBLE);
                    viewHolder.txtLastMsg.setText(mContext.getString(R.string.lblPhoto));
                } else {
                    if (Utils.isEmpty(groups.getLastMsg())) {
                        viewHolder.txtLastMsg.setText(mContext.getString(R.string.msgTapToStartChat));
                    } else {
                        viewHolder.txtLastMsg.setText(groups.getLastMsg());

                    }
                }
            }
            if (Utils.isEmpty(groups.getLastMsgTime())) {
                viewHolder.txtLastDate.setText("");
            } else {
                viewHolder.txtLastDate.setText(Utils.formatDateTime(mContext, groups.getLastMsgTime()));
            }
        } catch (Exception e) {

        }

        viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screens.openGroupParticipantActivity(groups);
            }
        });

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, GroupsMessageActivity.class);
                intent.putExtra(EXTRA_OBJ_GROUP, groups);
                mContext.startActivity(intent);
            }
        });

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;
        public TextView txtGroupName;
        private TextView txtLastMsg;
        private TextView txtLastDate;
        private ImageView imgPhoto;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView);
            txtGroupName = itemView.findViewById(R.id.txtGroupName);
            txtLastMsg = itemView.findViewById(R.id.txtLastMsg);
            txtLastDate = itemView.findViewById(R.id.txtLastDate);
            imgPhoto = itemView.findViewById(R.id.imgPhoto);
        }
    }

    @Override
    public int getItemCount() {
        return mGroups.size();
    }
}
