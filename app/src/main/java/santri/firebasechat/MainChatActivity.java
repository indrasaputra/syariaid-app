package santri.firebasechat;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import santri.firebasechat.fcm.ApplicationLifecycleManager;
import santri.firebasechat.fragments.ChatsFragment;
import santri.firebasechat.fragments.GroupsFragment;
import santri.firebasechat.managers.Utils;
import santri.firebasechat.models.User;
import santri.syaria.DetailPage;
import santri.syaria.R;
import santri.syaria.TanyaUstadzActivity;
import santri.syaria.WebViewActivity;
import santri.syaria.retrofit.ResponseChatStatus;
import santri.syaria.retrofit.RestAPI;
import santri.syaria.retrofit.RetrofitAdapter;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import static santri.firebasechat.constants.IConstants.ADS_SHOWN;
import static santri.firebasechat.constants.IConstants.EXTRA_DELAY;
import static santri.firebasechat.constants.IConstants.REF_USERS;

public class MainChatActivity extends BaseActivity {

    private CircleImageView mImageView;
    private TextView mTxtUsername;
    private Toolbar mToolbar;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private long exitTime = 0;
    private final int DEFAULT_DELAY = 2000;
    private AdView mAdView;
    private static final String MY_PREFS_NAME = "Fooddelivery";

    private Fragment chatFragment;
    private Fragment groupsFragment;
    private Thread thread;
    private ProgressBar progressBar;
    private MainChatActivity myApp;
    private boolean runThread = false;
    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_chat);

        myApp = this;

        mImageView = findViewById(R.id.imageView);
        mTxtUsername = findViewById(R.id.txtUsername);
        mToolbar = findViewById(R.id.toolbar);
        progressBar = findViewById(R.id.progress_bar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle("");

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mTabLayout = findViewById(R.id.tabLayout);

        //initPage();
    }

    private void initPage() {
//        if (checkingPartnerSignIn()) {
//            Log.e("Action:","initfirebase");
//            initFirebase();
//        } else {
//            Log.e("Action:","checkChatSubscription");
//            checkChatSubscription();
//        }
        initFirebase();
    }

    private void checkChatSubscription() {
        SharedPreferences sp = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String userid = sp.getString("userid", "");
        String subStatus = sp.getString("chatStatus",null);
        String timeStamp = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
        String chatStatus = subStatus!=null ? subStatus.split("\\|")[0] : "0";
        String lastCheck = subStatus!=null ? subStatus.split("\\|")[1] : timeStamp;
        String useridCheck = subStatus!=null ? subStatus.split("\\|")[2] : "0";

        Log.e("chat status",chatStatus+"|"+lastCheck+"|"+useridCheck);

        if (chatStatus.equals("1") && lastCheck.equals(timeStamp) && useridCheck.equals(userid)) {
            initFirebase();
        } else {
            getChatStatus(userid);
        }
    }

    private void getChatStatus(String userid) {
        progressBar.setVisibility(View.VISIBLE);
//        runThread = true;
//        thread = new Thread(new Runnable() {
//            @Override
//            public void run() {
                //while (runThread) {
                    JSONObject jsonReq = new JSONObject();
                    try {
                        jsonReq.put("personid", userid);
                        jsonReq.put("prodakid", "1");
                        jsonReq.put("token", "385ef7144b8ebc2d48aa915056e8cca5");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonReq.toString());
                    RestAPI api = RetrofitAdapter.createAPI();
                    Call<ResponseChatStatus> callbackCall = api.getUserChatStatus(bodyRequest);
                    callbackCall.enqueue(new Callback<ResponseChatStatus>() {
                        @Override
                        public void onResponse(Call<ResponseChatStatus> call, Response<ResponseChatStatus> response) {
                            ResponseChatStatus resp = response.body();
                            if (resp.status.equals("1")) {
                                String timeStamp = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
                                String chatStatus = "1";
                                String subStatus = chatStatus+"|"+timeStamp+"|"+userid;
                                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                editor.putString("chatStatus", subStatus);
                                editor.apply();
                                editor.commit();
                                FirebaseMessaging.getInstance().subscribeToTopic("chat_users");
                                initFirebase();
                                //if (thread.isInterrupted()) {
                                    progressBar.setVisibility(View.INVISIBLE);
                                    //return;
                                //}
                            } else {
                                FirebaseMessaging.getInstance().unsubscribeFromTopic("chat_users");
                                AlertDialog alertDialog = new AlertDialog.Builder(MainChatActivity.this, R.style.MyDialogTheme).create();
                                alertDialog.setTitle(getString(R.string.tanya_title));
                                alertDialog.setMessage(getString(R.string.tanya_ustad_msg));
                                // share on gmail,hike etc
                                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "INFAQ",
                                        new DialogInterface.OnClickListener() {

                                            public void onClick(DialogInterface dialog, int id) {
                                                Intent intent = new Intent(myApp, WebViewActivity.class);
                                                intent.putExtra("url", resp.link);
                                                intent.putExtra("purpose","order");
                                                startActivity(intent);
                                            }
                                        });

                                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Batal",
                                        new DialogInterface.OnClickListener() {

                                            public void onClick(DialogInterface dialog, int id) {
                                                alertDialog.cancel();
                                            }
                                        });
                                progressBar.setVisibility(View.INVISIBLE);
                                alertDialog.show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseChatStatus> call, Throwable t) {

                        }
                    });
                //}
//            }
//        });
//        thread.start();
    }

    private void initFirebase() {
        if (thread!=null) {
            thread.interrupt();
        }
        Log.e("initFirebase","called!!! "+count++);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference(REF_USERS).child(firebaseUser.getUid());
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    if (dataSnapshot.hasChildren()) {
                        final User user = dataSnapshot.getValue(User.class);
                        mTxtUsername.setText(user.getUsername());
//                        if (Utils.isEmpty(user.getGender())) {
//                            Utils.selectGenderPopup(mActivity, firebaseUser.getUid(), "");
//                        } else {
//                        }

                        Utils.setProfileImage(getApplicationContext(), user.getImageURL(), mImageView);
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(2);
            }
        });
        initializeViewPager();
    }

    private void initializeViewPager() {
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        boolean isChannelLast = prefs.getBoolean("isChannelLast", false);
        Log.e("init View Pager","init view pager");
        mViewPager = findViewById(R.id.viewPager);
        chatFragment = new ChatsFragment();
        groupsFragment = new GroupsFragment();

        ViewPageAdapter viewPageAdapter = new ViewPageAdapter(getSupportFragmentManager());
        viewPageAdapter.addFragment(chatFragment, getString(R.string.strChats));
        viewPageAdapter.addFragment(groupsFragment, getString(R.string.strGroups));
//        viewPageAdapter.addFragment(new ProfileFragment(), getString(R.string.strProfile));

        mViewPager.setAdapter(viewPageAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setOffscreenPageLimit(viewPageAdapter.getCount() - 1);
        Intent i = getIntent();
        if (!checkingPartnerSignIn()) {
            if (!i.getBooleanExtra("from_home", false)) {
                mViewPager.setCurrentItem(1);
            }
            if (isChannelLast) {
                mViewPager.setCurrentItem(1);
            }
        }

        Button btn_choose = findViewById(R.id.btn_choose);
        btn_choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainChatActivity.this, TanyaUstadzActivity.class);
                intent.putExtra("catName","TanyaUstad");
                intent.putExtra("sub_category_name","Pengajian Umum");
                intent.putExtra("CityName","all");
            }
        });

        ImageView banner_infaq = findViewById(R.id.banner_infaq);
        banner_infaq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://kitabisa.com/sedekahustadzkonsultasionline"));
                startActivity(browserIntent);
            }
        });
    }

    private boolean checkingPartnerSignIn() {
        //getting shared preference
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        Log.e("user", "" + prefs.getBoolean("isDeliverAccountActive", false));
        // check user is created or not
        // if user is already logged in
        if (prefs.getBoolean("isDeliverAccountActive", false)) {
            //String userid = prefs.getString("userid", null);
            //return !userid.equals("delete");
            return true;
        } else {
            return false;
        }
    }

    class ViewPageAdapter extends FragmentPagerAdapter {

        public ArrayList<Fragment> fragments;
        public ArrayList<String> titles;

        public ViewPageAdapter(FragmentManager fm) {
            super(fm);
            fragments = new ArrayList<>();
            titles = new ArrayList<>();
        }

        @Override
        public Fragment getItem(int i) {
            return fragments.get(i);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        public void addFragment(Fragment fragment, String title) {
            fragments.add(fragment);
            titles.add(title);
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return titles.get(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//
//            case R.id.itemSettings:
//                screens.openSettingsActivity();
//                return true;
//
//            case R.id.itemLogout:
//                Utils.logout(mActivity);
//                return true;
//
//        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //exitApp();
        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putBoolean("isChannelLast", false);
        editor.apply();
        editor.commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Utils.readStatus(mActivity, getString(R.string.strOnline));
    }

    @Override
    protected void onPause() {
        super.onPause();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!ApplicationLifecycleManager.isAppVisible()) {
                    Utils.readStatus(mActivity, getString(R.string.strOffline));
                }
            }
        }, EXTRA_DELAY);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Utils.readStatus(mActivity, getString(R.string.strOffline));
    }

    @Override
    public void onResume() {
        super.onResume();
        //initializeViewPager();
        initPage();
    }
}
