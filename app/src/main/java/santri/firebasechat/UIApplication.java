package santri.firebasechat;

import android.app.Application;

import com.google.android.gms.ads.MobileAds;

import santri.firebasechat.fcm.ApplicationLifecycleManager;

public class UIApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        MobileAds.initialize(this);
        registerActivityLifecycleCallbacks(new ApplicationLifecycleManager());
    }
}
