package santri.video.data;

import android.app.Application;

import java.util.ArrayList;
import java.util.List;

import santri.video.model.Video;

public class ThisApplication extends Application {
    private static ThisApplication mInstance;
    private int countTotalVideo = 0;
    private List<Video> videos = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static synchronized ThisApplication getInstance() {
        return mInstance;
    }

    public void setCountTotalVideo(int countTotalVideo) {
        this.countTotalVideo = countTotalVideo;
    }

    public void addVideos(List<Video> videos) {
        this.videos.addAll(videos);
    }

    public void resetVideos() {
        this.videos.clear();
    }

    public List<Video> getVideos() {
        return videos;
    }

    public int getCountTotalVideo() {
        return countTotalVideo;
    }
}
