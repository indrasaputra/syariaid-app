package santri.video.data;

import santri.video.model.Video;

public class Constant {

    // Edit WEB_URL with your url. Make sure you have backslash('/') in the end url
    //public static String WEB_URL = "http://demo.dream-space.web.id/vido_panel/";
    public static String WEB_URL = "https://bo.syaria.id/vido_panel/";

    /* [ IMPORTANT ] be careful when edit this security code */
    /* This string must be same with security code at Server, if its different android unable to submit order */
    //public static final String SECURITY_CODE = "8V06LupAaMBLtQqyqTxmcN42nn27FlejvaoSM3zXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    public static final String SECURITY_CODE = "s6s3c7c19c9a9SKLh6ee4jD9WPXdR0Yb8RiU3Cocl9nPWDpkjvvFlrBHw0KLRaqNdaT23subExoOQTXWBA2mzu6UOXaAZ8rJsdz3";

    public static String YOUTUBE_BASE_URL = "https://www.youtube.com/watch?v=";

    public static String YOUTUBE_API_KEY = "AIzaSyBZZXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";

    // this limit value used for give pagination (request and display) to decrease payload
    public static int PLAYLIST_PER_REQUEST = 20;
    public static int VIDEO_PER_REQUEST = 20;
    public static int NOTIFICATION_PAGE = 20;

    // retry load image notification
    public static int LOAD_IMAGE_NOTIF_RETRY = 3;

    // Method get path to image
    public static String getURLimgVideo(String file_name) {
        return WEB_URL + "uploads/video/" + file_name;
    }

    public static String getURLimgPlaylist(String file_name) {
        return WEB_URL + "uploads/playlist/" + file_name;
    }

    public static String getImageURL(Video video) {
        if (video.image == null || video.image.equals("")) {
            return "https://img.youtube.com/vi/" + video.url + "/maxresdefault.jpg";
        }
        return getURLimgVideo(video.image);
    }
}
