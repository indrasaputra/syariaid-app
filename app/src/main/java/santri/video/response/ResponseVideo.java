package santri.video.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import santri.video.model.Video;

public class ResponseVideo implements Serializable {

    public String status = "";
    public int count = -1;
    public int count_total = -1;
    public int pages = -1;
    public List<Video> videos = new ArrayList<>();

}
