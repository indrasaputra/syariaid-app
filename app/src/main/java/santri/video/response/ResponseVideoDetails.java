package santri.video.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import santri.video.model.Playlist;
import santri.video.model.Tag;
import santri.video.model.Video;

public class ResponseVideoDetails implements Serializable {

    public String status = "";
    public Video video = new Video();
    public List<Playlist> playlists = new ArrayList<>();
    public List<Tag> tags = new ArrayList<>();
    public List<Video> related_video = new ArrayList<>();

}
