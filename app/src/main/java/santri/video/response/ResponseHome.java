package santri.video.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import santri.video.model.Playlist;
import santri.video.model.Tag;
import santri.video.model.Video;

public class ResponseHome implements Serializable {

    public String status = "";
    public List<Video> featured = new ArrayList<>();
    public List<Video> recent = new ArrayList<>();
    public List<Playlist> playlist = new ArrayList<>();
    public List<Tag> tag = new ArrayList<>();
}
