package santri.video.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.viewpager.widget.PagerAdapter;
import santri.syaria.R;
import santri.video.data.Constant;
import santri.video.model.Video;
import santri.video.utils.Tools;

public class AdapterVideoRecent extends PagerAdapter {

    private List<Video> items = new ArrayList<>();
    private Context ctx;

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public AdapterVideoRecent(Context ctx, List<Video> items) {
        this.ctx = ctx;
        this.items = items;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final Video video = items.get(position);
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.item_video_recent, container, false);
        ((TextView) view.findViewById(R.id.title)).setText(video.name);
        ((TextView) view.findViewById(R.id.duration)).setText(video.duration);
        ImageView image = view.findViewById(R.id.image);
        Tools.displayImage(ctx, image, Constant.getImageURL(video));

        ((View) view.findViewById(R.id.lyt_parent)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(v, video);
                }
            }
        });

        container.addView(view);
        return view;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, Video obj);
    }
}
