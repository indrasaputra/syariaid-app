package santri.video.model;

import java.io.Serializable;

public class Video implements Serializable {
    public long id = -1;
    public String name = "";
    public String url = "";
    public String image = "";
    public String duration = "";
    public String description = "";
    public long featured = 0;
    public long created_at = -1;
    public long last_update = -1;

    public boolean isDraft() {
        return (name == null || name.equals("") || url == null || url.equals(""));
    }
}
