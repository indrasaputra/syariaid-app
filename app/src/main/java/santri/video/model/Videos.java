package santri.video.model;

import java.util.ArrayList;
import java.util.List;

public class Videos {
    private int countTotalVideo = 0;
    private List<Video> videos = new ArrayList<>();

    public void setCountTotalVideo(int countTotalVideo) {
        this.countTotalVideo = countTotalVideo;
    }

    public void addVideos(List<Video> videos) {
        this.videos.addAll(videos);
    }

    public void resetVideos() {
        this.videos.clear();
    }

    public List<Video> getVideos() {
        return videos;
    }

    public int getCountTotalVideo() {
        return countTotalVideo;
    }
}
