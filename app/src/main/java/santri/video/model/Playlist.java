package santri.video.model;

import java.io.Serializable;

public class Playlist implements Serializable {
    public long id = -1;
    public String name = "";
    public String image = "";
    public String brief = "";
    public long video_count = -1;
    public long priority = -1;
    public long featured = 0;
    public long created_at = -1;
    public long last_update = -1;

    public Playlist() {
    }

    public Playlist(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public boolean isDraft() {
        return name == null || name.equals("");
    }
}
