package santri.video.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.util.TypedValue;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.Toast;

//import com.bumptech.glide.Glide;
//import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.core.content.FileProvider;
import jp.wasabeef.picasso.transformations.BlurTransformation;
import santri.Adapter.restaurentadapter;
import santri.syaria.BuildConfig;
import santri.syaria.R;

public class Tools {

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getPaddingCard(int height) {
        int res = (Tools.getScreenWidth() - height) / 2;
        return res;
    }

    public static int dpToPx(Context c, int dp) {
        Resources r = c.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public static void displayImage(Context ctx, ImageView img, String url) {
        Log.d("image url ===", url);
        AlphaAnimation anim = new AlphaAnimation(0, 1);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.ABSOLUTE);
        anim.setDuration(0);
        Picasso.with(ctx).load(url).placeholder(ctx.getResources().getDrawable(R.mipmap.ic_launcher_foreground)).error(ctx.getResources().getDrawable(R.mipmap.ic_launcher_foreground)).into(img);
        img.startAnimation(anim);
//        try {
//            try {
//                Glide.with(ctx).load(url)
//                        //.crossFade()
//                        //.diskCacheStrategy(DiskCacheStrategy.NONE)
//                        .into(img);
//            } catch (Exception e) {
//            }
//        } catch (Exception e) {
//        }
    }

    public static void displayBlurImage(Context ctx, ImageView img, String url) {
        Log.d("image url ===", url);
        AlphaAnimation anim = new AlphaAnimation(0, 1);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.ABSOLUTE);
        anim.setDuration(0);
        Picasso.with(ctx)
                .load(url)
                .transform(new BlurTransformation(ctx, 25, 1))
                .placeholder(ctx.getResources().getDrawable(R.mipmap.ic_launcher_foreground))
                .error(ctx.getResources().getDrawable(R.mipmap.ic_launcher_foreground))
                .into(img);
        img.startAnimation(anim);
    }

    public static void directLinkToBrowser(Activity activity, String url) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            activity.startActivity(intent);
        } catch (Exception e) {
            Toast.makeText(activity, "Ops, Cannot open url", Toast.LENGTH_LONG).show();
        }
    }

    public static String getFormattedDateFull(Long dateTime) {
        SimpleDateFormat newFormat = new SimpleDateFormat("dd MMM yyyy, hh:mm");
        return newFormat.format(new Date(dateTime));
    }

    public static String getFormattedDateSimple(Long dateTime) {
        SimpleDateFormat newFormat = new SimpleDateFormat("dd MMM yy hh:mm");
        return newFormat.format(new Date(dateTime));
    }

    public static Uri getLocalBitmapUri(Context mContext, ImageView imageView) {
        // Extract Bitmap numberOfRecords ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp;
        if (drawable instanceof BitmapDrawable) {
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            File file = new File(mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                    "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID + ".provider", file);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }
}
