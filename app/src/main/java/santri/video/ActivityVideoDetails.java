package santri.video;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.youtube.player.YouTubeApiServiceUtil;
import com.google.android.youtube.player.YouTubeInitializationResult;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.DrawableRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import santri.syaria.R;
import santri.video.connection.API;
import santri.video.connection.RestAdapter;
import santri.video.data.Constant;
import santri.video.data.ThisApplication;
import santri.video.model.Playlist;
import santri.video.model.Tag;
import santri.video.model.Video;
import santri.video.response.ResponseVideoDetails;
import santri.video.utils.NetworkCheck;
import santri.video.utils.Tools;
import santri.video.utils.ViewAnimation;

public class ActivityVideoDetails extends AppCompatActivity {

    private static final String EXTRA_OBJECT_ID = "key.EXTRA_OBJECT_ID";
    private static final String EXTRA_FROM_NOTIF = "key.EXTRA_FROM_NOTIF";

    // activity transition
    public static void navigate(Activity activity, Long id, Boolean from_notif) {
        Intent i = navigateBase(activity, id, from_notif);
        activity.startActivity(i);
    }

    public static Intent navigateBase(Context context, Long id, Boolean from_notif) {
        Intent i = new Intent(context, ActivityVideoDetails.class);
        i.putExtra(EXTRA_OBJECT_ID, id);
        i.putExtra(EXTRA_FROM_NOTIF, from_notif);
        return i;
    }

    private Video video;
    private List<Playlist> playlists = new ArrayList<>();
    private List<Tag> tags = new ArrayList<>();
    private List<Video> related = new ArrayList<>();

    private Call<ResponseVideoDetails> callbackCall = null;
    private Long video_id;
    private View parent_view;
    private View lyt_webview;
    private SwipeRefreshLayout swipe_refresh;
    private ShimmerFrameLayout shimmer_layout;
    private View lyt_main_content;
    private WebView webview;
    private FragmentVideoPlayer fragmentVideo;
    private Boolean fullscreen = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_details);
        //dao = AppDatabase.getDb(this).getDAO();

        video_id = getIntent().getLongExtra(EXTRA_OBJECT_ID, -1L);
        //from_notif = getIntent().getBooleanExtra(EXTRA_FROM_NOTIF, false);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SpannableString s = new SpannableString("Ceramah Ustad");
        s.setSpan(new TypefaceSpan("MyTypeface.otf"), 0, s.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(
                (Html.fromHtml("<font color=\"#ffffff\">" + s + "</font>")));

        initComponent();
        initToolbar();
        requestAction();
    }

    private void initComponent() {
        parent_view = findViewById(android.R.id.content);
        lyt_webview = findViewById(R.id.lyt_webview);
        swipe_refresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        shimmer_layout = findViewById(R.id.shimmer_layout);
        lyt_main_content = (View) findViewById(R.id.lyt_main_content);
        // on swipe
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestAction();
            }
        });
    }

    private void initToolbar() {

    }

    private void requestAction() {
        showFailedView(false, "", R.drawable.img_failed);
        swipeProgress(true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestVideoDetailsApi();
            }
        }, 1000);
    }

    private void onFailRequest() {
        swipeProgress(false);
        if (NetworkCheck.isConnect(this)) {
            showFailedView(true, getString(R.string.failed_text), R.drawable.img_failed);
        } else {
            showFailedView(true, getString(R.string.no_internet_text), R.drawable.img_no_internet);
        }
    }

    private void requestVideoDetailsApi() {
        API api = RestAdapter.createAPI();
        callbackCall = api.getVideoDetails(video_id);
        callbackCall.enqueue(new Callback<ResponseVideoDetails>() {
            @Override
            public void onResponse(Call<ResponseVideoDetails> call, Response<ResponseVideoDetails> response) {
                ResponseVideoDetails resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    video = resp.video;
                    playlists = resp.playlists;
                    tags = resp.tags;
                    related = resp.related_video;
                    displayVideoData();
                    //displayPlaylistsData();
                    //displayTagsData();
                    //displayRelatedData();
                    swipeProgress(false);
                } else {
                    onFailRequest();
                }
            }

            @Override
            public void onFailure(Call<ResponseVideoDetails> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest();
            }
        });
    }

    private void swipeProgress(final boolean show) {
        if (!show) {
            swipe_refresh.setRefreshing(show);
            shimmer_layout.setVisibility(View.GONE);
            shimmer_layout.stopShimmer();
            return;
        }
        shimmer_layout.setVisibility(View.VISIBLE);
        shimmer_layout.startShimmer();
        lyt_main_content.setVisibility(View.INVISIBLE);
        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(show);
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void displayVideoData() {

        ((TextView) findViewById(R.id.title)).setText(video.name);
        ((TextView) findViewById(R.id.duration)).setText(video.duration);

        webview = (WebView) findViewById(R.id.detail_desc);
        String html_data = "<style>img{max-width:100%;height:auto;} iframe{width:100%;}</style> ";
        html_data += video.description;
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings();
        webview.getSettings().setBuiltInZoomControls(true);
        webview.setBackgroundColor(Color.TRANSPARENT);
        webview.setWebChromeClient(new WebChromeClient());
        webview.loadData(html_data, "text/html; charset=UTF-8", null);
        // disable scroll on touch
        webview.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });
        // override url direct
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Tools.directLinkToBrowser(ActivityVideoDetails.this, url);
                return true;
            }
        });

        ((TextView) findViewById(R.id.date)).setText(Tools.getFormattedDateFull(video.last_update));
        if (video.featured == 0) {
            ((TextView) findViewById(R.id.featured)).setVisibility(View.GONE);
        }

        prepareYoutube();
        lyt_main_content.setVisibility(View.VISIBLE);
        lyt_webview.setVisibility(View.VISIBLE);

        ImageView btn_toggle_desc = findViewById(R.id.btn_toggle_desc);
        btn_toggle_desc.setRotation(180);
        btn_toggle_desc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleSectionText(v);
            }
        });

//        img_favorite = findViewById(R.id.img_favorite);
//        img_favorite.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!is_favorite) {
//                    dao.insertVideo(VideoEntity.entity(video));
//                } else {
//                    dao.deleteVideo(video.id);
//                }
//                refreshFavorite();
//                ThisApplication.getInstance().setFavoriteChange(true);
//            }
//        });
//
//        refreshFavorite();
//
//        // Analytics track
//        ThisApplication.getInstance().saveCustomLogEvent("VIDEO_DETAILS_" + video.id);
    }

    private void toggleSectionText(View view) {
        boolean show = toggleArrow(view);
        if (show) {
            ViewAnimation.collapse(lyt_webview);
        } else {
            ViewAnimation.expand(lyt_webview);
        }
    }

    public boolean toggleArrow(View view) {
        if (view.getRotation() == 0) {
            view.animate().setDuration(200).rotation(180);
            return true;
        } else {
            view.animate().setDuration(200).rotation(0);
            return false;
        }
    }

    private void prepareYoutube() {
        final YouTubeInitializationResult result = YouTubeApiServiceUtil.isYouTubeApiServiceAvailable(this);

        if (result != YouTubeInitializationResult.SUCCESS) {
            result.getErrorDialog(this, 0).show();
            return;
        }

        FragmentVideoPlayer fragment = displayPlayerFragment(null);

        final View fragmentView = findViewById(R.id.fragment_youtube);
        final ImageView image = findViewById(R.id.image);
        final ImageView btn_play = findViewById(R.id.btn_play);

        fragmentView.setVisibility(View.INVISIBLE);

        fragment.setFragmentCallback(new FragmentVideoPlayer.FragmentCallback() {
            @Override
            public void onViewCreated() {
                image.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, fragmentView.getHeight()));
                Tools.displayImage(ActivityVideoDetails.this, image, Constant.getImageURL(video));
            }
        });

        btn_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image.setVisibility(View.GONE);
                v.setVisibility(View.GONE);
                fragmentView.setVisibility(View.VISIBLE);
                displayPlayerFragment(video.url);
            }
        });
    }

    private FragmentVideoPlayer displayPlayerFragment(String videoId) {
        if (videoId != null) {
            fragmentVideo = FragmentVideoPlayer.newInstance(videoId);
        } else {
            fragmentVideo = new FragmentVideoPlayer();
        }

        fragmentVideo.setFullScreenListener(new FragmentVideoPlayer.FullScreenListener() {
            @Override
            public void onFullscreen(boolean b) {
                fullscreen = b;
            }
        });

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_youtube, fragmentVideo);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();
        return fragmentVideo;
    }

    private void showFailedView(boolean show, String message, @DrawableRes int icon) {
        View lyt_failed = (View) findViewById(R.id.lyt_failed);

        ((ImageView) findViewById(R.id.failed_icon)).setImageResource(icon);
        ((TextView) findViewById(R.id.failed_message)).setText(message);
        if (show) {
            lyt_main_content.setVisibility(View.INVISIBLE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            lyt_main_content.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestAction();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (fullscreen) {
            fragmentVideo.backFromFullscreen();
            return;
        }
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (webview != null) webview.onPause();
        if (fragmentVideo != null && fragmentVideo.getPlayer() != null) {
            //fragmentVideo.getPlayer().pause();
            fragmentVideo.getPlayer().release();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (webview != null) webview.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
