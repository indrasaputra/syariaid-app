package santri.video.connection;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import santri.video.data.Constant;
import santri.video.response.ResponseHome;
import santri.video.response.ResponseVideo;
import santri.video.response.ResponseVideoDetails;

public interface API {

    String CACHE = "Cache-Control: max-age=0";
    String AGENT = "User-Agent: Vido";
    String SECURITY = "Security: " + Constant.SECURITY_CODE;

    @Headers({CACHE, AGENT})
    @GET("services/getHome")
    Call<ResponseHome> getHome();

    @Headers({CACHE, AGENT})
    @GET("services/listVideo")
    Call<ResponseVideo> getListVideo(
            @Query("page") Integer page,
            @Query("count") Integer count,
            @Query("q") String query,
            @Query("playlist_id") Long playlist_id
    );

    @Headers({CACHE, AGENT})
    @GET("services/listCustomVideo")
    Call<ResponseVideo> getListCustomVideo(
            @Query("page") Integer page,
            @Query("count") Integer count,
            @Query("q") String query,
            @Query("playlist_id") Long playlist_id
    );

    @Headers({CACHE, AGENT})
    @GET("services/getVideoDetails")
    Call<ResponseVideoDetails> getVideoDetails(
            @Query("id") Long id
    );
}
