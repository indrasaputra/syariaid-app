package santri.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import santri.syaria.R;
import santri.syaria.retrofit.UserData;
import santri.video.utils.Tools;

public class AdapterTaarufUserList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private List<UserData> items = new ArrayList<>();

    private boolean loading;
    private AdapterTaarufUserList.OnLoadMoreListener onLoadMoreListener;

    private Context ctx;
    private AdapterTaarufUserList.OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, UserData obj, int position);
    }

    public void setOnItemClickListener(final AdapterTaarufUserList.OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterTaarufUserList(Context context, RecyclerView view, List<UserData> items) {
        this.items = items;
        ctx = context;
        lastItemViewDetector(view);
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txt_name, txt_certified, txt_distance, txt_domicile, txt_criteria;
        public ImageView image;
        public View lyt_parent;

        public OriginalViewHolder(View v) {
            super(v);
            txt_name = v.findViewById(R.id.txt_name);
            lyt_parent = v.findViewById(R.id.lyt_parent);
            txt_certified = v.findViewById(R.id.txt_certified);
            txt_distance = v.findViewById(R.id.txt_distance);
            txt_domicile = v.findViewById(R.id.txt_domicile);
            txt_criteria = v.findViewById(R.id.txt_criteria);
            image = v.findViewById(R.id.image);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = v.findViewById(R.id.progress_loading);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_user_list, parent, false);
            vh = new AdapterTaarufUserList.OriginalViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            vh = new AdapterTaarufUserList.ProgressViewHolder(v);
        }
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof AdapterTaarufUserList.OriginalViewHolder) {
            final UserData p = items.get(position);
            AdapterTaarufUserList.OriginalViewHolder vItem = (AdapterTaarufUserList.OriginalViewHolder) holder;
            vItem.txt_name.setText(p.fullname);
            vItem.txt_distance.setText(String.format("%.2f", Float.parseFloat(p.distance)));
            vItem.txt_domicile.setText(p.city +", "+p.province);
            vItem.txt_criteria.setText("Kriteria Calon: "+p.couple_criteria);

            if (p.certificate_no!=null && !p.certificate_no.equals(""))
                vItem.txt_certified.setVisibility(View.VISIBLE);
            else
                vItem.txt_certified.setVisibility(View.GONE);

            if (p.blurred_image.equals("1"))
                Tools.displayBlurImage(ctx, vItem.image, p.image);
            else
                Tools.displayImage(ctx, vItem.image, p.image);

            vItem.lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view, p, position);
                    }
                }
            });
        } else {
            ((AdapterTaarufUserList.ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return this.items.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    public void insertData(List<UserData> items) {
        setLoaded();
        int positionStart = getItemCount();
        int itemCount = items.size();
        this.items.addAll(items);
        notifyItemRangeInserted(positionStart, itemCount);
    }

    public void setLoaded() {
        loading = false;
        for (int i = 0; i < getItemCount(); i++) {
            if (items.get(i) == null) {
                items.remove(i);
                notifyItemRemoved(i);
            }
        }
    }

    public void setLoading() {
        if (getItemCount() != 0) {
            this.items.add(null);
            notifyItemInserted(getItemCount() - 1);
            loading = true;
        }
    }

    public void resetListData() {
        this.items = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void setOnLoadMoreListener(AdapterTaarufUserList.OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    private void lastItemViewDetector(RecyclerView recyclerView) {
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastPos = layoutManager.findLastVisibleItemPosition();
                    if (!loading && lastPos == getItemCount() - 1 && onLoadMoreListener != null) {
                        if (onLoadMoreListener != null) {
                            int current_page = getItemCount() / 10;
                            onLoadMoreListener.onLoadMore(current_page);
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    public interface OnLoadMoreListener {
        void onLoadMore(int current_page);
    }
}
