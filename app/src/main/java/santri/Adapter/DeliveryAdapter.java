package santri.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import santri.Getset.DeliveryGetSet;
import santri.syaria.R;

import static santri.syaria.MainActivity.tf_opensense_regular;

public class DeliveryAdapter extends BaseAdapter {
    private final ArrayList<DeliveryGetSet> dat;
    private final Context context;
    private LayoutInflater inflater = null;



    public DeliveryAdapter(ArrayList<DeliveryGetSet> dat, Context context) {
        this.dat = dat;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {
        return dat.size();
    }

    @Override
    public Object getItem(int position) {
        return dat.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      View vi = convertView;
        if(convertView==null)
        {
            vi = inflater.inflate(R.layout.celldelivery, parent,false);
        }
        ImageView img_user = vi.findViewById(R.id.img_user);

        AlphaAnimation anim = new AlphaAnimation(0, 1);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.ABSOLUTE);
        anim.setDuration(1000);
        Picasso.with(context).load(context.getResources().getString(R.string.link) + context.getString(R.string.imagepath) + dat.get(position).getUser_image()).into(img_user);
        img_user.startAnimation(anim);


//        if (dat.get(position).getComplete().equals("Order is processing")) {
//            img_user.setImageDrawable(context.getDrawable(R.drawable.img_orderprocess));
//        }
//        else if (dat.get(position).getComplete().equals("Order is out for delivery"))
//        {
//            img_user.setImageDrawable(context.getDrawable(R.drawable.img_orderprocess));
//
//        }
//        else if(dat.get(position).getComplete().equals("Order is Delivered"))
//        {
//            img_user.setImageDrawable(context.getDrawable(R.drawable.img_ordercomplete));
//
//        }
        TextView txt_orderNo = vi.findViewById(R.id.txt_orderNo);
        txt_orderNo.setTypeface(tf_opensense_regular);
        String orderNo=context.getString(R.string.txt_order_no)+dat.get(position).getOrderNo();
        txt_orderNo.setText(orderNo);


         TextView txt_orderAmount = vi.findViewById(R.id.txt_orderAmount);
        txt_orderAmount.setTypeface(tf_opensense_regular);

        String orderAmount = "Order Amount "+context.getString(R.string.currency)+dat.get(position).getOrderAmount();
        txt_orderAmount.setText(orderAmount);


        TextView txt_orderQuantity = vi.findViewById(R.id.txt_orderQuantity);
        txt_orderQuantity.setTypeface(tf_opensense_regular);
        String itemNum = dat.get(position).getOrderQuantity()+" Items";
        txt_orderQuantity.setText(itemNum);


        TextView txt_orderDateTime = vi.findViewById(R.id.txt_orderDateTime);
        txt_orderDateTime.setTypeface(tf_opensense_regular);
        txt_orderDateTime.setText(dat.get(position).getOrderTimeDate());

        TextView txt_user_name = vi.findViewById(R.id.txt_user_name);
        txt_user_name.setTypeface(tf_opensense_regular);
        txt_user_name.setText(dat.get(position).getUser_name());

        TextView txt_status = vi.findViewById(R.id.txt_status);
        txt_status.setTypeface(tf_opensense_regular);
        String status = dat.get(position).getComplete();
        if (status.equalsIgnoreCase("Order is processing")) {
            status = "Request";
            txt_status.setTextColor(context.getResources().getColor(R.color.hijau));
        } else if (status.equalsIgnoreCase("Order is out for delivery")) {
            status = "On Progress";
            txt_status.setTextColor(context.getResources().getColor(R.color.hijau));
        } else if (status.equalsIgnoreCase("Order is Completed")) {
            status = "Finished";
            txt_status.setTextColor(context.getResources().getColor(R.color.quantum_yellow));
        } else if (status.equalsIgnoreCase("Order is Rejected")) {
            status = "Rejected";
            txt_status.setTextColor(context.getResources().getColor(R.color.merah));
        }
        txt_status.setText(status);

        return vi;
    }



    private void flipImage(Boolean ifTrue, ImageView imageView){
        if(ifTrue)
        {
            imageView.setImageDrawable(context.getDrawable(R.drawable.img_ordercomplete));
        }
        else  imageView.setImageDrawable(context.getDrawable(R.drawable.img_orderprocess));

    }
}
