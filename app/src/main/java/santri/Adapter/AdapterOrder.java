package santri.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import santri.syaria.R;
import santri.syaria.retrofit.OrderData;
import santri.video.utils.Tools;

public class AdapterOrder extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private List<OrderData> items = new ArrayList<>();

    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, OrderData obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterOrder(Context context, RecyclerView view, List<OrderData> items) {
        this.items = items;
        ctx = context;
        lastItemViewDetector(view);
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView name;
        public TextView description;
        public TextView date;
        public TextView status;
        public ImageView image;
        public View lyt_parent;

        public OriginalViewHolder(View v) {
            super(v);
            name = v.findViewById(R.id.txt_name);
            description = v.findViewById(R.id.txt_address);
            date = v.findViewById(R.id.txt_order_date);
            status = v.findViewById(R.id.txt_status);
            image = v.findViewById(R.id.image);
            lyt_parent = v.findViewById(R.id.rel_main);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = v.findViewById(R.id.progress_loading);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_order, parent, false);
            vh = new OriginalViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            final OrderData p = items.get(position);
            OriginalViewHolder vItem = (OriginalViewHolder) holder;
            vItem.name.setText(p.produk);
            vItem.description.setText("Sedekah: "+p.infaq);
            //vItem.stock.setText("Stok: " + p.stock);
            //vItem.status.setText(p.status);

            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy HH:mm:ss");

            Date regDate = null;
            Date expiredDate = null;
            Date startDate = null;
            Date endDate = null;
            Date now = null;

            String strRegDate = null;
            String strExpiredDate = null;
            String strStartDate = null;
            String strEndDate = null;
            String strNow = null;
            try {
                regDate = sdf.parse(p.cratedtime);
                expiredDate = sdf.parse(p.expired_bayar);
                startDate = p.start_awal!=null ? sdf.parse(p.start_awal) : null;
                endDate = p.batas_akhir!=null ? sdf.parse(p.batas_akhir) : null;
                now = sdf.parse(sdf.format(new Date()));

                strRegDate = LocalDateTime.parse(p.cratedtime.replace(" ", "T")).format(formatter);
                strExpiredDate = LocalDateTime.parse(p.expired_bayar.replace(" ", "T")).format(formatter);
                strStartDate = p.start_awal!=null ? LocalDateTime.parse(p.start_awal.replace(" ", "T")).format(formatter) : "";
                strEndDate = p.batas_akhir!=null ? LocalDateTime.parse(p.batas_akhir.replace(" ", "T")).format(formatter) : "";
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }

            Log.e("now date",now.toString());
            Log.e("expired date",expiredDate.toString()+ " "+strExpiredDate);

            vItem.date.setText("Order Date: "+strRegDate);

            if(p.status!=null && p.status.equals("201")) {
                //vItem.status. setBackground(ctx.getResources().getDrawable(R.drawable.shape_round_green_overlay));
                if (now.before(expiredDate)) {
                    vItem.status.setText("MENUNGGU PEMBAYARAN");
                } else {
                    vItem.status.setText("EXPIRED");
                }
            } else if (p.status!=null && p.status.equals("200")) {
                //vItem.status.setBackground(ctx.getResources().getDrawable(R.drawable.shape_round_orange_overlay));
                if (now.before(endDate)) {
                    vItem.status.setText("ACTIVE");
                } else {
                    vItem.status.setText("COMPLETED");
                }
            } else {
                vItem.status.setText("EXPIRED");
            }
            //Tools.displayImage(ctx, vItem.image, ctx.getResources().getDrawable(R.drawable.logo, null));
            vItem.lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view, p, position);
                    }
                }
            });
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return this.items.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    public void insertData(List<OrderData> items) {
        setLoaded();
        int positionStart = getItemCount();
        int itemCount = items.size();
        this.items.addAll(items);
        notifyItemRangeInserted(positionStart, itemCount);
    }

    public void setLoaded() {
        loading = false;
        for (int i = 0; i < getItemCount(); i++) {
            if (items.get(i) == null) {
                items.remove(i);
                notifyItemRemoved(i);
            }
        }
    }

    public void setLoading() {
        if (getItemCount() != 0) {
            this.items.add(null);
            notifyItemInserted(getItemCount() - 1);
            loading = true;
        }
    }

    public void resetListData() {
        this.items = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    private void lastItemViewDetector(RecyclerView recyclerView) {
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastPos = layoutManager.findLastVisibleItemPosition();
                    if (!loading && lastPos == getItemCount() - 1 && onLoadMoreListener != null) {
                        if (onLoadMoreListener != null) {
                            int current_page = getItemCount() / 10;
                            onLoadMoreListener.onLoadMore(current_page);
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    public interface OnLoadMoreListener {
        void onLoadMore(int current_page);
    }

}
