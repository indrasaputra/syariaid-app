package santri.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageClickListener;
import com.synnapps.carouselview.ImageListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.recyclerview.widget.RecyclerView;
import santri.Getset.MainMenuDataSource;
import santri.azan.activity.ActivityCalendar;
import santri.azan.activity.ActivityNamesList;
import santri.azan.activity.ActivityQibla;
import santri.azan.activity.DateConverter;
import santri.azan.activity.PrayerTimesActivity;
import santri.azan.activity.QuranActivity;
import santri.azan.activity.Quranlist;
import santri.azan.activity.TasbeeActivity;
import santri.syaria.ActivityDoa;
import santri.syaria.ActivityPraNikah;
import santri.syaria.ActivityTaarufNonMandatory;
import santri.syaria.ActivityTaarufRegister;
import santri.syaria.ActivityTaarufUserList;
import santri.syaria.CategoryActivity;
import santri.syaria.MainActivity;
import santri.syaria.R;
import santri.syaria.TanyaUstadzActivity;
import santri.utils.Util;

public class MainMenuAdapter extends RecyclerView.Adapter<MainMenuAdapter.MyViewHolder> {
    private Context mContext;
    private ArrayList<MainMenuDataSource> mainMenuDataSourceList;
    private final Util util;

    String jenisLayout = "header";


    public class MyViewHolder extends RecyclerView.ViewHolder {
        //body
        public ImageView icon;
        public TextView title;
        public CardView cardView;
        public RelativeLayout backgroundColour;

        //notif_activity
        public CarouselView carouselView;

        //body2
        public ImageView icon2;
        public TextView title2;
        public CardView cardView2;
        public RelativeLayout backgroundColour2;

        //body2
        public ImageView icon3;
        public TextView title3;
        public CardView cardView3;
        public RelativeLayout backgroundColour3;

        public MyViewHolder(View view) {
            super(view);
            //body
            if (jenisLayout.equals("body")) {
                //body
                cardView = view.findViewById(R.id.card_view);
                icon = view.findViewById(R.id.icon);
                title = view.findViewById(R.id.title);
                backgroundColour = view.findViewById(R.id.backgroundColour);
            } else if (jenisLayout.equals("notif_activity")) {
                carouselView = view.findViewById(R.id.carouselView);
            } else if (jenisLayout.equals("body2")) {
                //body
                cardView2 = view.findViewById(R.id.card_view);
                icon2 = view.findViewById(R.id.icon);
                title2 = view.findViewById(R.id.title);
                backgroundColour2 = view.findViewById(R.id.backgroundColour);
            } else  if (jenisLayout.equals("elearn_content")) {
                //body
                cardView3 = view.findViewById(R.id.card_view);
                icon3 = view.findViewById(R.id.icon);
                title3 = view.findViewById(R.id.title);
                backgroundColour3 = view.findViewById(R.id.backgroundColour);
            }
        }
    }

    public MainMenuAdapter(Context mContext, ArrayList<MainMenuDataSource> mainMenuDataSourceList) {
        this.mContext = mContext;
        this.mainMenuDataSourceList = mainMenuDataSourceList;
        this.util = new Util(mContext);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;
        switch (viewType) {
            case 0:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.mainmenu_content_slide, parent, false);
                break;
            case 1:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.mainmenu_content_body, parent, false);
                break;
            case 2:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.mainmenu_content_body1, parent, false);
                break;
            case 3:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.mainmenu_content_body2, parent, false);
                break;
            case 4:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.mainmenu_content_body0, parent, false);
                break;
            case 5:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.mainmenu_elearn_title, parent, false);
                break;
            case 6:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.mainmenu_elearn_content, parent, false);
                break;
        }

        return new MyViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return mainMenuDataSourceList.size();
    }

    @Override
    public int getItemViewType(int position) {

        switch (mainMenuDataSourceList.get(position).jenisLayout) {
            case "body":
                this.jenisLayout = "body";
                return 1;
            case "notif_activity":
                this.jenisLayout = "notif_activity";
                return 0;
            case "body1":
                this.jenisLayout = "body1";
                return 2;
            case "body2":
                this.jenisLayout = "body2";
                return 3;
            case "main_title":
                this.jenisLayout = "main_title";
                return 4;
            case "elearn_title":
                this.jenisLayout = "elearn_title";
                return 5;
            case "elearn_content":
                this.jenisLayout = "elearn_content";
                return 6;
            default:
                return -1;
        }
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final MainMenuDataSource mainMenuDataSource = mainMenuDataSourceList.get(position);
        if (mainMenuDataSource.getJenisLayout().equals("notif_activity")) {
            //holderHeader = holder;
            this.setupStaticBanners(holder);
            //this.setupDynamicBanners(holder);
        } else if (mainMenuDataSource.getJenisLayout().equals("body")) {
            //set resource body
            holder.title.setText(mainMenuDataSource.getTitle());
            holder.title.setTextColor(ContextCompat.getColor(mContext, mainMenuDataSource.getTextColor()));
//            // loading homeDataSource cover using Glide library
//            BitmapFactoryOptimizer bitmapFactoryOptimizer = new BitmapFactoryOptimizer(mContext, mainMenuDataSource.getIcon());
//            Bitmap mBitmap = bitmapFactoryOptimizer.decodeSampledBitmapFromResourceMemOpt(100, 100);
//            final ByteArrayOutputStream stream = new ByteArrayOutputStream();
//            mBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
//            Glide.with(mContext).load(stream.toByteArray()).into(holder.icon);

            holder.icon.setImageResource(mainMenuDataSource.getIcon());

            //without sampling buffer
            //Glide.with(mContext).load(homeDataSource.getIcon()).into(holder.icon);
            holder.backgroundColour.setBackgroundResource(mainMenuDataSource.getBackgroundColor());

            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mainMenuDataSource.getId() == 1) {
                        Intent intent = new Intent(mContext, CategoryActivity.class); //MainActivity.class);
                        intent.putExtra("catName","Ustad");
                        mContext.startActivity(intent);
                    } else if (mainMenuDataSource.getId() == 2) {
                        //Intent intent = new Intent(mContext, MainActivity.class);
                        Intent intent = new Intent(mContext, TanyaUstadzActivity.class);
                        intent.putExtra("catName","TanyaUstad");
                        intent.putExtra("sub_category_name","Pengajian Umum");
                        intent.putExtra("CityName","all");
                        mContext.startActivity(intent);
                    /*} else if (mainMenuDataSource.getId() == 3) {
                        Intent intent = new Intent(mContext, QuranActivity.class);
                        mContext.startActivity(intent);
                    } else if (mainMenuDataSource.getId() == 4) {
                        //Intent intent = new Intent(mContext, MapActivity.class);
                        //Intent intent = new Intent(mContext, TasbeeActivity.class);
                        //Intent intent = new Intent(mContext, ActivityQibla.class);
                        Intent intent = new Intent(mContext, PrayerTimesActivity.class);
                        mContext.startActivity(intent);*/
                    } else if (mainMenuDataSource.getId() == 3) {
                        //Intent intent = new Intent(mContext, MainActivity.class);
                        Intent intent = new Intent(mContext, TanyaUstadzActivity.class);
                        intent.putExtra("catName", "TanyaUstad");
                        intent.putExtra("sub_category_name", "Pra Nikah");
                        intent.putExtra("CityName", "all");

                        mContext.startActivity(intent);
                    } else if (mainMenuDataSource.getId() == 5) {
                        //Intent intent = new Intent(mContext, ActivityTaarufRegister.class);
                        //Intent intent = new Intent(mContext, ActivityTaarufNonMandatory.class);
                        Intent intent = new Intent(mContext, ActivityTaarufUserList.class);
                        mContext.startActivity(intent);
                    } else if (mainMenuDataSource.getId() == 0) {
                        Toast.makeText(mContext, "Silakan login untuk mengakses " + holder.title.getText(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mContext, holder.title.getText() + " coming soon", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else if (mainMenuDataSource.getJenisLayout().equals("body2")) {
            //set resource body
            holder.title2.setText(mainMenuDataSource.getTitle());
            holder.title2.setTextColor(ContextCompat.getColor(mContext, mainMenuDataSource.getTextColor()));
//            // loading homeDataSource cover using Glide library
//            BitmapFactoryOptimizer bitmapFactoryOptimizer = new BitmapFactoryOptimizer(mContext, mainMenuDataSource.getIcon());
//            Bitmap mBitmap = bitmapFactoryOptimizer.decodeSampledBitmapFromResourceMemOpt(100, 100);
//            final ByteArrayOutputStream stream = new ByteArrayOutputStream();
//            mBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
//            Glide.with(mContext).load(stream.toByteArray()).into(holder.icon);

            holder.icon2.setImageResource(mainMenuDataSource.getIcon());

            //without sampling buffer
            //Glide.with(mContext).load(homeDataSource.getIcon()).into(holder.icon);
            holder.backgroundColour2.setBackgroundResource(mainMenuDataSource.getBackgroundColor());

            holder.cardView2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mainMenuDataSource.getId() == 1) {
                        Intent intent = new Intent(mContext, QuranActivity.class);
                        mContext.startActivity(intent);
                    } else if (mainMenuDataSource.getId() == 2) {
                        Intent intent = new Intent(mContext, Quranlist.class);
                        mContext.startActivity(intent);
                    } else if (mainMenuDataSource.getId() == 3) {
                        Intent intent = new Intent(mContext, TasbeeActivity.class);
                        mContext.startActivity(intent);
                    } else if (mainMenuDataSource.getId() == 4) {
                        Intent intent = new Intent(mContext, ActivityCalendar.class);
                        mContext.startActivity(intent);
                    } else if (mainMenuDataSource.getId() == 5) {
                        Intent intent = new Intent(mContext, PrayerTimesActivity.class);
                        mContext.startActivity(intent);
                    } else if (mainMenuDataSource.getId() == 6) {
                        Intent intent = new Intent(mContext, ActivityQibla.class);
                        mContext.startActivity(intent);
                    } else if (mainMenuDataSource.getId() == 7) {
                        Intent intent = new Intent(mContext, DateConverter.class);
                        mContext.startActivity(intent);
                    } else if (mainMenuDataSource.getId() == 8) {
                        Intent intent = new Intent(mContext, ActivityNamesList.class);
                        mContext.startActivity(intent);
                    }else if (mainMenuDataSource.getId() == 9) {
                        Intent intent = new Intent(mContext, ActivityDoa.class);
                        mContext.startActivity(intent);
                    } else {
                        Toast.makeText(mContext, holder.title.getText() + " coming soon", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else if (mainMenuDataSource.getJenisLayout().equals("elearn_content")) {
            //set resource body
            holder.title3.setText(mainMenuDataSource.getTitle());
            holder.title3.setTextColor(ContextCompat.getColor(mContext, mainMenuDataSource.getTextColor()));

            holder.icon3.setImageResource(mainMenuDataSource.getIcon());

            holder.backgroundColour3.setBackgroundResource(mainMenuDataSource.getBackgroundColor());

            holder.cardView3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mainMenuDataSource.getId() == 1) {
                        Intent intent = new Intent(mContext, ActivityPraNikah.class);
                        mContext.startActivity(intent);
                    } else if (mainMenuDataSource.getId() == 0) {
                        Toast.makeText(mContext, "Silakan login untuk mengakses "+ holder.title3.getText(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mContext, holder.title3.getText() + " coming soon", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void setupStaticBanners(final MyViewHolder holder) {
        int slide1 = R.drawable.slide1_;
        int slide2 = R.drawable.slide2_;
        int slide3 = R.drawable.slide3_;
        int[] imageResource = {slide1, slide2, slide3}; //, drawableIklan3, drawableIklan4, drawableIklan5};
        String[] actionUrl = {"http://www.google.co.id", "http://www.google.co.id", "http://www.truemoney.co.id", "http://kuis.truemoney.co.id", "http://lelang.truemoney.co.id"};

        HashMap<int[], String[]> hashMap = new HashMap<>();
        hashMap.put(imageResource, actionUrl);
        for (Map.Entry<int[], String[]> entry : hashMap.entrySet()) {
            imageResource = entry.getKey();
            actionUrl = entry.getValue();
        }

        //final String[] imgUrl = {"http://inventory.truemoney.co.id/prod/iklan/Apps-Slide-Banner.png","http://inventory.truemoney.co.id/prod/iklan/Apps-Slide-Banner-01.png"};
        //final ImageLoader imageLoader = new ImageLoader(mContext);

        final int[] finalImageResource = imageResource;
        ImageListener imageListener = new ImageListener() {
            @Override
            public void setImageForPosition(int position, ImageView imageView) {
                //imageLoader.DisplayImage(imgUrl[position], imageView);
                imageView.setImageResource(finalImageResource[position]);
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView.setAdjustViewBounds(true);
            }
        };

        holder.carouselView.setSlideInterval(5000);
        holder.carouselView.setImageListener(imageListener);
        //holder.carouselView.setPageCount(imageDrawable.length);
        holder.carouselView.setPageCount(imageResource.length);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, util.dpToPx(180));
        holder.carouselView.setLayoutParams(layoutParams);

        final String[] finalActionUrl = actionUrl;
        holder.carouselView.setImageClickListener(new ImageClickListener() {
            @Override
            public void onClick(int position) {
                Intent browserIntent = new Intent();
                browserIntent.setAction(Intent.ACTION_VIEW);
                browserIntent.addCategory(Intent.CATEGORY_BROWSABLE);
                browserIntent.setData(Uri.parse(finalActionUrl[position]));
                mContext.startActivity(browserIntent);
            }
        });
    }

}
