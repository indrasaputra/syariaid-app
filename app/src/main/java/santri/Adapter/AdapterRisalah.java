package santri.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import santri.syaria.R;
import santri.syaria.retrofit.ContentData;
import santri.video.utils.Tools;

public class AdapterRisalah extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private List<ContentData> items = new ArrayList<>();

    private boolean loading;
    private AdapterRisalah.OnLoadMoreListener onLoadMoreListener;

    private Context ctx;
    private AdapterRisalah.OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, ContentData obj, int position);
    }

    public void setOnItemClickListener(final AdapterRisalah.OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterRisalah(Context context, RecyclerView view, List<ContentData> items) {
        this.items = items;
        ctx = context;
        lastItemViewDetector(view);
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView img_hikmah;
        public TextView title;
        public TextView content;
        public View lyt_parent_risalah;

        public OriginalViewHolder(View v) {
            super(v);
            img_hikmah = v.findViewById(R.id.img_risalah);
            title = v.findViewById(R.id.title_risalah);
            content = v.findViewById(R.id.content_risalah);
            lyt_parent_risalah = v.findViewById(R.id.lyt_parent_risalah);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = v.findViewById(R.id.progress_loading);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_risalah, parent, false);
            vh = new AdapterRisalah.OriginalViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            vh = new AdapterRisalah.ProgressViewHolder(v);
        }
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof AdapterRisalah.OriginalViewHolder) {
            final ContentData p = items.get(position);
            AdapterRisalah.OriginalViewHolder vItem = (AdapterRisalah.OriginalViewHolder) holder;
            vItem.title.setText(p.title);
            vItem.content.setText(Html.fromHtml(p.content));
            Tools.displayImage(ctx, vItem.img_hikmah, p.image.replace("http:","https:"));
//            vItem.img_share.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    String shareMsg = p.title+"\n\n"+p.content;
//                    String footerMsg = "\n\nDownload Aplikasi konsultasi dengan Ustadz/Ustadzah di sini:\nhttps://play.google.com/store/apps/details?id=santri.syaria";
//                    Intent share = new Intent(Intent.ACTION_SEND);
//                    share.setType("text/plain");
//                    share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                    share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
//                    share.putExtra(Intent.EXTRA_SUBJECT, "Syariaid Hikmah");
//                    Uri bmpUri = Tools.getLocalBitmapUri(ctx, vItem.img_hikmah);
//                    share.setType("image/*");
//                    share.setType("image/jpeg");
//                    share.putExtra(Intent.EXTRA_STREAM, bmpUri);
//                    share.putExtra(Intent.EXTRA_TEXT, shareMsg+footerMsg);
//                    ctx.startActivity(Intent.createChooser(share, "Share Hikmah Syariaid"));
//                }
//            });
            vItem.lyt_parent_risalah.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view, p, position);
                    }
                }
            });
        } else {
            ((AdapterRisalah.ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return this.items.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    public void insertData(List<ContentData> items) {
        setLoaded();
        int positionStart = getItemCount();
        int itemCount = items.size();
        this.items.addAll(items);
        notifyItemRangeInserted(positionStart, itemCount);
    }

    public void setLoaded() {
        loading = false;
        for (int i = 0; i < getItemCount(); i++) {
            if (items.get(i) == null) {
                items.remove(i);
                notifyItemRemoved(i);
            }
        }
    }

    public void setLoading() {
        if (getItemCount() != 0) {
            this.items.add(null);
            notifyItemInserted(getItemCount() - 1);
            loading = true;
        }
    }

    public void resetListData() {
        this.items = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void setOnLoadMoreListener(AdapterRisalah.OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    private void lastItemViewDetector(RecyclerView recyclerView) {
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastPos = layoutManager.findLastVisibleItemPosition();
                    if (!loading && lastPos == getItemCount() - 1 && onLoadMoreListener != null) {
                        if (onLoadMoreListener != null) {
                            int current_page = getItemCount() / 20;
                            onLoadMoreListener.onLoadMore(current_page);
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    public interface OnLoadMoreListener {
        void onLoadMore(int current_page);
    }
}
