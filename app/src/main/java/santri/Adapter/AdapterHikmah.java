package santri.Adapter;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import santri.syaria.R;
import santri.syaria.retrofit.ContentData;
import santri.syaria.retrofit.ResponseContent;
import santri.syaria.retrofit.ResponseLike;
import santri.syaria.retrofit.RestAPI;
import santri.syaria.retrofit.RetrofitAdapter;
import santri.video.utils.Tools;

public class AdapterHikmah extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private List<ContentData> items = new ArrayList<>();

    private boolean loading;
    private AdapterHikmah.OnLoadMoreListener onLoadMoreListener;

    private Context ctx;
    private AdapterHikmah.OnItemClickListener mOnItemClickListener;

    private String userid;

    private boolean successLike;
    private int likeIndex = -1;

    public interface OnItemClickListener {
        void onItemClick(View view, ContentData obj, int position);
    }

    public void setOnItemClickListener(final AdapterHikmah.OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterHikmah(Context context, RecyclerView view, List<ContentData> items, String userid) {
        this.items = items;
        ctx = context;
        this.userid = userid;
        lastItemViewDetector(view);
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView img_hikmah;
        public ImageView img_love;
        public ImageView img_share;
        public TextView txt_like_number;
        public TextView title;
        public TextView content;

        public OriginalViewHolder(View v) {
            super(v);
            img_hikmah = v.findViewById(R.id.img_hikmah);
            img_love = v.findViewById(R.id.img_love);
            img_share = v.findViewById(R.id.img_share);
            txt_like_number = v.findViewById(R.id.txt_like_number);
            title = v.findViewById(R.id.title);
            content = v.findViewById(R.id.content);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = v.findViewById(R.id.progress_loading);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_flyer, parent, false);
            vh = new AdapterHikmah.OriginalViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            vh = new AdapterHikmah.ProgressViewHolder(v);
        }
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof AdapterHikmah.OriginalViewHolder) {
            final ContentData p = items.get(position);
            AdapterHikmah.OriginalViewHolder vItem = (AdapterHikmah.OriginalViewHolder) holder;
            vItem.title.setText(p.title);
            vItem.content.setText(Html.fromHtml(p.content));
            vItem.txt_like_number.setText(p.like);
            Tools.displayImage(ctx, vItem.img_hikmah, p.image.replace("http:","https:"));
            if (p.liked) {
                vItem.img_love.setImageDrawable(ctx.getDrawable(R.drawable.ic_love_on));
                vItem.img_love.setTag("liked");
            }

            vItem.img_love.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (userid!=null && !userid.equals("delete")) {
                        likeIndex = position;
                        Log.e("like clicked",position+"");
                        if (vItem.img_love.getTag().equals("unliked")) {
                            vItem.img_love.setImageDrawable(ctx.getDrawable(R.drawable.ic_love_on));
                            vItem.img_love.setTag("liked");
                            notifyDataSetChanged();
                        } //else {
                            //vItem.img_love.setImageDrawable(ctx.getDrawable(R.drawable.ic_love));
                            //vItem.img_love.setTag("unliked");
                            //notifyDataSetChanged();
                        //}
                    }
                }
            });

            if (likeIndex==position) {
                if (userid!=null && !userid.equals("delete")) {
                    vItem.img_love.setImageDrawable(ctx.getDrawable(R.drawable.ic_love_on));
                    vItem.img_love.setTag("liked");
                    sendLike(p.id);
                    vItem.txt_like_number.setText((Integer.parseInt(vItem.txt_like_number.getText()+"")+1)+"");
                }

//                if (userid!=null && !userid.equals("delete")) {
//                    Log.e("position",position+"");
//                    Log.e("tag",vItem.img_love.getTag()+"");
//                    if (vItem.img_love.getTag().equals("liked")) {
//                        Log.e("like detected",position+"");
//                        if (sendLike(p.id)) {
//                            vItem.img_love.setImageResource(R.drawable.ic_love_on);
//                            vItem.img_love.setTag("liked");
//                            vItem.txt_like_number.setText((Integer.parseInt(vItem.txt_like_number.getText()+"")+1)+"");
//                        } else {
//                            vItem.img_love.setImageResource(R.drawable.ic_love);
//                            vItem.img_love.setTag("unliked");
//                        }
//                    } else {
//                        Log.e("unlike detected",position+"");
//                        if (sendUnlike(p.id)) {
//                            vItem.img_love.setImageResource(R.drawable.ic_love);
//                            vItem.img_love.setTag("unliked");
//                            vItem.txt_like_number.setText((Integer.parseInt(vItem.txt_like_number.getText()+"")-1)+"");
//                        } else {
//                            vItem.img_love.setImageResource(R.drawable.ic_love_on);
//                            vItem.img_love.setTag("liked");
//                        }
//                    }
//                }
            }

            vItem.img_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String shareMsg = p.title+"\n\n"+p.content;
                    String footerMsg = "\n\nDownload Aplikasi konsultasi dengan Ustadz/Ustadzah di sini:\nhttps://play.google.com/store/apps/details?id=santri.syaria";
                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.setType("text/plain");
                    share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    share.putExtra(Intent.EXTRA_SUBJECT, "Syariaid Hikmah");
                    Uri bmpUri = Tools.getLocalBitmapUri(ctx, vItem.img_hikmah);
                    share.setType("image/*");
                    share.setType("image/jpeg");
                    share.putExtra(Intent.EXTRA_STREAM, bmpUri);
                    share.putExtra(Intent.EXTRA_TEXT, shareMsg+footerMsg);
                    ctx.startActivity(Intent.createChooser(share, "Share Hikmah Syariaid"));
                }
            });
        } else {
            ((AdapterHikmah.ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    private boolean sendLike(String id) {
        successLike = false;
        JSONObject jsonReq = new JSONObject();
        try {
            jsonReq.put("idcontent", id);
            jsonReq.put("iduser",userid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonReq.toString());
        RestAPI api = RetrofitAdapter.createAPI();
        Call<ResponseLike> call = api.likeContent(bodyRequest);

        call.enqueue(new Callback<ResponseLike>() {
            @Override
            public void onResponse(Call<ResponseLike> call, Response<ResponseLike> response) {
                ResponseLike resp = response.body();
                if (resp!=null) {
                    if (resp.like > 0) {
                        successLike = true;
                    } else {
                        //img.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_love));
                        successLike = false;
                    }
                } else {
                    //img.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_love));
                    successLike = false;
                }
            }

            @Override
            public void onFailure(Call<ResponseLike> call, Throwable t) {
                //img.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_love));
                successLike = false;
            }
        });

        return successLike;
    }

    private boolean sendUnlike(String id) {
        successLike = false;
        JSONObject jsonReq = new JSONObject();
        try {
            jsonReq.put("idcontent", id);
            jsonReq.put("iduser",userid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonReq.toString());
        RestAPI api = RetrofitAdapter.createAPI();
        Call<ResponseLike> call = api.unlikeContent(bodyRequest);

        call.enqueue(new Callback<ResponseLike>() {
            @Override
            public void onResponse(Call<ResponseLike> call, Response<ResponseLike> response) {
                ResponseLike resp = response.body();
                if (resp!=null) {
                    if (resp.like > 0) {
                        //txt.setText(resp.like);
                        successLike = true;
                    } else {
                        //img.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_love));
                        successLike = false;
                    }
                } else {
                    //img.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_love_on));
                    successLike = false;
                }
            }

            @Override
            public void onFailure(Call<ResponseLike> call, Throwable t) {
                //img.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_love_on));
                successLike = false;
            }
        });

        return successLike;
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return this.items.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    public void insertData(List<ContentData> items) {
        setLoaded();
        int positionStart = getItemCount();
        int itemCount = items.size();
        this.items.addAll(items);
        notifyItemRangeInserted(positionStart, itemCount);
    }

    public void setLoaded() {
        loading = false;
        for (int i = 0; i < getItemCount(); i++) {
            if (items.get(i) == null) {
                items.remove(i);
                notifyItemRemoved(i);
            }
        }
    }

    public void setLoading() {
        if (getItemCount() != 0) {
            this.items.add(null);
            notifyItemInserted(getItemCount() - 1);
            loading = true;
        }
    }

    public void resetListData() {
        this.items = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void setOnLoadMoreListener(AdapterHikmah.OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    private void lastItemViewDetector(RecyclerView recyclerView) {
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastPos = layoutManager.findLastVisibleItemPosition();
                    if (!loading && lastPos == getItemCount() - 1 && onLoadMoreListener != null) {
                        if (onLoadMoreListener != null) {
                            int current_page = getItemCount() / 10;
                            onLoadMoreListener.onLoadMore(current_page);
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    public interface OnLoadMoreListener {
        void onLoadMore(int current_page);
    }
}
