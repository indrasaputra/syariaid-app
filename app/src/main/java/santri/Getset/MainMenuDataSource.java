package santri.Getset;

public class MainMenuDataSource {
    public String jenisLayout;

    //body
    private String title;
    private int icon;
    private int backgroundColor;
    private int textColor;
    private int id;

    //slide
    public MainMenuDataSource(String jenisLayout) {
        this.jenisLayout = jenisLayout;
    }

    //body
    public MainMenuDataSource(int id, String title, int icon, int backgroundColor, int textColor, String jenisLayout) {
        this.title = title;
        this.icon = icon;
        this.backgroundColor = backgroundColor;
        this.textColor = textColor;
        this.jenisLayout = jenisLayout;
        this.id = id;
    }

    public String getJenisLayout() { return this.jenisLayout; }

    //-------------------body get--------------
    public String getTitle(){
        return this.title;
    }
    public int getIcon(){
        return icon;
    }
    public int getBackgroundColor(){
        return this.backgroundColor;
    }
    public int getTextColor(){
        return this.textColor;
    }
    public int getId(){
        return id;
    }

}
