package santri.Getset;

/**
 * Created by RedixbitUser on 3/23/2018.
 */

public class orderTimelineGetSet {
    public String getBook_date() {
        return book_date;
    }

    public void setBook_date(String book_date) {
        this.book_date = book_date;
    }

    public String getBook_notes() {
        return book_notes;
    }

    public void setBook_notes(String book_notes) {
        this.book_notes = book_notes;
    }

    public String getBook_duration() {
        return book_duration;
    }

    public void setBook_duration(String book_duration) {
        this.book_duration = book_duration;
    }

    private String book_date;

    public String getBook_time() {
        return book_time;
    }

    public void setBook_time(String book_time) {
        this.book_time = book_time;
    }

    private String book_time;
    private String book_notes;
    private String book_duration;

    private String order_date_time;

    public String getOrder_date_time() {
        return order_date_time;
    }

    public void setOrder_date_time(String order_date_time) {
        this.order_date_time = order_date_time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private String status;
}
