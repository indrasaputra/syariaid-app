package santri.azan.utils;

import android.content.Context;
import android.util.Log;

import com.dd.plist.NSArray;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.dd.plist.NSObject;
import com.dd.plist.PropertyListParser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import santri.azan.model.AllahNames;

public class PListParser {

    public static final boolean LOAD_ALL_PACK = true;

    public static List<AllahNames> getAllahNames(Context context) {
        List<AllahNames> namesPacks = new ArrayList<AllahNames>();
        AllahNames names = null;
        try {

            NSDictionary rootDict = null;
            String filePath = getAppPath(context) + "/allahNames.plist";
            File f = new File(filePath);
            if (f.exists()) {
                rootDict = getNSDFromPath(filePath);
            } else {
                rootDict = getNSDFromPath(context, "allahNames.plist");
            }

            NSObject[] packs = ((NSArray) rootDict.objectForKey("packs"))
                    .getArray();

            Log.i(" ", packs.length + " packs " + packs);
            for (NSObject pack : packs) {
                names = new AllahNames();
                NSObject title = ((NSDictionary) pack).objectForKey("title");
                NSObject meaning = ((NSDictionary) pack)
                        .objectForKey("meaning");
                NSObject desc1 = ((NSDictionary) pack)
                        .objectForKey("description1");
                NSObject desc2 = ((NSDictionary) pack)
                        .objectForKey("description2");

                names.setTitle(nsObjectToString(title));
                names.setMeaning(nsObjectToString(meaning));
                names.setDescription1(nsObjectToString(desc1));
                names.setDescription2(nsObjectToString(desc2));
                System.out.println("!!!pack name " + names.getTitle());
                namesPacks.add(names);
            }

            LogUtils.i(" ", " mustache path " + namesPacks.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return namesPacks;
    }

    public static String getAppPath(Context context) {
        String appPath = context.getFilesDir().getAbsolutePath();
        return appPath;
    }

    private static String nsObjectToString(NSObject nsObject) {
        String s = "";
        if (nsObject != null && nsObject.toJavaObject() != null) {
            s = nsObject.toJavaObject().toString();
        }
        return s;
    }

    @SuppressWarnings("unused")
    private static boolean nsObjectToBoolean(NSObject nsObject) {
        boolean b = false;
        if (nsObject != null) {
            if (nsObject.getClass().equals(NSNumber.class)) {
                NSNumber nsNumber = (NSNumber) nsObject;
                if (nsNumber.type() == NSNumber.BOOLEAN) {
                    b = nsNumber.boolValue();
                }
            }

        }
        return b;
    }

    private static NSDictionary getNSDFromPath(Context context, String path) {
        try {
            return (NSDictionary) PropertyListParser.parse(context.getAssets()
                    .open(path));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(path);
        }
        return null;
    }

    private static NSDictionary getNSDFromPath(String path) {
        try {
            return (NSDictionary) PropertyListParser.parse(new File(path));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
