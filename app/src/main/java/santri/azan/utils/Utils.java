package santri.azan.utils;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import com.afollestad.materialdialogs.MaterialDialog;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import santri.azan.activity.ActivitySettings;
import santri.azan.receiver.AlarmReceiver;
import santri.azan.receiver.NotificationReceiver;
import santri.syaria.R;

public class Utils extends AppCompatActivity implements LocationListener {
    // All Static variables
    public static final boolean IS_DEBUG = false;

    public static final boolean SHOULD_PRINT_LOG = IS_DEBUG;

    protected Typeface tf, tf1, tf2, tf3,tf_arabic;

    protected String ar_font = "1", en_font = "1", pref_time = "0",notify="0";
    protected int afont = 1, efont = 1;
    Intent alarmIntent_1;
    SharedPreferences pref;
    SharedPreferences spref;
    SharedPreferences.Editor editor;

    public static String SharedPref_filename = "Prayer_pref";

    public String USER_COUNTRY = "user_country";
    public String USER_STATE = "user_state";
    public String USER_CITY = "user_city";
    public String USER_LAT = "user_lat";
    public String USER_LNG = "user_lng";
    public String USER_MLAT = "user_mlat";
    public String USER_MLNG = "user_mlng";
    public String USER_STREET = "user_street";
    public String USER_LANGUAGES = "user_langu";
    public String CITY_CODE_PREF = "city_code_pref";
    public String CITY_NAME_PREF = "city_name_pref";

    public static final String USER_COMPASS = "user_compass";

    Location location;
    double latitude;
    double longitude;
    protected LocationManager locationManager;
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    boolean canGetLocation = false;

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 5; // 10 meters

    private static final long MIN_TIME_BW_UPDATES = 1000 * 30 * 1; // 1 minute

    protected int style[] = { R.style.FontStyle_Small,
            R.style.FontStyle_Medium, R.style.FontStyle_Large };
    protected int styleheader[] = { R.style.FontStyle_title_small,
            R.style.FontStyle_title_medium, R.style.FontStyle_title_large };

    public void Actionbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SpannableString s = new SpannableString(title);
        s.setSpan(new TypefaceSpan("MyTypeface.otf"), 0, s.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(
                (Html.fromHtml("<font color=\"#ffffff\">" + s + "</font>")));

    }

    public void typeface() {
        tf = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Thin.ttf");
        tf1 = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
        tf2 = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Light.ttf");
        tf3 = Typeface.createFromAsset(getAssets(), "fonts/Helvetica.otf");
        //tf_arabic = Typeface.createFromAsset(getAssets(), "fonts/Aceh-Darusalam.ttf");
        //tf_arabic = Typeface.createFromAsset(getAssets(), "fonts/noorehidayat.ttf");
        tf_arabic = Typeface.createFromAsset(getAssets(), "fonts/Scheherazade-Bold.ttf");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id==android.R.id.home){

            finish();
        }else
        if(id==R.id.action_settings){

            Intent intent = new Intent(Utils.this, ActivitySettings.class);
            startActivity(intent);
        }

        /*if(id==R.id.action_qibla){

            Intent intent = new Intent(Utils.this, ActivityThemes.class);
            startActivity(intent);
        }*/


        return super.onOptionsItemSelected(item);
    }

    public void font() {

        alarmIntent_1 = new Intent(Utils.this, NotificationReceiver.class);
        pref = PreferenceManager
                .getDefaultSharedPreferences(this);
        ar_font = pref.getString("arabicfont", "1");
        en_font = pref.getString("englishfont", "1");
        notify = pref.getString("notification", "0");

        if (en_font.equals("0")) {
            efont = 0;
        } else if (en_font.equals("1")) {
            efont = 1;
        } else if (en_font.equals("2")) {
            efont = 2;
        }

        if (ar_font.equals("0")) {
            afont = 0;
        } else if (ar_font.equals("1")) {
            afont = 1;
        } else if (ar_font.equals("2")) {
            afont = 2;
        }

        pref_time = pref.getString("timeformat", "0");
        LogUtils.i("notification "+ notify);

    }

    public String LoadPref(String key) {
        spref = getSharedPreferences(SharedPref_filename, MODE_PRIVATE);
        return spref.getString(key, "");
    }

    public void SavePref(String key, String value) {
        spref = getSharedPreferences(SharedPref_filename, MODE_PRIVATE);
        editor = spref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    // Method to load map type setting
    public Long loadLong(String param) {
        SharedPreferences sharedPreferences = getSharedPreferences(
                "Prayer_pref", 0);
        Long selectedPosition = sharedPreferences.getLong(param, 0);

        return selectedPosition;
    }

    // Method to load map type setting
    public String loadString(String param) {

        SharedPreferences sharedPreferences = getSharedPreferences(
                "Prayer_pref", 0);
        String selectedPosition = sharedPreferences.getString(param, "");
        return selectedPosition;
    }

    // Method to save map type setting to SharedPreferences
    public void saveString(String param, String value) {
        SharedPreferences sharedPreferences = getSharedPreferences(
                "Prayer_pref", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(param, value);
        editor.commit();
    }

    public boolean isOnline() {
        ConnectivityManager manager = (ConnectivityManager) this
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager.getActiveNetworkInfo() != null
                && manager.getActiveNetworkInfo().isAvailable()
                && manager.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    public void showalert() {

        new MaterialDialog.Builder(this).title("No Internet Connection")
                .content("Enable your Network Connectivity").cancelable(false)
                .positiveText("Ok").positiveColor(Color.parseColor("#379e24"))
                .negativeText(android.R.string.cancel)
                .negativeColor(Color.parseColor("#379e24"))
                .callback(new MaterialDialog.ButtonCallback() {

                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                        // finish();
                    }

                }).build().show();
    }

    public double getLatitude() {
        if (location != null) {
            latitude = location.getLatitude();
        }

        return latitude;
    }

    public double getLongitude() {
        if (location != null) {
            longitude = location.getLongitude();
        }

        return longitude;
    }

    @SuppressLint("MissingPermission")
    public Location getLocation() {
        try {

            locationManager = (LocationManager) this
                    .getSystemService(Context.LOCATION_SERVICE);

            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            Log.d("Network", isNetworkEnabled + " Network " + isGPSEnabled);
            if (isNetworkEnabled) {

                this.canGetLocation = true;
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }

                }
            } else if (isGPSEnabled) {
                if (location == null) {
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("GPS Enabled", "GPS Enabled");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
            }

            Log.d("Network", latitude + " Network " + longitude);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    public void showSettingsAlert() {

        new MaterialDialog.Builder(this).title("GPS is Not Enabled")
                .content(getResources().getString(R.string.gps_openmsg)).cancelable(false)
                .positiveText("Ok").positiveColor(Color.parseColor("#379e24"))
                .negativeText(android.R.string.cancel)
                .negativeColor(Color.parseColor("#379e24"))
                .callback(new MaterialDialog.ButtonCallback() {

                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                        // finish();
                    }

                }).build().show();
    }

    public void Alert(String title, String msg) {

        new MaterialDialog.Builder(this).title(title).content(msg)
                .cancelable(false).positiveText("Ok")
                .positiveColor(Color.parseColor("#379e24"))
                .negativeText(android.R.string.cancel)
                .negativeColor(Color.parseColor("#379e24"))
                .callback(new MaterialDialog.ButtonCallback() {

                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                        // finish();
                    }

                }).build().show();
    }

    public void fullscreen() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

    }

    public int LoadPrefInt(String key) {
        spref = getSharedPreferences(SharedPref_filename, MODE_PRIVATE);
        return spref.getInt(key, 0);
    }

    public static double bearing(double lat1, double lon1, double lat2,
                                 double lon2) {
        double lat1Rad = Math.toRadians(lat1);
        double lat2Rad = Math.toRadians(lat2);
        double deltaLonRad = Math.toRadians(lon2 - lon1);

        double y = Math.sin(deltaLonRad) * Math.cos(lat2Rad);
        double x = Math.cos(lat1Rad) * Math.sin(lat2Rad) - Math.sin(lat1Rad)
                * Math.cos(lat2Rad) * Math.cos(deltaLonRad);
        return radToBearing(Math.atan2(y, x));
    }

    public static double radToBearing(double rad) {
        return (Math.toDegrees(rad) + 360) % 360;
    }

    public String getTimeDate(int time) throws ParseException {

        DateFormat df = new SimpleDateFormat("hh:mm a MMMM d, yyyy" , Locale.US);
        String now_time = df.format(new Date());
        Calendar cal = Calendar.getInstance();
        cal.setTime(df.parse(now_time));
        cal.add(Calendar.DATE, time);
        String new_time = df.format(cal.getTime());
        return new_time;

    }

    public String changeTimeFormat(String time) throws ParseException {
        SimpleDateFormat displayFormat;
        SimpleDateFormat parseFormat;
        if (this.pref_time.equals("0")) {
            displayFormat = new SimpleDateFormat("HH:mm", Locale.US);
            parseFormat = new SimpleDateFormat("hh:mm a", Locale.US);
        } else {
            displayFormat = new SimpleDateFormat("HH:mm", Locale.US);
            parseFormat = new SimpleDateFormat("hh:mm", Locale.US);
        }
        return displayFormat.format(parseFormat.parse(time));
    }

    public double getTimeZone(String selectedId) {

        TimeZone tzo = TimeZone.getTimeZone(selectedId);
        int TZOR = tzo.getOffset(new Date().getTime()) / 1000/ 60;

        int hrs = TZOR / 60;
        int mins = TZOR % 60;
        String result = hrs + "." + (mins == 30 ? 5 : 0);

        return Double.parseDouble(result);
    }

    public String addDayLight(String time, int daylight) throws ParseException {
        DateFormat df;
        if (this.pref_time.equals("0")) {
            df = new SimpleDateFormat("hh:mm a", Locale.US);
        } else {
            df = new SimpleDateFormat("HH:mm", Locale.US);
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(df.parse(time));
        cal.add(12, daylight);
        return df.format(cal.getTime());
    }

    public void cancel_all_alarm(Context context) {

        Intent alarmIntent = new Intent(context, AlarmReceiver.class);
        for (int id = 0; id <= 6; id++) {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                    id, alarmIntent, 0);
            AlarmManager manager = (AlarmManager) context
                    .getSystemService(Context.ALARM_SERVICE);
            manager.cancel(pendingIntent);
        }
    }

    public String getTimeNow(int time) throws ParseException {

        DateFormat df = new SimpleDateFormat("MMMM d, yyyy" , Locale.US);
        String now_time = df.format(new Date());
        Calendar cal = Calendar.getInstance();
        cal.setTime(df.parse(now_time));
        cal.add(Calendar.DATE, time);
        String new_time = df.format(cal.getTime());
        return new_time;

    }

    public Calendar getTimeByCal(int time) throws ParseException {

        DateFormat df = new SimpleDateFormat("MMMM d, yyyy", Locale.US);
        String now_time = df.format(new Date());
        Calendar cal = Calendar.getInstance();
        cal.setTime(df.parse(now_time));
        cal.add(Calendar.DATE, time);

        return cal;
    }

    public String getDateByCal(int time) throws ParseException {

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String now_time = df.format(new Date());
        Calendar cal = Calendar.getInstance();
        cal.setTime(df.parse(now_time));
        cal.add(Calendar.DATE, time);
        return df.format(cal.getTime());
    }

    public String addDay_Light(String time, int daylight) throws ParseException {
        DateFormat format = new SimpleDateFormat("HH:mm", Locale.US);
        //DateFormat df = new SimpleDateFormat("HH:mm");

        Calendar cal = Calendar.getInstance();


        cal.setTime(format.parse(time));
        cal.add(Calendar.MINUTE, daylight);

        String new_time = format.format(cal.getTime());
        return new_time;

    }

    public String LoadStringPref(String key) {
        spref = getSharedPreferences(SharedPref_filename, MODE_PRIVATE);
        return spref.getString(key, "0");
    }


}
