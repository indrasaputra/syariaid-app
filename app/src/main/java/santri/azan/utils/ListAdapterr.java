package santri.azan.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import santri.azan.activity.QuranPlayActivity;
import santri.syaria.R;

public class ListAdapterr extends BaseAdapter {

    LayoutInflater inflater;
    private List<Data> Datalist = null;
    private ArrayList<Data> arraylist;
    Context context;

    public ListAdapterr(Activity context, List<Data> openSite) {
        this.context=context;
        this.Datalist = openSite;
        inflater = LayoutInflater.from(context);
        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(openSite);

    }

    @Override
    public int getCount() {
        return Datalist.size();
    }

    @Override
    public Object getItem(int position) {
        return Datalist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        View Item = inflater.inflate(R.layout.quran_listview_item, null, true);

        ImageView QuranListIcon = (ImageView) Item.findViewById(R.id.quranitemicoid);
        Drawable quranlisticon1 = context.getResources().getDrawable(R.drawable.quranlist1_1);
        Drawable quranlisticon2 = context.getResources().getDrawable(R.drawable.quranlist1_2);
        Drawable quranlisticon3 = context.getResources().getDrawable(R.drawable.quranlist1_3);
        Drawable quranlisticon4 = context.getResources().getDrawable(R.drawable.quranlist1_4);
        Drawable quranlisticon5 = context.getResources().getDrawable(R.drawable.quranlist1_5);
        Drawable quranlisticon6 = context.getResources().getDrawable(R.drawable.quranlist1_6);
        Drawable quranlisticon7 = context.getResources().getDrawable(R.drawable.quranlist1_7);


        TextView txtTitle = (TextView) Item.findViewById(R.id.titleid);
        txtTitle.setText(Datalist.get(position).getSubject());

        TextView txtNumber = (TextView) Item.findViewById(R.id.txt_number);
        int number = position+1;
        txtNumber.setText(number+"");


        Item.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(context, QuranPlayActivity.class);
                i.putExtra("name",Datalist.get(position).getSubject());
                i.putExtra("url",Datalist.get(position).getLink());
                Log.d("SURAH ::::", Datalist.get(position).getSubject());
                Log.d("Link ::::", Datalist.get(position).getLink());
                context.startActivity(i);
            }
        });


        if (position % 1 == 0){
            QuranListIcon.setImageDrawable(quranlisticon1);
        }

        if (position % 2 == 0){
            QuranListIcon.setImageDrawable(quranlisticon2);
        }

        if (position % 3 == 0){
            QuranListIcon.setImageDrawable(quranlisticon3);
        }

        if (position % 4 == 0){
            QuranListIcon.setImageDrawable(quranlisticon4);
        }
        if (position % 5 == 0){
            QuranListIcon.setImageDrawable(quranlisticon5);
        }
        if (position % 6 == 0){
            QuranListIcon.setImageDrawable(quranlisticon6);
        }
        if (position % 7 == 0){
            QuranListIcon.setImageDrawable(quranlisticon7);
        }




        return Item;



    };
}
