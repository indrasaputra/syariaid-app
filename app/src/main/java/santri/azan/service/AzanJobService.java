package santri.azan.service;

import android.app.job.JobParameters;
import android.app.job.JobService;

public class AzanJobService extends JobService {
    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        AzanScheduleUpdateTask azanScheduleUpdateTask = new AzanScheduleUpdateTask();
        azanScheduleUpdateTask.getAzanSchedule(this);

        return false;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        return false;
    }
}
