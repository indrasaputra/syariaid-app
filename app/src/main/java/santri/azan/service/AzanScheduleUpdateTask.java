package santri.azan.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.Executor;

import androidx.annotation.NonNull;
import santri.azan.activity.PrayerTimesActivity;
import santri.azan.utils.Utils;
import santri.syaria.R;

public class AzanScheduleUpdateTask extends Utils {
    double latitude = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    double longitude = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    String Error;
    String[] prayer12, prayer24, p_time;
    SharedPreferences pref;
    Context context;
    String alarm_time;
    PendingIntent pendingIntent;
    Intent alarmIntent;
    int id;
    String[] p_name;
    TaskExecutor taskExecutor;
    SharedPreferences spref;

    public AzanScheduleUpdateTask() {
        taskExecutor = new TaskExecutor();
    }

    public String LoadPref(String key) {
        spref = getSharedPreferences(Utils.SharedPref_filename, MODE_PRIVATE);
        return spref.getString(key, "");
    }

    public void getAzanSchedule(Context context) {
        taskExecutor.execute(new AzanUpdateTask());
    }

    public class TaskExecutor implements Executor {
        @Override
        public void execute(@NonNull Runnable runnable) {
            Thread t =  new Thread(runnable);
            t.start();
        }
    }

    public class AzanUpdateTask implements Runnable {

        public AzanUpdateTask() {
            latitude = Double.parseDouble(LoadPref(USER_LAT));
            longitude = Double.parseDouble(LoadPref(USER_LNG));
        }

        @Override
        public void run() {
            URL hp;
            p_name = getResources().getStringArray(R.array.prayer_array);

            try {
                //hp = new URL("https://muslimsalat.com/"+ city + "/daily/"+getDateByCal(time)+".json?key=cd03fabbc81af5960df410e72601ecce");
                hp = new URL("https://api.pray.zone/v2/times/day.json?latitude="+latitude+"&longitude="+longitude+"&elevation=500&date="+getDateByCal(0)+"&timeformat=1&juristic=0");
                Log.e("URLs", "" + hp);
                URLConnection hpCon = hp.openConnection();
                hpCon.connect();
                InputStream input = hpCon.getInputStream();
                Log.d("input", "" + input);
                BufferedReader r = new BufferedReader(new InputStreamReader(input));
                String x;
                x = r.readLine();
                StringBuilder total = new StringBuilder();
                while (x != null) {
                    total.append(x);
                    x = r.readLine();
                }
                JSONObject jsonObject = new JSONObject(total.toString());

                if (jsonObject.has("status")) {
                    JSONObject jsonResults = jsonObject.getJSONObject("results");
                    JSONArray jArrayDatetime = jsonResults.getJSONArray("datetime");
                    JSONObject jObj = jArrayDatetime.getJSONObject(0);
                    JSONObject jsonTimes = jObj.getJSONObject("times");

                    prayer12 = new String[6];
                    prayer12[0] = jsonTimes.getString("Imsak");
                    prayer12[1] = jsonTimes.getString("Fajr");
                    prayer12[2] = jsonTimes.getString("Dhuhr");
                    prayer12[3] = jsonTimes.getString("Asr");
                    prayer12[4] = jsonTimes.getString("Maghrib");
                    prayer12[5] = jsonTimes.getString("Isha");

                    Log.d("strArr", prayer12[0]);
                    p_time = prayer12;

                    prayer24 = new String[6];
                    //if (pref_time.equals("1")) {
                        //Format of the date defined in the input String
                    DateFormat df = new SimpleDateFormat("hh:mm aa");
                    //Desired format: 24 hour format: Change the pattern as per the need
                    DateFormat outputformat = new SimpleDateFormat("HH:mm");

                    prayer24[0] = outputformat.format(df.parse(jsonTimes.getString("Imsak")));
                    prayer24[1] = outputformat.format(df.parse(jsonTimes.getString("Fajr")));
                    prayer24[2] = outputformat.format(df.parse(jsonTimes.getString("Dhuhr")));
                    prayer24[3] = outputformat.format(df.parse(jsonTimes.getString("Asr")));
                    prayer24[4] = outputformat.format(df.parse(jsonTimes.getString("Maghrib")));
                    prayer24[5] = outputformat.format(df.parse(jsonTimes.getString("Isha")));
                    Log.d("strArr", prayer24[0]);
                    p_time = prayer24;
                    //}

                    for (int i=0;i<p_name.length;i++) {
                        if(LoadPref(p_name[i])==null || LoadPref(p_name[i]).equals("tru")) {
                            setAlarm(p_name[i], p_time[i]);
                        }
                    }

                } else {
                    Error = "Unable to get schedule";
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                //Error = e.getMessage();
            } catch (NullPointerException e) {
                // TODO: handle exception
                //Error = e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    public void setAlarm(String stime, String name) {
        DateFormat df = new SimpleDateFormat("MMMM d, yyyy", Locale.US);
        String now_date = df.format(new Date());
        int hour = 0;
        int minute = 0;
        int position;

        try {
            String timenow = changeTimeFormat(stime);
            hour = Integer.parseInt(timenow.substring(0, 2));
            minute = Integer.parseInt(timenow.substring(3, 5));
        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(df.parse(now_date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        calendar.set(11, hour);
        calendar.set(12, minute + Integer.parseInt(this.pref.getString(name + "daylight", "0")));
        Date now = new Date();
        Calendar cal_now = Calendar.getInstance();
        cal_now.setTime(now);

        cal_now.setTime(now);

        Log.d("Setting Alarm", name + " " + stime);

        if (cal_now.before(calendar)) {
            try {
                cal_now.setTime(now);
                alarm_time = "tru";
                pendingIntent = PendingIntent.getBroadcast(context, id, alarmIntent, 0);
                ((AlarmManager) getSystemService(Context.ALARM_SERVICE)).set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), this.pendingIntent);
                //Toast.makeText(this, " Alarm Enabled", Toast.LENGTH_SHORT).show();
                cal_now.setTime(now);

            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else {
            this.alarm_time = "tru";
            this.pendingIntent = PendingIntent.getBroadcast(context, id, alarmIntent,0 );
            ((AlarmManager) getSystemService(Context.ALARM_SERVICE)).cancel(this.pendingIntent);
            //Toast.makeText(this, "Prayer has been passed, Alarm set for Next Day", Toast.LENGTH_SHORT).show();
        }
    }



}
