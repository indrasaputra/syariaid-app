package santri.azan.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import santri.azan.model.QuranScript;

public class Database extends SQLiteAssetHelper {

    private static final int DATABASE_VERSION = 1;
    public static final String TABLE_VERSES= "verses";
    public Database(Context context, String dbfilename) {
        super(context, dbfilename, null, DATABASE_VERSION);

    }

    public List<QuranScript> getTransliteration(int suracount){

        SQLiteDatabase db = getReadableDatabase();
        List<QuranScript> dblist = new ArrayList<QuranScript>();

        String Qurey = "select * from " + TABLE_VERSES + " where sura = "+ suracount ;

        Cursor cursor = db.rawQuery(Qurey, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    QuranScript qs = new QuranScript();
                    qs.setSura(cursor.getInt(0));
                    qs.setAyah(cursor.getInt(1));
                    qs.setText(cursor.getString(2));
                    dblist.add(qs);
                } while (cursor.moveToNext());
            }
        }
        cursor.close();
        return dblist;

    }
}
