package santri.azan.hijridate;

public abstract class MoonPhases {
    public  abstract double calculatePhase(double T);
}
