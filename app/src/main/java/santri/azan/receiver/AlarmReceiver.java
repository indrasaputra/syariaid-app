package santri.azan.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import santri.azan.activity.ActivityAlarm;
import santri.azan.utils.LogUtils;

public class AlarmReceiver extends BroadcastReceiver {

    String type,time;
    @Override
    public void onReceive(Context context, Intent intent) {

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        Bundle extras = intent.getExtras();
        LogUtils.i( extras.getString("type") +" REceiver "+  extras.getString("time") +" noti "+pref.getString("notification", "0"));
        if(extras!=null){
            type = extras.getString("type");
            time = extras.getString("time");
        }





        Intent intent1 = new Intent(context, ActivityAlarm.class);
        intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if(extras!=null){
            intent1.putExtra("type",type);
            intent1.putExtra("time",time);

            context.startActivity(intent1);
        }
    }


}
