package santri.azan.receiver;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.core.app.NotificationCompat;

import santri.azan.activity.PrayerTimesActivity;
import santri.azan.utils.LogUtils;
import santri.syaria.R;

public class NotificationReceiver extends BroadcastReceiver {

    String type,time;
    int id;
    @Override
    public void onReceive(Context context, Intent intent) {

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        Bundle extras = intent.getExtras();
        LogUtils.i( extras.getString("type") +" REceiver "+  extras.getString("time") +" noti "+pref.getString("notification", "1"));
        if(extras!=null){
            type = extras.getString("type");
            time = extras.getString("time");
            id= extras.getInt("id");
        }


        if(pref.getString("notification", "0").equals("0"))
        {

            NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(context)
                            .setSmallIcon(R.drawable.ic_launcher)
                            .setContentTitle(context.getString(R.string.app_name))
                            .setAutoCancel(true)
                            .setSound(Uri.parse("android.resource://com.ironcodes.praytimes.azan/" + R.raw.azaan))
                            .setContentText(type+" "+ time);



            //icon appears in device notification bar and right hand corner of notification
            builder.setSmallIcon(R.drawable.ic_launcher);

            // This intent is fired when notification is clicked
            Intent resultIntent = new Intent(context, PrayerTimesActivity.class);

            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT
                    | PendingIntent.FLAG_ONE_SHOT);

            // Set the intent that will fire when the user taps the notification.
            builder.setContentIntent(pendingIntent);

            // Large icon appears on the left of the notification

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            // Will display the notification in the notification bar
            notificationManager.notify(id, builder.build());


        }
    }


}
