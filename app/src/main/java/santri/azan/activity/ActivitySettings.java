package santri.azan.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Spinner;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import androidx.appcompat.widget.Toolbar;
import santri.azan.preference.PreferenceFragment;
import santri.azan.support.AppLocationService;
import santri.azan.utils.LogUtils;
import santri.azan.utils.Utils;
import santri.syaria.HomeMenuActivity;
import santri.syaria.R;

public class ActivitySettings extends Utils {


    int[] tones_array = new int[]{R.raw.azan0, R.raw.azan1, R.raw.azan2, R.raw.azan3, R.raw.azan4, R.raw.azan5, R.raw.azan6};
    private MediaPlayer mp;
    int playingFlag = 0;
    Toolbar toolbar;
    boolean menu_show;
    Runnable r; Handler handle;

    Spinner Langspinner;
    Locale myLocale;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.preference_activity_custom);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Actionbar("Settings");
        typeface();
        //Analytics("Settings");
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            menu_show = intent.getBooleanExtra("menu_show", false);
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new SettingsFragment()).commit();




        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String userlango = preferences.getString(USER_LANGUAGES , USER_LANGUAGES);
        LogUtils.i("in onCreate" ,"preferences = "+ userlango);

        if (userlango !=null) {

            Locale locale = new Locale(userlango);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        MenuItem menu_ok = menu.findItem(R.id.menu_ok);
        if (!menu_show) {
            menu_ok.setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.menu_ok) {
/*
			Intent intent = new Intent(ActivitySettings.this,
					ActivityMain.class);
			startActivity(intent);*/
            finish();
        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressLint({"ValidFragment"})
    public static class SettingsFragment extends PreferenceFragment implements
            SharedPreferences.OnSharedPreferenceChangeListener {

        AppLocationService appLocationService;
        ListPreference lpm, lpl, lpj, lpt, lph,lar,len,lt,ln,la,langu;
        SharedPreferences sp;
        //MaterialEditTextPreference ep;
        Preference city_manual, city_automatic,ltln,select_languagess;
        RingtonePreference rp;
        SharedPreferences.Editor editor;


        private static final int LOCATION_INTENT_CALLED = 1;
        private static final String TAG_CITY_MANUAL = "change_city_manual";
        private static final String TAG_CITY_AUTO = "change_city_auto";
        private static final String TAG_METHOD = "method";
        private static final String TAG_TIMEZONE = "timezone";
        private static final String TAG_JURISTIC = "juristic";
        private static final String TAG_LATITUDE = "higherLatitudes";
        private static final String TAG_RINGTONE = "ringtone";
        private static final String TAG_RINGTONE_TITLE = "ringtone_title";
        //	private static final String TAG_DAYLIGHT = "daylight";
        private static final String TAG_HIJRI = "hijriday";
        private static final String TAG_ARABIC_FONT = "arabicfont";
        private static final String TAG_ENGLISH_FONT = "englishfont";
        private static final String TAG_TIME = "timeformat";
        private static final String TAG_NOTIFICATION = "notification";
        private static final String TAG_LATLNG = "latlng";
        public static final String TAG_AZAN = "azan";
        public static final String TAG_LANGUAGES = "languages";

        String selected_azan;


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.adjust);

            sp = PreferenceManager.getDefaultSharedPreferences(getActivity());

            rp = (RingtonePreference) findPreference(TAG_RINGTONE);
            lpm = (ListPreference) findPreference(TAG_METHOD);
            lpj = (ListPreference) findPreference(TAG_JURISTIC);
            lpl = (ListPreference) findPreference(TAG_LATITUDE);
            lpl = (ListPreference) findPreference(TAG_LATITUDE);
            lpt = (ListPreference) findPreference(TAG_TIMEZONE);
            lph = (ListPreference) findPreference(TAG_HIJRI);
            lt = (ListPreference) findPreference(TAG_TIME);
            ln = (ListPreference) findPreference(TAG_NOTIFICATION);
            la = (ListPreference) findPreference(TAG_AZAN);


            this.selected_azan = this.la.getValue();

            //lpm.setSummary(lpm.getEntry());
            //lpj.setSummary(lpj.getEntry());
            //lpl.setSummary(lpl.getEntry());
            lph.setSummary(lph.getEntry());
            lt.setSummary(lt.getEntry());
            ln.setSummary(ln.getEntry());
            la.setSummary(la.getEntry());


            String[] idArray = TimeZone.getAvailableIDs();
            //lpt.setEntries(idArray);

            //lpt.setEntryValues(idArray);

            String ringtonePath = sp
                    .getString(TAG_RINGTONE, "default ringtone");

//            if (sp.getString("timezone", "").equals("")) {
//
//                Date now = new Date();
//                Calendar cal = Calendar.getInstance();
//                cal.setTime(now);
//
//                String TimeZoneName = cal.getTimeZone().getID().toString();
//
//                lpt.setValue(sp.getString("timezone", TimeZoneName));
//                LogUtils.i("time" + "ee" + TimeZoneName);
//
//            } else {
//
//                lpt.setValue(lpt.getValue());
//            }
//
//            lpt.setSummary(lpt.getEntry());

            select_languagess = findPreference(TAG_LANGUAGES);
            select_languagess
                    .setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

                        @Override
                        public  boolean onPreferenceClick(Preference preference) {
                            if(getActivity() instanceof  ActivitySettings) {
                                ((ActivitySettings) getActivity()).cancel_all_alarm(getActivity());
                            }
                            Intent viewIntent = new Intent(getActivity(),
                                    Activitylangradio.class);
                            startActivity(viewIntent);
                            if(getActivity() instanceof  ActivitySettings) {
                                ((ActivitySettings)getActivity()).finish();
                            }
                            return true;

                        }

                    });

            city_manual = findPreference(TAG_CITY_MANUAL);
            city_manual
                    .setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

                        @Override
                        public boolean onPreferenceClick(Preference preference) {
                            if(getActivity() instanceof  ActivitySettings) {

                                ((ActivitySettings)getActivity()).cancel_all_alarm(getActivity());
                            }
//                            Intent viewIntent = new Intent(getActivity(),
//                                    ActivitySearch.class);
//                            viewIntent.putExtra("type", "country");
//                            startActivity(viewIntent);
//                            getActivity().finish();
                            return true;
                        }
                    });

            ltln = findPreference(TAG_LATLNG);
            ltln
                    .setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

                        @Override
                        public boolean onPreferenceClick(Preference preference) {



                            if(getActivity() instanceof  ActivitySettings) {
                                ((ActivitySettings)getActivity()).cancel_all_alarm(getActivity());
                            }
                            Intent viewIntent = new Intent(getActivity(),
                                    ActivityLatLng.class);
                            startActivity(viewIntent);
                            if(getActivity() instanceof  ActivitySettings) {
                                ((ActivitySettings)getActivity()).finish();
                            }
//				finish();
                            return true;
                        }
                    });
            city_automatic = findPreference(TAG_CITY_AUTO);
            city_automatic
                    .setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

                        @Override
                        public boolean onPreferenceClick(Preference preference) {
                            if(getActivity() instanceof  ActivitySettings) {



                                if (((ActivitySettings)getActivity()).isOnline()) {
                                    ((ActivitySettings)getActivity()).cancel_all_alarm(getActivity());

                                    getLocation();
                                }else
                                {
                                    ((ActivitySettings)getActivity()).showalert();
                                }
                            }

                            return true;
                        }
                    });

        }

        public void getLocation() {
            try {
                appLocationService = new AppLocationService(getActivity());

                Location location = appLocationService.getLocation();

                LogUtils.i("location "+location);
                if (location != null) {
                    if(getActivity() instanceof  ActivitySettings) {
                        double latitude = location.getLatitude();
                        double longitude = location.getLongitude();
                        if(getActivity() instanceof  ActivitySettings) {
                            ((ActivitySettings)getActivity()).saveString(((ActivitySettings)getActivity()).USER_LAT, String.valueOf(latitude));
                            ((ActivitySettings)getActivity()).saveString(((ActivitySettings)getActivity()).USER_LNG, String.valueOf(longitude));
                        }
                        ltln.setSummary(latitude+","+longitude);


                        Geocoder geocoder = new Geocoder(getActivity(), Locale.ENGLISH);

                        List<Address> addresses = geocoder.getFromLocation(

                                Double.parseDouble(((ActivitySettings)getActivity()).loadString(((ActivitySettings)getActivity()).USER_LAT)),
                                Double.parseDouble(((ActivitySettings)getActivity()).loadString(((ActivitySettings)getActivity()).USER_LNG)), 1);

                        for (Address adrs : addresses) {

                            LogUtils.i(adrs.getLocality()+ " city "+ adrs.getAdminArea() +" state "+ adrs.getCountryName());
                            if (adrs != null) {

                                String city = adrs.getLocality();
                                String Country = adrs.getCountryName();
                                String State = adrs.getSubAdminArea();
                                String Street = adrs.getSubLocality();
                                //LogUtils.i("getSubAdminArea " + adrs.getSubAdminArea() + " getFeatureName " + adrs.getFeatureName() + " getPremises " + adrs.getPremises());
                                Log.d("getSubAdminArea", adrs.getSubAdminArea()+"");
                                Log.d("getFeatureName", adrs.getFeatureName()+"");
                                Log.d("getPremises", adrs.getPremises()+"");
                                Log.d("getLocality", adrs.getLocality()+"");
                                Log.d("getSubLocality", adrs.getSubLocality()+"");
                                Log.d("getAdminArea", adrs.getAdminArea()+"");

                                ((ActivitySettings)getActivity()).SavePref(((ActivitySettings)getActivity()).USER_CITY, city);
                                ((ActivitySettings)getActivity()).SavePref(((ActivitySettings)getActivity()).USER_STATE, State);
                                ((ActivitySettings)getActivity()).SavePref(((ActivitySettings)getActivity()).USER_COUNTRY, Country);
                                ((ActivitySettings)getActivity()).SavePref(((ActivitySettings)getActivity()).USER_STREET, Street);

                                city_automatic.setSummary(getString(R.string.lblcitycountry,
                                        city,State, Country));
                                city_manual.setSummary(getString(R.string.lblcitycountry, city,State,
                                        Country));

                            }

                        }

                    }} }catch (IOException e) {
                e.printStackTrace();
                ((ActivitySettings)getActivity()).SavePref(((ActivitySettings)getActivity()).USER_CITY, "");
                ((ActivitySettings)getActivity()).SavePref(((ActivitySettings)getActivity()).USER_STATE, "");

                ((ActivitySettings)getActivity()).SavePref(((ActivitySettings)getActivity()).USER_STREET, "");
                ((ActivitySettings)getActivity()).SavePref(((ActivitySettings)getActivity()).USER_COUNTRY, "");
            }

        }

        @Override
        public void onResume() {
            super.onResume();
            // Set up a listener whenever a key changes
            LogUtils.i("resume");
            getPreferenceScreen().getSharedPreferences()
                    .registerOnSharedPreferenceChangeListener(this);
            if(getActivity() instanceof  ActivitySettings) {
                String city = ((ActivitySettings)getActivity()).LoadPref(((ActivitySettings)getActivity()).USER_CITY);
                String country = ((ActivitySettings)getActivity()).LoadPref(((ActivitySettings)getActivity()).USER_COUNTRY);
                String state = ((ActivitySettings)getActivity()).LoadPref(((ActivitySettings)getActivity()).USER_STATE);
                city_automatic.setSummary(getString(R.string.lblcitycountry,
                        city, state, country));
                city_manual.setSummary(getString(R.string.lblcitycountry, city, state,
                        country));
                ltln.setSummary(((ActivitySettings)getActivity()).loadString("user_lat") + "," + ((ActivitySettings)getActivity()).loadString("user_lng"));
            }
        }

        @Override
        public void onPause() {
            super.onPause();
            LogUtils.i("pause");
            // Set up a listener whenever a key changes
            getPreferenceScreen().getSharedPreferences()
                    .unregisterOnSharedPreferenceChangeListener(this);
            if(getActivity() instanceof  ActivitySettings) {
                String city = ((ActivitySettings)getActivity()).LoadPref(((ActivitySettings)getActivity()).USER_CITY);
                String country = ((ActivitySettings)getActivity()).LoadPref(((ActivitySettings)getActivity()).USER_COUNTRY);
                String state = ((ActivitySettings)getActivity()).LoadPref(((ActivitySettings)getActivity()).USER_STATE);
                city_automatic.setSummary(getString(R.string.lblcitycountry,
                        city, state, country));
                city_manual.setSummary(getString(R.string.lblcitycountry, city, state,
                        country));
            }
        }

        @Override
        public void onDestroy() {
            // TODO Auto-generated method stub
            LogUtils.i("", "destroy fragment settings");

            if(appLocationService !=null)
            {
                appLocationService.stopUsingGPS();
                getActivity().stopService(
                        new Intent(getActivity(), HomeMenuActivity.class));
            }super.onDestroy();
        }

        @Override
        public void onSharedPreferenceChanged(
                SharedPreferences sharedPreferences, String key) {
            ((ActivitySettings)getActivity()).cancel_all_alarm(getActivity());

            //lpm.setSummary(lpm.getEntry());
            //lpj.setSummary(lpj.getEntry());
            //lpl.setSummary(lpl.getEntry());
            //lpt.setSummary(lpt.getEntry());
            lph.setSummary(lph.getEntry());
            lt.setSummary(lt.getEntry());
            ln.setSummary(ln.getEntry());
            la.setSummary(la.getEntry());
            LogUtils.i("SP Changed Azan Clicked" + la.getEntry());
            LogUtils.i("SP Changed Azan Clicked" + la.getEntryValues());
            LogUtils.i("SP Changed Azan Clicked" + la.getValue());
            if (!selected_azan.equals(la.getValue())) {
                if (((ActivitySettings)getActivity()).mp != null && ((ActivitySettings)getActivity()).mp.isPlaying()) {
                    ((ActivitySettings)getActivity()).mp.stop();
                }
                LogUtils.i("tones_array : " + ((ActivitySettings)getActivity()).tones_array[Integer.parseInt(la.getValue())]);
                ((ActivitySettings)getActivity()).mp = MediaPlayer.create(getActivity(), ((ActivitySettings)getActivity()).tones_array[Integer.parseInt(la.getValue())]);
                if (((AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE)).getStreamVolume(3) != 0) {
                    ((ActivitySettings)getActivity()).mp.setAudioStreamType(3);
                    ((ActivitySettings)getActivity()).mp.start();

                    Handler handler =new Handler();
                    handler.postDelayed ( new Runnable() {
                        public void run() {
                            if (((ActivitySettings)getActivity()).mp != null && ((ActivitySettings)getActivity()).mp.isPlaying()) {

                                ((ActivitySettings) getActivity()).mp.stop();

                            }
                        }


                    }, 2500);


                }


            }


        }

        @Override
        public void onActivityResult(int requestCode, int resultCode,
                                     Intent data) {

            LogUtils.i(" on activity result" + resultCode + " " + data);

            if (resultCode == -1) {

                final Uri uri = data
                        .getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);

                if (uri != null) {
                    final Ringtone ringtone = RingtoneManager.getRingtone(
                            getActivity(), uri);
                    rp.setSummary(ringtone.getTitle(getActivity()));
                    editor = sp.edit();
                    editor.putString(TAG_RINGTONE, uri.toString());
                    editor.putString(TAG_RINGTONE_TITLE,
                            ringtone.getTitle(getActivity()));
                    editor.commit();
                }

            } else if (resultCode == LOCATION_INTENT_CALLED) {

                try {

                    getLocation();
                } catch (Exception e) {
                    // TODO: handle exception
                }

            } else {

                try {
                    getLocation();
                } catch (Exception e) {
                    // TODO: handle exception
                }

            }

        }




        public void showSettingsAlert() {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                    getActivity());
            alertDialog.setTitle("SETTINGS");
            alertDialog
                    .setMessage("Enable Location Provider! Go to settings menu?");
            alertDialog.setPositiveButton("Settings",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getActivity()
                                    .startActivityForResult(
                                            new Intent(
                                                    Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                                            LOCATION_INTENT_CALLED);
                        }
                    });
            alertDialog.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            alertDialog.show();
        }


    }


    @Override
    public void onBackPressed() {

        if (!this.menu_show) {
            finish();
        }

        Handler handler =new Handler();
        handler.postDelayed ( new Runnable() {
            public void run() {
                if (mp != null && mp.isPlaying()) {
                    mp.stop();
                    finish();

                }
            }


        }, 1500);
        super.onBackPressed();


    }




}
