package santri.azan.activity;

import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.List;
import java.util.Locale;

import santri.azan.tasbeeh.DataBase;
import santri.azan.tasbeeh.DataBaseModel;
import santri.azan.utils.LogUtils;
import santri.azan.utils.Utils;
import santri.syaria.R;

public class TasbeeActivity extends Utils {

    TextView text1, text2, text3, texttitle;//, textcount;
    ImageView adding, deleting, clear, next, counting,countMinus;// menu
    int n = 0;
    String count;
    String title="", description, type = "normal" ;
    int countval=0;
    List<DataBaseModel> databasefetch;
    DataBase db = null;
    private AdView mAdView;
    private InterstitialAd mInterstitialAd;
    private TasbeeActivity myApp;

    MyBaseAdapter adapter;
    ListView lvpost;
    //int n1=0;
    EditText txt_title,txt_desc,txt_count;
    int id;
    Bundle extra;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasbee);
        myApp = this;

        MobileAds.initialize(myApp, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(LoadAdError errorCode) {
                Log.e("failed load banner", errorCode+"");
                // Code to be executed when an ad request fails.
                //myApp.findViewById(R.id.startAppBanner).setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });
//        mInterstitialAd = new InterstitialAd(this);
//        mInterstitialAd.setAdUnitId(getResources().getString(R.string.admob_insertitial_id));
//        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String userlango = preferences.getString(USER_LANGUAGES , USER_LANGUAGES);
        LogUtils.i("in onCreate" ,"preferences = "+ userlango);

        if (userlango !=null) {

            Locale locale = new Locale(userlango);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }

        Actionbar(getString(R.string.lbl_tasbee));
        //Analytics(getString(R.string.lbl_tasbee));
        //typeface();banner_ad();
        adding = (ImageView) findViewById(R.id.three);
        deleting = (ImageView) findViewById(R.id.one);
        clear = (ImageView) findViewById(R.id.two);
        next = (ImageView) findViewById(R.id.four);
        counting = (ImageView) findViewById(R.id.countbtn);
        countMinus = (ImageView) findViewById(R.id.countMinus);
        //menu = (ImageView) findViewById(R.id.menubtn);
        text1 = (TextView) findViewById(R.id.t1);
        text2 = (TextView) findViewById(R.id.t2);
        text3 = (TextView) findViewById(R.id.t3);
        texttitle = (TextView) findViewById(R.id.tastitle);
        //	textcount = (TextView) findViewById(R.id.tascount);
        text1.setTypeface(tf3);text2.setTypeface(tf3);
        text3.setTypeface(tf3);
        extra = getIntent().getExtras();
        if(extra != null && extra.getInt("countvalue")>0)
        {
            countval = extra.getInt("countvalue");
            title = extra.getString("maintitle");

            texttitle.setText(title.toUpperCase());
            n=0;
            id = extra.getInt("idvalue");
            type = "list";
        }
        db = new DataBase(getApplication());
        db.openDataBase();
        final Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        adding.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showCustomView();
            }
        });

        countMinus.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //	if(countval >0){
                //if (n !=0)

                if(extra != null || type.equals("list"))
                {

                    if (n >0)
                    {
                        n--;
                        counterUpdate();
                    }
                    if (n == countval) {
                        //	vibrator.vibrate(300);
                        //	Toast.makeText(getApplicationContext(), "Reached",Toast.LENGTH_SHORT).show();
                    }
                }else if (n >0)
                {
                    n--;
                    counterUpdate();
                }

			/*	if (n == countval) {
					vibrator.vibrate(300);
					Toast.makeText(getApplicationContext(), "Reached",
							Toast.LENGTH_SHORT).show();
				}
				}else
				{

					Toast.makeText(getApplicationContext(), "Select any Tasbeeh",
							Toast.LENGTH_SHORT).show();
				}
*/
            }
        });
        counting.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

			/*	if(countval >0)
				{
			*/	//if (n <countval)

                if(extra != null || type.equals("list"))
                {

                    if (n <countval)
                    {
                        n++;
                        counterUpdate();
                    }
                    if (n == countval) {
                        vibrator.vibrate(300);
                        Toast.makeText(getApplicationContext(), "Reached",
                                Toast.LENGTH_SHORT).show();
                    }
                }else
                {
                    n++;
                    counterUpdate();
                }
			/*	}else
				{

					Toast.makeText(getApplicationContext(), "Select any Tasbeeh",
							Toast.LENGTH_SHORT).show();
				}
*/			}

        });
        next.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //	if(countval >0)
                {

                    Intent intent = new Intent(TasbeeActivity.this,
                            FullscreenActivity.class);
                    intent.putExtra("count", n);
                    intent.putExtra("maincount", countval);
                    intent.putExtra("title", title);
                    intent.putExtra("type", type);
                    startActivityForResult(intent, 1);
                    Log.d("Sending value", n + "");

                }/*else
				{

					Toast.makeText(getApplicationContext(), "Select any Tasbeeh",
							Toast.LENGTH_SHORT).show();
				}
*/
            }
        });
        clear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                text1.setText("0");
                text2.setText("0");
                text3.setText("0");
                n=0;

            }
        });



        deleting.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(countval >0)
                {

                    delete();
                }else
                {

                    Toast.makeText(getApplicationContext(), "Select any Tasbeeh",
                            Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    public void delete()
    {
        new MaterialDialog.Builder(this)
                .title("Are you sure want to delete the Tasbeeh list")
                .cancelable(false)
                .positiveText("Ok")
                .positiveColor(Color.parseColor("#379e24"))
                .negativeText(android.R.string.cancel)
                .negativeColor(Color.parseColor("#379e24"))
                .callback(new MaterialDialog.ButtonCallback() {

                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);


                        Log.d("Receiving id value", id + "");
                        db.deleteTasbeehList(id);
                        text1.setText("0");
                        text2.setText("0");
                        text3.setText("0");

                        databasefetch = db.fetchAllFeedList(0);

                        Log.d("Size", databasefetch.size() + "");
                        Toast.makeText(getApplicationContext(),
                                "Deleted successfully",
                                Toast.LENGTH_SHORT).show();
                        if(databasefetch.size()>0)
                        {
                            if(databasefetch.size() > id)
                            {
                                id = id+1;
                            }else
                            {
                                id= id-1;
                            }

                            List<DataBaseModel> data = db.gettasbeeh(id);
                            for (DataBaseModel d: data) {
                                n=0;
                                Log.d("Size", d.getTitle());
                                countval = d.getCount();
                                id= d.getKey_id();
                                texttitle.setText(d.getTitle().toUpperCase());
                                title= d.getTitle();

                            }
                        }else
                        {
                            n=0;
                            countval =0;
                            id=0;
                            texttitle.setText("");
                            type = "list";
                            extra = null;
                        }



                    }

                }).build().show();


    }
    private void showCustomView() {

        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Add New Tasbeeh")
                .cancelable(true)
                .positiveText("Ok")
                .positiveColor(Color.parseColor("#379e24"))
                .customView(R.layout.menucount, true)
                .negativeText(android.R.string.cancel)
                .negativeColor(Color.parseColor("#379e24"))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        String title = txt_title.getText().toString();
                        String desc = txt_desc.getText().toString();
                        String count = txt_count.getText().toString();

                        Log.d("Title", title + "");
                        Log.d("Describtion", desc + "");
                        Log.d("count_number", count + "");
                        if(title.equals("") || desc.equals("") || count.equals(""))
                        {
                            Toast.makeText(getApplicationContext(),
                                    "Enter the Details",
                                    Toast.LENGTH_SHORT).show();
                            showCustomView();
                        }else
                        {
                            db.inserTasbeehList(title, desc,Integer.parseInt(count));
                            Toast.makeText(getApplicationContext(),
                                    "Tasbee Added successfully",
                                    Toast.LENGTH_SHORT).show();

                        }

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }
                }).build();
        txt_title = (EditText) dialog.getCustomView()
                .findViewById(R.id.title);
        txt_desc = (EditText) dialog.getCustomView()
                .findViewById(R.id.Describtion);
        txt_count = (EditText) dialog.getCustomView()
                .findViewById(R.id.countno);


        dialog.show();
    }


    public void counterUpdate() {

        count = String.valueOf(n);
        if (n < 10) {

            text3.setText(n + "");

            text2.setText("0");
            text1.setText("0");
        } else if (n > 9 && n < 100) {

            text3.setText(count.substring(1));
            text2.setText(count.substring(0, 1));
        } else {
            text3.setText(count.substring(2));
            text2.setText(count.substring(1, 2));
            text1.setText(count.substring(0, 1));
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtils.i("reqestcode "+ requestCode);

        // check if the request code is same as what is passed here it is 2
        if (requestCode == 1) {

            int count = data.getIntExtra("count", 0);
            n = count;
            counterUpdate();

        }else if (requestCode == 2) {

            int count = data.getIntExtra("count", 0);
            n = count;
            counterUpdate();
            countval = data.getIntExtra("countvalue",0);
            title = data.getStringExtra("maintitle");

            texttitle.setText(title.toUpperCase());
            n=0;
            id = data.getIntExtra("idvalue",0);
            type = "list";

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.tasbee, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_menu) {

            databasefetch = db.fetchAllFeedList(0);

            Log.d("Size", databasefetch.size() + "");
/*			for (int i = 0; i < databasefetch.size(); i++) {
				Log.d("Size", databasefetch.get(i).getTitle());
			}
*/
            if(databasefetch.size() >0)
            {
                Intent intent = new Intent(TasbeeActivity.this,
                        TasbeeListActivity.class);
                startActivityForResult(intent,2);
                //finish();
            } else
            {
                Toast.makeText(TasbeeActivity.this, "No Tasbeeh to List", Toast.LENGTH_SHORT).show();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint({ "ViewHolder", "InflateParams" })
    public class MyBaseAdapter extends BaseAdapter {

        String formatted_excerpt;

        @Override
        public int getCount() {
            return databasefetch.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            LayoutInflater inflater = null;
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View vi = convertView;
            vi = inflater.inflate(R.layout.listviewcontent, null);

            TextView title = (TextView) vi.findViewById(R.id.tv_title);
            TextView description = (TextView) vi.findViewById(R.id.tv_desc);
            TextView count = (TextView) vi.findViewById(R.id.tv_count);

            title.setText(databasefetch.get(position).getTitle());
            description.setText(databasefetch.get(position).getDescription());
            count.setText(String
                    .valueOf(databasefetch.get(position).getCount()));

            Log.d("Fetching titles", title + "");

            return vi;
        }
    }

    public void reset(){


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        mInterstitialAd.show();
    }

}
