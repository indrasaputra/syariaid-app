package santri.azan.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.List;

import santri.azan.tasbeeh.DataBase;
import santri.azan.tasbeeh.DataBaseModel;
import santri.azan.utils.Utils;
import santri.syaria.R;

public class TasbeeListActivity extends Utils {
    List<DataBaseModel> databasefetch;
    DataBase db = null;
    MyBaseAdapter adapter;
    ListView lvpost;
    int countvalue;
    private AdView mAdView;
    private TasbeeListActivity myApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasbee_list);
        myApp = this;

        MobileAds.initialize(myApp, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(LoadAdError errorCode) {
                Log.e("failed load banner", errorCode+"");
                // Code to be executed when an ad request fails.
                //myApp.findViewById(R.id.startAppBanner).setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });

        Actionbar("Tasbeeh List");
        //Analytics("Tasbeeh List");
        typeface();
        //banner_ad();
        lvpost = (ListView) findViewById(R.id.listcontent);
        adapter = new MyBaseAdapter();
        db = new DataBase(getApplication());
        db.openDataBase();
        databasefetch = db.fetchAllFeedList(0);
        // Log.d("some values",databasefetch.get(1).getCount()+"");

        for (int i = 0; i < databasefetch.size(); i++) {
            Log.d("Size", databasefetch.get(i).getTitle());
            int id = databasefetch.get(i).getKey_id();
            Log.d("ID", id + "");
        }

        lvpost.setAdapter(adapter);

        lvpost.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                int count = databasefetch.get(arg2).getCount();
                int id_list = databasefetch.get(arg2).getKey_id();
                String title = databasefetch.get(arg2).getTitle();
                Log.d("ID", id_list + "");
                backClick(count,id_list,title);
				/*Intent intent = new Intent(ListActivity.this,
						TasbeeActivity.class);
				intent.putExtra("countvalue", count);
				Log.d("sending count from listactivity", count + "");
				intent.putExtra("idvalue", id_list);
				intent.putExtra("maintitle", title);
				startActivity(intent);*/
                //	finish();

            }

        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                backClick(-1, -1, "");

                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    public void backClick(int count,int id_list,String title)
    {
        Intent intent = new Intent();
        intent.putExtra("countvalue", count);
        Log.d("tasbeelistactivity", count + "");
        intent.putExtra("idvalue", id_list);
        intent.putExtra("maintitle", title);
        //startActivity(intent);
        setResult(RESULT_OK, intent);
        finish();

    }

    @Override
    public void onBackPressed() {

        backClick(-1, -1, "");

    }

    @SuppressLint({ "InflateParams", "ViewHolder" })
    public class MyBaseAdapter extends BaseAdapter {

        String formatted_excerpt;

        @Override
        public int getCount() {
            return databasefetch.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            LayoutInflater inflater = null;
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View vi = convertView;
            vi = inflater.inflate(R.layout.listviewcontent, null);

            TextView title = (TextView) vi.findViewById(R.id.tv_title);
            TextView description = (TextView) vi.findViewById(R.id.tv_desc);
            TextView count = (TextView) vi.findViewById(R.id.tv_count);

            title.setText(databasefetch.get(position).getTitle());
            description.setText(databasefetch.get(position).getDescription());
            count.setText(String
                    .valueOf(databasefetch.get(position).getCount()));

            Log.d("Fetching titles", title + "");

            return vi;
        }
    }
}
