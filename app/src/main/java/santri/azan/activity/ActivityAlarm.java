package santri.azan.activity;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.core.app.NotificationCompat;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import santri.azan.receiver.AlarmReceiver;
import santri.azan.support.PrayTime;
import santri.azan.utils.LogUtils;
import santri.azan.utils.Utils;
import santri.syaria.R;

public class ActivityAlarm extends Utils {

    private MediaPlayer mMediaPlayer;
    ImageView stopAlarm ;
    Intent alarmIntent;
    private PendingIntent pendingIntent;
    TextView txt_date,txt_location,txt_type;
    int id=0;
    int[] tones_array = new int[]{R.raw.azan0, R.raw.azan1, R.raw.azan2, R.raw.azan3, R.raw.azan4, R.raw.azan5, R.raw.azan6};
    SharedPreferences.Editor editor;
    SharedPreferences pref;
    String type_nawaz;
    TextView txt_name;
    private AdView mAdView = null;
    private ActivityAlarm myApp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myApp = this;

        setContentView(R.layout.activity_alarm);
        Actionbar(getString(R.string.app_name));

        //Analytics(getString(R.string.app_name));
        typeface();

        pref = PreferenceManager.getDefaultSharedPreferences(this);

        Intent intent = getIntent();

        Bundle extras = intent.getExtras();


        txt_date=(TextView) findViewById(R.id.txt_date);
        txt_date.setTypeface(tf1);

        try {

            txt_date.setText(getTimeDate(0));

        } catch (ParseException e) {

            e.printStackTrace();
        }


        /* Retrieve a PendingIntent that will perform a broadcast */
        alarmIntent = new Intent(this, AlarmReceiver.class);
        txt_type=(TextView) findViewById(R.id.txt_type);
        txt_type.setTypeface(tf1);
        txt_location=(TextView) findViewById(R.id.txt_location);
        txt_location.setTypeface(tf1);
        String city=loadString(USER_CITY);
        String country=loadString(USER_COUNTRY);
        txt_location.setText(city +" , "+country);


        if (extras != null) {

            type_nawaz = extras.getString("type");

            String time = extras.getString("time");
            if(time!=null && type_nawaz!=null){
                txt_type.setText(time+", "+type_nawaz);
                SavePref(type_nawaz, "tru");
                generateNotification(this,time,type_nawaz);
            }

        }

        stopAlarm = (ImageView) findViewById(R.id.stopAlarm);
        stopAlarm.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                mMediaPlayer.stop();
                finish();
                return false;
            }
        });

        String alarms = pref.getString("ringtone","content://settings/system/alarm_alert");

        LogUtils.i("alarms"+alarms);

        if(alarms.equals("content://settings/system/alarm_alert")){

            playSound(this);

        }else{

            playSoundAlarm(this, getPrefAlarmUri());

        }

        try {
            //		initPrayerTime(1);

        } catch (Exception e) {

            e.printStackTrace();
        }

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(LoadAdError errorCode) {
                Log.e("failed load banner", errorCode+"");
                // Code to be executed when an ad request fails.
                //myApp.findViewById(R.id.startAppBanner).setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        MenuItem menu_ok = menu.findItem(R.id.menu_ok);
        menu_ok.setVisible(false);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id==android.R.id.home){
            mMediaPlayer.stop();
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
    private static void generateNotification(Context context, String time, String type) {

        int icon = R.drawable.ic_launcher;
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        String title = context.getString(R.string.app_name);

        Intent notificationIntent = new Intent(context, ActivityAlarm.class);
        notificationIntent.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK);
        // notificationIntent.putExtra("FRND_ID",id);
        //	notificationIntent.putExtra("NAME",name);


        PendingIntent intent =
                PendingIntent.getActivity(context, 0, notificationIntent,PendingIntent.FLAG_ONE_SHOT);


        NotificationCompat.Builder notification =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(icon)
                        .setAutoCancel(false)
                        .setContentTitle(title)
                        .setContentText(time+","+type)
                        .setContentIntent(intent);
        notificationManager.notify(0, notification.build());

    }

    private void playSound(Context context) {
        mMediaPlayer = new MediaPlayer();

        {
            LogUtils.i("playSound Called : ");
            String selected_azan = this.pref.getString(ActivitySettings.SettingsFragment.TAG_AZAN, "0");
            LogUtils.i("selected_azan : " + selected_azan);
            this.mMediaPlayer = MediaPlayer.create(this, this.tones_array[Integer.parseInt(selected_azan)]);
            final AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
            if (audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mMediaPlayer.start();
            }
        }
    }

    private void playSoundAlarm(Context context, Uri alert) {
        mMediaPlayer = new MediaPlayer();
        try {
            mMediaPlayer.setDataSource(context, alert);
            final AudioManager audioManager = (AudioManager) context
                    .getSystemService(Context.AUDIO_SERVICE);
            if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            }
        } catch (IOException e) {
            System.out.println("OOPS");
        }
    }

    //Get an alarm sound. Try for an alarm. If none set, try notification,
    //Otherwise, ringtone.
/*    private Uri getAlarmUri() {

        Uri alert = RingtoneManager
                .getDefaultUri(RingtoneManager.TYPE_ALARM);
        if (alert == null) {
            alert = RingtoneManager
                    .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            if (alert == null) {
                alert = RingtoneManager
                        .getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            }
        }

        return alert;
    }*/

    private Uri getPrefAlarmUri(){

        SharedPreferences getAlarms = PreferenceManager.getDefaultSharedPreferences(this);
        String alarms = getAlarms.getString("ringtone","content://settings/system/alarm_alert");
        Uri uri = Uri.parse(alarms);
        return uri;
    }

    @Override
    public void onBackPressed() {

        mMediaPlayer.stop();
        finish();
        super.onBackPressed();
    }


    public void set_alarm(String time)  {

        DateFormat df = new SimpleDateFormat("MMMM d, yyyy", Locale.US);
        String now_date = df.format(new Date());
        int hour=0,minute = 0;
        try {
            String timenow=changeTimeFormat(time);
            hour = Integer.parseInt(timenow.substring(0, 2));
            minute = Integer.parseInt(timenow.substring(3, 5));
        } catch (ParseException e1) {
            e1.printStackTrace();
        }

        pendingIntent = PendingIntent.getBroadcast(this,id, alarmIntent, 0);
        AlarmManager manager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        //int interval = 1000 * 60 * 20;
        Calendar calendar = Calendar.getInstance();
        try {

            calendar.setTime(df.parse(now_date));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        calendar.add( Calendar.DATE, 1 );
        calendar.set(Calendar.HOUR_OF_DAY,hour);
        int adjust_time=Integer.parseInt(pref.getString("adjust_alarm","-5"));
        minute= minute+adjust_time;
        calendar.set(Calendar.MINUTE,minute);

        manager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);

        //Toast.makeText(this," Alarm Enabled", Toast.LENGTH_SHORT).show();
        LogUtils.i("","time "+calendar);

    }

    public void initPrayerTime(int time) throws ParseException{

        double latitude =  Double.parseDouble(loadString(USER_LAT)); // 13.05033;
        double longitude = Double.parseDouble(loadString(USER_LNG)); //80.18859;


        String timeZone=pref.getString("timezone","");
        double timezone;

        // Test Prayer times here
        PrayTime prayers = new PrayTime();

        prayers.setTimeFormat(prayers.Time12);
        prayers.setCalcMethod(Integer.parseInt(pref.getString("method","1")));
        prayers.setAsrJuristic(Integer.parseInt(pref.getString("juristic","1")));
        prayers.setAdjustHighLats(Integer.parseInt(pref.getString("higherLatitudes","1")));
        int[] offsets = {0, 0, 0, 0, 0, 0, 0}; // {Fajr,Sunrise,Dhuhr,Asr,Sunset,Maghrib,Isha}
        prayers.tune(offsets);

        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DATE, time);

        if(timeZone.equals("")){
            timezone= getTimeZone(cal.getTimeZone().getID().toString());
            editor = pref.edit();
            editor.putString("timezone", timeZone);
            editor.commit();

        }else{
            timezone = getTimeZone(timeZone);
        }

        ArrayList<String> prayerTimes = prayers.getPrayerTimes(cal,latitude, longitude, timezone);
        ArrayList<String> prayerNames = prayers.getTimeNames();


        if(latitude==0.0 || longitude==0.0){


        }else{

            int dayLightTime=Integer.parseInt(pref.getString("daylight","0"));
            for (int i = 0; i < prayerTimes.size(); i++) {

                if (latitude != FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE && longitude != FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE) {

                    for (int i2 = 0; i2 < prayerTimes.size(); i2++) {
                        if (((String) prayerNames.get(i2)).equals("Fajr")) {
                            this.id = 0;
                            if (this.type_nawaz.equals("Fajar")) {
                                set_alarm(addDayLight((String) prayerTimes.get(i2), dayLightTime));
                            }
                        } else if (((String) prayerNames.get(i2)).equals("Sunrise")) {
                            this.id = 1;
                            if (this.type_nawaz.equals("Shorook")) {
                                set_alarm(addDayLight((String) prayerTimes.get(i2), dayLightTime));
                            }
                        } else if (((String) prayerNames.get(i2)).equals("Dhuhr")) {
                            this.id = 2;
                            if (this.type_nawaz.equals("Zuher")) {
                                set_alarm(addDayLight((String) prayerTimes.get(i2), dayLightTime));
                            }
                        } else if (((String) prayerNames.get(i2)).equals("Asr")) {
                            this.id = 3;
                            if (this.type_nawaz.equals("Asar")) {
                                set_alarm(addDayLight((String) prayerTimes.get(i2), dayLightTime));
                            }
                        } else if (((String) prayerNames.get(i2)).equals("Maghrib")) {
                            this.id = 4;
                            if (this.type_nawaz.equals("Maghrib")) {
                                set_alarm(addDayLight((String) prayerTimes.get(i2), dayLightTime));
                            }
                        } else if (((String) prayerNames.get(i2)).equals("Isha")) {
                            this.id = 5;
                            if (this.type_nawaz.equals("Isha")) {
                                set_alarm(addDayLight((String) prayerTimes.get(i2), dayLightTime));
                            }
                        }
                    }
                }
            }

        }


    }

}
