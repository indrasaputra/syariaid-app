package santri.azan.activity;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import santri.azan.utils.LogUtils;
import santri.azan.utils.Utils;
import santri.syaria.R;

public class ActivityLatLng extends Utils {

    EditText txt_lat, txt_lng;
    String str_lng, str_lat;
    Button btn_submit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_latlng);
        Actionbar("Enter Latitude & Longitude");
        //Analytics(getString(R.string.settings));
        typeface();
        txt_lat = (EditText) findViewById(R.id.txt_lat);
        txt_lng = (EditText) findViewById(R.id.txt_lng);

        txt_lat.setText(LoadPref(USER_LAT));
        txt_lng.setText(LoadPref(USER_LNG));
        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                str_lat = txt_lat.getText().toString();
                str_lng = txt_lng.getText().toString();

                if(!str_lat.equals("") || !str_lng.equals(""))
                {

                    saveString(USER_LAT, str_lat);
                    saveString(USER_LNG, str_lng);
                    saveString(USER_MLAT, str_lat);
                    saveString(USER_MLNG, str_lng);

                    Location();
                }else
                {
                    Toast.makeText(ActivityLatLng.this, "Enter Valid Latitude & Longitude", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void Location() {


        try {
            Geocoder geocoder = new Geocoder(ActivityLatLng.this, Locale.ENGLISH);

            LogUtils.i(str_lat+" lat "+ str_lng+" lng");
            List<Address> addresses = geocoder.getFromLocation(
                    Double.parseDouble(loadString(USER_LAT)),
                    Double.parseDouble(loadString(USER_LNG)), 1);

            for (Address adrs : addresses) {

                LogUtils.i(adrs.getLocality()+ " city "+ adrs.getAdminArea() +" state "+ adrs.getCountryName());
                if (adrs != null) {

                    String city = adrs.getLocality();
                    String Country = adrs.getCountryName();
                    String State = adrs.getAdminArea();
                    String Street = adrs.getSubLocality();
                    SavePref(USER_CITY, city);
                    SavePref(USER_STATE, State);
                    SavePref(USER_COUNTRY, Country);
                    SavePref(USER_STREET, Street);


                }

            }

        } catch (IOException e) {
            e.printStackTrace();
            SavePref(USER_CITY, "");
            SavePref(USER_STATE, "");

            SavePref(USER_STREET, "");
            SavePref(USER_COUNTRY, "");
        }

        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        MenuItem menu_ok = menu.findItem(R.id.menu_ok);
        menu_ok.setVisible(false);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {



        return super.onOptionsItemSelected(item);
    }
}
