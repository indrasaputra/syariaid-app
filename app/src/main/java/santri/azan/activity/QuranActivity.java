package santri.azan.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.Locale;

import santri.azan.utils.LogUtils;
import santri.azan.utils.QuranInfo;
import santri.azan.utils.Utils;
import santri.syaria.R;

public class QuranActivity extends Utils {

    ListView lv_quran_chapters;
    QuranInfo quranInfo;
    MyBaseAdapter adapter;
    private AdView mAdView;
    private InterstitialAd mInterstitialAd;
    private QuranActivity myApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quran);
        myApp = this;

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String userlango = preferences.getString(USER_LANGUAGES , USER_LANGUAGES);
        LogUtils.i("in onCreate" ,"preferences = "+ userlango);

        if (userlango !=null) {

            Locale locale = new Locale(userlango);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }

        Actionbar("Quran");

        MobileAds.initialize(myApp, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(LoadAdError errorCode) {
                Log.e("failed load banner", errorCode+"");
                // Code to be executed when an ad request fails.
                //myApp.findViewById(R.id.startAppBanner).setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });
//        mInterstitialAd = new InterstitialAd(this);
//        mInterstitialAd.setAdUnitId(getResources().getString(R.string.admob_insertitial_id));
//        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        //Analytics("Quran");
        typeface();
        //banner_ad();

        lv_quran_chapters = (ListView) findViewById(R.id.lv_quran_chapters);
        adapter = new MyBaseAdapter();
        lv_quran_chapters.setAdapter(adapter);


    }

    @SuppressLint({ "InflateParams", "ViewHolder" })
    public class MyBaseAdapter extends BaseAdapter {

        String formatted_excerpt;

        @Override
        public int getCount() {
            return QuranInfo.SURA_NAMES.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            LayoutInflater inflater = null;
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View vi = convertView;
            vi = inflater.inflate(R.layout.quranchapterlistitem, null);

            TextView tv_quran_count = (TextView) vi
                    .findViewById(R.id.tv_chapter_count);
            final TextView tv_quran_chapter_title = (TextView) vi
                    .findViewById(R.id.tv_chapter_title);
            TextView tv_quran_chapter_desc = (TextView) vi
                    .findViewById(R.id.tv_chapter_desc);
            TextView tv_quran_chapter_title_ar = (TextView) vi
                    .findViewById(R.id.tv_chapter_title_arabic);
            LinearLayout quranchapterrl = (LinearLayout) vi
                    .findViewById(R.id.quranchapterlist);
            tv_quran_count.setTextAppearance(QuranActivity.this, styleheader[efont]);

            tv_quran_chapter_title.setTextAppearance(QuranActivity.this, style[efont]);
            tv_quran_chapter_desc.setTextAppearance(QuranActivity.this, style[efont]);
            //tv_quran_chapter_title_ar.setTextAppearance(QuranActivity.this, style[efont]);
            //tv_quran_chapter_title_ar.setTextAppearance(QuranActivity.this, style[2]);
            tv_quran_count.setText(String.valueOf(position + 1) + ".");
            tv_quran_chapter_title.setText(QuranInfo.SURA_NAMES[position]);
            tv_quran_chapter_desc
                    .setText(QuranInfo.SURA_DESC[position]
                            + " ("
                            + String.valueOf(QuranInfo.SURA_NUM_AYAHS[position])
                            + ") ");
            tv_quran_chapter_title_ar.setTypeface(tf_arabic);
            tv_quran_chapter_title_ar
                    .setText(QuranInfo.SURA_NAMES_AR[position]);

            quranchapterrl.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Intent intent = new Intent(QuranActivity.this,
                            IndividualChapter.class);
                    intent.putExtra("count", String.valueOf(position + 1));
                    intent.putExtra("title", QuranInfo.SURA_NAMES[position]);
                    startActivity(intent);
                }
            });

            return vi;
        }
    }

    @Override
    protected void onResume() {
        font();
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        mInterstitialAd.show();
    }
}
