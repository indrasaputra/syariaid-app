package santri.azan.activity;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.MaterialDialog.ButtonCallback;

import santri.azan.map.GPSTracker;
import santri.azan.map.GooglePlaces;
import santri.azan.map.Place;
import santri.azan.map.PlaceDetails;
import santri.azan.map.PlacesList;
import santri.azan.support.AppLocationService;
import santri.azan.utils.LogUtils;
import santri.azan.utils.Utils;
import santri.syaria.R;

public class MapActivity extends Utils {

    // flag for Internet connection status
    Boolean isInternetPresent = false;

    // Google Places
    GooglePlaces googlePlaces;

    // Places List
    PlacesList nearPlaces;

    double fiveradius;
    // GPS Location
    GPSTracker gps;

    // Button
    TextView btnShowOnMap;
    ListAdapter adapter;
    // Progress dialog

    PlaceDetails placeDetails;

    // Places Listview
    ListView lv;
    RadioGroup radiobutton;
    RadioButton fivekm, tenkm, fifteenkm, allkm;
    int n = 0;
    public static ArrayList<String> lat = new ArrayList<>();
    public static ArrayList<String> lng = new ArrayList<>();

    public static ArrayList<String> lat_5 = new ArrayList<>();
    public static ArrayList<String> lng_5 = new ArrayList<>();

    public static ArrayList<String> lat_10 = new ArrayList<>();
    public static ArrayList<String> lng_10 = new ArrayList<>();

    public static ArrayList<String> lat_15 = new ArrayList<>();
    public static ArrayList<String> lng_15 = new ArrayList<>();

    public static ArrayList<String> lat_all = new ArrayList<>();
    public static ArrayList<String> lng_all = new ArrayList<>();

    public static ArrayList<String> title = new ArrayList<>();

    ArrayList<HashMap<String, String>> placesListItems = new ArrayList<HashMap<String, String>>();
    ArrayList<HashMap<String, String>> maplist = new ArrayList<HashMap<String, String>>();
    ArrayList<HashMap<String, String>> maplist_5 = new ArrayList<HashMap<String, String>>();
    ArrayList<HashMap<String, String>> maplist_10 = new ArrayList<HashMap<String, String>>();
    ArrayList<HashMap<String, String>> maplist_15 = new ArrayList<HashMap<String, String>>();
    ArrayList<HashMap<String, String>> maplist_all = new ArrayList<HashMap<String, String>>();

    // KEY Strings
    public static String KEY_REFERENCE = "reference"; // id of the place
    public static String KEY_NAME = "name"; // name of the place
    public static String KEY_VICINITY = "vicinity"; // Place area name
    public static String KEY_LAT = "lat";
    public static String KEY_LNG = "lng";
    public static String KEY_DISTANCE = "distance";
    double latitude, longitude, nradius = 20000;
    public static ArrayList<HashMap<String, String>> ListItems = new ArrayList<HashMap<String, String>>();

    AppLocationService appLocationService;
    ProgressDialog pDialog;
    int i = 0;
    TextView btn_show_map ;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_list);
        Actionbar("Masjid Finder");
        typeface();
        //banner_ad();
        //Analytics(getString(R.string.lbl_mosquee));
        radiobutton = (RadioGroup) findViewById(R.id.radiogroupbox);
        fivekm = (RadioButton) findViewById(R.id.fivekm);
        tenkm = (RadioButton) findViewById(R.id.tenkm);
        fifteenkm = (RadioButton) findViewById(R.id.fifteenkm);
        allkm = (RadioButton) findViewById(R.id.allkm);
        allkm.setSelected(true);
        fivekm.setSelected(false);
        tenkm.setSelected(false);
        fifteenkm.setSelected(false);
        gps = new GPSTracker(this);



        appLocationService = new AppLocationService(MapActivity.this);
        // check if GPS location can get

        if (!loadString(USER_LAT).equals("") && latitude != 0.0) {
            getLoc();

            /*
             * latitude = Double.parseDouble(loadString(USER_LAT)); // //
             * 13.05033; longitude = Double.parseDouble(loadString(USER_LNG));
             *
             * LogUtils.i(latitude + " lat in " + longitude); // // 80.18859; }
             * else if (gps.canGetLocation()) { Log.d("Your Location",
             * "latitude:" + gps.getLatitude() + ", longitude: " +
             * gps.getLongitude()); latitude = gps.getLatitude(); longitude =
             * gps.getLongitude();
             */
        } else {

            // showalert();
        }

        // Getting listv
        lv = (ListView) findViewById(R.id.list);
        btnShowOnMap = (TextView) findViewById(R.id.btn_show_map);
        btnShowOnMap.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {


                nradius = 5000;
                placesListItems = maplist_5;
                run();

                //	Intent i = new Intent(MapActivity.this, PlacesMapActivity.class);
                //		i.putExtra("user_latitude", Double.toString(latitude));
                //	i.putExtra("user_longitude", Double.toString(longitude));
                //	i.putExtra("near_places", nearPlaces);
                //	startActivity(i);
            }
        });



        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String uri = String.format(
                        Locale.ENGLISH,
                        "http://maps.google.com/maps?daddr=%f,%f",
                        Double.parseDouble(placesListItems.get(position).get(
                                KEY_LAT)),
                        Double.parseDouble(placesListItems.get(position).get(
                                KEY_LNG)));
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setClassName("com.google.android.apps.maps",
                        "com.google.android.maps.MapsActivity");
                startActivity(intent);
            }
        });

        fivekm.setChecked(true);
        run();
        radiobutton.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                googlePlaces = new GooglePlaces();
                try {

                    if (checkedId == R.id.fivekm) {
                        nradius = 5000;

                        placesListItems = maplist_5;

                        lat = lat_5;
                        lng = lng_5;
                        adapter = null;
                        adapter = new SimpleAdapter(MapActivity.this,
                                maplist_5, R.layout.list_item, new String[]{
                                KEY_DISTANCE, KEY_NAME}, new int[]{
                                R.id.reference, R.id.name});
                        lv.setAdapter(adapter);
                        run();

                    } else if (checkedId == R.id.tenkm) {
                        nradius = 10000;

                        lat = lat_10;
                        lng = lng_10;
                        adapter = null;
                        adapter = new SimpleAdapter(MapActivity.this,
                                maplist_10, R.layout.list_item, new String[]{
                                KEY_DISTANCE, KEY_NAME}, new int[]{
                                R.id.reference, R.id.name});
                        lv.setAdapter(adapter);
                        run();

                    } else if (checkedId == R.id.fifteenkm) {

                        nradius = 15000;

                        lat = lat_15;
                        lng = lng_15;
                        adapter = null;
                        adapter = new SimpleAdapter(MapActivity.this,
                                maplist_15, R.layout.list_item, new String[]{
                                KEY_DISTANCE, KEY_NAME}, new int[]{
                                R.id.reference, R.id.name});
                        lv.setAdapter(adapter);
                        run();
                    } else if (checkedId == R.id.allkm) {

                        nradius = 100000;
                        lat = lat_all;
                        lng = lng_all;
                        adapter = null;
                        adapter = new SimpleAdapter(MapActivity.this,
                                maplist_all, R.layout.list_item, new String[]{
                                KEY_DISTANCE, KEY_NAME}, new int[]{
                                R.id.reference, R.id.name});
                        lv.setAdapter(adapter);
                    }
                    i++;
                } catch (Exception e) {
                    Log.d("Error message", e.getMessage());
                }

            }
        });

    }

    public void getLoc() {
        Location location = appLocationService.getLocation();

        LogUtils.i("location " + location);
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            saveString(USER_LAT, String.valueOf(latitude));
            saveString(USER_LNG, String.valueOf(longitude));
        } else {
            latitude = 0.0;
            longitude = 0.0;
            showSettingsAlert();
        }

    }


    public void run() {

        if (!isOnline()) {
            showalert();

        }
        if (isOnline() ) {
            this.latitude = getLatitude();
            this.longitude = getLongitude();
            this.maplist.clear();
            this.maplist_10.clear();
            this.maplist_15.clear();
            this.maplist_all.clear();
            if (isOnline() && latitude != 0 && longitude != 0) {
                getLocation();
                new LoadPlaces().execute();
            }
            else if ( isOnline() && latitude == 0 && longitude == 0) {

                getLocation();
                this.latitude = getLatitude();
                this.longitude = getLongitude();
                this.maplist.clear();
                this.maplist_10.clear();
                this.maplist_15.clear();
                this.maplist_all.clear();

                if ( latitude == 0 && longitude == 0) {

                    showalert();
                    showSettingsAlert();

                }
            }else {
                getLocation();
                new LoadPlaces().execute();
            }

        }

    }







    @Override
    public void showSettingsAlert() {

        new MaterialDialog.Builder(this)
                .title("GPS is settings")
                .content(
                        "GPS is not enabled. Do you want to go to settings menu?")
                .cancelable(false).positiveText("Settings")
                .positiveColor(Color.parseColor("#379e24"))
                .negativeText(android.R.string.cancel)
                .negativeColor(Color.parseColor("#379e24"))
                .callback(new ButtonCallback() {

                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }

                }).build().show();

    }

    /**
     * Background Async Task to Load Google places
     * */
    class LoadPlaces extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MapActivity.this);
            pDialog.setMessage(Html
                    .fromHtml("<b>Search</b><br/>Loading Places..."));
            // pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting Places JSON
         * */
        @Override
        protected String doInBackground(String... args) {
            // creating Places class object
            googlePlaces = new GooglePlaces();

            try {
                // Separeate your place types by PIPE symbol "|"
                // If you want all types places make it as null
                // Check list of types supported by google
                //
                String types = "mosque"; // Listing places only cafes,
                // restaurants

                // Radius in meters - increase this value if you don't find any
                // places
                double radius = nradius; // 1000 meters

                Log.d("main radius", radius + "");

                // get nearest places
                nearPlaces = googlePlaces.search(latitude, longitude, radius,
                        types);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog and show
         * the data in UI Always use runOnUiThread(new Runnable()) to update UI
         * from background thread, otherwise you will get error
         * **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    /**
                     * Updating parsed Places into LISTVIEW
                     * */
                    // Get json response status
                    String status = nearPlaces.status;

                    // Check for all possible status
                    if (status.equals("OK")) {
                        // Successfully got places details
                        if (nearPlaces.results != null) {
                            // loop through each place
                            for (Place p : nearPlaces.results) {
                                HashMap<String, String> map = new HashMap<String, String>();

                                // Place reference won't display in listview -
                                // it will be hidden
                                // Place reference is used to get
                                // "place full details"

                                map.put(KEY_REFERENCE, p.reference);
                                // Place name
                                map.put(KEY_NAME, p.name);
                                map.put(KEY_LAT,
                                        String.valueOf(p.geometry.location.lat));

                                map.put(KEY_LNG,
                                        String.valueOf(p.geometry.location.lng));
                                // map.put(KEY_radius,String.valueOf(placeDetails.result.radius));

                                // map.put(KEY_radius,String.valueOf(p.radius));

                                // adding HashMap to ArrayList
                                title.add(p.name);

                                placesListItems.add(map);
                                maplist.add(map);
                            }
                            ListItems = placesListItems;
                            LogUtils.i(" map size " + placesListItems.size());
                            if (placesListItems.size() > 0) {
                                new distance().execute();
                            }
                            // list adapter
                            /*
                             * adapter = new SimpleAdapter( MapActivity.this,
                             * placesListItems, R.layout.list_item, new String[]
                             * { KEY_NAME }, new int[] { R.id.name });
                             *
                             * // Adding data into listview
                             * lv.setAdapter(adapter);
                             */

                        }
                    } else {/*
                     * else if (status.equals("ZERO_RESULTS")) {
                     * Alert("Near Places",
                     * "Sorry no places found. Try to change the types of places"
                     * ); // Zero results found } else if
                     * (status.equals("UNKNOWN_ERROR")) {
                     * Alert("Places Error"
                     * ,"Sorry unknown error occured.");
                     *
                     * } else if (status.equals("OVER_QUERY_LIMIT")) {
                     * Alert("Places Error",
                     * "Sorry query limit to google places is reached");
                     *
                     * } else if (status.equals("REQUEST_DENIED")) {
                     * Alert("Places Error",
                     * "Sorry error occured. Request is denied");
                     *
                     * } else if (status.equals("INVALID_REQUEST")) {
                     * Alert("Places Error",
                     * "Sorry error occured. Invalid Request"); } else {
                     */

                        Alert(getString(R.string.app_name),
                                "Error occured, Try after some time");
                    }
                }
            });

        }

    }

    public void listrefresh() {
        /*
         * googlePlaces = new GooglePlaces(); fiveradius=5000;
         */

        ListAdapter adapter = new SimpleAdapter(MapActivity.this,
                placesListItems, R.layout.list_item, new String[] { KEY_LAT,
                KEY_LNG }, new int[] { R.id.reference, R.id.name });
        lv.setAdapter(adapter);

    }

    @Override
    protected void onDestroy() {

        if (appLocationService != null) {
            appLocationService.stopUsingGPS();
            stopService(new Intent(this, AppLocationService.class));
        }
        super.onDestroy();

    }

    class distance extends AsyncTask<Void, Void, Void> {

        ArrayList<HashMap<String, String>> placesListItems = new ArrayList<HashMap<String, String>>();

        @Override
        public void onPreExecute() {
            super.onPreExecute();
        }

        @SuppressWarnings("unchecked")
        @Override
        protected Void doInBackground(Void... params) {
            for (int i = 0; i < maplist.size(); i++) {
                HashMap<String, String> mapdis = new HashMap<String, String>();
                String url = "http://maps.google.com/maps/api/directions/xml?origin="
                        + latitude
                        + ","
                        + longitude
                        + "&destination="
                        + maplist.get(i).get(KEY_LAT)
                        + ","
                        + maplist.get(i).get(KEY_LNG)
                        + "&sensor=false&units=metric";

                String tag[] = { "text" };
                HttpResponse response = null;
                try {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpContext localContext = new BasicHttpContext();
                    HttpPost httpPost = new HttpPost(url);
                    response = httpClient.execute(httpPost, localContext);
                    InputStream is = response.getEntity().getContent();
                    DocumentBuilder builder = DocumentBuilderFactory
                            .newInstance().newDocumentBuilder();
                    Document doc = builder.parse(is);
                    if (doc != null) {
                        NodeList nl;
                        @SuppressWarnings("rawtypes")
                        ArrayList args = new ArrayList();
                        for (String s : tag) {
                            nl = doc.getElementsByTagName(s);
                            if (nl.getLength() > 0) {
                                Node node = nl.item(nl.getLength() - 1);
                                args.add(node.getTextContent());
                            } else {
                                args.add(" - ");
                            }
                        }
                        String result_in_kms = String.format("%s", args.get(0));
                        Log.i("" + maplist.get(i).get(KEY_LAT), maplist.get(i)
                                .get(KEY_LNG) + " url " + result_in_kms);

                        String val = result_in_kms.replace(" km", "");
                        if (val.contains("-")) {
                            val = "0";
                        }

                        if (Float.parseFloat(val) <= 5) {
                            mapdis.put(KEY_LAT, maplist.get(i).get(KEY_LAT));
                            mapdis.put(KEY_LNG, maplist.get(i).get(KEY_LNG));
                            mapdis.put(KEY_DISTANCE, result_in_kms);
                            mapdis.put(KEY_NAME, maplist.get(i).get(KEY_NAME));
                            mapdis.put(KEY_REFERENCE,
                                    maplist.get(i).get(KEY_REFERENCE));

                            lat_5.add(maplist.get(i).get(KEY_LAT));
                            lng_5.add(maplist.get(i).get(KEY_LNG));
                            maplist_5.add(mapdis);
                        }
                        if (Float.parseFloat(val) <= 10) {
                            mapdis.put(KEY_LAT, maplist.get(i).get(KEY_LAT));
                            mapdis.put(KEY_LNG, maplist.get(i).get(KEY_LNG));
                            mapdis.put(KEY_DISTANCE, result_in_kms);
                            mapdis.put(KEY_NAME, maplist.get(i).get(KEY_NAME));
                            mapdis.put(KEY_REFERENCE,
                                    maplist.get(i).get(KEY_REFERENCE));
                            lat_10.add(maplist.get(i).get(KEY_LAT));
                            lng_10.add(maplist.get(i).get(KEY_LNG));
                            maplist_10.add(mapdis);
                        }
                        if (Float.parseFloat(val) <= 15) {
                            mapdis.put(KEY_LAT, maplist.get(i).get(KEY_LAT));
                            mapdis.put(KEY_LNG, maplist.get(i).get(KEY_LNG));
                            mapdis.put(KEY_DISTANCE, result_in_kms);
                            mapdis.put(KEY_NAME, maplist.get(i).get(KEY_NAME));
                            mapdis.put(KEY_REFERENCE,
                                    maplist.get(i).get(KEY_REFERENCE));
                            lat_15.add(maplist.get(i).get(KEY_LAT));
                            lng_15.add(maplist.get(i).get(KEY_LNG));
                            maplist_15.add(mapdis);
                        }

                        mapdis.put(KEY_LAT, maplist.get(i).get(KEY_LAT));
                        mapdis.put(KEY_LNG, maplist.get(i).get(KEY_LNG));
                        mapdis.put(KEY_DISTANCE, result_in_kms);
                        mapdis.put(KEY_NAME, maplist.get(i).get(KEY_NAME));
                        mapdis.put(KEY_REFERENCE,
                                maplist.get(i).get(KEY_REFERENCE));

                        lat_all.add(maplist.get(i).get(KEY_LAT));
                        lng_all.add(maplist.get(i).get(KEY_LNG));

                        lat = lat_all;
                        lng = lng_all;
                        maplist_all.add(mapdis);

                        placesListItems.add(mapdis);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void file_url) {
            pDialog.dismiss();
            adapter = new SimpleAdapter(MapActivity.this, placesListItems,
                    R.layout.list_item,
                    new String[] { KEY_DISTANCE, KEY_NAME }, new int[] {
                    R.id.reference, R.id.name });

            lv.setAdapter(adapter);

        }

    }

}
