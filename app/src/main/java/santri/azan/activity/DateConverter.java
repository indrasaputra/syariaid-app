package santri.azan.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import santri.azan.hijridate.GregorianCalendar;
import santri.azan.hijridate.HijriCalendar;
import santri.azan.hijridate.HijriCalendarDate;
import santri.azan.utils.LogUtils;
import santri.azan.utils.Utils;
import santri.azan.widget.OnWheelScrollListener;
import santri.azan.widget.WheelView;
import santri.azan.widget.adapters.ArrayWheelAdapter;
import santri.azan.widget.adapters.NumericWheelAdapter;
import santri.syaria.R;

public class DateConverter extends Utils {
    public static boolean isHijri = false;
    int curMonth;
    WheelView day;
    int gendday = 31;
    TextView greg;
    TextView gregorianc_date;
    int gstartday = 1;
    int hcurMonth;
    WheelView hday;
    int hendday = 31;
    TextView hij;
    int hijri_day;
    int hijri_month;
    int hijri_year;
    TextView hijric_date;
    WheelView hmonth;
    public int hsel_day;
    int hstartday = 1;
    WheelView hyear;
    LinearLayout layoutconverter;
    OnWheelScrollListener listener;
    int mili_day;
    int mili_month;
    int mili_year;
    int minimum_year = 623;
    WheelView month;
    String[] months = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    String[] monthsh = new String[]{"", "Muharram", "Safar", "Rabiul Awwal", "Rabiul Akhir", "Jumadal Ula", "Jumadal Akhira", "Rajab", "Shaaban", "Ramadhan", "Shawwal", "Dhulqaada", "Dhulhijja"};
    public int sel_day;
    WheelView year;
    private AdView mAdView;
    private InterstitialAd mInterstitialAd;
    private DateConverter myApp;

    private class DateArrayAdapter extends ArrayWheelAdapter<String> {
        int currentItem;
        int currentValue;

        public DateArrayAdapter(Context context, String[] items, int current) {
            super(context, items);
            this.currentValue = current;
            setTextSize(16);
        }


        public void configureTextView(TextView view) {
            super.configureTextView(view);
            if (this.currentItem == this.currentValue) {
                view.setTextColor(Color.parseColor("#06310f"));
                view.setTypeface(DateConverter.this.tf1, Typeface.BOLD);
                return;
            }
            view.setTextColor(Color.parseColor("#73c484"));
            view.setTypeface(DateConverter.this.tf1);
        }

        public View getItem(int index, View cachedView, ViewGroup parent) {
            this.currentItem = index;
            return super.getItem(index, cachedView, parent);
        }
    }
    private class DateNumericAdapter extends NumericWheelAdapter {
        int currentItem;
        int currentValue;

        public DateNumericAdapter(Context context, int minValue, int maxValue, int current) {
            super(context, minValue, maxValue);
            this.currentValue = current;
            setTextSize(16);
        }

        public void configureTextView(TextView view) {
            super.configureTextView(view);
            if (this.currentItem == this.currentValue) {
                view.setTextColor(Color.parseColor("#06310f"));
                view.setTypeface(DateConverter.this.tf1, Typeface.BOLD);
                return;
            }
            view.setTextColor(Color.parseColor("#73c484"));
            view.setTypeface(DateConverter.this.tf1);
        }

        public View getItem(int index, View cachedView, ViewGroup parent) {
            this.currentItem = index;
            return super.getItem(index, cachedView, parent);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        fullscreen();
        super.onCreate(savedInstanceState);
        myApp = this;
        setContentView(R.layout.activity_converter);

        MobileAds.initialize(myApp, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(LoadAdError errorCode) {
                Log.e("failed load banner", errorCode+"");
                // Code to be executed when an ad request fails.
                //myApp.findViewById(R.id.startAppBanner).setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });
//        mInterstitialAd = new InterstitialAd(this);
//        mInterstitialAd.setAdUnitId(getResources().getString(R.string.admob_insertitial_id));
//        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String userlango = preferences.getString(USER_LANGUAGES , USER_LANGUAGES);
        LogUtils.i("in onCreate" ,"preferences = "+ userlango);

        if (userlango !=null) {

            Locale locale = new Locale(userlango);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }

        Actionbar(getString(R.string.lbl_date));
        //Analytics(getString(R.string.lbl_date));
        typeface();
        //banner_ad();
        final Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        Calendar calendar = Calendar.getInstance();
        HijriCalendar hijriCalendar = new HijriCalendar(calendar.get(1), calendar.get(2) + 1, calendar.get(5));
        this.greg = (TextView) findViewById(R.id.gregorianc);
        this.greg.setTypeface(this.tf2);
        this.hij = (TextView) findViewById(R.id.hijric);
        this.hij.setTypeface(this.tf2);
        this.gregorianc_date = (TextView) findViewById(R.id.gregorianc_date);
        this.hijric_date = (TextView) findViewById(R.id.hijric_date);
        this.mili_year = calendar.get(1);
        this.mili_month = calendar.get(2);
        this.mili_day = calendar.get(5);
        this.hijri_year = hijriCalendar.getHijriYear();
        this.hijri_month = hijriCalendar.getHijriMonth();
        this.hijri_day = hijriCalendar.getHijriDay();
        Log.d("Year", "" + this.hijri_year);
        Log.d("Year", "" + this.hijri_month);
        Log.d("Year", "" + this.hijri_day);
        this.month = (WheelView) findViewById(R.id.month);
        this.year = (WheelView) findViewById(R.id.year);
        this.day = (WheelView) findViewById(R.id.day);
        this.hmonth = (WheelView) findViewById(R.id.hmonth);
        this.hyear = (WheelView) findViewById(R.id.hyear);
        this.hday = (WheelView) findViewById(R.id.hday);
        this.listener = new OnWheelScrollListener() {
            public void onScrollingStarted(WheelView wheel) {
            }

            public void onScrollingFinished(WheelView wheel) {
                if (wheel.getId() == R.id.day || wheel.getId() == R.id.month || wheel.getId() == R.id.year) {
                    DateConverter.this.updateDays(DateConverter.this.year, DateConverter.this.month, DateConverter.this.day);
                    DateConverter.this.newhijridate();
                    vibrator.vibrate(30);
                } else if (wheel.getId() == R.id.hday || wheel.getId() == R.id.hmonth || wheel.getId() == R.id.hyear) {
                    DateConverter.this.hupdateDays(DateConverter.this.hyear, DateConverter.this.hmonth, DateConverter.this.hday);
                    DateConverter.this.newgregoriandate();
                    vibrator.vibrate(30);
                }
            }
        };
        this.curMonth = this.mili_month;
        this.month.setViewAdapter(new DateArrayAdapter(this, this.months, this.curMonth));
        this.month.setCurrentItem(this.curMonth);
        this.month.addScrollingListener(this.listener);
        int curYear = this.mili_year;
        this.year.setViewAdapter(new DateNumericAdapter(this, this.minimum_year, 2500, curYear - this.minimum_year));
        this.year.setCurrentItem(curYear - this.minimum_year);
        this.year.addScrollingListener(this.listener);
        this.day.setCurrentItem(this.mili_day - 1);
        this.sel_day = this.mili_day - 1;
        this.day.setViewAdapter(new DateNumericAdapter(this, this.gstartday, this.gendday, this.sel_day));
        updateDays(this.year, this.month, this.day);
        this.day.setCurrentItem(this.mili_day - 1);
        this.day.addScrollingListener(this.listener);
        this.hcurMonth = this.hijri_month;
        this.hmonth.setViewAdapter(new DateArrayAdapter(this, this.monthsh, this.hcurMonth));
        this.hmonth.setCurrentItem(this.hcurMonth);
        this.hmonth.addScrollingListener(this.listener);
        int hcurYear = this.hijri_year;
        this.hyear.setViewAdapter(new DateNumericAdapter(this, this.minimum_year, 2500, hcurYear - this.minimum_year));
        this.hyear.setCurrentItem(hcurYear - this.minimum_year);
        this.hyear.addScrollingListener(this.listener);
        this.hday.setCurrentItem(this.hijri_day - 1);
        this.hsel_day = this.hijri_day - 1;
        this.hday.setViewAdapter(new DateNumericAdapter(this, this.hstartday, this.hendday, this.hsel_day));
        hupdateDays(this.hyear, this.hmonth, this.hday);
        this.hday.setCurrentItem(this.hijri_day - 1);
        this.hday.addScrollingListener(this.listener);
        String monthName = new SimpleDateFormat("MMMM").format(calendar.getTime());
        Calendar Qurancal = Calendar.getInstance();
        Qurancal.add(5, 0);
        this.gregorianc_date.setText(this.mili_day + "/" + monthName + "/" + curYear);
        this.hijric_date.setText(this.hijri_day + "/" + HijriCalendarDate.getSimpleDateMonth(Qurancal, 0) + "/" + hcurYear);
        LogUtils.i("hi created");
    }



    public void setHijriCalendar() {
        this.hcurMonth = this.hijri_month;
        this.hmonth.setCurrentItem(this.hcurMonth);
        this.hyear.setCurrentItem(this.hijri_year - this.minimum_year);
        this.hday.setCurrentItem(this.hijri_day - 1);
        hupdateDays(this.hyear, this.hmonth, this.hday);
        this.hday.setCurrentItem(this.hijri_day - 1);
    }


    public void setGregorianCalendar() {
        this.curMonth = this.mili_month;
        this.month.setCurrentItem(this.curMonth);
        this.year.setCurrentItem(this.mili_year - this.minimum_year);
        this.day.setCurrentItem(this.mili_day - 1);
        updateDays(this.year, this.month, this.day);
        this.day.setCurrentItem(this.mili_day - 1);
    }



    public void updateDays(WheelView year, WheelView month, WheelView day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(1, calendar.get(1) + year.getCurrentItem());
        calendar.set(2, month.getCurrentItem());
        int maxDays = calendar.getActualMaximum(5);
        day.setViewAdapter(new DateNumericAdapter(this, this.gstartday, maxDays, this.sel_day));
        day.setCurrentItem(Math.min(maxDays, day.getCurrentItem() + 1) - 1, true);
    }


    public void hupdateDays(WheelView hyear, WheelView hmonth, WheelView hday) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(1, calendar.get(1) + hyear.getCurrentItem());
        calendar.set(2, hmonth.getCurrentItem());
        int maxDays = calendar.getActualMaximum(5);
        hday.setViewAdapter(new DateNumericAdapter(this, this.hstartday, maxDays, this.hsel_day));
        hday.setCurrentItem(Math.min(maxDays, hday.getCurrentItem() + 1) - 1, true);
    }



    public void newhijridate() {
        HijriCalendar hijriCalendar = new HijriCalendar(this.year.getCurrentItem() + this.minimum_year, this.month.getCurrentItem() + 1, this.day.getCurrentItem() + 1);
        this.hijri_year = hijriCalendar.getHijriYear();
        this.hijri_month = hijriCalendar.getHijriMonth();
        this.hijri_day = hijriCalendar.getHijriDay();
        setHijriCalendar();
        LogUtils.i("newhijridate " + this.hijri_month + " hcurYear " + this.hijri_year);
        LogUtils.i(" hijri_day " + this.hijri_day);
        this.hijric_date.setText(this.hijri_day + "/" + this.monthsh[this.hijri_month] + "/" + this.hijri_year);
        int[] res = new GregorianCalendar().islToChr(this.hyear.getCurrentItem() + this.minimum_year, this.hmonth.getCurrentItem(), this.hday.getCurrentItem() + 1, 0);
        this.mili_year = res[2];
        this.mili_month = res[1] - 1;
        this.mili_day = res[0] - 1;
        this.gregorianc_date.setText(this.mili_day + "/" + this.months[this.mili_month] + "/" + this.mili_year);
    }



    public void newgregoriandate() {
        int[] res = new GregorianCalendar().islToChr(this.hyear.getCurrentItem() + this.minimum_year, this.hmonth.getCurrentItem(), this.hday.getCurrentItem() + 1, 0);
        this.mili_year = res[2];
        this.mili_month = res[1] - 1;
        this.mili_day = res[0] - 1;
        setGregorianCalendar();
        this.gregorianc_date.setText(this.mili_day + "/" + this.months[this.mili_month] + "/" + this.mili_year);
        HijriCalendar hijriCalendar = new HijriCalendar(this.year.getCurrentItem() + this.minimum_year, this.month.getCurrentItem() + 1, this.day.getCurrentItem() + 1);
        this.hijri_year = hijriCalendar.getHijriYear();
        this.hijri_month = hijriCalendar.getHijriMonth();
        this.hijri_day = hijriCalendar.getHijriDay();
        this.hijric_date.setText(this.hijri_day + "/" + this.monthsh[this.hijri_month] + "/" + this.hijri_year);
        LogUtils.i("newgregoriandate " + this.mili_year + " mili_month " + this.mili_month);
        LogUtils.i(" mili_day " + this.mili_day);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        mInterstitialAd.show();
    }
}
