package santri.azan.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.Locale;

import santri.azan.utils.Utils;
import santri.syaria.HomeMenuActivity;
import santri.syaria.R;

public class Activitylangradio extends Utils {


    Locale myLocale ;

    public static String langholder;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_langradio);




        RadioGroup rg = (RadioGroup) findViewById(R.id.radiolang);



        if (rg != null) {

            rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                public void onCheckedChanged(RadioGroup group, int checkedId ) {


                    switch (checkedId) {

                        case R.id.bahradio:
                            langholder  = ("id");
                            Toast.makeText(Activitylangradio.this,
                                    "You have selected Bahasa Language", Toast.LENGTH_SHORT)
                                    .show();
                            saveuserlang ();
                            setLocale(langholder);
                            break;
                        case R.id.trradio:
                            langholder  = ("tr");
                            Toast.makeText(Activitylangradio.this,
                                    "You have selected Türkçe Language", Toast.LENGTH_SHORT)
                                    .show();
                            saveuserlang ();
                            setLocale(langholder);
                            break;
                        case R.id.arradio:
                            langholder  = ("ar");
                            Toast.makeText(Activitylangradio.this,
                                    "You have selected Arabic Language", Toast.LENGTH_SHORT)
                                    .show();
                            saveuserlang ();
                            setLocale(langholder);
                            break;

                        case R.id.urradio:
                            langholder  = ("ur");
                            Toast.makeText(Activitylangradio.this,
                                    "You have selected Urdu Language", Toast.LENGTH_SHORT)
                                    .show();
                            saveuserlang ();
                            setLocale(langholder);
                            break;

                        case R.id.esradio:
                            langholder  = ("es");
                            Toast.makeText(Activitylangradio.this,
                                    "You have selected Spanish Language", Toast.LENGTH_SHORT)
                                    .show();
                            saveuserlang ();
                            setLocale(langholder);
                            break;

                        case R.id.enradio:
                            langholder  = ("en");
                            Toast.makeText(Activitylangradio.this,
                                    "You have selected English Language", Toast.LENGTH_SHORT)
                                    .show();
                            saveuserlang ();
                            setLocale(langholder);
                            break;

                        case R.id.frradio:
                            langholder  = ("fr");
                            Toast.makeText(Activitylangradio.this,
                                    "You have selected French Language", Toast.LENGTH_SHORT)
                                    .show();
                            saveuserlang ();
                            setLocale(langholder);
                            break;

                        case R.id.ruradio:
                            langholder  = ("ru");
                            Toast.makeText(Activitylangradio.this,
                                    "You have selected Russian Language", Toast.LENGTH_SHORT)
                                    .show();
                            saveuserlang ();
                            setLocale(langholder);
                            break;
                    }


                }
            });
        }


    }


    public void saveuserlang () {

        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit();
        prefEditor.putString(USER_LANGUAGES, langholder);
        prefEditor.apply();

    }


    public void setLocale (String lang){

        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, HomeMenuActivity.class);
        startActivity(refresh);
        Activitylangradio.this.finish();

    }


}
