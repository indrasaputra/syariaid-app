package santri.azan.activity;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import java.text.DecimalFormat;
import java.util.Locale;

import santri.azan.utils.LogUtils;
import santri.azan.utils.Utils;
import santri.syaria.R;

public class ActivityQibla extends Utils implements SensorEventListener, LocationListener {

    TextView CityCountryName;
    TextView Country_Name;
    TextView Direction_Qibla;
    TextView Qibla_Distance;
    int angleFormated;
    int bearing;
    int[] compass = new int[]{R.drawable.compass, R.drawable.compass1, R.drawable.compass2, R.drawable.compass3, R.drawable.compass4, R.drawable.compass5, R.drawable.compass6, R.drawable.compass7, R.drawable.compass8, R.drawable.compass9, R.drawable.compass10, R.drawable.compass11, R.drawable.compass12, R.drawable.compass13, R.drawable.compass14};
    private float currentDegree = BitmapDescriptorFactory.HUE_RED;
    int i = 0;
    private ImageView image;
    private ImageView imageArrow;
    double lat1;
    double lat2;
    RelativeLayout layoutcompass;
    double lon1;
    double lon2;
    AdView mAdmobView;
    private SensorManager mSensorManager;
    private AdView mAdView;
    private ActivityQibla myApp;
    private InterstitialAd mInterstitialAd;
    int[] needle = new int[]{R.drawable.needle, R.drawable.needle1, R.drawable.needle2, R.drawable.needle3, R.drawable.needle4, R.drawable.needle5, R.drawable.needle6, R.drawable.needle7, R.drawable.needle8, R.drawable.needle9, R.drawable.needle10, R.drawable.needle11, R.drawable.needle12, R.drawable.needle13, R.drawable.needle14};

    public void onCreate(Bundle savedInstanceState) {
        fullscreen();
        super.onCreate(savedInstanceState);
        myApp = this;
        setContentView(R.layout.activity_compass);

        MobileAds.initialize(myApp, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(LoadAdError errorCode) {
                Log.e("failed load banner", errorCode+"");
                // Code to be executed when an ad request fails.
                //myApp.findViewById(R.id.startAppBanner).setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });
//        mInterstitialAd = new InterstitialAd(this);
//        mInterstitialAd.setAdUnitId(getResources().getString(R.string.admob_insertitial_id));
//        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String userlango = preferences.getString(USER_LANGUAGES , USER_LANGUAGES);
        LogUtils.i("in onCreate" ,"preferences = "+ userlango);

        if (userlango !=null) {

            Locale locale = new Locale(userlango);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }


        Actionbar(getString(R.string.lbl_qibla));
        //Analytics(getString(R.string.lbl_qibla));
        typeface();
        this.image = (ImageView) findViewById(R.id.main_image_compass);
        this.imageArrow = (ImageView) findViewById(R.id.main_image_arrow);
        this.CityCountryName = (TextView) findViewById(R.id.CityCountryName);
        this.CityCountryName.setTypeface(this.tf2, 1);
        this.Direction_Qibla = (TextView) findViewById(R.id.Direction_Qibla);
        this.Qibla_Distance = (TextView) findViewById(R.id.Qibla_Distance);
        this.Direction_Qibla.setTypeface(this.tf2, 1);
        this.Qibla_Distance.setTypeface(this.tf2, 1);
        this.mSensorManager = (SensorManager) getSystemService("sensor");
        //banner_ad();
    }

    public void magnetometer() {
        Location locationA = new Location("point A");
        locationA.setLatitude(this.lat1);
        locationA.setLongitude(this.lon1);
        Location locationB = new Location("point B");
        locationB.setLatitude(this.lat2);
        locationB.setLongitude(this.lon2);
        float distance = locationA.distanceTo(locationB);
        RotateAnimation ra = new RotateAnimation(this.currentDegree, distance, 1, 0.5f, 1, 0.5f);
        ra.setFillAfter(true);
        this.currentDegree = distance;
        this.imageArrow.startAnimation(ra);
        LogUtils.i("distance " + distance);
    }

    public void onLocationChanged(Location location) {
        LogUtils.i(location.getLatitude() + " onlocation " + location.getLongitude());
        super.onLocationChanged(location);
    }

    public void onResume() {
        super.onResume();
        this.image.setBackgroundResource(this.compass[LoadPrefInt(Utils.USER_COMPASS)]);
        this.imageArrow.setBackgroundResource(this.needle[LoadPrefInt(Utils.USER_COMPASS)]);
        this.i = LoadPrefInt("compass_count");
        String city = loadString(this.USER_CITY);
        String state = "";
        this.CityCountryName.setText(loadString(this.USER_STREET) + " " + city + " " + loadString(this.USER_STATE) + " " + loadString(this.USER_COUNTRY));
        this.lat2 = 21.42251d;
        this.lon2 = 39.826168d;
        if (loadString(this.USER_LAT).equals("") && loadString(this.USER_LNG).equals("")) {
        } else {
            this.lat1 = Double.parseDouble(loadString(this.USER_LAT));
            this.lon1 = Double.parseDouble(loadString(this.USER_LNG));
        }
        LogUtils.i(loadString(this.USER_LNG) + " lat " + loadString(this.USER_LAT));
        if (this.mSensorManager.getDefaultSensor(2) == null) {
            if ((loadString(this.USER_LAT).equals("") && loadString(this.USER_LNG).equals("")) || (loadString(this.USER_LAT).equals("0.0") && loadString(this.USER_LNG).equals("0.0"))) {
                Toast.makeText(this, "Can't get the lat,lng", 0).show();
            } else {
                magnetometer();
            }
        }
        this.mSensorManager.registerListener(this, this.mSensorManager.getDefaultSensor(3), 1);
        int bearing = (int) Utils.bearing(this.lat1, this.lon1, this.lat2, this.lon2);
        DecimalFormat df = new DecimalFormat("#");
        this.angleFormated = bearing;
        LogUtils.i(bearing + " bearing " + this.angleFormated);
        this.Direction_Qibla.setText(getString(R.string.degree, new Object[]{String.valueOf(this.angleFormated)}));
        int n = (int) distFrom(this.lat1, this.lon1, this.lat2, this.lon2);
        LogUtils.i(" n " + n);
        this.Qibla_Distance.setText(getString(R.string.distance) + " " + n + " KM");
    }

    public void onPause() {
        super.onPause();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_qibla) {
        }
        return super.onOptionsItemSelected(item);
    }

    public void onStop() {
        this.mSensorManager.unregisterListener(this);
        super.onStop();
    }

    public void onSensorChanged(SensorEvent event) {
        float degree = (float) Math.round(event.values[0]);
        LogUtils.i((((float) this.angleFormated) - degree) + " degree " + degree);
        this.imageArrow.setRotation(((float) this.angleFormated) - degree);
        try {
            int v = (int) (((float) this.angleFormated) - degree);
            this.Direction_Qibla.setText(getString(R.string.degree, new Object[]{String.valueOf(v)}));
        } catch (Exception e) {
        }
        RotateAnimation ra = new RotateAnimation(this.currentDegree, -degree, 1, 0.5f, 1, 0.5f);
        ra.setDuration(50);
        ra.setFillAfter(true);
        this.image.startAnimation(ra);
        this.currentDegree = -degree;
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public static float distFrom(double lat1, double lng1, double lat2, double lng2) {
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = (Math.sin(dLat / 2.0d) * Math.sin(dLat / 2.0d)) + (((Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))) * Math.sin(dLng / 2.0d)) * Math.sin(dLng / 2.0d));
        return ((float) (6371000.0d * (2.0d * Math.atan2(Math.sqrt(a), Math.sqrt(1.0d - a))))) / 1000.0f;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        mInterstitialAd.show();
    }
}
