package santri.azan.activity;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import santri.azan.adapter.IndividualChapterAdapter;
import santri.azan.database.Database;
import santri.azan.model.QuranScript;
import santri.azan.utils.LogUtils;
import santri.azan.utils.Utils;
import santri.syaria.R;

public class IndividualChapter extends Utils {

    ListView lv_single_chapter;
    ArrayList<String> english;
    ArrayList<String> arabic;

    ArrayList<String> lang = new ArrayList<String>();
    ArrayList<String> text_lang = new ArrayList<String>();
    IndividualChapterAdapter adapter;

    String chaptercount,title="";

    private Database db, db1;
    List<QuranScript> dblist, dblist1, dben, dbfr,dbgr,dbin,dbma,dbsp,dbtr,dbur;
    public static String lang_name="Indonesian";
    private AdView mAdView;
    private IndividualChapter myApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_individual_chapter);
        myApp = this;

        lv_single_chapter = (ListView) findViewById(R.id.single_chapter_list);

        Bundle intent = getIntent().getExtras();

        MobileAds.initialize(myApp, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(LoadAdError errorCode) {
                Log.e("failed load banner", errorCode+"");
                // Code to be executed when an ad request fails.
                //myApp.findViewById(R.id.startAppBanner).setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });


        chaptercount = intent.getString("count");
        title = intent.getString("title");
        Actionbar(title);

        //Analytics(title);
        typeface();

        //banner_ad();
        font();
        english = new ArrayList<>();
        arabic = new ArrayList<>();
        dblist = new ArrayList<QuranScript>();
        dben = new ArrayList<QuranScript>();
        dbfr = new ArrayList<QuranScript>();
        dbgr = new ArrayList<QuranScript>();
        dbin = new ArrayList<QuranScript>();
        dbma = new ArrayList<QuranScript>();
        dbsp = new ArrayList<QuranScript>();
        dbtr = new ArrayList<QuranScript>();
        dbur = new ArrayList<QuranScript>();

        text_lang.add("english-false");
        text_lang.add("french-false");
        text_lang.add("german-false");
        //text_lang.add("indonesian-false");
        text_lang.add("malay-false");
        text_lang.add("spanish-false");
        text_lang.add("trukish-false");
        text_lang.add("urdu-false");

        Transliteration();
        //Translation("quran.english.db");
        Translation("quran.indonesian.db");

        String filename = chaptercount;

        if (filename.length() == 1) {
            filename = "00" + filename;
        } else if (filename.length() == 2) {
            filename = "0" + filename;
        } else {
            filename = "" + filename;
        }

        BufferedReader reader1 = null;

        try {
            final InputStream file1 = getAssets().open(
                    "text_arabic/" + filename + ".txt");
            reader1 = new BufferedReader(new InputStreamReader(file1));
            String line1 = reader1.readLine();

            while (line1 != null) {
                Log.d("Arabic", line1);
                arabic.add(line1);
                line1 = reader1.readLine();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        //adapter = new IndividualChapterAdapter(this, dblist, arabic, dblist1, dbfr,dbgr,dbin,dbma,dbsp,dbtr,dbur,afont,efont,tf_arabic);
        adapter = new IndividualChapterAdapter(this, dblist, arabic, dblist1, dbfr,dbgr,dben,dbma,dbsp,dbtr,dbur,afont,efont,tf_arabic);
        Log.i("Size", dblist.size() + "");
        if (dblist.size() > 0) {
            lv_single_chapter.setAdapter(adapter);
        }

    }


    public void Transliteration() {
        db = new Database(this, "quran.script.db");
        dblist = db.getTransliteration(Integer.parseInt(chaptercount));
    }

    public void Translation(String dbfilename) {
        //dbfilename="quran.english.db";
        dbfilename="quran.indonesian.db";
        db1 = new Database(this, dbfilename);
        dblist1 = db1.getTransliteration(Integer.parseInt(chaptercount));

        if(LoadPref("english_lang").equals("true")){
            db1 = new Database(this, "quran.english.db");
            dben = db1.getTransliteration(Integer.parseInt(chaptercount));
        }
        if(LoadPref("french_lang").equals("true")){
            db1 = new Database(this, "quran.french.db");
            dbfr = db1.getTransliteration(Integer.parseInt(chaptercount));
        }
        if(LoadPref("german_lang").equals("true")){
            db1 = new Database(this, "quran.german.db");
            dbgr = db1.getTransliteration(Integer.parseInt(chaptercount));
        }
        if(LoadPref("indonesian_lang").equals("true")){
            db1 = new Database(this, "quran.indonesian.db");
            dbin = db1.getTransliteration(Integer.parseInt(chaptercount));
        }
        if(LoadPref("malay_lang").equals("true")){
            db1 = new Database(this, "quran.malay.db");
            dbma = db1.getTransliteration(Integer.parseInt(chaptercount));
        }
        if(LoadPref("spanish_lang").equals("true")){
            db1 = new Database(this, "quran.spanish.db");
            dbsp = db1.getTransliteration(Integer.parseInt(chaptercount));
        }
        if(LoadPref("trukish_lang").equals("true")){
            db1 = new Database(this, "quran.trukish.db");
            dbtr = db1.getTransliteration(Integer.parseInt(chaptercount));
        }
        if(LoadPref("urdu_lang").equals("true")){
            db1 = new Database(this, "quran.urdu.db");
            dbur = db1.getTransliteration(Integer.parseInt(chaptercount));
        }

        //adapter = new IndividualChapterAdapter(this, dblist, arabic, dblist1,dbfr,dbgr,dbin,dbma,dbsp,dbtr,dbur,afont,efont,tf_arabic);
        adapter = new IndividualChapterAdapter(this, dblist, arabic, dblist1,dbfr,dbgr,dben,dbma,dbsp,dbtr,dbur,afont,efont,tf_arabic);
        Log.i("Size", dblist.size() + "");
        if (dblist.size() > 0) {
            lv_single_chapter.setAdapter(adapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) { // Inflate the menu; this
        // adds items to the action
        // bar if it is present.
        getMenuInflater().inflate(R.menu.quran_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.langugage_settings) {
            showCustomView();


            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setadapter()
    {

        for(int i=0;i<lang.size();i++)
        {
            lang_name = lang.get(i).trim();


            db1 = new Database(IndividualChapter.this, "quran."+lang.get(i)+".db");
            LogUtils.i("lang "+ "quran."+lang.get(i)+".db");

            if(lang.get(i).equals("english"))
            {
                dben = db1.getTransliteration(Integer.parseInt(chaptercount));
            }

            if(lang.get(i).equals("french"))
            {
                dbfr = db1.getTransliteration(Integer.parseInt(chaptercount));
            }

            if(lang.get(i).equals("german"))
            {
                dbgr = db1.getTransliteration(Integer.parseInt(chaptercount));
            }

            if(lang.get(i).equals("indonesian"))
            {
                dbin = db1.getTransliteration(Integer.parseInt(chaptercount));
            }

            if(lang.get(i).equals("malay"))
            {
                dbma = db1.getTransliteration(Integer.parseInt(chaptercount));
            }

            if(lang.get(i).equals("spanish"))
            {
                dbsp = db1.getTransliteration(Integer.parseInt(chaptercount));
            }

            if(lang.get(i).equals("trukish"))
            {
                dbtr = db1.getTransliteration(Integer.parseInt(chaptercount));
            }

            if(lang.get(i).equals("urdu"))
            {
                dbur = db1.getTransliteration(Integer.parseInt(chaptercount));
            }
        }
	/*for(int i=0;i<text_lang.size();i++)
		{
			String text[] = text_lang.get(i).split("-");

			LogUtils.i(text[0]+" text "+ text[1]);
			if(text[1].trim().equals("true"))
			{
			if(text[0].equals("french") && text[1].equals("false"))
			{
				dbfr.clear();
			}
			else
			if(text[0].equals("german") && text[1].equals("false"))
			{
				dbgr.clear();
			}else
			if(text[0].equals("indonesian") && text[1].equals("false"))
			{
				dbin.clear();
			}else
			if(text[0].equals("malay") && text[1].equals("false"))
			{
				dbma.clear();
			}else
			if(text[0].equals("spanish") && text[1].equals("false"))
			{
				dbsp.clear();
			}else
			if(text[0].equals("trukish") && text[1].equals("false"))
			{
				dbtr.clear();
			}else
			if(text[0].equals("urdu") && text[1].equals("false"))
			{
				dbur.clear();
			}
			}
		}*/

        if(LoadPref("english_lang").equals("false")){ dben.clear();}
        if(LoadPref("french_lang").equals("false")){ dbfr.clear();}
        if(LoadPref("german_lang").equals("false")){ dbgr.clear();}
        if(LoadPref("indonesian_lang").equals("false")){dbin.clear();}
        if(LoadPref("malay_lang").equals("false")){dbma.clear();}
        if(LoadPref("spanish_lang").equals("false")){dbsp.clear();}
        if(LoadPref("trukish_lang").equals("false")){dbtr.clear();}
        if(LoadPref("urdu_lang").equals("false")){dbur.clear();}
        adapter = null;
        //adapter = new IndividualChapterAdapter(this, dblist, arabic, dblist1,dbfr,dbgr,dbin,dbma,dbsp,dbtr,dbur,afont,efont,tf_arabic);
        adapter = new IndividualChapterAdapter(this, dblist, arabic, dblist1,dbfr,dbgr,dben,dbma,dbsp,dbtr,dbur,afont,efont,tf_arabic);
        LogUtils.i("dbfr "+ dbfr.size());
        if (dblist.size() > 0) {
            lv_single_chapter.setAdapter(adapter);
        }


    }

    private void showCustomView() {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Select Language")
                .customView(R.layout.language, true)
                .positiveText(R.string.ok)
                .negativeText(android.R.string.cancel)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                        setadapter();


                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }
                }).build();

        CheckBox ch_eng = (CheckBox) dialog.getCustomView().findViewById(R.id.ch_eng);
        if(LoadPref("english_lang").equals("true"))
        {
            ch_eng.setChecked(true);
        }
        ch_eng.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    lang.add("english");
                    SavePref("english_lang", "true");
                    text_lang.add("english-true");
                }else
                {
                    SavePref("english_lang", "false");
                    text_lang.add("english-false");
                }
            }
        });
        CheckBox ch_fren = (CheckBox) dialog.getCustomView().findViewById(R.id.ch_fren);
        if(LoadPref("french_lang").equals("true"))
        {
            ch_fren.setChecked(true);
        }
        ch_fren.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    lang.add("french");
                    SavePref("french_lang", "true");
                    text_lang.add("french-true");
                }else
                {
                    SavePref("french_lang", "false");
                    text_lang.add("french-false");
                }
            }
        });

        CheckBox ch_germ = (CheckBox) dialog.getCustomView().findViewById(R.id.ch_germ);
        if(LoadPref("german_lang").equals("true"))
        {
            ch_germ.setChecked(true);
        }
        ch_germ.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    lang.add("german");

                    SavePref("german_lang", "true");
                    text_lang.add("german-true");
                }else
                {
                    SavePref("german_lang", "false");
                    text_lang.add("german-false");
                }
            }
        });
        CheckBox ch_indo = (CheckBox) dialog.getCustomView().findViewById(R.id.ch_indo);
        if(LoadPref("indonesian_lang").equals("true"))
        {
            ch_indo.setChecked(true);
        }
        ch_indo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    lang.add("indonesian");
                    SavePref("indonesian_lang", "true");
                    text_lang.add("indonesian-true");
                }else
                {
                    SavePref("indonesian_lang", "false");
                    text_lang.add("indonesian-false");
                }
            }
        });
        CheckBox ch_mala = (CheckBox) dialog.getCustomView().findViewById(R.id.ch_mala);
        if(LoadPref("malay_lang").equals("true"))
        {
            ch_mala.setChecked(true);
        }
        ch_mala.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    lang.add("malay");
                    SavePref("malay_lang", "true");
                    text_lang.add("malay-true");
                }else
                {

                    SavePref("malay_lang", "false");
                    text_lang.add("malay-false");
                }
            }
        });
        CheckBox ch_span = (CheckBox) dialog.getCustomView().findViewById(R.id.ch_span);
        if(LoadPref("spanish_lang").equals("true"))
        {
            ch_span.setChecked(true);
        }
        ch_span.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    lang.add("spanish");
                    text_lang.add("spanish-true");
                    SavePref("spanish_lang", "true");
                }else
                {
                    SavePref("spanish_lang", "false");
                    text_lang.add("spanish-false");
                }
            }
        });
        CheckBox ch_turk = (CheckBox) dialog.getCustomView().findViewById(R.id.ch_turk);
        if(LoadPref("trukish_lang").equals("true"))
        {
            ch_turk.setChecked(true);
        }
        ch_turk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    lang.add("trukish");
                    SavePref("trukish_lang", "true");
                    text_lang.add("trukish-true");
                }else
                {
                    SavePref("trukish_lang", "false");
                    text_lang.add("trukish-false");
                }
            }
        });
        CheckBox ch_urdu = (CheckBox) dialog.getCustomView().findViewById(R.id.ch_urdu);
        if(LoadPref("urdu_lang").equals("true"))
        {
            ch_urdu.setChecked(true);
        }
        ch_urdu.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    lang.add("urdu");
                    SavePref("urdu_lang", "true");
                    text_lang.add("urdu-true");
                }else
                {
                    SavePref("urdu_lang", "false");
                    text_lang.add("urdu-false");
                }
            }
        });


        dialog.show();
    }

}
