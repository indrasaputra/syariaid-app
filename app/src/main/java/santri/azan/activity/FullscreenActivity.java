package santri.azan.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.List;
import java.util.Locale;

import santri.azan.tasbeeh.DataBase;
import santri.azan.tasbeeh.DataBaseModel;
import santri.azan.utils.LogUtils;
import santri.azan.utils.Utils;
import santri.syaria.R;

public class FullscreenActivity extends Utils {

    int count = 0, maincount = 0;	RelativeLayout linear;
    TextView txtview, txttitle;
    Button btnresetcount, btnclearcount;
    String type = "";
    List<DataBaseModel> databasefetch;
    DataBase db = null;
    private AdView mAdView;
    private FullscreenActivity myApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset);
        myApp = this;

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String userlango = preferences.getString(USER_LANGUAGES , USER_LANGUAGES);
        LogUtils.i("in onCreate" ,"preferences = "+ userlango);

        if (userlango !=null) {

            Locale locale = new Locale(userlango);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }

        Actionbar("Full Screen Tasbeeh");
        //Analytics("Full Screen Tasbeeh");
        typeface();
        //banner_ad();
        txtview = (TextView) findViewById(R.id.txtcount);
        txttitle = (TextView) findViewById(R.id.txttitle);
        linear = (RelativeLayout) findViewById(R.id.llayout1);
        btnresetcount = (Button) findViewById(R.id.btnreset_count);
        btnclearcount = (Button) findViewById(R.id.btnclear_count);
        final Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        db = new DataBase(getApplication());
        db.openDataBase();
        databasefetch = db.fetchAllFeedList(0);
        // final int maincount = databasefetch.get(count).getCount();
        Log.d("main activity count", maincount + "");
        txtview.setTypeface(tf3);
        Bundle intent = getIntent().getExtras();
        count = intent.getInt("count");
        maincount = intent.getInt("maincount");
        type = intent.getString("type");
        txttitle.setText(intent.getString("title").toUpperCase());
        txtview.setText(Integer.toString(count));
        linear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(type.equals("normal"))
                {
                    count++;

                    txtview.setText(count + "");

                }else if(type.equals("list"))
                {
                    if (count == 0) {
                        count++;

                        txtview.setText(count + "");
                    }else
                    if (count > 0) {
                        if (count < maincount) {
                            count++;
                            Log.d("value", +count + "");
                            txtview.setText(count + "");
                        }
                    }

                    if (count == maincount) {
                        vibrator.vibrate(300);
                        Toast.makeText(getApplicationContext(), "Reached",
                                Toast.LENGTH_SHORT).show();

                    }
                    if (maincount == 0) {
                        txtview.setText(Integer.toString(0));
                        count++;

                        txtview.setText(count + "");

                    }
                }
                LogUtils.i("count "+count);

            }

        });

        btnresetcount.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                count = 0;
                txtview.setText(count + "");
            }
        });

        btnclearcount.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                count = count - 1;
                txtview.setText(count + "");
            }
        });

        MobileAds.initialize(myApp, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(LoadAdError errorCode) {
                Log.e("failed load banner", errorCode+"");
                // Code to be executed when an ad request fails.
                //myApp.findViewById(R.id.startAppBanner).setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                Intent returnIntent = new Intent();
                returnIntent.putExtra("count", count);
                setResult(RESULT_OK, returnIntent);
                finish();

                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        Intent returnIntent = new Intent();
        returnIntent.putExtra("count", count);
        setResult(RESULT_OK, returnIntent);
        finish();

    }

}
