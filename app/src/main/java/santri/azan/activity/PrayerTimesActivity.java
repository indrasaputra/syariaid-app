package santri.azan.activity;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.api.client.http.ExponentialBackOffPolicy;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import santri.azan.hijridate.HijriCalendarDate;
import santri.azan.receiver.AlarmReceiver;
import santri.azan.support.AppLocationService;
import santri.azan.support.PrayTime;
import santri.azan.utils.LogUtils;
import santri.azan.utils.Utils;
import santri.syaria.MainActivity;
import santri.syaria.R;
import santri.utils.ConnectionDetector;

import static android.graphics.Typeface.BOLD;

@SuppressLint({"ViewHolder", "InflateParams"})
public class PrayerTimesActivity extends Utils implements OnClickListener {
    Calendar Qurancal;
    int ad_count;
    AdapterPrayer adapter;
    Intent alarmIntent;
    String alarm_time = "fals";
    public final ArrayList<String> alarm_value = new ArrayList<>();
    AppLocationService appLocationService;
    Calendar cal;
    String cur_time = "";
    String d_time;
    SharedPreferences.Editor editor;
    SharedPreferences.Editor editor1;
    int id = 0;
    ImageView img_slider_next;
    ImageView img_slider_previous;
    int interflagnext = 1;
    int interflagprevious = 1;
    double latitude = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    double longitude = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    GridView lv_gridview;
    ArrayList<Double> n_array = new ArrayList<>();
    boolean notify;
    DecimalFormat numberFormat;
    String nxt_time = "";
    String[] p_name;
    String[] p_next_prayer;
    String[] p_time;
    String[] p_time_1;
    String[] prayer12, prayer24;
    private PendingIntent pendingIntent;
    SharedPreferences pref;
    SharedPreferences sp;
    EditText text;
    int time = 0;
    double timezone;
    int today = 0;
    TextView txt_date;
    TextView txt_place;
    TextView txt_prayer_time;
    TextView txt_street;
    TextView txt_time;
    TextView txt_title;
    AutoCompleteTextView autotext_city;
    String cityCodePref;
    String cityNamePref;
    List<String> cityNames;
    List<String> cityCodes;
    boolean cityListLoaded = false;
    Button btn_reset;
    ProgressBar pbCity;
    ArrayList<String> values = new ArrayList<>();
    String weekDay;
    String Error;
    private AdView mAdView;
    private InterstitialAd mInterstitialAd;
    LinearLayout ll_place_date, ll_notification;

    ProgressDialog pd;
    private Timer timer;
    private PrayerTimesActivity myApp;

    public class AdapterPrayer extends BaseAdapter {
        boolean alarm = false;
        ImageView btn_alarm;
        LayoutInflater inflater;
        TextView lbl_time;
        LinearLayout lyt_listview;
        Context mContext;
        TextView name;

        public AdapterPrayer(Context context) {
            this.mContext = context;
            this.inflater = (LayoutInflater) this.mContext.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        }

        public int getCount() {
            return PrayerTimesActivity.this.p_name.length;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public void setimage(int position) {
            if (position == 0) {
                this.btn_alarm.setImageResource(R.drawable.fajar_icon);
            } else if (position == 1) {
                this.btn_alarm.setImageResource(R.drawable.zuher_icon);
            } else if (position == 2) {
                this.btn_alarm.setImageResource(R.drawable.asar_icon);
            } else if (position == 3) {
                this.btn_alarm.setImageResource(R.drawable.maghrib_icon);
            } else if (position == 4) {
                this.btn_alarm.setImageResource(R.drawable.isha_icon);
            } else if (position == 5) {
                this.btn_alarm.setImageResource(R.drawable.shorook_icon);
            }
        }

        public void setnormalimage(int position) {
            if (position == 0) {
                this.btn_alarm.setImageResource(R.drawable.fajar_icon_disabled);
            } else if (position == 1) {
                this.btn_alarm.setImageResource(R.drawable.zuher_icon_disabled);
            } else if (position == 2) {
                this.btn_alarm.setImageResource(R.drawable.asar_icon_disabled);
            } else if (position == 3) {
                this.btn_alarm.setImageResource(R.drawable.maghrib_icon_disabled);
            } else if (position == 4) {
                this.btn_alarm.setImageResource(R.drawable.isha_icon_disabled);
            } else if (position == 5) {
                this.btn_alarm.setImageResource(R.drawable.shorook_icon_disabled);
            }
        }

        public View getView(final int position, View view, ViewGroup parent) {
            view = this.inflater.inflate(R.layout.prayer_list, null);
            this.lyt_listview = (LinearLayout) view.findViewById(R.id.lyt_listview);
            this.name = (TextView) view.findViewById(R.id.name);
            this.lbl_time = (TextView) view.findViewById(R.id.time);
            this.btn_alarm = (ImageView) view.findViewById(R.id.btn_alarm);
            this.lbl_time.setTypeface(PrayerTimesActivity.this.tf2, Typeface.BOLD);
            this.name.setTypeface(PrayerTimesActivity.this.tf2, Typeface.NORMAL);
            this.name.setText(PrayerTimesActivity.this.p_name[position]);
            this.lbl_time.setText(PrayerTimesActivity.this.p_time[position]);

            Log.d("p_time ======== ", position + ": " + p_time[position]);

            this.btn_alarm.setVisibility(View.VISIBLE);

            final Date now = new Date();
            final Calendar cal_now = Calendar.getInstance();
            cal_now.setTime(now);



            cal_now.setTime(now);


            if (!PrayerTimesActivity.this.LoadPref(PrayerTimesActivity.this.p_name[position]).equals("fals")) {
                PrayerTimesActivity.this.SavePref(PrayerTimesActivity.this.p_name[position], "tru");
                AdapterPrayer.this.alarm = true;
                cal_now.setTime(now);
                setimage(position);

            }

            if (PrayerTimesActivity.this.LoadPref(PrayerTimesActivity.this.p_name[position]).equals("tru")) {
                PrayerTimesActivity.this.SavePref(PrayerTimesActivity.this.p_name[position], "tru");
                AdapterPrayer.this.alarm = true;
                cal_now.setTime(now);
                setimage(position);
            }

            if (PrayerTimesActivity.this.LoadPref(PrayerTimesActivity.this.p_name[position]).equals("fals")) {
                PrayerTimesActivity.this.SavePref(PrayerTimesActivity.this.p_name[position], "fals");
                setnormalimage(position);
            }

            if (cal_now.before(LoadPref(p_name[position]).equals("tru") )) {
                PrayerTimesActivity.this.SavePref(PrayerTimesActivity.this.p_name[position], "tru");
                AdapterPrayer.this.alarm = true;
                cal_now.setTime(now);
                setimage(position);
            }

            if (cal_now.before(LoadPref(p_name[position]).equals("fals") )) {
                PrayerTimesActivity.this.SavePref(PrayerTimesActivity.this.p_name[position], "fals");
                setnormalimage(position);
            }


            if (cal_now.after(LoadPref(p_name[position]).equals("fals") )) {
                PrayerTimesActivity.this.SavePref(PrayerTimesActivity.this.p_name[position], "fals");
                setnormalimage(position);
            }

            if (cal_now.after(LoadPref(p_name[position]).equals("tru") )) {
                PrayerTimesActivity.this.SavePref(PrayerTimesActivity.this.p_name[position], "tru");
                setimage(position);
            }

            this.lyt_listview.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                }
            });
            this.btn_alarm.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    LogUtils.i(PrayerTimesActivity.this.LoadPref(PrayerTimesActivity.this.p_name[position]) + " click ");
                    if (cal_now.after(PrayerTimesActivity.this.p_time[position])) {
                        Toast.makeText(PrayerTimesActivity.this, "Prayer has been passed", Toast.LENGTH_LONG).show();
                        return;
                    }
                    if (PrayerTimesActivity.this.LoadPref(PrayerTimesActivity.this.p_name[position]).equals("tru")) {
                        AdapterPrayer.this.alarm = false;

                    } else {
                        AdapterPrayer.this.alarm = true;
                        cal_now.setTime(now);
                    }
                    PrayerTimesActivity.this.id = position;
                    PrayerTimesActivity.this.setalarm(PrayerTimesActivity.this.p_name[position].trim().toString(), PrayerTimesActivity.this.p_time[position].trim().toString(), AdapterPrayer.this.alarm);



                }
            });
            return view;
        }
    }


    public void onCreate(Bundle savedInstanceState) {
        fullscreen();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prayer_times);
        this.notify = getIntent().getBooleanExtra("Notify", false);
        this.sp = PreferenceManager.getDefaultSharedPreferences(this);
        LogUtils.i("Notify" + this.notify);
        //banner_ad();

        myApp = this;

        MobileAds.initialize(myApp, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(LoadAdError errorCode) {
                Log.e("failed load banner", errorCode+"");
                // Code to be executed when an ad request fails.
                //myApp.findViewById(R.id.startAppBanner).setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });
//        mInterstitialAd = new InterstitialAd(this);
//        mInterstitialAd.setAdUnitId(getResources().getString(R.string.admob_insertitial_id));
//        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String userlango = preferences.getString(USER_LANGUAGES , USER_LANGUAGES);
        LogUtils.i("in onCreate" ,"preferences = "+ userlango);

        cityCodePref = LoadPref(CITY_CODE_PREF);
        cityNamePref = LoadPref(CITY_NAME_PREF);

        if (userlango !=null) {

            Locale locale = new Locale(userlango);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }

        if (this.notify) {
            this.ad_count = this.sp.getInt("ad_count", 0);
            LogUtils.i("notify if called" + this.ad_count + " fff");
            this.ad_count++;
            LogUtils.i("ad_count incremented : " + this.ad_count + " fff");
            this.editor1 = this.sp.edit();
            this.editor1.putInt("ad_count", this.ad_count);
            this.editor1.commit();
            if (this.ad_count % 5 == 0) {
                LogUtils.i("ad_count %3 ==0 if called");
            }
        }
        this.p_name = getResources().getStringArray(R.array.prayer_array);
        Actionbar(getString(R.string.lbl_prayertimes_title));
        typeface();
        //Analytics(getString(R.string.lbl_prayertimes_title));
        this.ll_notification = findViewById(R.id.ll_notification);
        this.ll_place_date = findViewById(R.id.ll_place_date);
        this.autotext_city = (AutoCompleteTextView) findViewById(R.id.autotext_city);
        this.pbCity = (ProgressBar) findViewById(R.id.pbCity);
        this.appLocationService = new AppLocationService(this);
        this.img_slider_next = (ImageView) findViewById(R.id.img_slider_next);
        this.img_slider_previous = (ImageView) findViewById(R.id.img_slider_previous);
        this.pref = PreferenceManager.getDefaultSharedPreferences(this);
        this.txt_time = (TextView) findViewById(R.id.txt_time);
        this.txt_date = (TextView) findViewById(R.id.txt_date);
        this.txt_place = (TextView) findViewById(R.id.txt_place);
        this.txt_title = (TextView) findViewById(R.id.txt_title);
        this.txt_prayer_time = (TextView) findViewById(R.id.txt_prayer_time);
        this.txt_street = (TextView) findViewById(R.id.txt_street);
        this.d_time = new SimpleDateFormat("hh:mm", Locale.US).format(new Date());
        this.lv_gridview = (GridView) findViewById(R.id.lv_gridview);
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE", Locale.US);
        String monthName = new SimpleDateFormat("MMMM",Locale.US).format(calendar.getTime());
        this.cur_time = new SimpleDateFormat("hh:mm aa", Locale.US).format(new Date());
        this.txt_time.setText(this.cur_time);
        this.weekDay = dayFormat.format(calendar.getTime());
        this.txt_date.setText(this.weekDay + ", " + monthName + " " + calendar.get(5) + ", " + calendar.get(1));
        this.Qurancal = Calendar.getInstance();
        this.Qurancal.add(5, 0);
        this.txt_title.setText(HijriCalendarDate.getSimpleDateDay(this.Qurancal, 0) + " " + HijriCalendarDate.getSimpleDateMonth(this.Qurancal, 0) + " " + HijriCalendarDate.getSimpleDateYear(this.Qurancal, 0));
        this.alarmIntent = new Intent(this, AlarmReceiver.class);
        this.numberFormat = new DecimalFormat("#.##");
        this.img_slider_next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PrayerTimesActivity.this.interflagnext % 3 == 0) {
                }
                PrayerTimesActivity prayerTimesActivity = PrayerTimesActivity.this;
                prayerTimesActivity.interflagnext++;
                Animation RightSwipe = AnimationUtils.loadAnimation(PrayerTimesActivity.this, R.anim.push_left_out);
                PrayerTimesActivity.this.lv_gridview.startAnimation(RightSwipe);
                PrayerTimesActivity.this.txt_date.startAnimation(RightSwipe);
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        try {
                            PrayerTimesActivity.this.time++;
                            LogUtils.i("previous time " + PrayerTimesActivity.this.time);
                            PrayerTimesActivity.this.today = PrayerTimesActivity.this.time;
                            PrayerTimesActivity.this.txt_date.setText(PrayerTimesActivity.this.getTimeNow(PrayerTimesActivity.this.time));
                            PrayerTimesActivity.this.Qurancal.add(5, 1);
                            PrayerTimesActivity.this.txt_title.setText(HijriCalendarDate.getSimpleDateDay(PrayerTimesActivity.this.Qurancal, 0) + " " + HijriCalendarDate.getSimpleDateMonth(PrayerTimesActivity.this.Qurancal, 0) + " " + HijriCalendarDate.getSimpleDateYear(PrayerTimesActivity.this.Qurancal, 0));
                            //PrayerTimesActivity.this.initPrayerTime(PrayerTimesActivity.this.time);
                            PrayerTimesActivity.this.loadPrayerTime(PrayerTimesActivity.this.time);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        Animation RightSwipe1 = AnimationUtils.loadAnimation(PrayerTimesActivity.this, R.anim.push_left_in);
                        PrayerTimesActivity.this.lv_gridview.startAnimation(RightSwipe1);
                        PrayerTimesActivity.this.txt_date.startAnimation(RightSwipe1);
                        PrayerTimesActivity.this.txt_title.startAnimation(RightSwipe1);
                    }
                }, 150);
            }
        });
        this.img_slider_previous.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PrayerTimesActivity.this.interflagprevious % 3 == 0) {
                }
                PrayerTimesActivity prayerTimesActivity = PrayerTimesActivity.this;
                prayerTimesActivity.interflagprevious++;
                Animation LeftSwipe = AnimationUtils.loadAnimation(PrayerTimesActivity.this, R.anim.push_right_out);
                PrayerTimesActivity.this.lv_gridview.startAnimation(LeftSwipe);
                PrayerTimesActivity.this.txt_date.startAnimation(LeftSwipe);
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        try {
                            PrayerTimesActivity.this.time--;
                            LogUtils.i("previous time " + PrayerTimesActivity.this.time);
                            PrayerTimesActivity.this.today = PrayerTimesActivity.this.time;
                            PrayerTimesActivity.this.txt_date.setText(PrayerTimesActivity.this.getTimeNow(PrayerTimesActivity.this.time));
                            PrayerTimesActivity.this.Qurancal.add(5, -1);
                            PrayerTimesActivity.this.txt_title.setText(HijriCalendarDate.getSimpleDateDay(PrayerTimesActivity.this.Qurancal, 0) + " " + HijriCalendarDate.getSimpleDateMonth(PrayerTimesActivity.this.Qurancal, 0) + " " + HijriCalendarDate.getSimpleDateYear(PrayerTimesActivity.this.Qurancal, 0));
                            //PrayerTimesActivity.this.initPrayerTime(PrayerTimesActivity.this.time);
                            PrayerTimesActivity.this.loadPrayerTime(PrayerTimesActivity.this.time);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        Animation LeftSwipe1 = AnimationUtils.loadAnimation(PrayerTimesActivity.this, R.anim.push_right_in);
                        PrayerTimesActivity.this.lv_gridview.startAnimation(LeftSwipe1);
                        PrayerTimesActivity.this.txt_date.startAnimation(LeftSwipe1);
                        PrayerTimesActivity.this.txt_title.startAnimation(LeftSwipe1);
                    }
                }, 150);
            }
        });
        this.txt_time.setTypeface(this.tf2, BOLD);
        this.txt_title.setTypeface(this.tf2, BOLD);
        this.txt_date.setTypeface(this.tf2, BOLD);
        this.txt_place.setTypeface(this.tf2, BOLD);
        this.txt_prayer_time.setTypeface(this.tf2, BOLD);
        this.txt_street.setTypeface(this.tf2, BOLD);

        final long DELAY = 100; // milliseconds
        this.autotext_city.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("city length", autotext_city.getText().toString().length() + "");
                timer = new Timer();
                if (autotext_city.isFocused()) {
                        //&& !autotext_city.getText().toString().equals("")
                        //&& autotext_city.getText().toString().length() >= 0) {
                    // is only executed if the EditText was directly changed by the user
                    //timer.cancel();
                    if (!cityListLoaded) {
                        timer = new Timer();
                        timer.schedule(
                                new TimerTask() {
                                    @Override
                                    public void run() {
                                        // TODO: do what you need here (refresh list)
                                        // you will probably need to use runOnUiThread(Runnable action) for some specific actions
                                        //get kelurahan to api
                                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (this != null) {
                                                    runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            loadCity();
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    }
                                },
                                DELAY
                        );
                    }
                } else {
                    timer.cancel();
                    timer = new Timer();
//                    if (controllerAllianz != null) {
//                        controllerAllianz.cancelRequest();
//                    }
                }
                //validator.validate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        autotext_city.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(myApp, position+" "+id, Toast.LENGTH_LONG).show();
                int index = cityNames.indexOf(autotext_city.getText().toString());
                cityCodePref = cityCodes.get(index);
                cityNamePref = autotext_city.getText().toString();
                SavePref(CITY_CODE_PREF, cityCodePref);
                SavePref(CITY_NAME_PREF, cityNamePref);
                txt_street.setVisibility(View.GONE);
                txt_place.setText(cityNamePref);
                ll_notification.setVisibility(View.GONE);
                ll_place_date.setVisibility(View.VISIBLE);
                lv_gridview.setVisibility(View.VISIBLE);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(autotext_city.getWindowToken(), 0);
                try {
                    loadPrayerTime(time);
                } catch (ParseException e) {
                    Log.e("error", e.getMessage());
                }

            }
        });

        btn_reset = (Button) findViewById(R.id.btn_reset);
        btn_reset.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!cityListLoaded) {
                    loadCity();
                }
//                SavePref(CITY_CODE_PREF, "auto");
//                SavePref(CITY_NAME_PREF, "");
//                txt_street.setVisibility(View.VISIBLE);
//                txt_street.setText(LoadPref(USER_STREET));
//                txt_place.setText(LoadPref(USER_CITY) + " " + LoadPref(USER_STATE) + " " + LoadPref(USER_COUNTRY));
//                try {
//                    loadPrayerTime(time);
//                } catch (ParseException e) {
//                    Log.e("Error", e.getMessage());
//                }
            }
        });
    }

    /* Access modifiers changed, original: protected */
    public void onDestroy() {
        if (this.appLocationService != null) {
            this.appLocationService.stopUsingGPS();
            stopService(new Intent(this, AppLocationService.class));
        }
        super.onDestroy();
    }

    /* Access modifiers changed, original: protected */
    public void onResume() {
        font();
        this.cur_time = new SimpleDateFormat("hh:mm aa", Locale.US).format(new Date());
        if (this.pref_time.equals("0")) {
            this.txt_time.setText(this.cur_time);
        } else {
            String formattedDateTime = new SimpleDateFormat("HH:mm",Locale.US).format(new Date());
            System.out.println(formattedDateTime);
            this.txt_time.setText(formattedDateTime);
        }
        LogUtils.i(" LoadPref(USER_LAT) " + LoadPref(this.USER_LAT));
        this.latitude = Double.parseDouble(loadString(this.USER_LAT));
        this.longitude = Double.parseDouble(loadString(this.USER_LNG));
        LogUtils.i(this.latitude + " lat in " + this.longitude);
        if (cityCodePref==null || cityCodePref.equals("") || cityCodePref.equals("auto")) {
            this.txt_street.setText(LoadPref(this.USER_STREET));
            this.txt_place.setText(LoadPref(this.USER_CITY) + " " + LoadPref(this.USER_STATE) + " " + LoadPref(this.USER_COUNTRY));
        } else {
            this.txt_place.setText(cityNamePref);
            this.txt_street.setVisibility(View.GONE);
            //this.autotext_city.setText(cityNamePref);
        }

        LogUtils.i("txt_time " + this.txt_time.getTypeface());
        Log.d("USER CITY ==== ", LoadPref(this.USER_CITY));
        Log.d("USER STATE ==== ", LoadPref(this.USER_STATE));
        Log.d("USER COUNTRY ==== ", LoadPref(this.USER_COUNTRY));
        this.adapter = new AdapterPrayer(this);

        if (isOnline()) {
            try {
                //initPrayerTime(this.time);
                loadPrayerTime(this.time);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }




        super.onResume();
    }

    public void setalarm(String alarm_name, String alarm_time, boolean alarm_val) {
        LogUtils.i(alarm_name + " setalarm " + alarm_val);
        if (alarm_val) {
            this.alarmIntent.putExtra("type", alarm_name);
            this.alarmIntent.putExtra("time", alarm_time);
            set_alarm(alarm_time, alarm_name);
            return;
        }
        setbg(alarm_name, "fals");
    }

    private void showCustomView(final String name, final String n_time) {
        MaterialDialog dialog = new MaterialDialog.Builder(this).title(getString(R.string.edittext_pref_dialogtitle)).content(R.string.optional_dialog_message).cancelable(false).positiveText("Ok").positiveColor(Color.parseColor("#379e24")).customView(R.layout.alert_dialog, true).negativeText(android.R.string.cancel).negativeColor(Color.parseColor("#379e24")).callback(new MaterialDialog.ButtonCallback() {
            public void onPositive(MaterialDialog dialog) {
                PrayerTimesActivity.this.editor = PrayerTimesActivity.this.pref.edit();
                PrayerTimesActivity.this.editor.putString(name + "daylight", PrayerTimesActivity.this.text.getText().toString().trim());
                PrayerTimesActivity.this.editor.commit();
                try {
                    PrayerTimesActivity.this.setalarm(name, n_time, false);
                    PrayerTimesActivity.this.setalarm(name, PrayerTimesActivity.this.addDayLight(n_time, Integer.parseInt(PrayerTimesActivity.this.LoadStringPref(name + "daylight"))), true);
                    LogUtils.i(PrayerTimesActivity.this.latitude + " lat " + PrayerTimesActivity.this.longitude);
                    PrayerTimesActivity.this.initPrayerTime(PrayerTimesActivity.this.time);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            public void onNegative(MaterialDialog dialog) {
            }
        }).build();
        this.text = (EditText) dialog.getCustomView().findViewById(R.id.text);
        LogUtils.i(" name: " + name);
        this.text.setText(this.pref.getString(name + "daylight", "0"));
        this.text.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void afterTextChanged(Editable s) {
            }
        });
        dialog.show();
    }

    public boolean setalarmimage(String name, String stime) {
        DateFormat df = new SimpleDateFormat("MMMM d, yyyy", Locale.US);
        String now_date = df.format(new Date());
        int hour = 0;
        int minute = 0;
        try {
            String timenow = changeTimeFormat(stime);
            hour = Integer.parseInt(timenow.substring(0, 2));
            minute = Integer.parseInt(timenow.substring(3, 5));
        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(df.parse(now_date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        calendar.set(11, hour);
        calendar.set(12, minute + Integer.parseInt(this.pref.getString(name + "daylight", "0")));
        Date now = new Date();
        Calendar cal_now = Calendar.getInstance();
        cal_now.setTime(now);
        if (cal_now.after(calendar)) {

            return true;
        }
        return false;
    }
    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }
    public  void set_alarm(String stime, String name) {
        DateFormat df = new SimpleDateFormat("MMMM d, yyyy", Locale.US);
        String now_date = df.format(new Date());
        int hour = 0;
        int minute = 0;
        int position;

        try {
            String timenow = changeTimeFormat(stime);
            hour = Integer.parseInt(timenow.substring(0, 2));
            minute = Integer.parseInt(timenow.substring(3, 5));
        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(df.parse(now_date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        calendar.set(11, hour);
        calendar.set(12, minute + Integer.parseInt(this.pref.getString(name + "daylight", "0")));
        Date now = new Date();
        Calendar cal_now = Calendar.getInstance();
        cal_now.setTime(now);


        if (PrayerTimesActivity.this.today == 0) {
            cal_now.setTime(now);

            if (cal_now.before(calendar)) {
                try {
                    cal_now.setTime(now);
                    this.alarm_time = "tru";
                    this.pendingIntent = PendingIntent.getBroadcast(this, this.id, this.alarmIntent, 0);
                    ((AlarmManager) getSystemService(Context.ALARM_SERVICE)).set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), this.pendingIntent);
                    Toast.makeText(this, " Alarm Enabled", Toast.LENGTH_SHORT).show();
                    setbg(name, this.alarm_time);
                    cal_now.setTime(now);

                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else {
                this.alarm_time = "tru";
                this.pendingIntent = PendingIntent.getBroadcast(this, this.id, this.alarmIntent,0 );
                ((AlarmManager) getSystemService(Context.ALARM_SERVICE)).cancel(this.pendingIntent);
                Toast.makeText(this, "Prayer has been passed, Alarm set for Next Day", Toast.LENGTH_SHORT).show();
                setbg(name, this.alarm_time);
            }


            setbg(name, this.alarm_time);
        }else  if (PrayerTimesActivity.this.today != 0) {

            if (cal_now.before(calendar)) {
                try {
                    this.alarm_time = "tru";
                    this.pendingIntent = PendingIntent.getBroadcast(this, this.id, this.alarmIntent, 0);
                    ((AlarmManager) getSystemService(Context.ALARM_SERVICE)).cancel(this.pendingIntent);
                    Toast.makeText(this, " Alarm Enabled", Toast.LENGTH_SHORT).show();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else {
                this.alarm_time = "fals";
                this.pendingIntent = PendingIntent.getBroadcast(this, this.id, this.alarmIntent, 0);
                ((AlarmManager) getSystemService(Context.ALARM_SERVICE)).cancel(this.pendingIntent);
                Toast.makeText(this, "Alarm Disabled", Toast.LENGTH_SHORT).show();

            }

        }

        setbg(name, this.alarm_time);
    }



    public void setbg(String name, String alarm_time) {

        SavePref(name, alarm_time);
        LogUtils.i(LoadPref(name) + " calendar name " + name);
        this.adapter.notifyDataSetChanged();
    }



    public void PrayerTimeTomorrow(int time, String name) throws ParseException {
        ArrayList<String> prayerTimes = preinitPrayerTime(1).getPrayerTimes(this.cal, this.latitude, this.longitude, this.timezone);
        if (this.latitude != FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE && this.longitude != FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE) {
            String[] p_name = getResources().getStringArray(R.array.prayer_array);
            for (int i = 0; i < prayerTimes.size(); i++) {
                if (this.id == 0) {
                    set_alarm_tomoro(addDayLight((String) prayerTimes.get(i), Integer.parseInt(this.pref.getString(p_name[0] + "daylight", "0"))));
                    set_alarm_tomoro(addDayLight((String) prayerTimes.get(0), Integer.parseInt(this.pref.getString(p_name[1] + "daylight", "0"))));
                } else if (this.id == 3) {
                    set_alarm_tomoro(addDayLight((String) prayerTimes.get(i), Integer.parseInt(this.pref.getString(p_name[2] + "daylight", "0")) - 66));
                } else if (this.id == 4) {
                    set_alarm_tomoro(addDayLight((String) prayerTimes.get(i), Integer.parseInt(this.pref.getString(p_name[3] + "daylight", "0"))));
                } else if (this.id == 5) {
                    set_alarm_tomoro(addDayLight((String) prayerTimes.get(i), Integer.parseInt(this.pref.getString(p_name[4] + "daylight", "0"))));
                } else if (this.id == 6) {
                    set_alarm_tomoro(addDayLight((String) prayerTimes.get(i), Integer.parseInt(this.pref.getString(p_name[5] + "daylight", "0"))));
                }
            }
        }
    }

    public void set_alarm_tomoro(String time) {
        DateFormat df = new SimpleDateFormat("MMMM d, yyyy", Locale.US);
        String now_date = df.format(new Date());
        int hour = 0;
        int minute = 0;
        try {
            String timenow = changeTimeFormat(time);
            hour = Integer.parseInt(timenow.substring(0, 2));
            minute = Integer.parseInt(timenow.substring(3, 5));
        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(df.parse(now_date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        calendar.set(11, hour);
        calendar.set(12, minute + Integer.parseInt(this.pref.getString("adjust_alarm", "-5")));
        calendar.add(5, 1);
        this.pendingIntent = PendingIntent.getBroadcast(this, this.id, this.alarmIntent, 0);
        ((AlarmManager) getSystemService(Context.ALARM_SERVICE)).set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), this.pendingIntent);
        Toast.makeText(this, "Prayer has been passed, Alarm set for Next Day", Toast.LENGTH_SHORT).show();
    }

    public void cancel_alarm() {
        this.pendingIntent = PendingIntent.getBroadcast(this, this.id, this.alarmIntent, 0);
        ((AlarmManager) getSystemService(Context.ALARM_SERVICE)).cancel(this.pendingIntent);
        Toast.makeText(this, "Alarm disable", Toast.LENGTH_SHORT).show();
    }

    public void onClick(View v) {
        v.getId();
    }

    public String main(String time) {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm", Locale.US);
        Date date = null;
        try {
            date = new SimpleDateFormat("hh:mm a", Locale.US).parse(time);
        } catch (Exception e) {
        }
        return displayFormat.format(date);
    }

    public void initPrayerTime(int times) throws ParseException {
        int i;
        String[] strArr;
        DateFormat simpleDateFormat;
        Date d1;
        long difference;
        int days;
        int hours;
        int min;
        String timeDiffString;
        font();
        PrayTime prayers = preinitPrayerTime(times);
        ArrayList<String> prayerTimes = prayers.getPrayerTimes(getTimeByCal(times), this.latitude, this.longitude, this.timezone);
        ArrayList<String> prayerNames = prayers.getTimeNames();
        Log.d("pref_time ==== ",this.pref_time);
        for (i = 0; i < prayerTimes.size(); i++) {
            if (this.pref_time.equals("0")) {
                strArr = new String[6];
                strArr[0] = addDayLight((String) prayerTimes.get(0), Integer.parseInt(this.pref.getString(this.p_name[0] + "daylight", "0")) - 15);
                strArr[1] = addDayLight((String) prayerTimes.get(0), Integer.parseInt(this.pref.getString(this.p_name[1] + "daylight", "0")));
                strArr[2] = addDayLight((String) prayerTimes.get(2), Integer.parseInt(this.pref.getString(this.p_name[2] + "daylight", "0")));
                strArr[3] = addDayLight((String) prayerTimes.get(3), Integer.parseInt(this.pref.getString(this.p_name[3] + "daylight", "0")) - 66);
                strArr[4] = addDayLight((String) prayerTimes.get(5), Integer.parseInt(this.pref.getString(this.p_name[4] + "daylight", "0")));
                strArr[5] = addDayLight((String) prayerTimes.get(6), Integer.parseInt(this.pref.getString(this.p_name[5] + "daylight", "0")));
                this.p_time = strArr;
                Log.d("p_time init ==== ", p_time[0]);
            } else {
                strArr = new String[6];
                strArr[0] = addDayLight(main((String) prayerTimes.get(0)), Integer.parseInt(this.pref.getString(this.p_name[0] + "daylight", "0")) - 15);
                strArr[1] = addDayLight(main((String) prayerTimes.get(0)), Integer.parseInt(this.pref.getString(this.p_name[1] + "daylight", "0")));
                strArr[2] = addDayLight(main((String) prayerTimes.get(2)), Integer.parseInt(this.pref.getString(this.p_name[2] + "daylight", "0")));
                strArr[3] = addDayLight(main((String) prayerTimes.get(3)), Integer.parseInt(this.pref.getString(this.p_name[3] + "daylight", "0")) - 66);
                strArr[4] = addDayLight(main((String) prayerTimes.get(5)), Integer.parseInt(this.pref.getString(this.p_name[4] + "daylight", "0")));
                strArr[5] = addDayLight(main((String) prayerTimes.get(6)), Integer.parseInt(this.pref.getString(this.p_name[5] + "daylight", "0")));
                this.p_time = strArr;
                Log.d("p_time init ==== ", p_time[0]);
            }
            strArr = new String[6];
            strArr[0] = set_time((String) prayerTimes.get(0), Integer.parseInt(this.pref.getString(this.p_name[0] + "daylight", "0")) - 15, 0);
            strArr[1] = set_time((String) prayerTimes.get(0), Integer.parseInt(this.pref.getString(this.p_name[1] + "daylight", "0")), 0);
            strArr[2] = set_time((String) prayerTimes.get(2), Integer.parseInt(this.pref.getString(this.p_name[2] + "daylight", "0")), 0);
            strArr[3] = set_time((String) prayerTimes.get(3), Integer.parseInt(this.pref.getString(this.p_name[3] + "daylight", "0")) - 66, 0);
            strArr[4] = set_time((String) prayerTimes.get(5), Integer.parseInt(this.pref.getString(this.p_name[4] + "daylight", "0")), 0);
            strArr[5] = set_time((String) prayerTimes.get(6), Integer.parseInt(this.pref.getString(this.p_name[5] + "daylight", "0")), 0);
            this.p_time_1 = strArr;
            strArr = new String[6];
            strArr[0] = this.p_time_1[0] + " " + addDay_Light(main((String) prayerTimes.get(0)), Integer.parseInt(this.pref.getString(this.p_name[0] + "daylight", "0")) - 15);
            strArr[1] = this.p_time_1[1] + " " + addDay_Light(main((String) prayerTimes.get(0)), Integer.parseInt(this.pref.getString(this.p_name[1] + "daylight", "0")));
            strArr[2] = this.p_time_1[2] + " " + addDay_Light(main((String) prayerTimes.get(2)), Integer.parseInt(this.pref.getString(this.p_name[2] + "daylight", "0")));
            strArr[3] = this.p_time_1[3] + " " + addDay_Light(main((String) prayerTimes.get(3)), Integer.parseInt(this.pref.getString(this.p_name[3] + "daylight", "0")) - 66);
            strArr[4] = this.p_time_1[4] + " " + addDay_Light(main((String) prayerTimes.get(5)), Integer.parseInt(this.pref.getString(this.p_name[4] + "daylight", "0")));
            strArr[5] = this.p_time_1[5] + " " + addDay_Light(main((String) prayerTimes.get(6)), Integer.parseInt(this.pref.getString(this.p_name[5] + "daylight", "0")));
            this.p_next_prayer = strArr;
        }
        this.n_array.clear();
        this.values.clear();
        this.nxt_time = "";
        this.alarm_value.clear();
        for (i = 0; i < this.p_next_prayer.length; i++) {
            Log.d("p_time_1 ====== ", i + ": " + p_time_1[i]);
            Log.d("p_next_prayer ====== ", i + ": " + p_next_prayer[i]);

            simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.US);
            d1 = simpleDateFormat.parse(simpleDateFormat.format(new Date()));
            difference = simpleDateFormat.parse(this.p_next_prayer[i]).getTime() - d1.getTime();
            days = (int) (difference / 86400000);
            hours = (int) ((difference - ((long) (86400000 * days))) / 3600000);
            min = ((int) ((difference - ((long) (86400000 * days))) - ((long) (3600000 * hours)))) / ExponentialBackOffPolicy.DEFAULT_MAX_INTERVAL_MILLIS;
            if (hours >= 0 && min >= 0) {
                timeDiffString = "";
                if (min < 9) {
                    timeDiffString = hours + ".0" + min;
                } else {
                    timeDiffString = hours + "." + min;
                }
                if (min > 9) {
                    this.alarm_value.add(String.format("%.2f", new Object[]{Double.valueOf(Double.parseDouble(hours + "." + min))}) + " " + this.p_name[i]);
                } else {
                    this.alarm_value.add(String.format("%.1f", new Object[]{Double.valueOf(Double.parseDouble(hours + "." + min))}) + " " + this.p_name[i]);
                }
                this.n_array.add(Double.valueOf(Double.parseDouble(timeDiffString.replace("-", "").replace(",", ".").trim())));
                this.values.add(timeDiffString.replace("-", "").replace(",", ".") + " " + this.p_name[i]);
            }
        }
        String n_time = "";
        int n;
        String[] m;
        int p;
        String[] val;
        if (this.n_array.size() > 0) {
            Collections.sort(this.n_array);
            for (n = 0; n < this.values.size(); n++) {
                m = ((String) this.values.get(n)).split(" ");
                if (((Double) this.n_array.get(0)).doubleValue() == Double.parseDouble(m[0].trim())) {
                    this.nxt_time = m[1].trim();
                    for (p = 0; p < this.alarm_value.size(); p++) {
                        if (((String) this.alarm_value.get(p)).contains(this.nxt_time)) {
                            val = ((String) this.alarm_value.get(p)).replace(this.nxt_time, "").replace(".", "/").replace("٫", "/").replace(",", "/").split("/");
                            LogUtils.i(((String) this.alarm_value.get(p)) + " val[0].trim() " + val[0].trim() + " nxt_time " + this.nxt_time);
                            if (val[0].trim().equals("0")) {
                                n_time = val[1].trim() + " minutes";
                            } else {
                                n_time = val[0].trim() + " Hours " + val[1].trim() + " Minutes";
                            }
                        }
                    }
                }
            }
            this.txt_prayer_time.setText(this.nxt_time + " " + n_time);




        } else {
            strArr = new String[6];
            strArr[0] = set_time((String) prayerTimes.get(0), Integer.parseInt(this.pref.getString(this.p_name[0] + "daylight", "0")) - 15, 1);
            strArr[1] = set_time((String) prayerTimes.get(0), Integer.parseInt(this.pref.getString(this.p_name[1] + "daylight", "0")), 1);
            strArr[2] = set_time((String) prayerTimes.get(2), Integer.parseInt(this.pref.getString(this.p_name[2] + "daylight", "0")), 1);
            strArr[3] = set_time((String) prayerTimes.get(3), Integer.parseInt(this.pref.getString(this.p_name[3] + "daylight", "0")) - 66, 1);
            strArr[4] = set_time((String) prayerTimes.get(5), Integer.parseInt(this.pref.getString(this.p_name[4] + "daylight", "0")), 1);
            strArr[5] = set_time((String) prayerTimes.get(6), Integer.parseInt(this.pref.getString(this.p_name[5] + "daylight", "0")), 1);
            this.p_time_1 = strArr;
            strArr = new String[6];
            strArr[0] = this.p_time_1[0] + " " + addDay_Light(main((String) prayerTimes.get(0)), Integer.parseInt(this.pref.getString(this.p_name[0] + "daylight", "0")) - 15);
            strArr[1] = this.p_time_1[1] + " " + addDay_Light(main((String) prayerTimes.get(0)), Integer.parseInt(this.pref.getString(this.p_name[1] + "daylight", "0")));
            strArr[2] = this.p_time_1[2] + " " + addDay_Light(main((String) prayerTimes.get(2)), Integer.parseInt(this.pref.getString(this.p_name[2] + "daylight", "0")));
            strArr[3] = this.p_time_1[3] + " " + addDay_Light(main((String) prayerTimes.get(3)), Integer.parseInt(this.pref.getString(this.p_name[3] + "daylight", "0")) - 66);
            strArr[4] = this.p_time_1[4] + " " + addDay_Light(main((String) prayerTimes.get(5)), Integer.parseInt(this.pref.getString(this.p_name[4] + "daylight", "0")));
            strArr[5] = this.p_time_1[5] + " " + addDay_Light(main((String) prayerTimes.get(6)), Integer.parseInt(this.pref.getString(this.p_name[5] + "daylight", "0")));
            this.p_next_prayer = strArr;
            this.n_array.clear();
            this.values.clear();
            this.alarm_value.clear();
            n_time = "";
            this.nxt_time = "";
            this.alarm_value.clear();
            val = new String[0];
            for (i = 0; i < this.p_next_prayer.length; i++) {
                simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.US);
                d1 = simpleDateFormat.parse(simpleDateFormat.format(new Date()));
                Date d2 = simpleDateFormat.parse(this.p_next_prayer[i]);

                Log.d("p_next_prayer ====== ", i + ": " + p_next_prayer[i]);

                difference = d2.getTime() - d1.getTime();
                days = (int) (difference / 86400000);
                hours = (int) ((difference - ((long) (86400000 * days))) / 3600000);
                min = ((int) ((difference - ((long) (86400000 * days))) - ((long) (3600000 * hours)))) / ExponentialBackOffPolicy.DEFAULT_MAX_INTERVAL_MILLIS;
                LogUtils.i(d1 + " date " + d2);
                if (hours >= 0 && min >= 0) {
                    timeDiffString = "";
                    if (min < 9) {
                        timeDiffString = hours + ".0" + min;
                    } else {
                        timeDiffString = hours + "." + min;
                    }
                    LogUtils.i(" hours " + timeDiffString);
                    if (min > 9) {
                        this.alarm_value.add(String.format("%.2f", new Object[]{Double.valueOf(Double.parseDouble(hours + "." + min))}) + " " + this.p_name[i]);
                    } else {
                        this.alarm_value.add(String.format("%.1f", new Object[]{Double.valueOf(Double.parseDouble(hours + "." + min))}) + " " + this.p_name[i]);
                    }
                    this.n_array.add(Double.valueOf(Double.parseDouble(timeDiffString.replace("-", "").replace(",", ".").trim())));
                    this.values.add(timeDiffString.replace("-", "").replace(",", ".") + " " + this.p_name[i]);
                }
            }
            if (this.n_array.size() > 0) {
                Collections.sort(this.n_array);
                for (n = 0; n < this.values.size(); n++) {
                    m = ((String) this.values.get(n)).split(" ");
                    LogUtils.i(this.n_array.get(0) + " values value " + this.n_array.get(1));
                    if (((Double) this.n_array.get(0)).doubleValue() == Double.parseDouble(m[0].trim())) {
                        this.nxt_time = m[1].trim();
                        for (p = 0; p < this.alarm_value.size(); p++) {
                            if (((String) this.alarm_value.get(p)).contains(this.nxt_time)) {
                                LogUtils.i(((String) this.alarm_value.get(p)) + " alaram value " + this.nxt_time);
                                val = ((String) this.alarm_value.get(p)).replace(this.nxt_time, "").replace(".", "/").replace("٫", "/").replace(",", "/").split("/");
                                if (val[0].trim().equals("0")) {
                                    n_time = val[1].trim() + " minutes";
                                } else {
                                    n_time = val[0].trim() + " Hours " + val[1].trim() + " Minutes";
                                }
                            }
                        }
                    }
                }
                this.txt_prayer_time.setText(this.nxt_time + " " + n_time);
            }
        }
        this.lv_gridview.setAdapter(this.adapter);
    }

    public String set_time(String stime, int min, int o) {
        String new_time = "";
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy", Locale.US);
        String now_date = df.format(new Date());
        int hour = 0;
        int minute = 0;
        try {
            String timenow = changeTimeFormat(stime);
            hour = Integer.parseInt(timenow.substring(0, 2));
            minute = Integer.parseInt(timenow.substring(3, 5));
        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(df.parse(now_date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (o == 1) {
            calendar.set(11, hour + 12);
            calendar.add(5, 1);
        } else {
            calendar.set(11, hour);
        }
        calendar.set(12, minute);
        return df.format(calendar.getTime());
    }

    public PrayTime preinitPrayerTime(int time) {
        String timeZone = this.pref.getString("timezone", "");
        PrayTime prayers = new PrayTime();
        prayers.setTimeFormat(prayers.Time12);
        prayers.setCalcMethod(Integer.parseInt(this.pref.getString("method", "1")));
        prayers.setAsrJuristic(Integer.parseInt(this.pref.getString("juristic", "1")));
        prayers.setAdjustHighLats(Integer.parseInt(this.pref.getString("higherLatitudes", "1")));
        prayers.tune(new int[]{0, 0, 0, 0, 0, 0, 0});
        Date now = new Date();
        this.cal = Calendar.getInstance();
        this.cal.setTime(now);
        this.cal.add(5, time);
        if (timeZone.equals("")) {
            this.timezone = getTimeZone(this.cal.getTimeZone().getID().toString());
            String TimeZoneName = this.cal.getTimeZone().getID().toString();
            this.editor = this.pref.edit();
            this.editor.putString("timezone", TimeZoneName);
            this.editor.commit();
        } else {
            this.timezone = getTimeZone(timeZone);
        }

        return prayers;
    }

    public void loadPrayerTime(int times) throws ParseException {
        if (cityCodePref==null || cityCodePref.equals("") || cityCodePref.equals("auto")) {
            ll_notification.setVisibility(View.VISIBLE);
            ll_place_date.setVisibility(View.GONE);
            lv_gridview.setVisibility(View.GONE);
        } else {
            new PrayerTimesActivity.GetDataAsyncTask().execute();
        }

    }

    public void loadCity() {
        new PrayerTimesActivity.GetCityDataAsyncTask().execute();
    }

    class GetDataAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(PrayerTimesActivity.this);
            pd.setMessage(getString(R.string.loading));
            pd.setCancelable(true);
            pd.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            URL hp;
            String city = LoadPref(USER_STATE);
            cityCodePref = LoadPref(CITY_CODE_PREF);
            cityNamePref = LoadPref(CITY_NAME_PREF);
            try {
                if (cityCodePref==null || cityCodePref.equals("") || cityCodePref.equals("auto")) {

                    //hp = new URL("https://muslimsalat.com/"+ city + "/daily/"+getDateByCal(time)+".json?key=cd03fabbc81af5960df410e72601ecce");
                    hp = new URL("https://api.pray.zone/v2/times/day.json?latitude="+latitude+"&longitude="+longitude+"&elevation=500&date="+getDateByCal(time)+"&timeformat=1&juristic=0");
                    Log.e("URLs", "" + hp);
                    URLConnection hpCon = hp.openConnection();
                    hpCon.connect();
                    InputStream input = hpCon.getInputStream();
                    Log.d("input", "" + input);
                    BufferedReader r = new BufferedReader(new InputStreamReader(input));
                    String x;
                    x = r.readLine();
                    StringBuilder total = new StringBuilder();
                    while (x != null) {
                        total.append(x);
                        x = r.readLine();
                    }
                    JSONObject jsonObject = new JSONObject(total.toString());

                    if (jsonObject.has("status")) {
                        JSONObject jsonResults = jsonObject.getJSONObject("results");
                        JSONArray jArrayDatetime = jsonResults.getJSONArray("datetime");
                        JSONObject jObj = jArrayDatetime.getJSONObject(0);
                        JSONObject jsonTimes = jObj.getJSONObject("times");

                        prayer12 = new String[6];
                        prayer12[0] = jsonTimes.getString("Imsak");
                        prayer12[1] = jsonTimes.getString("Fajr");
                        prayer12[2] = jsonTimes.getString("Dhuhr");
                        prayer12[3] = jsonTimes.getString("Asr");
                        prayer12[4] = jsonTimes.getString("Maghrib");
                        prayer12[5] = jsonTimes.getString("Isha");

                        Log.d("strArr", prayer12[0]);
                        p_time = prayer12;

                        prayer24 = new String[6];
                        if (pref_time.equals("1")) {
                            //Format of the date defined in the input String
                            DateFormat df = new SimpleDateFormat("hh:mm aa");
                            //Desired format: 24 hour format: Change the pattern as per the need
                            DateFormat outputformat = new SimpleDateFormat("HH:mm");

                            prayer24[0] = outputformat.format(df.parse(jsonTimes.getString("Imsak")));
                            prayer24[1] = outputformat.format(df.parse(jsonTimes.getString("Fajr")));
                            prayer24[2] = outputformat.format(df.parse(jsonTimes.getString("Dhuhr")));
                            prayer24[3] = outputformat.format(df.parse(jsonTimes.getString("Asr")));
                            prayer24[4] = outputformat.format(df.parse(jsonTimes.getString("Maghrib")));
                            prayer24[5] = outputformat.format(df.parse(jsonTimes.getString("Isha")));
                            Log.d("strArr", prayer24[0]);
                            p_time = prayer24;
                        }
                        SavePref(CITY_CODE_PREF, "auto");
                    } else {
                        Error = "Unable to get schedule";
                    }
                } else {
                    hp = new URL("https://api.banghasan.com/sholat/format/json/jadwal/kota/"+cityCodePref+"/tanggal/"+getDateByCal(time));
                    Log.e("URLs", "" + hp);
                    URLConnection hpCon = hp.openConnection();
                    hpCon.connect();
                    InputStream input = hpCon.getInputStream();
                    Log.d("input", "" + input);
                    BufferedReader r = new BufferedReader(new InputStreamReader(input));
                    String x;
                    x = r.readLine();
                    StringBuilder total = new StringBuilder();
                    while (x != null) {
                        total.append(x);
                        x = r.readLine();
                    }
                    JSONObject jsonObject = new JSONObject(total.toString());

                    if (jsonObject.getString("status").equals("ok")) {
                        JSONObject jsonData = jsonObject.getJSONObject("jadwal").getJSONObject("data");
                        if (!jsonData.toString().equals("kosong")) {
                            prayer24 = new String[6];
                            prayer24[0] = jsonData.getString("imsak");
                            prayer24[1] = jsonData.getString("subuh");
                            prayer24[2] = jsonData.getString("dzuhur");
                            prayer24[3] = jsonData.getString("ashar");
                            prayer24[4] = jsonData.getString("maghrib");
                            prayer24[5] = jsonData.getString("isya");

                            if (pref_time.equals("1")) {
                                p_time = prayer24;
                            } else {
                                //Format of the date defined in the input String
                                DateFormat df = new SimpleDateFormat("HH:mm");
                                //Desired format: 24 hour format: Change the pattern as per the need
                                DateFormat outputformat = new SimpleDateFormat("hh:mm aa");

                                prayer12 = new String[6];
                                prayer12[0] = outputformat.format(df.parse(prayer24[0]));
                                prayer12[1] = outputformat.format(df.parse(prayer24[1]));
                                prayer12[2] = outputformat.format(df.parse(prayer24[2]));
                                prayer12[3] = outputformat.format(df.parse(prayer24[3]));
                                prayer12[4] = outputformat.format(df.parse(prayer24[4]));
                                prayer12[5] = outputformat.format(df.parse(prayer24[5]));

                                p_time = prayer12;
                            }
                        }
                    } else {
                        Error = "Unable to get schedule";
                    }
                }



//                int status_code = jsonObject.getInt("status_code");
//
//                if (status_code == 1) {
//                    JSONArray jArrayItems = jsonObject.getJSONArray("items");
//                    JSONObject jsonSchedule = jArrayItems.getJSONObject(0);
//
//                    prayer12 = new String[6];
//                    prayer12[0] = jsonSchedule.getString("fajr");
//                    prayer12[1] = jsonSchedule.getString("shurooq");
//                    prayer12[2] = jsonSchedule.getString("dhuhr");
//                    prayer12[3] = jsonSchedule.getString("asr");
//                    prayer12[4] = jsonSchedule.getString("maghrib");
//                    prayer12[5] = jsonSchedule.getString("isha");
//                    Log.d("strArr", prayer12[0]);
//                    p_time = prayer12;
//
//                    prayer24 = new String[6];
//                    if (pref_time.equals("1")) {
//                        //Format of the date defined in the input String
//                        DateFormat df = new SimpleDateFormat("hh:mm aa");
//                        //Desired format: 24 hour format: Change the pattern as per the need
//                        DateFormat outputformat = new SimpleDateFormat("HH:mm");
//
//                        prayer24[0] = outputformat.format(df.parse(jsonSchedule.getString("fajr")));
//                        prayer24[1] = outputformat.format(df.parse(jsonSchedule.getString("shurooq")));
//                        prayer24[2] = outputformat.format(df.parse(jsonSchedule.getString("dhuhr")));
//                        prayer24[3] = outputformat.format(df.parse(jsonSchedule.getString("asr")));
//                        prayer24[4] = outputformat.format(df.parse(jsonSchedule.getString("maghrib")));
//                        prayer24[5] = outputformat.format(df.parse(jsonSchedule.getString("isha")));
//                        Log.d("strArr", prayer24[0]);
//                        p_time = prayer24;
//                    }
//                } else {
//                    JSONObject jsonStatusError = jsonObject.getJSONObject("status_error");
//                    if (jsonStatusError.has("invalid_query")) {
//                        Error = jsonStatusError.getString("invalid_query");
//                    } else {
//                        Error = jsonObject.getString("status_description");
//                    }
//                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                //Error = e.getMessage();
            } catch (NullPointerException e) {
                // TODO: handle exception
                //Error = e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (pd.isShowing()) {
                pd.dismiss();
            }

            if (Error!=null) {
                Toast.makeText(PrayerTimesActivity.this, Error, Toast.LENGTH_SHORT).show();
            } else {
                int times = 0;
                try {
                    PrayTime prayers = preinitPrayerTime(times);
                    ArrayList<String> prayerTimes = prayers.getPrayerTimes(getTimeByCal(times), latitude, longitude, timezone);
                    String[] strArr = new String[6];
                    strArr[0] = set_time((String) prayerTimes.get(0), Integer.parseInt(pref.getString(p_name[0] + "daylight", "0")) - 15, 0);
                    strArr[1] = set_time((String) prayerTimes.get(0), Integer.parseInt(pref.getString(p_name[1] + "daylight", "0")), 0);
                    strArr[2] = set_time((String) prayerTimes.get(2), Integer.parseInt(pref.getString(p_name[2] + "daylight", "0")), 0);
                    strArr[3] = set_time((String) prayerTimes.get(3), Integer.parseInt(pref.getString(p_name[3] + "daylight", "0")) - 66, 0);
                    strArr[4] = set_time((String) prayerTimes.get(5), Integer.parseInt(pref.getString(p_name[4] + "daylight", "0")), 0);
                    strArr[5] = set_time((String) prayerTimes.get(6), Integer.parseInt(pref.getString(p_name[5] + "daylight", "0")), 0);
                    p_time_1 = strArr;

                    strArr = new String[6];
                    strArr[0] = p_time_1[0] + " " + p_time[0];
                    strArr[1] = p_time_1[1] + " " + p_time[1];
                    strArr[2] = p_time_1[2] + " " + p_time[2];
                    strArr[3] = p_time_1[3] + " " + p_time[3];
                    strArr[4] = p_time_1[4] + " " + p_time[4];
                    strArr[5] = p_time_1[5] + " " + p_time[5];
                    p_next_prayer = strArr;

                    adapter.notifyDataSetChanged();
                    lv_gridview.setAdapter(adapter);

                    int i;
                    DateFormat simpleDateFormat;
                    Date d1;
                    long difference;
                    int days;
                    int hours;
                    int min;
                    String timeDiffString;
                    font();

                    n_array.clear();
                    values.clear();
                    nxt_time = "";
                    alarm_value.clear();
                    for (i = 0; i < p_next_prayer.length; i++) {
                        Log.d("p_time_1 ====== ", i + ": " + p_time_1[i]);
                        Log.d("p_next_prayer ====== ", i + ": " + p_next_prayer[i]);

                        simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.US);
                        d1 = simpleDateFormat.parse(simpleDateFormat.format(new Date()));
                        difference = simpleDateFormat.parse(p_next_prayer[i]).getTime() - d1.getTime();
                        days = (int) (difference / 86400000);
                        hours = (int) ((difference - ((long) (86400000 * days))) / 3600000);
                        min = ((int) ((difference - ((long) (86400000 * days))) - ((long) (3600000 * hours)))) / ExponentialBackOffPolicy.DEFAULT_MAX_INTERVAL_MILLIS;
                        if (hours >= 0 && min >= 0) {
                            timeDiffString = "";
                            if (min < 9) {
                                timeDiffString = hours + ".0" + min;
                            } else {
                                timeDiffString = hours + "." + min;
                            }
                            if (min > 9) {
                                alarm_value.add(String.format("%.2f", new Object[]{Double.valueOf(Double.parseDouble(hours + "." + min))}) + " " + p_name[i]);
                            } else {
                                alarm_value.add(String.format("%.1f", new Object[]{Double.valueOf(Double.parseDouble(hours + "." + min))}) + " " + p_name[i]);
                            }
                            n_array.add(Double.valueOf(Double.parseDouble(timeDiffString.replace("-", "").replace(",", ".").trim())));
                            values.add(timeDiffString.replace("-", "").replace(",", ".") + " " + p_name[i]);
                        }
                    }
                    String n_time = "";
                    int n;
                    String[] m;
                    int p;
                    String[] val;
                    if (n_array.size() > 0) {
                        Collections.sort(n_array);
                        for (n = 0; n < values.size(); n++) {
                            m = ((String) values.get(n)).split(" ");
                            if (((Double) n_array.get(0)).doubleValue() == Double.parseDouble(m[0].trim())) {
                                nxt_time = m[1].trim();
                                for (p = 0; p < alarm_value.size(); p++) {
                                    if (((String) alarm_value.get(p)).contains(nxt_time)) {
                                        val = ((String) alarm_value.get(p)).replace(nxt_time, "").replace(".", "/").replace("٫", "/").replace(",", "/").split("/");
                                        LogUtils.i(((String) alarm_value.get(p)) + " val[0].trim() " + val[0].trim() + " nxt_time " + nxt_time);
                                        if (val[0].trim().equals("0")) {
                                            n_time = val[1].trim() + " minutes";
                                        } else {
                                            n_time = val[0].trim() + " Hours " + val[1].trim() + " Minutes";
                                        }
                                    }
                                }
                            }
                        }
                        txt_prayer_time.setText(nxt_time + " " + n_time);




                    } else {
                        strArr = new String[6];
                        p_time_1 = prayer24;
                        strArr = new String[6];
                        strArr[0] = p_time_1[0] + " " + addDay_Light(main((String) prayerTimes.get(0)), Integer.parseInt(pref.getString(p_name[0] + "daylight", "0")) - 15);
                        strArr[1] = p_time_1[1] + " " + addDay_Light(main((String) prayerTimes.get(0)), Integer.parseInt(pref.getString(p_name[1] + "daylight", "0")));
                        strArr[2] = p_time_1[2] + " " + addDay_Light(main((String) prayerTimes.get(2)), Integer.parseInt(pref.getString(p_name[2] + "daylight", "0")));
                        strArr[3] = p_time_1[3] + " " + addDay_Light(main((String) prayerTimes.get(3)), Integer.parseInt(pref.getString(p_name[3] + "daylight", "0")) - 66);
                        strArr[4] = p_time_1[4] + " " + addDay_Light(main((String) prayerTimes.get(5)), Integer.parseInt(pref.getString(p_name[4] + "daylight", "0")));
                        strArr[5] = p_time_1[5] + " " + addDay_Light(main((String) prayerTimes.get(6)), Integer.parseInt(pref.getString(p_name[5] + "daylight", "0")));
                        p_next_prayer = strArr;
                        n_array.clear();
                        values.clear();
                        alarm_value.clear();
                        n_time = "";
                        nxt_time = "";
                        alarm_value.clear();
                        val = new String[0];
                        for (i = 0; i < p_next_prayer.length; i++) {
                            simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.US);
                            d1 = simpleDateFormat.parse(simpleDateFormat.format(new Date()));
                            Date d2 = simpleDateFormat.parse(p_next_prayer[i]);

                            Log.d("p_next_prayer ====== ", i + ": " + p_next_prayer[i]);

                            difference = d2.getTime() - d1.getTime();
                            days = (int) (difference / 86400000);
                            hours = (int) ((difference - ((long) (86400000 * days))) / 3600000);
                            min = ((int) ((difference - ((long) (86400000 * days))) - ((long) (3600000 * hours)))) / ExponentialBackOffPolicy.DEFAULT_MAX_INTERVAL_MILLIS;
                            LogUtils.i(d1 + " date " + d2);
                            if (hours >= 0 && min >= 0) {
                                timeDiffString = "";
                                if (min < 9) {
                                    timeDiffString = hours + ".0" + min;
                                } else {
                                    timeDiffString = hours + "." + min;
                                }
                                LogUtils.i(" hours " + timeDiffString);
                                if (min > 9) {
                                    alarm_value.add(String.format("%.2f", new Object[]{Double.valueOf(Double.parseDouble(hours + "." + min))}) + " " + p_name[i]);
                                } else {
                                    alarm_value.add(String.format("%.1f", new Object[]{Double.valueOf(Double.parseDouble(hours + "." + min))}) + " " + p_name[i]);
                                }
                                n_array.add(Double.valueOf(Double.parseDouble(timeDiffString.replace("-", "").replace(",", ".").trim())));
                                values.add(timeDiffString.replace("-", "").replace(",", ".") + " " + p_name[i]);
                            }
                        }
                        if (n_array.size() > 0) {
                            Collections.sort(n_array);
                            for (n = 0; n < values.size(); n++) {
                                m = ((String) values.get(n)).split(" ");
                                LogUtils.i(n_array.get(0) + " values value " + n_array.get(1));
                                if (((Double) n_array.get(0)).doubleValue() == Double.parseDouble(m[0].trim())) {
                                    nxt_time = m[1].trim();
                                    for (p = 0; p < alarm_value.size(); p++) {
                                        if (((String) alarm_value.get(p)).contains(nxt_time)) {
                                            LogUtils.i(((String) alarm_value.get(p)) + " alaram value " + nxt_time);
                                            val = ((String) alarm_value.get(p)).replace(nxt_time, "").replace(".", "/").replace("٫", "/").replace(",", "/").split("/");
                                            if (val[0].trim().equals("0")) {
                                                n_time = val[1].trim() + " minutes";
                                            } else {
                                                n_time = val[0].trim() + " Hours " + val[1].trim() + " Minutes";
                                            }
                                        }
                                    }
                                }
                            }
                            txt_prayer_time.setText(nxt_time + " " + n_time);
                        }
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                    Log.e("error", e.getMessage());
                }

            }

        }
    }

    class GetCityDataAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(PrayerTimesActivity.this);
            pd.setMessage(getString(R.string.loading));
            pd.setCancelable(true);
            //pd.show();
            pbCity.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            URL hp;
            try {
                hp = new URL("https://api.banghasan.com/sholat/format/json/kota");
                Log.e("URLs", "" + hp);
                URLConnection hpCon = hp.openConnection();
                hpCon.connect();
                InputStream input = hpCon.getInputStream();
                Log.d("input", "" + input);
                BufferedReader r = new BufferedReader(new InputStreamReader(input));
                String x;
                x = r.readLine();
                StringBuilder total = new StringBuilder();
                while (x != null) {
                    total.append(x);
                    x = r.readLine();
                }
                JSONObject jsonObject = new JSONObject(total.toString());

                if (jsonObject.getString("status").equals("ok")) {
                    JSONArray jsonKota = jsonObject.getJSONArray("kota");
                    cityNames = new ArrayList<>();
                    cityCodes = new ArrayList<>();

                    for (int i=0;i<jsonKota.length();i++) {
                        JSONObject obj = jsonKota.getJSONObject(i);
                        cityCodes.add(obj.getString("id"));
                        cityNames.add(obj.getString("nama"));
                    }
                } else {
                    Error = "Unable to get schedule";
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                //Error = e.getMessage();
            } catch (NullPointerException e) {
                // TODO: handle exception
                //Error = e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
//            } catch (ParseException e) {
//                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (pd.isShowing()) {
                pd.dismiss();
            }
            pbCity.setVisibility(View.GONE);

            if (Error!=null) {
                Toast.makeText(PrayerTimesActivity.this, Error, Toast.LENGTH_SHORT).show();
            } else {
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(myApp,
                        R.layout.custom_list_item, R.id.text_view_list_item, cityNames);
                autotext_city.setAdapter(adapter);
                cityListLoaded = true;
            }

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        mInterstitialAd.show();
    }

}
