package santri.azan.activity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.widget.Toolbar;
import santri.azan.caldroid.AdapterNames;
import santri.azan.model.AllahNames;
import santri.azan.utils.PListParser;
import santri.azan.utils.Utils;
import santri.syaria.R;

public class ActivityNamesList extends Utils {

    ListView list_names;

    public static List<AllahNames> data = new ArrayList<AllahNames>();
    List<AllahNames> dblist = new ArrayList<AllahNames>();
    public static AdapterNames adapter;
    RelativeLayout layout_allah;
    Toolbar toolbar;
    int limit = 0;
    private AdView mAdView;
    private InterstitialAd mInterstitialAd;
    private ActivityNamesList myApp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        fullscreen();
        super.onCreate(savedInstanceState);

        myApp = this;

        setContentView(R.layout.activity_names_list);
        Actionbar("Allah Names");
        //Analytics("Allah Names");
        //banner_ad();
        list_names = (ListView) findViewById(R.id.list_names);

        new Namelist().execute();

        MobileAds.initialize(myApp, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(LoadAdError errorCode) {
                Log.e("failed load banner", errorCode+"");
                // Code to be executed when an ad request fails.
                //myApp.findViewById(R.id.startAppBanner).setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });
//        mInterstitialAd = new InterstitialAd(this);
//        mInterstitialAd.setAdUnitId(getResources().getString(R.string.admob_insertitial_id));
//        mInterstitialAd.loadAd(new AdRequest.Builder().build());

    }

    public  List<AllahNames> getAllahNames() {
        return dblist;
    }


    public class Namelist extends AsyncTask<String, String, String>

    {
        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityNamesList.this);
            pDialog.setMessage("Loading Names List");
            pDialog.setCancelable(false);
            if (limit == 0) {
                pDialog.show();
            }
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            data = PListParser.getAllahNames(ActivityNamesList.this);

		/*	dblist = db.getAllahNames(limit);

			for (int i = 0; i < dblist.size(); i++) {
				LogUtils.i("async id " + dblist.get(i).id);
			}
*/			return null;
        }

        @Override
        protected void onPostExecute(String result) {

            if (limit == 0) {
                data.addAll(dblist);
                pDialog.dismiss();
                adapter = new AdapterNames(ActivityNamesList.this, data, tf,
                        tf2);
                list_names.setAdapter(adapter);

            } else {
                for (AllahNames post : dblist) {

                    data.add(post);
                }

                adapter.notifyDataSetChanged();
            }

            limit = limit + 10;
            //		list_names.onRefreshComplete();
            super.onPostExecute(result);
        }
    }

    @Override
    public void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //	db.close();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        mInterstitialAd.show();
    }

}
