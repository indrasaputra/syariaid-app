package santri.azan.activity;

import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;

import santri.azan.model.AllahNames;
import santri.azan.utils.LogUtils;
import santri.azan.utils.Utils;
import santri.syaria.R;

public class ActivityNames extends Utils {

    ListView list;

    ImageView img_allah, img_tone;
    LinearLayout lyt_content;
    List<AllahNames> dblist;
    TextView txt_name_ar, txt_name_en, txt_counter,txt_name_meaning;
    int id,nid=0;
    RelativeLayout layout_allah;
    Bundle extra;
    boolean play = false;
    String title="";
    ImageView img_slider_next, img_slider_previous;
    private AdView mAdView;
    private ActivityNames myApp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        fullscreen();
        super.onCreate(savedInstanceState);

        myApp = this;

        setContentView(R.layout.activity_names);

        MobileAds.initialize(myApp, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(LoadAdError errorCode) {
                Log.e("failed load banner", errorCode+"");
                // Code to be executed when an ad request fails.
                //myApp.findViewById(R.id.startAppBanner).setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String userlango = preferences.getString(USER_LANGUAGES , USER_LANGUAGES);
        LogUtils.i("in onCreate" ,"preferences = "+ userlango);

        if (userlango !=null) {

            Locale locale = new Locale(userlango);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }


        Actionbar("Allah Names");
        //Analytics("Allah Names");
        typeface();//banner_ad();
        extra = getIntent().getExtras();
        id = extra.getInt("fid");
        txt_counter = (TextView)findViewById(R.id.txt_counter);

        img_tone = (ImageView) findViewById(R.id.img_tone);
        img_allah = (ImageView) findViewById(R.id.img_allah);
        img_slider_next = (ImageView) findViewById(R.id.img_slider_next);
        img_slider_previous = (ImageView) findViewById(R.id.img_slider_previous);
        lyt_content = (LinearLayout) findViewById(R.id.lyt_content);


        txt_name_ar = (TextView) findViewById(R.id.txt_name_ar);
        txt_name_en = (TextView) findViewById(R.id.txt_name_en);
        txt_name_meaning = (TextView) findViewById(R.id.txt_name_meaning);
		/*txt_name_ar.setText(id + "." + extra.getString("title"));
		txt_name_en.setText(extra.getString("desc1")+"\n"+ extra.getString("desc2"));
		txt_name_meaning.setText(extra.getString("meaning"));
		*/txt_name_ar.setTypeface(tf, Typeface.BOLD);

        txt_name_meaning.setTypeface(tf,Typeface.BOLD);
        txt_counter.setTypeface(tf2,Typeface.BOLD);
        txt_name_en.setTypeface(tf1);
        //nid = id;
        if ( id == 0) {

            img_slider_previous.setVisibility(View.INVISIBLE);

        }
        img_slider_next.setVisibility(View.VISIBLE);

        getContent(id);
        LogUtils.i(id+ " fid "+ (id+1));


        if (id == 98) {

            img_slider_next.setVisibility(View.INVISIBLE);

            img_slider_previous.setVisibility(View.VISIBLE);
        }

        img_tone.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(play == false)
                {
                    playTone(title);
                } else {
                    Toast.makeText(ActivityNames.this, "Allah Name is Playing Already",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
        img_slider_next.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                Animation RightSwipe = AnimationUtils.loadAnimation(
                        ActivityNames.this, R.anim.push_left_out);
                lyt_content.startAnimation(RightSwipe);

                Runnable mMyRunnable = new Runnable() {
                    @Override
                    public void run() {
                        id = id + 1;
                        if (id > 99) {

                            id = 99;
                        }
                        //	nid = id;
                        getContent(id);
                        Animation RightSwipe1 = AnimationUtils.loadAnimation(
                                ActivityNames.this, R.anim.push_left_in);
                        lyt_content.startAnimation(RightSwipe1);

                        changeSliderView();

                    }
                };

                Handler myHandler = new Handler();
                myHandler.postDelayed(mMyRunnable, 150);

            }
        });

        img_slider_previous.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Animation LeftSwipe = AnimationUtils.loadAnimation(
                        ActivityNames.this, R.anim.push_right_out);
                lyt_content.startAnimation(LeftSwipe);

                Runnable mMyRunnable = new Runnable() {
                    @Override
                    public void run() {
                        id = id - 1;
                        if (id < 0) {
                            id = 1;
                        }
                        getContent(id);
                        Animation LeftSwipe1 = AnimationUtils.loadAnimation(
                                ActivityNames.this, R.anim.push_right_in);
                        lyt_content.startAnimation(LeftSwipe1);

                        changeSliderView();
                    }
                };

                Handler myHandler = new Handler();
                myHandler.postDelayed(mMyRunnable, 150);

            }
        });


    }

    public void changeSliderView() {

        if ( id == 0) {

            img_slider_previous.setVisibility(View.INVISIBLE);

            img_slider_next.setVisibility(View.VISIBLE);
        } else {

            img_slider_previous.setVisibility(View.VISIBLE);

            img_slider_next.setVisibility(View.VISIBLE);
        }

        if (id == 98) {

            img_slider_next.setVisibility(View.INVISIBLE);

            img_slider_previous.setVisibility(View.VISIBLE);
        } else {

            img_slider_next.setVisibility(View.VISIBLE);

            img_slider_previous.setVisibility(View.VISIBLE);
        }

    }


    public void playTone(String name) {

        AssetFileDescriptor afd;
        try {
            String title= name;
//            if(title.contains("-"))
//            {
//                String[] n = name.split("-");
//
//                if(n[0].trim().contains(" "))
//                {
//                    title = title.replace("’", "").replace(" ", "_");
//
//                }
//                if(n[1].trim().startsWith("’") || n[1].trim().endsWith("’") || n[1].trim().contains(" "))
//                {
//                    title = title.replace("’", "").replace(" ", "_");
//                }
//            }
            title= title.trim().toLowerCase().replace("-","_").replace("’","_").replace(" ","_").replace("'","_");
            //afd = ActivityNames.this.getAssets().openFd("sound/" +(id+1)+"_" + title.toLowerCase().replace("-","_").replace("’", "_") + ".mp3");
            afd = ActivityNames.this.getAssets().openFd("sound/" +(id+1)+"_" + title + ".mp3");
            MediaPlayer player = new MediaPlayer();
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(),
                    afd.getLength());
            player.prepare();
            player.start();
            play = true;

            img_tone.setImageResource(R.drawable.ic_play_clicked);

            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {

                    img_tone.setImageResource(R.drawable.ic_play);
                    play = false;
                }
            });



        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public Drawable loadImageFromAsset(String name, int id) {

        Drawable d = null;
        try {

            ///	LogUtils.i("title "+ name);
            String title= name;
            if(title.contains("-"))
            {
                String[] n = name.split("-");
                if(n[0].trim().contains(" "))
                {
                    title = title.replace("’", "").replace(" ", "_");

                }
                if(n[1].trim().startsWith("’") || n[1].trim().endsWith("’") || n[1].trim().contains(" "))
                {
                    title = title.replace("’", "").replace(" ", "_");
                }
                //title= title.toLowerCase().replace("-","_").replace("’", "_");

            }
            title= title.trim().toLowerCase().replace("-","_").replace("’","_").replace(" ","_").replace("'","_");
            LogUtils.i(id + " act title "+ title);

            // get input stream
            InputStream ims = ActivityNames.this.getAssets().open(
                    //"images/"+id+"_" + title.toLowerCase().replace("-","_").replace("’", "_") + "_480.jpg");
                    "images/"+id+"_" + title+ "_480.jpg");
            // load image as Drawable
            d = Drawable.createFromStream(ims, null);
            // set image to ImageView
            img_allah.setImageDrawable(d);
            return d;
        } catch (IOException ex) {
        }

        return d;

    }

    @Override
    protected void onResume() {
        font();
        super.onResume();
    }
    public void getContent(int id) {


        txt_counter.setText((id+1)+"/99");
        txt_name_ar.setText((id+1) + "." + ActivityNamesList.data.get(id).title);
        LogUtils.i("getcontent  "+ActivityNamesList.data.get(id).title);
        title = ActivityNamesList.data.get(id).title.toLowerCase().replace("-","_").replace("’", "_");
        txt_name_en.setText(ActivityNamesList.data.get(id).description1+"\n"+ ActivityNamesList.data.get(id).description2);
        txt_name_meaning.setText(ActivityNamesList.data.get(id).meaning);
        loadImageFromAsset(ActivityNamesList.data.get(id).title, (id+1));

        txt_name_ar.setTextAppearance(ActivityNames.this, styleheader[efont]);

        txt_counter.setTextAppearance(ActivityNames.this, style[efont]);
        txt_name_en.setTextAppearance(ActivityNames.this, style[efont]);
        txt_name_meaning.setTextAppearance(ActivityNames.this, style[efont]);
    }

    @Override
    public void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //	db.close();
    }

}
