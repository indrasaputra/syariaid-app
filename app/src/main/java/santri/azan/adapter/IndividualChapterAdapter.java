package santri.azan.adapter;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import santri.azan.model.QuranScript;
import santri.azan.utils.LogUtils;
import santri.syaria.R;

@SuppressLint({ "ViewHolder", "InflateParams" })
public class IndividualChapterAdapter extends BaseAdapter {

    Context context;

    ArrayList<String> arabic;
    List<QuranScript> dblist;
    List<QuranScript> dblist1,dben,dbfr,dbgrm,dbindo,dbmal,dbspan,dbtru,dbur;
    String translation ="";
    Typeface tf;
    int ar_font,en_font;

    protected int style[] = { R.style.FontStyle_Small,
            R.style.FontStyle_Medium, R.style.FontStyle_Large, R.style.FontStyle_Extra_Large};
    protected int styleheader[] = { R.style.FontStyle_title_small,
            R.style.FontStyle_title_medium, R.style.FontStyle_title_large };


    public IndividualChapterAdapter(Context context, List<QuranScript> dblist,
                                    ArrayList<String> arabic,List<QuranScript> dblist1,List<QuranScript> dbfr,List<QuranScript> dbgrm,List<QuranScript> dbindo,
                                    List<QuranScript> dbmal,List<QuranScript> dbspan,List<QuranScript> dbtru,List<QuranScript> dbur,int ar_font,int en_font,Typeface tf) {
        this.context = context;
        this.dblist = dblist;
        this.arabic = arabic;
        this.dblist1 = dblist1;
        this.dbfr= dbfr;
        this.dbgrm=dbgrm;
        //this.dbindo=dbindo;
        this.dben=dbindo;
        this.dbmal=dbmal;
        this.dbspan=dbspan;
        this.dbtru=dbtru;
        this.dbur=dbur;
        this.ar_font = 3;// ar_font;
        this.en_font= en_font;
        this.tf= tf;

        LogUtils.i("french "+ dbfr.size());
    }

    @Override
    public int getCount() {

        return arabic.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = null;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View vi = convertView;
        vi = inflater.inflate(R.layout.quransinglechapterlistitem, null);
        final TextView tv_english = (TextView) vi.findViewById(R.id.tv_ayah_english);
        final TextView tv_arabic = (TextView) vi.findViewById(R.id.tv_ayah_arabic);
        final TextView tv_transliteration = (TextView) vi
                .findViewById(R.id.tv_ayah_transliteration);

        final TextView tv_ayah_french = (TextView) vi.findViewById(R.id.tv_ayah_french);
        final TextView tv_ayah_german = (TextView) vi.findViewById(R.id.tv_ayah_german);
        final TextView tv_ayah_indonesian = (TextView) vi.findViewById(R.id.tv_ayah_indonesian);
        final TextView tv_ayah_malay = (TextView) vi.findViewById(R.id.tv_ayah_malay);
        final TextView tv_ayah_spanish = (TextView) vi.findViewById(R.id.tv_ayah_spanish);
        final TextView tv_ayah_turkish = (TextView) vi.findViewById(R.id.tv_ayah_turkish);
        final TextView tv_ayah_urdu = (TextView) vi.findViewById(R.id.tv_ayah_urdu);

        //Typeface tf_arabic = Typeface.createFromAsset(context.getAssets(), "fonts/noorehidayat.ttf");
        //tv_arabic.setTypeface(tf_arabic);
        tv_arabic.setTypeface(tf);
        //tv_english.setText("\n English \n"+dblist1.get(position).text);
        tv_ayah_indonesian.setText("\n \n"+dblist1.get(position).text);
        tv_arabic.setText(arabic.get(position));
        tv_transliteration.setText((position + 1) + "."+ dblist.get(position).text);

        //tv_ayah_french.setVisibility(View.VISIBLE);


        tv_english.setTextAppearance(context, style[en_font]);
        //tv_arabic.setTextAppearance(context, style[ar_font]);
        tv_transliteration.setTextAppearance(context, style[en_font]);
        tv_ayah_french.setTextAppearance(context, style[en_font]);
        tv_ayah_german.setTextAppearance(context, style[en_font]);
        tv_ayah_indonesian.setTextAppearance(context, style[en_font]);
        tv_ayah_malay.setTextAppearance(context, style[en_font]);
        tv_ayah_spanish.setTextAppearance(context, style[en_font]);
        tv_ayah_turkish.setTextAppearance(context, style[en_font]);
        tv_ayah_urdu.setTextAppearance(context, style[en_font]);

        if(dben.size()>0)
        {
            LogUtils.i(" english "+dben.get(position).text);
            tv_english.setVisibility(View.VISIBLE);//tv_ayah_french.setTextIsSelectable(true);
            tv_english.setText("\n English \n"+ dben.get(position).text);
        }else
        {
            tv_ayah_french.setVisibility(View.GONE);
        }
        if(dbfr.size()>0)
        {
            LogUtils.i(" french "+dbfr.get(position).text);
            tv_ayah_french.setVisibility(View.VISIBLE);//tv_ayah_french.setTextIsSelectable(true);
            tv_ayah_french.setText("\n French \n"+ dbfr.get(position).text);
        }else
        {
            tv_ayah_french.setVisibility(View.GONE);
        }
        if(dbgrm.size()>0)
        {
            tv_ayah_german.setVisibility(View.VISIBLE);///tv_ayah_german.setTextIsSelectable(true);
            tv_ayah_german.setText("\n German \n"+ dbgrm.get(position).text);
        }else
        {
            tv_ayah_german.setVisibility(View.GONE);
        }

//        if(dbindo.size()>0)
//        {
//            tv_ayah_indonesian.setVisibility(View.VISIBLE);//tv_ayah_indonesian.setTextIsSelectable(true);
//            tv_ayah_indonesian.setText("\n Indonesian \n"+ dbindo.get(position).text);
//        }else
//        {
//            tv_ayah_indonesian.setVisibility(View.GONE);
//        }

        if(dbmal.size()>0)
        {
            tv_ayah_malay.setVisibility(View.VISIBLE);//tv_ayah_malay.setTextIsSelectable(true);
            tv_ayah_malay.setText("\n Malay \n"+ dbmal.get(position).text);
        }else
        {
            tv_ayah_malay.setVisibility(View.GONE);
        }

        if(dbspan.size()>0)
        {
            tv_ayah_spanish.setVisibility(View.VISIBLE);//tv_ayah_spanish.setTextIsSelectable(true);
            tv_ayah_spanish.setText("\n Spanish \n"+ dbspan.get(position).text);
        }else
        {
            tv_ayah_spanish.setVisibility(View.GONE);
        }

        if(dbtru.size()>0)
        {
            tv_ayah_turkish.setVisibility(View.VISIBLE);//tv_ayah_turkish.setTextIsSelectable(true);
            tv_ayah_turkish.setText("\n Trukish \n"+ dbtru.get(position).text);
        }else
        {
            tv_ayah_turkish.setVisibility(View.GONE);
        }

        if(dbur.size()>0)
        {
            tv_ayah_urdu.setVisibility(View.VISIBLE);//tv_ayah_urdu.setTextIsSelectable(true);
            tv_ayah_urdu.setText("\n Urdu \n"+ dbur.get(position).text);
        }else
        {
            tv_ayah_urdu.setVisibility(View.GONE);
        }
        tv_english.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getText(tv_english.getText().toString());
            }
        });
        tv_ayah_french.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getText(tv_ayah_french.getText().toString());
            }
        });
        tv_ayah_german.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getText(tv_ayah_german.getText().toString());
            }
        });
        tv_ayah_indonesian.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getText(tv_ayah_indonesian.getText().toString());
            }
        });
        tv_ayah_malay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getText(tv_ayah_malay.getText().toString());
            }
        });tv_ayah_spanish.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getText(tv_ayah_spanish.getText().toString());
            }
        });
        tv_ayah_turkish.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getText(tv_ayah_turkish.getText().toString());
            }
        });
        tv_ayah_urdu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getText(tv_ayah_urdu.getText().toString());
            }
        });

        return vi;
    }

    public void getText(String text)
    {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("label",
                    text);
            clipboard.setPrimaryClip(clip);
        } else {
            @SuppressWarnings("deprecation")
            android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText(text);
        }
        Toast.makeText(context, "Copied to clipboard", Toast.LENGTH_SHORT).show();


    }
}
