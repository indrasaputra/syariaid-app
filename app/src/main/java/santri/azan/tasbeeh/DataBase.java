package santri.azan.tasbeeh;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DataBase {

    // Database Name
    public static final String DATABASE_NAME = "Tasbeeh";

    // Database Version
    public static final int DATABASE_VERSION = 1;

    // All Static variables
    public static final String TABLE_TASBEEHLIST = "table_tasbeehlist";
    public static final String TABLE_INSERTTASBEEH = "table_inserttasbeeh";
    public static final String KEY_ID = "key_id";
    public static final String KEY_TITLE = "title";
    public static final String KEY_DESCRIBTION = "description";
    public static final String KEY_COUNT = "countvalue";
    private static SQLiteDatabase mDb;
    private static DatabaseHelper mDbHelper;
    private Context mCtx;

    private static final String TABLE_TASBEEHLIST_CREATE = "CREATE TABLE if not exists "
            + TABLE_TASBEEHLIST
            + " ("
            + KEY_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + KEY_TITLE
            + " TEXT, "
            + KEY_DESCRIBTION + " TEXT," + KEY_COUNT + " INTEGER);";

    public DataBase(Context ctx) {
        this.mCtx = ctx;
    }

    public DataBase openDataBase() throws SQLException {

        if (mDbHelper == null) {
            mDbHelper = new DatabaseHelper(mCtx);
        }
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        if (mDbHelper != null) {
            mDbHelper.close();
        }
    }

    public long inserTasbeehList(String title, String description,
                                 int countvalue) {

        try {
            ContentValues val = new ContentValues();
            val.put(KEY_TITLE, title);
            val.put(KEY_DESCRIBTION, description);
            val.put(KEY_COUNT, countvalue);
            mDb.insert(TABLE_TASBEEHLIST, null, val);
            return 1;
        } catch (SQLException e) {
            Log.d("Error Message", e.getMessage());
        }
        return countvalue;

    }

    public long deleteTasbeehList(int key_id) {

        try {

            mDb.delete(TABLE_TASBEEHLIST, KEY_ID + "=" + key_id, null);
            return 1;
        } catch (SQLException e) {
            Log.d("Error Message", e.getMessage());
        }
        return 0;

    }

    public long checkTasbeehList(int key_id, String title, String description,
                                 String countvalue) {

        String Query = "SELECT * FROM " + TABLE_TASBEEHLIST + " WHERE "
                + KEY_ID + "=" + key_id;

        Cursor cursor = mDb.rawQuery(Query, null);
        if (cursor.getCount() <= 0) {
            ContentValues val = new ContentValues();
            val.put(KEY_ID, key_id);
            val.put(KEY_TITLE, title);
            val.put(KEY_DESCRIBTION, description);
            val.put(KEY_COUNT, countvalue);
            mDb.insert(TABLE_TASBEEHLIST, null, val);
            return 1;
        } else {
            return 2;
        }

    }

    public List<DataBaseModel> fetchAllFeedList(int limit) {

        List<DataBaseModel> dblist = new ArrayList<DataBaseModel>();
        String selection = null;

        Cursor cursor = mDb.query(TABLE_TASBEEHLIST, new String[] { KEY_ID,
                        KEY_TITLE, KEY_DESCRIBTION, KEY_COUNT }, selection, null, null,
                null, KEY_ID +" ASC ");// LIMIT " + limit + ",100");

        if (cursor != null) {

            if (cursor.moveToFirst()) {

                do {
                    DataBaseModel pn = new DataBaseModel();
                    pn.setKey_id(cursor.getInt(0));
                    pn.setTitle(cursor.getString(1));
                    pn.setDescription(cursor.getString(2));
                    pn.setCount(cursor.getInt(3));
                    dblist.add(pn);
                } while (cursor.moveToNext());
            }
        }
        cursor.close();
        return dblist;
    }
    public List<DataBaseModel> gettasbeeh(int id) {
        List<DataBaseModel> dblist = new ArrayList<DataBaseModel>();


        String Query = "SELECT * FROM " + TABLE_TASBEEHLIST + " WHERE "+KEY_ID +"="+id;

        Cursor cursor= mDb.rawQuery(Query, null);

        if (cursor != null) {

            if (cursor.moveToFirst()) {

                do {
                    DataBaseModel pn = new DataBaseModel();
                    pn.setKey_id(cursor.getInt(0));
                    pn.setTitle(cursor.getString(1));
                    pn.setDescription(cursor.getString(2));
                    pn.setCount(cursor.getInt(3));
                    dblist.add(pn);
                } while (cursor.moveToNext());
            }
        }
        cursor.close();
        return dblist;

    }

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        // Creating Tables
        @Override
        public void onCreate(SQLiteDatabase db) {

            db.execSQL(TABLE_TASBEEHLIST_CREATE);

        }

        // Upgrading database
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }

    }
}
