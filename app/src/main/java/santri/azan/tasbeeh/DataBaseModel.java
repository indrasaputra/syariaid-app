package santri.azan.tasbeeh;

public class DataBaseModel {

    private String title;
    private String description;
    private int count;
    private int key_id;


    public int getKey_id() {
        return key_id;
    }
    public void setKey_id(int key_id) {
        this.key_id = key_id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public int getCount() {
        return count;
    }
    public void setCount(int count) {
        this.count = count;
    }

}
