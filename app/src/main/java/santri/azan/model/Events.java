package santri.azan.model;

public class Events {
    private String month;
    private String date;
    private String event;

    public Events(String month, String date, String event) {
        this.month = month;
        this.date = date;
        this.event = event;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

}
