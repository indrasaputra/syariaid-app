package santri.azan.model;

public class AllahNames {
    public String title;
    public String meaning;
    public String description1;
    public String description2;

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getMeaning() {
        return meaning;
    }
    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }
    public String getDescription1() {
        return description1;
    }
    public void setDescription1(String description1) {
        this.description1 = description1;
    }
    public String getDescription2() {
        return description2;
    }
    public void setDescription2(String description2) {
        this.description2 = description2;
    }

			/*public String language_en;
			public String language_eu;
			public String language_ar;
			public String name;
			public int id;


			public String getLanguage_en() {
				return language_en;
			}
			public void setLanguage_en(String language_en) {
				this.language_en = language_en;
			}
			public String getLanguage_eu() {
				return language_eu;
			}
			public void setLanguage_eu(String language_eu) {
				this.language_eu = language_eu;
			}
			public String getName() {
				return name;
			}
			public void setName(String name) {
				this.name = name;
			}
			public int getId() {
				return id;
			}
			public void setId(int id) {
				this.id = id;
			}
			public String getLanguage_ar() {
				return language_ar;
			}
			public void setLanguage_ar(String language_ar) {
				this.language_ar = language_ar;
			}
			*/




}
