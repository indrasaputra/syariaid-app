package santri.azan.caldroid;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import santri.azan.activity.ActivityNames;
import santri.azan.activity.ActivityNamesList;
import santri.azan.model.AllahNames;
import santri.azan.utils.LogUtils;
import santri.syaria.R;

@SuppressLint({ "ViewHolder", "InflateParams" })
public class AdapterNames extends BaseAdapter {

    // Declare Variables
    Context mContext;
    LayoutInflater inflater;
    List<AllahNames> data;
    TextView allah_name, allah_title;
    LinearLayout lyt_listview;
    ImageView btn_play, btn_image;
    Typeface tf,tf1;

    boolean play = false;

    public AdapterNames(Context context, List<AllahNames> names, Typeface tf, Typeface tf1) {
        this.tf = tf;
        this.tf1 = tf1;
        data = names;
        mContext = context;
        inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        view = inflater.inflate(R.layout.names_list, null);
        lyt_listview = (LinearLayout) view.findViewById(R.id.lyt_listview);
        allah_name = (TextView) view.findViewById(R.id.allah_name);
        btn_play = (ImageView) view.findViewById(R.id.btn_play);
        btn_image = (ImageView) view.findViewById(R.id.btn_image);
        allah_title = (TextView) view.findViewById(R.id.allah_title);
        allah_name.setText((position+1)+"."+data.get(position).title);
        allah_title.setText(data.get(position).meaning);
        btn_image.setImageDrawable(loadImageFromAsset(data.get(position).title, (position+1)));

        btn_play.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (play == false) {
                    playTone(data.get(position).title,position+1);
                } else {
                    Toast.makeText(mContext, "Allah Name audio is Playing Already",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        lyt_listview.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                LogUtils.i("id " + (position+1));
                mContext.startActivity(new Intent(mContext, ActivityNames.class)
                        .putExtra("fid", (position)).putExtra("title", data.get(position).title).putExtra("meaning", data.get(position).meaning)
                        .putExtra("desc1", data.get(position).description1).putExtra("desc2", data.get(position).description2));//data.get(position).id));
            }
        });

        return view;
    }

    public void playTone(String name, int id) {

        AssetFileDescriptor afd;
        try {
            String title= name;
//            String[] n = name.split("-");
//
//            if(n[0].trim().contains(" "))
//            {
//                title = title.replace("’", "").replace(" ", "_");
//
//            }
//            if(n[1].trim().startsWith("’") || n[1].trim().endsWith("’") )
//            {
//                title = title.replace("’", "").replace("", "_");
//            }
            //title= title.toLowerCase().replace("-","_").replace("’", "_");
            title= title.trim().toLowerCase().replace("-","_").replace("’","_").replace(" ","_").replace("'","_");

            afd = mContext.getAssets().openFd("sound/" +id+"_" + title + ".mp3");
            MediaPlayer player = new MediaPlayer();
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(),
                    afd.getLength());
            player.prepare();
            player.start();
            btn_play.setImageResource(R.drawable.ic_play_clicked);
            play = true;

            ActivityNamesList.adapter.notifyDataSetChanged();
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {

                    btn_play.setImageResource(R.drawable.ic_play);
                    play = false;
                    ActivityNamesList.adapter.notifyDataSetChanged();
                }
            });



        } catch (IOException e) {

            e.printStackTrace();
        }
    }


    public Drawable loadImageFromAsset(String name,int id) {

        Drawable d = null;
        try {

            LogUtils.i("title "+ name);
            String title= name;
//            String[] n = name.split("-");
//            if(n[0].trim().contains(" "))
//            {
//                title = title.replace("’", "").replace(" ", "_");
//
//            }
//            if(n[1].trim().startsWith("’") || n[1].trim().endsWith("’") || n[1].trim().contains(" "))
//            {
//                title = title.replace("’", "").replace(" ", "_");
//            }
            title= title.trim().toLowerCase().replace("-","_").replace("’","_").replace(" ","_").replace("'","_");

            Log.d("title result: ", id+"_"+title);
            LogUtils.i( id +" adat title "+ title);
            InputStream ims = mContext.getAssets().open(
                    "list_image/"+id+"_" + title+ "_480.jpg");

            d = Drawable.createFromStream(ims, null);

            return d;
        } catch (IOException ex) {
        }

        return null;

    }
}
