package santri.azan.map;

import com.google.api.client.util.Key;

import java.io.Serializable;

@SuppressWarnings("serial")
public class PlaceDetails implements Serializable {

    @Key
    public String status;

    @Key
    public Place result;

    @Override
    public String toString() {
        if (result!=null) {
            return result.toString();
        }
        return super.toString();
    }
}
