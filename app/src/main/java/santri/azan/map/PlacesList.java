package santri.azan.map;

import com.google.api.client.util.Key;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class PlacesList implements Serializable {

    @Key
    public String status;

    @Key
    public List<Place> results;

}
